
[00;31m#########################################################[00m
[00;31m#[00m [00;33mLocal Linux Enumeration & Privilege Escalation Script[00m [00;31m#[00m
[00;31m#########################################################[00m
[00;33m# www.rebootuser.com[00m
[00;33m# version 0.95[00m

[-] Debug Info
[00;33m[+] Thorough tests = Disabled[00m


[00;33mScan started at:
Wed Jan 30 01:44:41 UTC 2019
[00m

[00;33m### SYSTEM ##############################################[00m
[00;31m[-] Kernel information:[00m
Linux curling 4.15.0-22-generic #24-Ubuntu SMP Wed May 16 12:15:17 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux


[00;31m[-] Kernel information (continued):[00m
Linux version 4.15.0-22-generic (buildd@lgw01-amd64-013) (gcc version 7.3.0 (Ubuntu 7.3.0-16ubuntu3)) #24-Ubuntu SMP Wed May 16 12:15:17 UTC 2018


[00;31m[-] Specific release information:[00m
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=18.04
DISTRIB_CODENAME=bionic
DISTRIB_DESCRIPTION="Ubuntu 18.04 LTS"
NAME="Ubuntu"
VERSION="18.04 LTS (Bionic Beaver)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 18.04 LTS"
VERSION_ID="18.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=bionic
UBUNTU_CODENAME=bionic


[00;31m[-] Hostname:[00m
curling


[00;33m### USER/GROUP ##########################################[00m
[00;31m[-] Current user/group info:[00m
uid=1000(floris) gid=1004(floris) groups=1004(floris)


[00;31m[-] Users that have previously logged onto the system:[00m
Username         Port     From             Latest
root             tty1                      Tue Sep 25 21:56:22 +0000 2018
floris           pts/1    10.10.13.207     Wed Jan 30 01:31:21 +0000 2019


[00;31m[-] Who else is logged on:[00m
 01:44:41 up 14 min,  2 users,  load average: 0.02, 0.06, 0.09
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
floris   pts/0    10.10.15.2       01:31    9.00s  0.14s  0.00s /bin/bash ./le.sh
floris   pts/1    10.10.13.207     01:31   23.00s  0.13s  0.13s -bash


[00;31m[-] Group memberships:[00m
uid=0(root) gid=0(root) groups=0(root)
uid=1(daemon) gid=1(daemon) groups=1(daemon)
uid=2(bin) gid=2(bin) groups=2(bin)
uid=3(sys) gid=3(sys) groups=3(sys)
uid=4(sync) gid=65534(nogroup) groups=65534(nogroup)
uid=5(games) gid=60(games) groups=60(games)
uid=6(man) gid=12(man) groups=12(man)
uid=7(lp) gid=7(lp) groups=7(lp)
uid=8(mail) gid=8(mail) groups=8(mail)
uid=9(news) gid=9(news) groups=9(news)
uid=10(uucp) gid=10(uucp) groups=10(uucp)
uid=13(proxy) gid=13(proxy) groups=13(proxy)
uid=33(www-data) gid=33(www-data) groups=33(www-data)
uid=34(backup) gid=34(backup) groups=34(backup)
uid=38(list) gid=38(list) groups=38(list)
uid=39(irc) gid=39(irc) groups=39(irc)
uid=41(gnats) gid=41(gnats) groups=41(gnats)
uid=65534(nobody) gid=65534(nogroup) groups=65534(nogroup)
uid=100(systemd-network) gid=102(systemd-network) groups=102(systemd-network)
uid=101(systemd-resolve) gid=103(systemd-resolve) groups=103(systemd-resolve)
uid=102(syslog) gid=106(syslog) groups=106(syslog),4(adm)
uid=103(messagebus) gid=107(messagebus) groups=107(messagebus)
uid=104(_apt) gid=65534(nogroup) groups=65534(nogroup)
uid=105(lxd) gid=65534(nogroup) groups=65534(nogroup)
uid=106(uuidd) gid=110(uuidd) groups=110(uuidd)
uid=107(dnsmasq) gid=65534(nogroup) groups=65534(nogroup)
uid=108(landscape) gid=112(landscape) groups=112(landscape)
uid=109(pollinate) gid=1(daemon) groups=1(daemon)
uid=110(sshd) gid=65534(nogroup) groups=65534(nogroup)
uid=1000(floris) gid=1004(floris) groups=1004(floris)
uid=111(mysql) gid=114(mysql) groups=114(mysql)


[00;31m[-] It looks like we have some admin users:[00m
uid=102(syslog) gid=106(syslog) groups=106(syslog),4(adm)


[00;31m[-] Contents of /etc/passwd:[00m
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
sshd:x:110:65534::/run/sshd:/usr/sbin/nologin
floris:x:1000:1004:floris:/home/floris:/bin/bash
mysql:x:111:114:MySQL Server,,,:/nonexistent:/bin/false


[00;31m[-] Super user account(s):[00m
root


[00;31m[-] Are permissions on /home directories lax:[00m
total 12K
drwxr-xr-x  3 root   root   4.0K May 22  2018 .
drwxr-xr-x 23 root   root   4.0K May 22  2018 ..
drwxr-xr-x  6 floris floris 4.0K Jan 30 01:43 floris


[00;33m### ENVIRONMENTAL #######################################[00m
[00;31m[-] Environment information:[00m
SSH_CONNECTION=10.10.15.2 42416 10.10.10.150 22
LESSCLOSE=/usr/bin/lesspipe %s %s
LANG=en_US.UTF-8
OLDPWD=/var/www/html
XDG_SESSION_ID=1
USER=floris
PWD=/tmp
HOME=/home/floris
SSH_CLIENT=10.10.15.2 42416 22
XDG_DATA_DIRS=/usr/local/share:/usr/share:/var/lib/snapd/desktop
SSH_TTY=/dev/pts/0
MAIL=/var/mail/floris
SHELL=/bin/bash
TERM=xterm-256color
SHLVL=2
LOGNAME=floris
XDG_RUNTIME_DIR=/run/user/1000
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
LESSOPEN=| /usr/bin/lesspipe %s
_=/usr/bin/env


[00;31m[-] Path information:[00m
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin


[00;31m[-] Available shells:[00m
# /etc/shells: valid login shells
/bin/sh
/bin/bash
/bin/rbash
/bin/dash
/usr/bin/tmux
/usr/bin/screen


[00;31m[-] Current umask value:[00m
0002
u=rwx,g=rwx,o=rx


[00;31m[-] umask value as specified in /etc/login.defs:[00m
UMASK		022


[00;31m[-] Password and storage information:[00m
PASS_MAX_DAYS	99999
PASS_MIN_DAYS	0
PASS_WARN_AGE	7
ENCRYPT_METHOD SHA512


[00;33m### JOBS/TASKS ##########################################[00m
[00;31m[-] Cron jobs:[00m
-rw-r--r-- 1 root root  722 Nov 16  2017 /etc/crontab

/etc/cron.d:
total 24
drwxr-xr-x  2 root root 4096 May 22  2018 .
drwxr-xr-x 94 root root 4096 Sep 25 21:40 ..
-rw-r--r--  1 root root  589 Mar  7  2018 mdadm
-rw-r--r--  1 root root  712 Jan 17  2018 php
-rw-r--r--  1 root root  102 Nov 16  2017 .placeholder
-rw-r--r--  1 root root  189 Apr 26  2018 popularity-contest

/etc/cron.daily:
total 64
drwxr-xr-x  2 root root 4096 May 22  2018 .
drwxr-xr-x 94 root root 4096 Sep 25 21:40 ..
-rwxr-xr-x  1 root root  539 Oct 23  2017 apache2
-rwxr-xr-x  1 root root  376 Nov 20  2017 apport
-rwxr-xr-x  1 root root 1478 Apr 20  2018 apt-compat
-rwxr-xr-x  1 root root  355 Dec 29  2017 bsdmainutils
-rwxr-xr-x  1 root root 1176 Nov  2  2017 dpkg
-rwxr-xr-x  1 root root  372 Aug 21  2017 logrotate
-rwxr-xr-x  1 root root 1065 Apr  7  2018 man-db
-rwxr-xr-x  1 root root  539 Mar  7  2018 mdadm
-rwxr-xr-x  1 root root  538 Mar  1  2018 mlocate
-rwxr-xr-x  1 root root  249 Jan 25  2018 passwd
-rw-r--r--  1 root root  102 Nov 16  2017 .placeholder
-rwxr-xr-x  1 root root 3477 Feb 21  2018 popularity-contest
-rwxr-xr-x  1 root root  246 Mar 21  2018 ubuntu-advantage-tools
-rwxr-xr-x  1 root root  214 Jul 12  2013 update-notifier-common

/etc/cron.hourly:
total 12
drwxr-xr-x  2 root root 4096 Apr 26  2018 .
drwxr-xr-x 94 root root 4096 Sep 25 21:40 ..
-rw-r--r--  1 root root  102 Nov 16  2017 .placeholder

/etc/cron.monthly:
total 12
drwxr-xr-x  2 root root 4096 Apr 26  2018 .
drwxr-xr-x 94 root root 4096 Sep 25 21:40 ..
-rw-r--r--  1 root root  102 Nov 16  2017 .placeholder

/etc/cron.weekly:
total 20
drwxr-xr-x  2 root root 4096 May 22  2018 .
drwxr-xr-x 94 root root 4096 Sep 25 21:40 ..
-rwxr-xr-x  1 root root  723 Apr  7  2018 man-db
-rw-r--r--  1 root root  102 Nov 16  2017 .placeholder
-rwxr-xr-x  1 root root  211 Jul 12  2013 update-notifier-common


[00;31m[-] Crontab contents:[00m
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user	command
17 *	* * *	root    cd / && run-parts --report /etc/cron.hourly
25 6	* * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6	* * 7	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6	1 * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#


[00;31m[-] Systemd timers:[00m
NEXT                         LEFT          LAST                         PASSED              UNIT                         ACTIVATES
Wed 2019-01-30 01:45:07 UTC  20s left      n/a                          n/a                 systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service
Wed 2019-01-30 02:07:34 UTC  22min left    Tue 2018-09-25 20:17:12 UTC  4 months 4 days ago motd-news.timer              motd-news.service
Wed 2019-01-30 02:09:00 UTC  24min left    Wed 2019-01-30 01:39:01 UTC  5min ago            phpsessionclean.timer        phpsessionclean.service
Wed 2019-01-30 06:18:19 UTC  4h 33min left Wed 2019-01-30 01:30:24 UTC  14min ago           apt-daily-upgrade.timer      apt-daily-upgrade.service
Wed 2019-01-30 09:54:26 UTC  8h left       Wed 2019-01-30 01:30:24 UTC  14min ago           apt-daily.timer              apt-daily.service
Mon 2019-02-04 00:00:00 UTC  4 days left   Wed 2019-01-30 01:30:24 UTC  14min ago           fstrim.timer                 fstrim.service

6 timers listed.
[2mEnable thorough tests to see inactive timers[00m


[00;33m### NETWORKING  ##########################################[00m
[00;31m[-] Network and IP info:[00m
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.10.10.150  netmask 255.255.255.0  broadcast 10.10.10.255
        inet6 dead:beef::250:56ff:feb9:6ed7  prefixlen 64  scopeid 0x0<global>
        inet6 fe80::250:56ff:feb9:6ed7  prefixlen 64  scopeid 0x20<link>
        ether 00:50:56:b9:6e:d7  txqueuelen 1000  (Ethernet)
        RX packets 72096  bytes 7439471 (7.4 MB)
        RX errors 0  dropped 12  overruns 0  frame 0
        TX packets 59632  bytes 15796293 (15.7 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 2936  bytes 395634 (395.6 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2936  bytes 395634 (395.6 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


[00;31m[-] ARP history:[00m
_gateway (10.10.10.2) at 00:50:56:aa:9c:8d [ether] on ens33


[00;31m[-] Nameserver(s):[00m
nameserver 127.0.0.53


[00;31m[-] Nameserver(s):[00m
Global
          DNSSEC NTA: 10.in-addr.arpa
                      16.172.in-addr.arpa
                      168.192.in-addr.arpa
                      17.172.in-addr.arpa
                      18.172.in-addr.arpa
                      19.172.in-addr.arpa
                      20.172.in-addr.arpa
                      21.172.in-addr.arpa
                      22.172.in-addr.arpa
                      23.172.in-addr.arpa
                      24.172.in-addr.arpa
                      25.172.in-addr.arpa
                      26.172.in-addr.arpa
                      27.172.in-addr.arpa
                      28.172.in-addr.arpa
                      29.172.in-addr.arpa
                      30.172.in-addr.arpa
                      31.172.in-addr.arpa
                      corp
                      d.f.ip6.arpa
                      home
                      internal
                      intranet
                      lan
                      local
                      private
                      test

Link 2 (ens33)
      Current Scopes: none
       LLMNR setting: yes
MulticastDNS setting: no
      DNSSEC setting: no
    DNSSEC supported: no


[00;31m[-] Default route:[00m
default         _gateway        0.0.0.0         UG    0      0        0 ens33


[00;31m[-] Listening TCP:[00m
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 10.10.10.150:22         10.10.12.87:57716       TIME_WAIT   -                   
tcp        0      0 10.10.10.150:22         10.10.12.87:57750       TIME_WAIT   -                   
tcp        0      0 127.0.0.1:51108         127.0.0.1:80            TIME_WAIT   -                   
tcp        0      0 10.10.10.150:22         10.10.15.2:42416        ESTABLISHED -                   
tcp        0      0 10.10.10.150:22         10.10.12.87:57688       TIME_WAIT   -                   
tcp        0      0 10.10.10.150:22         10.10.12.87:57746       TIME_WAIT   -                   
tcp        0      0 10.10.10.150:22         10.10.15.115:47052      ESTABLISHED -                   
tcp        0      0 10.10.10.150:22         10.10.13.207:35718      ESTABLISHED -                   
tcp        0      0 10.10.10.150:22         10.10.12.87:57684       TIME_WAIT   -                   
tcp6       0      0 :::80                   :::*                    LISTEN      -                   
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47390       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49146       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49148       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47384       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47424       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.18:58770       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47376       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47400       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47406       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47416       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47354       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49154       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47362       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49152       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47408       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47412       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47398       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49172       ESTABLISHED -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49160       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47374       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47366       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47380       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47378       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49164       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47394       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49156       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47382       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47410       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47388       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47402       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47420       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47358       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.14.13:49166       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47372       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47356       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47418       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47368       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47428       TIME_WAIT   -                   
tcp6       0      0 10.10.10.150:80         10.10.12.87:47396       TIME_WAIT   -                   


[00;31m[-] Listening UDP:[00m
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
udp     6144      0 127.0.0.53:53           0.0.0.0:*                           -                   


[00;33m### SERVICES #############################################[00m
[00;31m[-] Running processes:[00m
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.4  0.4 159616  8992 ?        Ss   01:30   0:04 /sbin/init maybe-ubiquity
root         2  0.0  0.0      0     0 ?        S    01:30   0:00 [kthreadd]
root         4  0.0  0.0      0     0 ?        I<   01:30   0:00 [kworker/0:0H]
root         6  0.0  0.0      0     0 ?        I<   01:30   0:00 [mm_percpu_wq]
root         7  0.0  0.0      0     0 ?        S    01:30   0:00 [ksoftirqd/0]
root         8  0.0  0.0      0     0 ?        I    01:30   0:00 [rcu_sched]
root         9  0.0  0.0      0     0 ?        I    01:30   0:00 [rcu_bh]
root        10  0.0  0.0      0     0 ?        S    01:30   0:00 [migration/0]
root        11  0.0  0.0      0     0 ?        S    01:30   0:00 [watchdog/0]
root        12  0.0  0.0      0     0 ?        S    01:30   0:00 [cpuhp/0]
root        13  0.0  0.0      0     0 ?        S    01:30   0:00 [cpuhp/1]
root        14  0.0  0.0      0     0 ?        S    01:30   0:00 [watchdog/1]
root        15  0.0  0.0      0     0 ?        S    01:30   0:00 [migration/1]
root        16  0.0  0.0      0     0 ?        S    01:30   0:00 [ksoftirqd/1]
root        18  0.0  0.0      0     0 ?        I<   01:30   0:00 [kworker/1:0H]
root        19  0.0  0.0      0     0 ?        S    01:30   0:00 [kdevtmpfs]
root        20  0.0  0.0      0     0 ?        I<   01:30   0:00 [netns]
root        21  0.0  0.0      0     0 ?        S    01:30   0:00 [rcu_tasks_kthre]
root        22  0.0  0.0      0     0 ?        S    01:30   0:00 [kauditd]
root        23  0.0  0.0      0     0 ?        I    01:30   0:00 [kworker/0:1]
root        24  0.0  0.0      0     0 ?        S    01:30   0:00 [khungtaskd]
root        25  0.0  0.0      0     0 ?        S    01:30   0:00 [oom_reaper]
root        26  0.0  0.0      0     0 ?        I<   01:30   0:00 [writeback]
root        27  0.0  0.0      0     0 ?        S    01:30   0:00 [kcompactd0]
root        28  0.0  0.0      0     0 ?        SN   01:30   0:00 [ksmd]
root        29  0.0  0.0      0     0 ?        SN   01:30   0:00 [khugepaged]
root        30  0.0  0.0      0     0 ?        I<   01:30   0:00 [crypto]
root        31  0.0  0.0      0     0 ?        I<   01:30   0:00 [kintegrityd]
root        32  0.0  0.0      0     0 ?        I<   01:30   0:00 [kblockd]
root        33  0.0  0.0      0     0 ?        I<   01:30   0:00 [ata_sff]
root        34  0.0  0.0      0     0 ?        I<   01:30   0:00 [md]
root        35  0.0  0.0      0     0 ?        I<   01:30   0:00 [edac-poller]
root        36  0.0  0.0      0     0 ?        I<   01:30   0:00 [devfreq_wq]
root        37  0.0  0.0      0     0 ?        I<   01:30   0:00 [watchdogd]
root        39  0.0  0.0      0     0 ?        I    01:30   0:00 [kworker/1:1]
root        41  0.0  0.0      0     0 ?        S    01:30   0:00 [kswapd0]
root        42  0.0  0.0      0     0 ?        S    01:30   0:00 [ecryptfs-kthrea]
root        84  0.0  0.0      0     0 ?        I<   01:30   0:00 [kthrotld]
root        85  0.0  0.0      0     0 ?        I<   01:30   0:00 [acpi_thermal_pm]
root        86  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_0]
root        87  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_0]
root        88  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_1]
root        89  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_1]
root        94  0.0  0.0      0     0 ?        I<   01:30   0:00 [ipv6_addrconf]
root       103  0.0  0.0      0     0 ?        I<   01:30   0:00 [kstrp]
root       120  0.0  0.0      0     0 ?        I<   01:30   0:00 [charger_manager]
root       157  0.0  0.0      0     0 ?        I    01:30   0:00 [kworker/1:2]
root       171  0.0  0.0      0     0 ?        I<   01:30   0:00 [ttm_swap]
root       172  0.0  0.0      0     0 ?        S    01:30   0:00 [irq/16-vmwgfx]
root       204  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_2]
root       205  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_2]
root       206  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_3]
root       207  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_3]
root       208  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_4]
root       209  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_4]
root       210  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_5]
root       211  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_5]
root       212  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_6]
root       213  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_6]
root       214  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_7]
root       215  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_7]
root       216  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_8]
root       217  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_8]
root       218  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_9]
root       219  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_9]
root       220  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_10]
root       221  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_10]
root       222  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_11]
root       223  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_11]
root       224  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_12]
root       225  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_12]
root       226  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_13]
root       227  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_13]
root       228  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_14]
root       229  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_14]
root       230  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_15]
root       231  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_15]
root       232  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_16]
root       233  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_16]
root       234  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_17]
root       235  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_17]
root       236  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_18]
root       237  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_18]
root       238  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_19]
root       239  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_19]
root       240  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_20]
root       241  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_20]
root       242  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_21]
root       243  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_21]
root       244  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_22]
root       245  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_22]
root       246  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_23]
root       247  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_23]
root       248  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_24]
root       249  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_24]
root       250  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_25]
root       251  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_25]
root       252  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_26]
root       253  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_26]
root       254  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_27]
root       255  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_27]
root       256  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_28]
root       257  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_28]
root       258  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_29]
root       259  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_29]
root       260  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_30]
root       261  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_30]
root       262  0.0  0.0      0     0 ?        S    01:30   0:00 [scsi_eh_31]
root       263  0.0  0.0      0     0 ?        I<   01:30   0:00 [scsi_tmf_31]
root       288  0.0  0.0      0     0 ?        I    01:30   0:00 [kworker/u4:27]
root       294  0.0  0.0      0     0 ?        I<   01:30   0:00 [kworker/0:1H]
root       296  0.0  0.0      0     0 ?        I<   01:30   0:00 [kworker/1:1H]
root       365  0.0  0.0      0     0 ?        I<   01:30   0:00 [raid5wq]
root       413  0.0  0.0      0     0 ?        S    01:30   0:00 [jbd2/sda2-8]
root       414  0.0  0.0      0     0 ?        I<   01:30   0:00 [ext4-rsv-conver]
root       478  0.0  0.5 191256 10496 ?        Ssl  01:30   0:00 /usr/bin/vmtoolsd
root       479  0.0  0.8 127668 17712 ?        S<s  01:30   0:00 /lib/systemd/systemd-journald
root       485  0.0  0.0      0     0 ?        I<   01:30   0:00 [iscsi_eh]
root       488  0.0  0.0      0     0 ?        I<   01:30   0:00 [ib-comp-wq]
root       489  0.0  0.0      0     0 ?        I<   01:30   0:00 [ib_mcast]
root       490  0.0  0.0      0     0 ?        I<   01:30   0:00 [ib_nl_sa_wq]
root       496  0.0  0.0  97708  1668 ?        Ss   01:30   0:00 /sbin/lvmetad -f
root       502  0.4  0.2  46724  5600 ?        Ss   01:30   0:04 /lib/systemd/systemd-udevd
root       505  0.0  0.0      0     0 ?        I<   01:30   0:00 [rdma_cm]
root       535  0.0  0.0      0     0 ?        S<   01:30   0:00 [loop0]
systemd+   585  0.0  0.1 141908  3240 ?        Ssl  01:30   0:00 /lib/systemd/systemd-timesyncd
systemd+   814  0.0  0.2  71816  5080 ?        Ss   01:30   0:00 /lib/systemd/systemd-networkd
systemd+   832  0.0  0.2  70608  5172 ?        Ss   01:30   0:00 /lib/systemd/systemd-resolved
root       976  0.0  0.8 169132 17076 ?        Ssl  01:30   0:00 /usr/bin/python3 /usr/bin/networkd-dispatcher
root       982  0.0  0.1 110508  3444 ?        Ssl  01:30   0:00 /usr/sbin/irqbalance --foreground
root       986  0.0  0.0  95540  1612 ?        Ssl  01:30   0:00 /usr/bin/lxcfs /var/lib/lxcfs/
daemon     988  0.0  0.1  28332  2344 ?        Ss   01:30   0:00 /usr/sbin/atd -f
root       989  0.0  0.3 286244  6836 ?        Ssl  01:30   0:00 /usr/lib/accountsservice/accounts-daemon
syslog     991  0.0  0.2 267272  5064 ?        Ssl  01:30   0:00 /usr/sbin/rsyslogd -n
root       992  0.0  0.2  70584  6032 ?        Ss   01:30   0:00 /lib/systemd/systemd-logind
root       998  0.0  0.1  30028  3244 ?        Ss   01:30   0:00 /usr/sbin/cron -f
root      1003  0.0  0.8 692216 18272 ?        Ssl  01:30   0:00 /usr/lib/snapd/snapd
message+  1004  0.0  0.2  50068  4528 ?        Ss   01:30   0:00 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
root      1006  0.0  0.4  87760 10156 ?        Ss   01:30   0:00 /usr/bin/VGAuthService
root      1010  0.0  0.3  72296  6408 ?        Ss   01:30   0:00 /usr/sbin/sshd -D
root      1027  0.0  0.0  25376   288 ?        Ss   01:30   0:00 /sbin/iscsid
root      1028  0.0  0.2  25880  5268 ?        S<Ls 01:30   0:00 /sbin/iscsid
root      1052  0.0  0.3 288888  6620 ?        Ssl  01:30   0:00 /usr/lib/policykit-1/polkitd --no-debug
root      1112  0.0  0.0  14888  1924 tty1     Ss+  01:30   0:00 /sbin/agetty -o -p -- \u --noclear tty1 linux
mysql     1179  0.2 10.2 1614932 208876 ?      Sl   01:30   0:02 /usr/sbin/mysqld --daemonize --pid-file=/run/mysqld/mysqld.pid
root      1202  0.0  0.9 360164 20268 ?        Ss   01:30   0:00 /usr/sbin/apache2 -k start
www-data  1221  0.0  1.5 367868 30800 ?        S    01:30   0:00 /usr/sbin/apache2 -k start
root      1527  0.0  0.3 107984  7252 ?        Ss   01:30   0:00 sshd: floris [priv]
floris    1538  0.0  0.3  76620  7612 ?        Ss   01:30   0:00 /lib/systemd/systemd --user
floris    1545  0.0  0.1 193576  2324 ?        S    01:30   0:00 (sd-pam)
floris    1652  0.0  0.2 109772  5656 ?        S    01:30   0:00 sshd: floris@pts/0
floris    1657  0.0  0.2  21472  5240 pts/0    Ss   01:30   0:00 -bash
root      1687  0.0  0.3 105684  7124 ?        Ss   01:31   0:00 sshd: floris [priv]
floris    1754  0.0  0.2 107984  5468 ?        S    01:31   0:00 sshd: floris@pts/1
floris    1755  0.0  0.2  21608  5448 pts/1    Ss+  01:31   0:00 -bash
www-data  1812  0.0  1.2 365472 25680 ?        S    01:34   0:00 /usr/sbin/apache2 -k start
www-data  1831  0.0  1.1 365312 24060 ?        S    01:35   0:00 /usr/sbin/apache2 -k start
www-data  1832  0.1  1.4 367764 29168 ?        S    01:35   0:00 /usr/sbin/apache2 -k start
www-data  1835  0.0  1.5 369556 32004 ?        S    01:35   0:00 /usr/sbin/apache2 -k start
root      1837  0.0  0.0      0     0 ?        I    01:35   0:00 [kworker/u4:0]
www-data  1861  0.0  1.2 365296 26344 ?        S    01:38   0:00 /usr/sbin/apache2 -k start
www-data  1863  0.1  1.5 369940 32336 ?        S    01:38   0:00 /usr/sbin/apache2 -k start
www-data  1864  0.0  1.1 365396 23836 ?        S    01:38   0:00 /usr/sbin/apache2 -k start
www-data  1865  0.1  1.5 369492 32396 ?        S    01:38   0:00 /usr/sbin/apache2 -k start
www-data  1867  0.0  1.4 367720 28664 ?        S    01:38   0:00 /usr/sbin/apache2 -k start
root      1951  0.0  0.0      0     0 ?        I    01:39   0:00 [kworker/0:0]
root      2032  0.0  0.0      0     0 ?        I    01:44   0:00 [kworker/u4:1]
www-data  2041  0.2  1.1 365256 23796 ?        S    01:44   0:00 /usr/sbin/apache2 -k start
root      2073  0.0  0.3 105684  6860 ?        Ss   01:44   0:00 sshd: floris [priv]
sshd      2074  0.0  0.1  72296  2976 ?        S    01:44   0:00 sshd: floris [net]
floris    2075  0.1  0.1  12384  4020 pts/0    S+   01:44   0:00 /bin/bash ./le.sh
floris    2076  0.2  0.1  12516  2964 pts/0    S+   01:44   0:00 /bin/bash ./le.sh
floris    2077  0.0  0.0   6180   740 pts/0    S+   01:44   0:00 tee -a
floris    2263  0.0  0.1  12516  2772 pts/0    S+   01:44   0:00 /bin/bash ./le.sh
floris    2264  0.0  0.1  38372  3676 pts/0    R+   01:44   0:00 ps aux


[00;31m[-] Process binaries and associated permissions (from above list):[00m
1.1M -rwxr-xr-x 1 root root 1.1M Apr  4  2018 /bin/bash
1.6M -rwxr-xr-x 1 root root 1.6M Apr 20  2018 /lib/systemd/systemd
128K -rwxr-xr-x 1 root root 127K Apr 20  2018 /lib/systemd/systemd-journald
216K -rwxr-xr-x 1 root root 215K Apr 20  2018 /lib/systemd/systemd-logind
1.6M -rwxr-xr-x 1 root root 1.6M Apr 20  2018 /lib/systemd/systemd-networkd
372K -rwxr-xr-x 1 root root 371K Apr 20  2018 /lib/systemd/systemd-resolved
 40K -rwxr-xr-x 1 root root  39K Apr 20  2018 /lib/systemd/systemd-timesyncd
572K -rwxr-xr-x 1 root root 571K Apr 20  2018 /lib/systemd/systemd-udevd
 56K -rwxr-xr-x 1 root root  56K Mar 15  2018 /sbin/agetty
   0 lrwxrwxrwx 1 root root   20 Apr 20  2018 /sbin/init -> /lib/systemd/systemd
388K -rwxr-xr-x 1 root root 399K Feb 21  2018 /sbin/iscsid
 84K -rwxr-xr-x 1 root root  83K Apr 12  2018 /sbin/lvmetad
232K -rwxr-xr-x 1 root root 232K Nov 15  2017 /usr/bin/dbus-daemon
 20K -rwxr-xr-x 1 root root  19K Mar 27  2018 /usr/bin/lxcfs
   0 lrwxrwxrwx 1 root root    9 Apr 10  2018 /usr/bin/python3 -> python3.6
124K -rwxr-xr-x 1 root root 123K Apr 19  2018 /usr/bin/VGAuthService
 48K -rwxr-xr-x 1 root root  47K Apr 19  2018 /usr/bin/vmtoolsd
180K -rwxr-xr-x 1 root root 179K Dec 18  2017 /usr/lib/accountsservice/accounts-daemon
 16K -rwxr-xr-x 1 root root  15K Mar 27  2018 /usr/lib/policykit-1/polkitd
 16M -rwxr-xr-x 1 root root  16M May 11  2018 /usr/lib/snapd/snapd
656K -rwxr-xr-x 1 root root 656K Apr 25  2018 /usr/sbin/apache2
 28K -rwxr-xr-x 1 root root  27K Feb 20  2018 /usr/sbin/atd
 48K -rwxr-xr-x 1 root root  47K Nov 16  2017 /usr/sbin/cron
 64K -rwxr-xr-x 1 root root  63K Dec 11  2017 /usr/sbin/irqbalance
 24M -rwxr-xr-x 1 root root  24M Apr 24  2018 /usr/sbin/mysqld
668K -rwxr-xr-x 1 root root 665K Apr 24  2018 /usr/sbin/rsyslogd
772K -rwxr-xr-x 1 root root 769K Feb 10  2018 /usr/sbin/sshd


[00;31m[-] /etc/init.d/ binary permissions:[00m
total 200
drwxr-xr-x  2 root root 4096 Sep 25 21:40 .
drwxr-xr-x 94 root root 4096 Sep 25 21:40 ..
-rwxr-xr-x  1 root root 2269 Apr 22  2017 acpid
-rwxr-xr-x  1 root root 8181 Oct 23  2017 apache2
-rwxr-xr-x  1 root root 2489 Oct 23  2017 apache-htcacheclean
-rwxr-xr-x  1 root root 4335 Mar 22  2018 apparmor
-rwxr-xr-x  1 root root 2802 Nov 20  2017 apport
-rwxr-xr-x  1 root root 1071 Aug 21  2015 atd
-rwxr-xr-x  1 root root 1232 Apr 19  2018 console-setup.sh
-rwxr-xr-x  1 root root 3049 Nov 16  2017 cron
-rwxr-xr-x  1 root root  937 Mar 18  2018 cryptdisks
-rwxr-xr-x  1 root root  978 Mar 18  2018 cryptdisks-early
-rwxr-xr-x  1 root root 2813 Nov 15  2017 dbus
-rwxr-xr-x  1 root root 4352 Mar 15  2013 ebtables
-rwxr-xr-x  1 root root  985 Mar  4  2018 grub-common
-rwxr-xr-x  1 root root 3809 Feb 14  2018 hwclock.sh
-rwxr-xr-x  1 root root 2444 Oct 25  2017 irqbalance
-rwxr-xr-x  1 root root 1503 Feb 21  2018 iscsid
-rwxr-xr-x  1 root root 1479 Feb 15  2018 keyboard-setup.sh
-rwxr-xr-x  1 root root 2044 Aug 15  2017 kmod
-rwxr-xr-x  1 root root  695 Dec  3  2017 lvm2
-rwxr-xr-x  1 root root  571 Dec  3  2017 lvm2-lvmetad
-rwxr-xr-x  1 root root  586 Dec  3  2017 lvm2-lvmpolld
-rwxr-xr-x  1 root root 2378 Mar 27  2018 lxcfs
-rwxr-xr-x  1 root root 2240 Apr 17  2018 lxd
-rwxr-xr-x  1 root root 2653 Mar  7  2018 mdadm
-rwxr-xr-x  1 root root 1249 Mar  7  2018 mdadm-waitidle
-rwxr-xr-x  1 root root 5607 Jan 12  2018 mysql
-rwxr-xr-x  1 root root 1364 Mar 17  2017 netfilter-persistent
-rwxr-xr-x  1 root root 4597 Nov 25  2016 networking
-rwxr-xr-x  1 root root 2503 Feb 21  2018 open-iscsi
-rwxr-xr-x  1 root root 1846 Mar 22  2018 open-vm-tools
-rwxr-xr-x  1 root root 1366 Jan 17  2018 plymouth
-rwxr-xr-x  1 root root  752 Jan 17  2018 plymouth-log
-rwxr-xr-x  1 root root 1191 Jan 17  2018 procps
-rwxr-xr-x  1 root root 4355 Dec 13  2017 rsync
-rwxr-xr-x  1 root root 2864 Jan 14  2018 rsyslog
-rwxr-xr-x  1 root root 1222 May 21  2017 screen-cleanup
-rwxr-xr-x  1 root root 3837 Jan 25  2018 ssh
-rwxr-xr-x  1 root root 5974 Apr 20  2018 udev
-rwxr-xr-x  1 root root 2083 Aug 15  2017 ufw
-rwxr-xr-x  1 root root 1391 Apr 17  2018 unattended-upgrades
-rwxr-xr-x  1 root root 1306 Mar 15  2018 uuidd


[00;31m[-] /etc/init/ config file permissions:[00m
total 12
drwxr-xr-x  2 root root 4096 May 22  2018 .
drwxr-xr-x 94 root root 4096 Sep 25 21:40 ..
-rw-r--r--  1 root root 1757 Jan 12  2018 mysql.conf


[00;31m[-] /lib/systemd/* config file permissions:[00m
/lib/systemd/:
total 6.7M
drwxr-xr-x 23 root root  20K Sep 25 21:40 system
drwxr-xr-x  2 root root 4.0K May 22  2018 system-generators
drwxr-xr-x  2 root root 4.0K Apr 26  2018 system-sleep
drwxr-xr-x  2 root root 4.0K Apr 26  2018 system-shutdown
drwxr-xr-x  2 root root 4.0K Apr 26  2018 network
drwxr-xr-x  2 root root 4.0K Apr 26  2018 system-preset
-rw-r--r--  1 root root 2.3M Apr 20  2018 libsystemd-shared-237.so
-rwxr-xr-x  1 root root 1.3K Apr 20  2018 set-cpufreq
-rwxr-xr-x  1 root root 1.6M Apr 20  2018 systemd
-rwxr-xr-x  1 root root 6.0K Apr 20  2018 systemd-ac-power
-rwxr-xr-x  1 root root  18K Apr 20  2018 systemd-backlight
-rwxr-xr-x  1 root root  11K Apr 20  2018 systemd-binfmt
-rwxr-xr-x  1 root root  10K Apr 20  2018 systemd-cgroups-agent
-rwxr-xr-x  1 root root  22K Apr 20  2018 systemd-cryptsetup
-rwxr-xr-x  1 root root  15K Apr 20  2018 systemd-dissect
-rwxr-xr-x  1 root root  18K Apr 20  2018 systemd-fsck
-rwxr-xr-x  1 root root  23K Apr 20  2018 systemd-fsckd
-rwxr-xr-x  1 root root  19K Apr 20  2018 systemd-growfs
-rwxr-xr-x  1 root root  10K Apr 20  2018 systemd-hibernate-resume
-rwxr-xr-x  1 root root  23K Apr 20  2018 systemd-hostnamed
-rwxr-xr-x  1 root root  15K Apr 20  2018 systemd-initctl
-rwxr-xr-x  1 root root 127K Apr 20  2018 systemd-journald
-rwxr-xr-x  1 root root  35K Apr 20  2018 systemd-localed
-rwxr-xr-x  1 root root 215K Apr 20  2018 systemd-logind
-rwxr-xr-x  1 root root  10K Apr 20  2018 systemd-makefs
-rwxr-xr-x  1 root root  15K Apr 20  2018 systemd-modules-load
-rwxr-xr-x  1 root root 1.6M Apr 20  2018 systemd-networkd
-rwxr-xr-x  1 root root  19K Apr 20  2018 systemd-networkd-wait-online
-rwxr-xr-x  1 root root  11K Apr 20  2018 systemd-quotacheck
-rwxr-xr-x  1 root root  10K Apr 20  2018 systemd-random-seed
-rwxr-xr-x  1 root root  15K Apr 20  2018 systemd-remount-fs
-rwxr-xr-x  1 root root  10K Apr 20  2018 systemd-reply-password
-rwxr-xr-x  1 root root 371K Apr 20  2018 systemd-resolved
-rwxr-xr-x  1 root root  19K Apr 20  2018 systemd-rfkill
-rwxr-xr-x  1 root root  43K Apr 20  2018 systemd-shutdown
-rwxr-xr-x  1 root root  15K Apr 20  2018 systemd-sleep
-rwxr-xr-x  1 root root  23K Apr 20  2018 systemd-socket-proxyd
-rwxr-xr-x  1 root root  11K Apr 20  2018 systemd-sulogin-shell
-rwxr-xr-x  1 root root  15K Apr 20  2018 systemd-sysctl
-rwxr-xr-x  1 root root 1.3K Apr 20  2018 systemd-sysv-install
-rwxr-xr-x  1 root root  27K Apr 20  2018 systemd-timedated
-rwxr-xr-x  1 root root  39K Apr 20  2018 systemd-timesyncd
-rwxr-xr-x  1 root root 571K Apr 20  2018 systemd-udevd
-rwxr-xr-x  1 root root  15K Apr 20  2018 systemd-update-utmp
-rwxr-xr-x  1 root root  10K Apr 20  2018 systemd-user-sessions
-rwxr-xr-x  1 root root  10K Apr 20  2018 systemd-veritysetup
-rwxr-xr-x  1 root root  10K Apr 20  2018 systemd-volatile-root
-rw-r--r--  1 root root  685 Jan 28  2018 resolv.conf

/lib/systemd/system:
total 1.1M
drwxr-xr-x 2 root root 4.0K May 22  2018 apache2.service.d
-rw-r--r-- 1 root root  252 May 11  2018 snapd.autoimport.service
-rw-r--r-- 1 root root  320 May 11  2018 snapd.core-fixup.service
-rw-r--r-- 1 root root  237 May 11  2018 snapd.seeded.service
-rw-r--r-- 1 root root  308 May 11  2018 snapd.service
-rw-r--r-- 1 root root  287 May 11  2018 snapd.snap-repair.service
-rw-r--r-- 1 root root  281 May 11  2018 snapd.snap-repair.timer
-rw-r--r-- 1 root root  281 May 11  2018 snapd.socket
-rw-r--r-- 1 root root  474 May 11  2018 snapd.system-shutdown.service
-rw-r--r-- 1 root root  391 Apr 27  2018 cloud-config.service
-rw-r--r-- 1 root root  482 Apr 27  2018 cloud-final.service
-rw-r--r-- 1 root root  580 Apr 27  2018 cloud-init-local.service
-rw-r--r-- 1 root root  642 Apr 27  2018 cloud-init.service
-rw-r--r-- 1 root root  536 Apr 26  2018 cloud-config.target
-rw-r--r-- 1 root root  256 Apr 26  2018 cloud-init.target
lrwxrwxrwx 1 root root    9 Apr 26  2018 screen-cleanup.service -> /dev/null
drwxr-xr-x 2 root root 4.0K Apr 26  2018 halt.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 initrd-switch-root.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 kexec.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 multi-user.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 poweroff.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 reboot.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 sysinit.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 sockets.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 getty.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 graphical.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 local-fs.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 rc-local.service.d
drwxr-xr-x 2 root root 4.0K Apr 26  2018 rescue.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 timers.target.wants
drwxr-xr-x 2 root root 4.0K Apr 26  2018 user@.service.d
-rw-r--r-- 1 root root  290 Apr 24  2018 rsyslog.service
-rw-r--r-- 1 root root  266 Apr 23  2018 netplan-wpa@.service
lrwxrwxrwx 1 root root   14 Apr 20  2018 autovt@.service -> getty@.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 bootlogd.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 bootlogs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 bootmisc.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 checkfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 checkroot-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 checkroot.service -> /dev/null
-rw-r--r-- 1 root root 1.1K Apr 20  2018 console-getty.service
-rw-r--r-- 1 root root 1.3K Apr 20  2018 container-getty@.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 cryptdisks-early.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 cryptdisks.service -> /dev/null
lrwxrwxrwx 1 root root   13 Apr 20  2018 ctrl-alt-del.target -> reboot.target
lrwxrwxrwx 1 root root   25 Apr 20  2018 dbus-org.freedesktop.hostname1.service -> systemd-hostnamed.service
lrwxrwxrwx 1 root root   23 Apr 20  2018 dbus-org.freedesktop.locale1.service -> systemd-localed.service
lrwxrwxrwx 1 root root   22 Apr 20  2018 dbus-org.freedesktop.login1.service -> systemd-logind.service
lrwxrwxrwx 1 root root   25 Apr 20  2018 dbus-org.freedesktop.timedate1.service -> systemd-timedated.service
-rw-r--r-- 1 root root 1.1K Apr 20  2018 debug-shell.service
lrwxrwxrwx 1 root root   16 Apr 20  2018 default.target -> graphical.target
-rw-r--r-- 1 root root  797 Apr 20  2018 emergency.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 fuse.service -> /dev/null
-rw-r--r-- 1 root root 2.0K Apr 20  2018 getty@.service
-rw-r--r-- 1 root root  342 Apr 20  2018 getty-static.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 halt.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 hostname.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 hwclock.service -> /dev/null
-rw-r--r-- 1 root root  670 Apr 20  2018 initrd-cleanup.service
-rw-r--r-- 1 root root  830 Apr 20  2018 initrd-parse-etc.service
-rw-r--r-- 1 root root  589 Apr 20  2018 initrd-switch-root.service
-rw-r--r-- 1 root root  704 Apr 20  2018 initrd-udevadm-cleanup-db.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 killprocs.service -> /dev/null
lrwxrwxrwx 1 root root   28 Apr 20  2018 kmod.service -> systemd-modules-load.service
-rw-r--r-- 1 root root  717 Apr 20  2018 kmod-static-nodes.service
lrwxrwxrwx 1 root root   28 Apr 20  2018 module-init-tools.service -> systemd-modules-load.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 motd.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 mountall-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 mountall.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 mountdevsubfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 mountkernfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 mountnfs-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 mountnfs.service -> /dev/null
-rw-r--r-- 1 root root  362 Apr 20  2018 ondemand.service
lrwxrwxrwx 1 root root   22 Apr 20  2018 procps.service -> systemd-sysctl.service
-rw-r--r-- 1 root root  609 Apr 20  2018 quotaon.service
-rw-r--r-- 1 root root  716 Apr 20  2018 rc-local.service
lrwxrwxrwx 1 root root   16 Apr 20  2018 rc.local.service -> rc-local.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 rc.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 rcS.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 reboot.service -> /dev/null
-rw-r--r-- 1 root root  788 Apr 20  2018 rescue.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 rmnologin.service -> /dev/null
lrwxrwxrwx 1 root root   15 Apr 20  2018 runlevel0.target -> poweroff.target
lrwxrwxrwx 1 root root   13 Apr 20  2018 runlevel1.target -> rescue.target
drwxr-xr-x 2 root root 4.0K Apr 20  2018 runlevel1.target.wants
lrwxrwxrwx 1 root root   17 Apr 20  2018 runlevel2.target -> multi-user.target
drwxr-xr-x 2 root root 4.0K Apr 20  2018 runlevel2.target.wants
lrwxrwxrwx 1 root root   17 Apr 20  2018 runlevel3.target -> multi-user.target
drwxr-xr-x 2 root root 4.0K Apr 20  2018 runlevel3.target.wants
lrwxrwxrwx 1 root root   17 Apr 20  2018 runlevel4.target -> multi-user.target
drwxr-xr-x 2 root root 4.0K Apr 20  2018 runlevel4.target.wants
lrwxrwxrwx 1 root root   16 Apr 20  2018 runlevel5.target -> graphical.target
drwxr-xr-x 2 root root 4.0K Apr 20  2018 runlevel5.target.wants
lrwxrwxrwx 1 root root   13 Apr 20  2018 runlevel6.target -> reboot.target
lrwxrwxrwx 1 root root    9 Apr 20  2018 sendsigs.service -> /dev/null
-rw-r--r-- 1 root root 1.5K Apr 20  2018 serial-getty@.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 single.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 stop-bootlogd.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 stop-bootlogd-single.service -> /dev/null
-rw-r--r-- 1 root root  554 Apr 20  2018 suspend-then-hibernate.target
-rw-r--r-- 1 root root  724 Apr 20  2018 systemd-ask-password-console.service
-rw-r--r-- 1 root root  752 Apr 20  2018 systemd-ask-password-wall.service
-rw-r--r-- 1 root root  752 Apr 20  2018 systemd-backlight@.service
-rw-r--r-- 1 root root  999 Apr 20  2018 systemd-binfmt.service
-rw-r--r-- 1 root root  537 Apr 20  2018 systemd-exit.service
-rw-r--r-- 1 root root  551 Apr 20  2018 systemd-fsckd.service
-rw-r--r-- 1 root root  540 Apr 20  2018 systemd-fsckd.socket
-rw-r--r-- 1 root root  714 Apr 20  2018 systemd-fsck-root.service
-rw-r--r-- 1 root root  715 Apr 20  2018 systemd-fsck@.service
-rw-r--r-- 1 root root  584 Apr 20  2018 systemd-halt.service
-rw-r--r-- 1 root root  671 Apr 20  2018 systemd-hibernate-resume@.service
-rw-r--r-- 1 root root  541 Apr 20  2018 systemd-hibernate.service
-rw-r--r-- 1 root root 1.1K Apr 20  2018 systemd-hostnamed.service
-rw-r--r-- 1 root root  818 Apr 20  2018 systemd-hwdb-update.service
-rw-r--r-- 1 root root  559 Apr 20  2018 systemd-hybrid-sleep.service
-rw-r--r-- 1 root root  551 Apr 20  2018 systemd-initctl.service
-rw-r--r-- 1 root root  686 Apr 20  2018 systemd-journald-audit.socket
-rw-r--r-- 1 root root 1.6K Apr 20  2018 systemd-journald.service
-rw-r--r-- 1 root root  771 Apr 20  2018 systemd-journal-flush.service
-rw-r--r-- 1 root root  597 Apr 20  2018 systemd-kexec.service
-rw-r--r-- 1 root root 1.1K Apr 20  2018 systemd-localed.service
-rw-r--r-- 1 root root 1.5K Apr 20  2018 systemd-logind.service
-rw-r--r-- 1 root root  733 Apr 20  2018 systemd-machine-id-commit.service
-rw-r--r-- 1 root root 1007 Apr 20  2018 systemd-modules-load.service
-rw-r--r-- 1 root root 1.9K Apr 20  2018 systemd-networkd.service
-rw-r--r-- 1 root root  740 Apr 20  2018 systemd-networkd-wait-online.service
-rw-r--r-- 1 root root  593 Apr 20  2018 systemd-poweroff.service
-rw-r--r-- 1 root root  655 Apr 20  2018 systemd-quotacheck.service
-rw-r--r-- 1 root root  792 Apr 20  2018 systemd-random-seed.service
-rw-r--r-- 1 root root  588 Apr 20  2018 systemd-reboot.service
-rw-r--r-- 1 root root  833 Apr 20  2018 systemd-remount-fs.service
-rw-r--r-- 1 root root 1.7K Apr 20  2018 systemd-resolved.service
-rw-r--r-- 1 root root  724 Apr 20  2018 systemd-rfkill.service
-rw-r--r-- 1 root root  537 Apr 20  2018 systemd-suspend.service
-rw-r--r-- 1 root root  573 Apr 20  2018 systemd-suspend-then-hibernate.service
-rw-r--r-- 1 root root  693 Apr 20  2018 systemd-sysctl.service
-rw-r--r-- 1 root root 1.1K Apr 20  2018 systemd-timedated.service
-rw-r--r-- 1 root root 1.4K Apr 20  2018 systemd-timesyncd.service
-rw-r--r-- 1 root root  659 Apr 20  2018 systemd-tmpfiles-clean.service
-rw-r--r-- 1 root root  764 Apr 20  2018 systemd-tmpfiles-setup-dev.service
-rw-r--r-- 1 root root  744 Apr 20  2018 systemd-tmpfiles-setup.service
-rw-r--r-- 1 root root  985 Apr 20  2018 systemd-udevd.service
-rw-r--r-- 1 root root  863 Apr 20  2018 systemd-udev-settle.service
-rw-r--r-- 1 root root  755 Apr 20  2018 systemd-udev-trigger.service
-rw-r--r-- 1 root root  797 Apr 20  2018 systemd-update-utmp-runlevel.service
-rw-r--r-- 1 root root  794 Apr 20  2018 systemd-update-utmp.service
-rw-r--r-- 1 root root  628 Apr 20  2018 systemd-user-sessions.service
-rw-r--r-- 1 root root  690 Apr 20  2018 systemd-volatile-root.service
-rw-r--r-- 1 root root 1.4K Apr 20  2018 system-update-cleanup.service
lrwxrwxrwx 1 root root   21 Apr 20  2018 udev.service -> systemd-udevd.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 umountfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 umountnfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Apr 20  2018 umountroot.service -> /dev/null
lrwxrwxrwx 1 root root   27 Apr 20  2018 urandom.service -> systemd-random-seed.service
-rw-r--r-- 1 root root  593 Apr 20  2018 user@.service
lrwxrwxrwx 1 root root    9 Apr 20  2018 x11-common.service -> /dev/null
-rw-r--r-- 1 root root  326 Apr 20  2018 apt-daily.service
-rw-r--r-- 1 root root  156 Apr 20  2018 apt-daily.timer
-rw-r--r-- 1 root root  238 Apr 20  2018 apt-daily-upgrade.service
-rw-r--r-- 1 root root  184 Apr 20  2018 apt-daily-upgrade.timer
-rw-r--r-- 1 root root  254 Apr 20  2018 thermald.service
-rw-r--r-- 1 root root  306 Apr 19  2018 open-vm-tools.service
-rw-r--r-- 1 root root  258 Apr 19  2018 networkd-dispatcher.service
-rw-r--r-- 1 root root  320 Apr 17  2018 lxd-containers.service
-rw-r--r-- 1 root root  605 Apr 17  2018 lxd.service
-rw-r--r-- 1 root root  197 Apr 17  2018 lxd.socket
-rw-r--r-- 1 root root  368 Apr 17  2018 unattended-upgrades.service
-rw-r--r-- 1 root root  412 Apr 13  2018 plymouth-halt.service
-rw-r--r-- 1 root root  426 Apr 13  2018 plymouth-kexec.service
lrwxrwxrwx 1 root root   27 Apr 13  2018 plymouth-log.service -> plymouth-read-write.service
-rw-r--r-- 1 root root  421 Apr 13  2018 plymouth-poweroff.service
-rw-r--r-- 1 root root  194 Apr 13  2018 plymouth-quit.service
-rw-r--r-- 1 root root  200 Apr 13  2018 plymouth-quit-wait.service
-rw-r--r-- 1 root root  244 Apr 13  2018 plymouth-read-write.service
-rw-r--r-- 1 root root  416 Apr 13  2018 plymouth-reboot.service
lrwxrwxrwx 1 root root   21 Apr 13  2018 plymouth.service -> plymouth-quit.service
-rw-r--r-- 1 root root  532 Apr 13  2018 plymouth-start.service
-rw-r--r-- 1 root root  291 Apr 13  2018 plymouth-switch-root.service
-rw-r--r-- 1 root root  490 Apr 13  2018 systemd-ask-password-plymouth.path
-rw-r--r-- 1 root root  467 Apr 13  2018 systemd-ask-password-plymouth.service
-rw-r--r-- 1 root root  383 Apr 12  2018 blk-availability.service
-rw-r--r-- 1 root root  341 Apr 12  2018 dm-event.service
-rw-r--r-- 1 root root  248 Apr 12  2018 dm-event.socket
-rw-r--r-- 1 root root  345 Apr 12  2018 lvm2-lvmetad.service
-rw-r--r-- 1 root root  215 Apr 12  2018 lvm2-lvmetad.socket
-rw-r--r-- 1 root root  300 Apr 12  2018 lvm2-lvmpolld.service
-rw-r--r-- 1 root root  213 Apr 12  2018 lvm2-lvmpolld.socket
-rw-r--r-- 1 root root  693 Apr 12  2018 lvm2-monitor.service
-rw-r--r-- 1 root root  403 Apr 12  2018 lvm2-pvscan@.service
lrwxrwxrwx 1 root root    9 Apr 12  2018 lvm2.service -> /dev/null
-rw-r--r-- 1 root root  173 Apr  9  2018 motd-news.service
-rw-r--r-- 1 root root  175 Apr  9  2018 motd-news.timer
-rw-r--r-- 1 root root  808 Mar 29  2018 friendly-recovery.service
-rw-r--r-- 1 root root  175 Mar 27  2018 polkit.service
-rw-r--r-- 1 root root  311 Mar 27  2018 lxcfs.service
-rw-r--r-- 1 root root  544 Mar 22  2018 apparmor.service
-rw-r--r-- 1 root root  298 Mar 22  2018 vgauth.service
-rw-r--r-- 1 root root   92 Mar 15  2018 fstrim.service
-rw-r--r-- 1 root root  170 Mar 15  2018 fstrim.timer
-rw-r--r-- 1 root root  189 Mar 15  2018 uuidd.service
-rw-r--r-- 1 root root  126 Mar 15  2018 uuidd.socket
-rw-r--r-- 1 root root  481 Mar  7  2018 mdadm-grow-continue@.service
-rw-r--r-- 1 root root  207 Mar  7  2018 mdadm-last-resort@.service
-rw-r--r-- 1 root root  176 Mar  7  2018 mdadm-last-resort@.timer
lrwxrwxrwx 1 root root    9 Mar  7  2018 mdadm.service -> /dev/null
-rw-r--r-- 1 root root  670 Mar  7  2018 mdadm-shutdown.service
lrwxrwxrwx 1 root root    9 Mar  7  2018 mdadm-waitidle.service -> /dev/null
-rw-r--r-- 1 root root  388 Mar  7  2018 mdmonitor.service
-rw-r--r-- 1 root root 1.1K Mar  7  2018 mdmon@.service
-rw-r--r-- 1 root root  494 Feb 21  2018 iscsid.service
-rw-r--r-- 1 root root 1.3K Feb 21  2018 open-iscsi.service
-rw-r--r-- 1 root root  169 Feb 20  2018 atd.service
-rw-r--r-- 1 root root  287 Feb 15  2018 keyboard-setup.service
-rw-r--r-- 1 root root  312 Feb 15  2018 console-setup.service
-rw-r--r-- 1 root root  919 Jan 28  2018 basic.target
-rw-r--r-- 1 root root  419 Jan 28  2018 bluetooth.target
-rw-r--r-- 1 root root  465 Jan 28  2018 cryptsetup-pre.target
-rw-r--r-- 1 root root  412 Jan 28  2018 cryptsetup.target
-rw-r--r-- 1 root root  750 Jan 28  2018 dev-hugepages.mount
-rw-r--r-- 1 root root  665 Jan 28  2018 dev-mqueue.mount
-rw-r--r-- 1 root root  471 Jan 28  2018 emergency.target
-rw-r--r-- 1 root root  541 Jan 28  2018 exit.target
-rw-r--r-- 1 root root  480 Jan 28  2018 final.target
-rw-r--r-- 1 root root  506 Jan 28  2018 getty-pre.target
-rw-r--r-- 1 root root  500 Jan 28  2018 getty.target
-rw-r--r-- 1 root root  598 Jan 28  2018 graphical.target
-rw-r--r-- 1 root root  527 Jan 28  2018 halt.target
-rw-r--r-- 1 root root  509 Jan 28  2018 hibernate.target
-rw-r--r-- 1 root root  530 Jan 28  2018 hybrid-sleep.target
-rw-r--r-- 1 root root  593 Jan 28  2018 initrd-fs.target
-rw-r--r-- 1 root root  561 Jan 28  2018 initrd-root-device.target
-rw-r--r-- 1 root root  566 Jan 28  2018 initrd-root-fs.target
-rw-r--r-- 1 root root  754 Jan 28  2018 initrd-switch-root.target
-rw-r--r-- 1 root root  763 Jan 28  2018 initrd.target
-rw-r--r-- 1 root root  541 Jan 28  2018 kexec.target
-rw-r--r-- 1 root root  435 Jan 28  2018 local-fs-pre.target
-rw-r--r-- 1 root root  547 Jan 28  2018 local-fs.target
-rw-r--r-- 1 root root  445 Jan 28  2018 machine.slice
-rw-r--r-- 1 root root  532 Jan 28  2018 multi-user.target
-rw-r--r-- 1 root root  505 Jan 28  2018 network-online.target
-rw-r--r-- 1 root root  502 Jan 28  2018 network-pre.target
-rw-r--r-- 1 root root  521 Jan 28  2018 network.target
-rw-r--r-- 1 root root  554 Jan 28  2018 nss-lookup.target
-rw-r--r-- 1 root root  513 Jan 28  2018 nss-user-lookup.target
-rw-r--r-- 1 root root  394 Jan 28  2018 paths.target
-rw-r--r-- 1 root root  592 Jan 28  2018 poweroff.target
-rw-r--r-- 1 root root  417 Jan 28  2018 printer.target
-rw-r--r-- 1 root root  745 Jan 28  2018 proc-sys-fs-binfmt_misc.automount
-rw-r--r-- 1 root root  655 Jan 28  2018 proc-sys-fs-binfmt_misc.mount
-rw-r--r-- 1 root root  583 Jan 28  2018 reboot.target
-rw-r--r-- 1 root root  549 Jan 28  2018 remote-cryptsetup.target
-rw-r--r-- 1 root root  436 Jan 28  2018 remote-fs-pre.target
-rw-r--r-- 1 root root  522 Jan 28  2018 remote-fs.target
-rw-r--r-- 1 root root  492 Jan 28  2018 rescue.target
-rw-r--r-- 1 root root  540 Jan 28  2018 rpcbind.target
-rw-r--r-- 1 root root  442 Jan 28  2018 shutdown.target
-rw-r--r-- 1 root root  402 Jan 28  2018 sigpwr.target
-rw-r--r-- 1 root root  460 Jan 28  2018 sleep.target
-rw-r--r-- 1 root root  449 Jan 28  2018 slices.target
-rw-r--r-- 1 root root  420 Jan 28  2018 smartcard.target
-rw-r--r-- 1 root root  396 Jan 28  2018 sockets.target
-rw-r--r-- 1 root root  420 Jan 28  2018 sound.target
-rw-r--r-- 1 root root  503 Jan 28  2018 suspend.target
-rw-r--r-- 1 root root  393 Jan 28  2018 swap.target
-rw-r--r-- 1 root root  795 Jan 28  2018 sys-fs-fuse-connections.mount
-rw-r--r-- 1 root root  558 Jan 28  2018 sysinit.target
-rw-r--r-- 1 root root  767 Jan 28  2018 sys-kernel-config.mount
-rw-r--r-- 1 root root  710 Jan 28  2018 sys-kernel-debug.mount
-rw-r--r-- 1 root root 1.4K Jan 28  2018 syslog.socket
-rw-r--r-- 1 root root  704 Jan 28  2018 systemd-ask-password-console.path
-rw-r--r-- 1 root root  632 Jan 28  2018 systemd-ask-password-wall.path
-rw-r--r-- 1 root root  564 Jan 28  2018 systemd-initctl.socket
-rw-r--r-- 1 root root 1.2K Jan 28  2018 systemd-journald-dev-log.socket
-rw-r--r-- 1 root root  882 Jan 28  2018 systemd-journald.socket
-rw-r--r-- 1 root root  631 Jan 28  2018 systemd-networkd.socket
-rw-r--r-- 1 root root  657 Jan 28  2018 systemd-rfkill.socket
-rw-r--r-- 1 root root  490 Jan 28  2018 systemd-tmpfiles-clean.timer
-rw-r--r-- 1 root root  635 Jan 28  2018 systemd-udevd-control.socket
-rw-r--r-- 1 root root  610 Jan 28  2018 systemd-udevd-kernel.socket
-rw-r--r-- 1 root root  445 Jan 28  2018 system.slice
-rw-r--r-- 1 root root  592 Jan 28  2018 system-update.target
-rw-r--r-- 1 root root  445 Jan 28  2018 timers.target
-rw-r--r-- 1 root root  435 Jan 28  2018 time-sync.target
-rw-r--r-- 1 root root  457 Jan 28  2018 umount.target
-rw-r--r-- 1 root root  432 Jan 28  2018 user.slice
-rw-r--r-- 1 root root  493 Jan 25  2018 ssh.service
-rw-r--r-- 1 root root  244 Jan 25  2018 ssh@.service
lrwxrwxrwx 1 root root    9 Jan 18  2018 sudo.service -> /dev/null
-rw-r--r-- 1 root root  155 Jan 17  2018 phpsessionclean.service
-rw-r--r-- 1 root root  144 Jan 17  2018 phpsessionclean.timer
-rw-r--r-- 1 root root  216 Jan 16  2018 ssh.socket
-rw-r--r-- 1 root root  462 Jan 15  2018 mysql.service
-rw-r--r-- 1 root root  741 Dec 18  2017 accounts-daemon.service
-rw-r--r-- 1 root root  368 Dec 11  2017 irqbalance.service
-rw-r--r-- 1 root root  246 Nov 20  2017 apport-forward.socket
-rw-r--r-- 1 root root  142 Nov 20  2017 apport-forward@.service
-rw-r--r-- 1 root root  251 Nov 16  2017 cron.service
-rw-r--r-- 1 root root  505 Nov 15  2017 dbus.service
-rw-r--r-- 1 root root  106 Nov 15  2017 dbus.socket
-rw-r--r-- 1 root root  346 Oct 23  2017 apache2.service
-rw-r--r-- 1 root root  418 Oct 23  2017 apache2@.service
-rw-r--r-- 1 root root  528 Oct 23  2017 apache-htcacheclean.service
-rw-r--r-- 1 root root  537 Oct 23  2017 apache-htcacheclean@.service
-rw-r--r-- 1 root root  266 Aug 15  2017 ufw.service
-rw-r--r-- 1 root root  401 Aug 15  2017 ureadahead.service
-rw-r--r-- 1 root root  250 Aug 15  2017 ureadahead-stop.service
-rw-r--r-- 1 root root  242 Aug 15  2017 ureadahead-stop.timer
-rw-r--r-- 1 root root  330 Aug 10  2017 setvtrgb.service
-rw-r--r-- 1 root root  115 Apr 22  2017 acpid.path
-rw-r--r-- 1 root root  234 Apr 22  2017 acpid.service
-rw-r--r-- 1 root root  115 Apr 22  2017 acpid.socket
-rw-r--r-- 1 root root  626 Nov 28  2016 ifup@.service
-rw-r--r-- 1 root root  735 Nov 25  2016 networking.service
-rw-r--r-- 1 root root  309 Jun 17  2016 pollinate.service
-rw-r--r-- 1 root root  456 May 25  2016 ebtables.service
-rw-r--r-- 1 root root  188 Feb 24  2014 rsync.service

/lib/systemd/system/apache2.service.d:
total 4.0K
-rw-r--r-- 1 root root 42 Nov 10  2017 apache2-systemd.conf

/lib/systemd/system/halt.target.wants:
total 0
lrwxrwxrwx 1 root root 24 Apr 13  2018 plymouth-halt.service -> ../plymouth-halt.service

/lib/systemd/system/initrd-switch-root.target.wants:
total 0
lrwxrwxrwx 1 root root 25 Apr 13  2018 plymouth-start.service -> ../plymouth-start.service
lrwxrwxrwx 1 root root 31 Apr 13  2018 plymouth-switch-root.service -> ../plymouth-switch-root.service

/lib/systemd/system/kexec.target.wants:
total 0
lrwxrwxrwx 1 root root 25 Apr 13  2018 plymouth-kexec.service -> ../plymouth-kexec.service

/lib/systemd/system/multi-user.target.wants:
total 0
lrwxrwxrwx 1 root root 15 Apr 20  2018 getty.target -> ../getty.target
lrwxrwxrwx 1 root root 33 Apr 20  2018 systemd-ask-password-wall.path -> ../systemd-ask-password-wall.path
lrwxrwxrwx 1 root root 25 Apr 20  2018 systemd-logind.service -> ../systemd-logind.service
lrwxrwxrwx 1 root root 39 Apr 20  2018 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service
lrwxrwxrwx 1 root root 32 Apr 20  2018 systemd-user-sessions.service -> ../systemd-user-sessions.service
lrwxrwxrwx 1 root root 24 Apr 13  2018 plymouth-quit.service -> ../plymouth-quit.service
lrwxrwxrwx 1 root root 29 Apr 13  2018 plymouth-quit-wait.service -> ../plymouth-quit-wait.service
lrwxrwxrwx 1 root root 15 Nov 15  2017 dbus.service -> ../dbus.service

/lib/systemd/system/poweroff.target.wants:
total 0
lrwxrwxrwx 1 root root 28 Apr 13  2018 plymouth-poweroff.service -> ../plymouth-poweroff.service

/lib/systemd/system/reboot.target.wants:
total 0
lrwxrwxrwx 1 root root 26 Apr 13  2018 plymouth-reboot.service -> ../plymouth-reboot.service

/lib/systemd/system/sysinit.target.wants:
total 0
lrwxrwxrwx 1 root root 20 Apr 20  2018 cryptsetup.target -> ../cryptsetup.target
lrwxrwxrwx 1 root root 22 Apr 20  2018 dev-hugepages.mount -> ../dev-hugepages.mount
lrwxrwxrwx 1 root root 19 Apr 20  2018 dev-mqueue.mount -> ../dev-mqueue.mount
lrwxrwxrwx 1 root root 28 Apr 20  2018 kmod-static-nodes.service -> ../kmod-static-nodes.service
lrwxrwxrwx 1 root root 36 Apr 20  2018 proc-sys-fs-binfmt_misc.automount -> ../proc-sys-fs-binfmt_misc.automount
lrwxrwxrwx 1 root root 32 Apr 20  2018 sys-fs-fuse-connections.mount -> ../sys-fs-fuse-connections.mount
lrwxrwxrwx 1 root root 26 Apr 20  2018 sys-kernel-config.mount -> ../sys-kernel-config.mount
lrwxrwxrwx 1 root root 25 Apr 20  2018 sys-kernel-debug.mount -> ../sys-kernel-debug.mount
lrwxrwxrwx 1 root root 36 Apr 20  2018 systemd-ask-password-console.path -> ../systemd-ask-password-console.path
lrwxrwxrwx 1 root root 25 Apr 20  2018 systemd-binfmt.service -> ../systemd-binfmt.service
lrwxrwxrwx 1 root root 30 Apr 20  2018 systemd-hwdb-update.service -> ../systemd-hwdb-update.service
lrwxrwxrwx 1 root root 27 Apr 20  2018 systemd-journald.service -> ../systemd-journald.service
lrwxrwxrwx 1 root root 32 Apr 20  2018 systemd-journal-flush.service -> ../systemd-journal-flush.service
lrwxrwxrwx 1 root root 36 Apr 20  2018 systemd-machine-id-commit.service -> ../systemd-machine-id-commit.service
lrwxrwxrwx 1 root root 31 Apr 20  2018 systemd-modules-load.service -> ../systemd-modules-load.service
lrwxrwxrwx 1 root root 30 Apr 20  2018 systemd-random-seed.service -> ../systemd-random-seed.service
lrwxrwxrwx 1 root root 25 Apr 20  2018 systemd-sysctl.service -> ../systemd-sysctl.service
lrwxrwxrwx 1 root root 37 Apr 20  2018 systemd-tmpfiles-setup-dev.service -> ../systemd-tmpfiles-setup-dev.service
lrwxrwxrwx 1 root root 33 Apr 20  2018 systemd-tmpfiles-setup.service -> ../systemd-tmpfiles-setup.service
lrwxrwxrwx 1 root root 24 Apr 20  2018 systemd-udevd.service -> ../systemd-udevd.service
lrwxrwxrwx 1 root root 31 Apr 20  2018 systemd-udev-trigger.service -> ../systemd-udev-trigger.service
lrwxrwxrwx 1 root root 30 Apr 20  2018 systemd-update-utmp.service -> ../systemd-update-utmp.service
lrwxrwxrwx 1 root root 30 Apr 13  2018 plymouth-read-write.service -> ../plymouth-read-write.service
lrwxrwxrwx 1 root root 25 Apr 13  2018 plymouth-start.service -> ../plymouth-start.service

/lib/systemd/system/sockets.target.wants:
total 0
lrwxrwxrwx 1 root root 25 Apr 20  2018 systemd-initctl.socket -> ../systemd-initctl.socket
lrwxrwxrwx 1 root root 32 Apr 20  2018 systemd-journald-audit.socket -> ../systemd-journald-audit.socket
lrwxrwxrwx 1 root root 34 Apr 20  2018 systemd-journald-dev-log.socket -> ../systemd-journald-dev-log.socket
lrwxrwxrwx 1 root root 26 Apr 20  2018 systemd-journald.socket -> ../systemd-journald.socket
lrwxrwxrwx 1 root root 31 Apr 20  2018 systemd-udevd-control.socket -> ../systemd-udevd-control.socket
lrwxrwxrwx 1 root root 30 Apr 20  2018 systemd-udevd-kernel.socket -> ../systemd-udevd-kernel.socket
lrwxrwxrwx 1 root root 14 Nov 15  2017 dbus.socket -> ../dbus.socket

/lib/systemd/system/getty.target.wants:
total 0
lrwxrwxrwx 1 root root 23 Apr 20  2018 getty-static.service -> ../getty-static.service

/lib/systemd/system/graphical.target.wants:
total 0
lrwxrwxrwx 1 root root 39 Apr 20  2018 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service

/lib/systemd/system/local-fs.target.wants:
total 0
lrwxrwxrwx 1 root root 29 Apr 20  2018 systemd-remount-fs.service -> ../systemd-remount-fs.service

/lib/systemd/system/rc-local.service.d:
total 4.0K
-rw-r--r-- 1 root root 290 Apr 20  2018 debian.conf

/lib/systemd/system/rescue.target.wants:
total 0
lrwxrwxrwx 1 root root 39 Apr 20  2018 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service

/lib/systemd/system/timers.target.wants:
total 0
lrwxrwxrwx 1 root root 31 Apr 20  2018 systemd-tmpfiles-clean.timer -> ../systemd-tmpfiles-clean.timer

/lib/systemd/system/user@.service.d:
total 4.0K
-rw-r--r-- 1 root root 125 Apr 20  2018 timeout.conf

/lib/systemd/system/runlevel1.target.wants:
total 0

/lib/systemd/system/runlevel2.target.wants:
total 0

/lib/systemd/system/runlevel3.target.wants:
total 0

/lib/systemd/system/runlevel4.target.wants:
total 0

/lib/systemd/system/runlevel5.target.wants:
total 0

/lib/systemd/system-generators:
total 232K
-rwxr-xr-x 1 root root  19K May 11  2018 snapd-generator
-rwxr-xr-x 1 root root 4.8K Apr 26  2018 cloud-init-generator
lrwxrwxrwx 1 root root   22 Apr 23  2018 netplan -> ../../netplan/generate
-rwxr-xr-x 1 root root  23K Apr 20  2018 systemd-cryptsetup-generator
-rwxr-xr-x 1 root root  10K Apr 20  2018 systemd-debug-generator
-rwxr-xr-x 1 root root  31K Apr 20  2018 systemd-fstab-generator
-rwxr-xr-x 1 root root  14K Apr 20  2018 systemd-getty-generator
-rwxr-xr-x 1 root root  22K Apr 20  2018 systemd-gpt-auto-generator
-rwxr-xr-x 1 root root  10K Apr 20  2018 systemd-hibernate-resume-generator
-rwxr-xr-x 1 root root  10K Apr 20  2018 systemd-rc-local-generator
-rwxr-xr-x 1 root root  10K Apr 20  2018 systemd-system-update-generator
-rwxr-xr-x 1 root root  31K Apr 20  2018 systemd-sysv-generator
-rwxr-xr-x 1 root root  14K Apr 20  2018 systemd-veritysetup-generator
-rwxr-xr-x 1 root root  11K Apr 12  2018 lvm2-activation-generator

/lib/systemd/system-sleep:
total 8.0K
-rwxr-xr-x 1 root root 219 Apr 17  2018 unattended-upgrades
-rwxr-xr-x 1 root root  92 Feb 22  2018 hdparm

/lib/systemd/system-shutdown:
total 4.0K
-rwxr-xr-x 1 root root 160 Mar  7  2018 mdadm.shutdown

/lib/systemd/network:
total 16K
-rw-r--r-- 1 root root 645 Jan 28  2018 80-container-host0.network
-rw-r--r-- 1 root root 718 Jan 28  2018 80-container-ve.network
-rw-r--r-- 1 root root 704 Jan 28  2018 80-container-vz.network
-rw-r--r-- 1 root root 412 Jan 28  2018 99-default.link

/lib/systemd/system-preset:
total 4.0K
-rw-r--r-- 1 root root 951 Jan 28  2018 90-systemd.preset


[00;33m### SOFTWARE #############################################[00m
[00;31m[-] Sudo version:[00m
Sudo version 1.8.21p2


[00;31m[-] MYSQL version:[00m
mysql  Ver 14.14 Distrib 5.7.22, for Linux (x86_64) using  EditLine wrapper


[00;31m[-] Apache version:[00m
Server version: Apache/2.4.29 (Ubuntu)
Server built:   2018-04-25T11:38:24


[00;31m[-] Apache user configuration:[00m
APACHE_RUN_USER=www-data
APACHE_RUN_GROUP=www-data


[00;31m[-] Installed Apache modules:[00m
Loaded Modules:
 core_module (static)
 so_module (static)
 watchdog_module (static)
 http_module (static)
 log_config_module (static)
 logio_module (static)
 version_module (static)
 unixd_module (static)
 access_compat_module (shared)
 alias_module (shared)
 auth_basic_module (shared)
 authn_core_module (shared)
 authn_file_module (shared)
 authz_core_module (shared)
 authz_host_module (shared)
 authz_user_module (shared)
 autoindex_module (shared)
 deflate_module (shared)
 dir_module (shared)
 env_module (shared)
 filter_module (shared)
 mime_module (shared)
 mpm_prefork_module (shared)
 negotiation_module (shared)
 php7_module (shared)
 reqtimeout_module (shared)
 setenvif_module (shared)
 status_module (shared)


[00;33m### INTERESTING FILES ####################################[00m
[00;31m[-] Useful file locations:[00m
/bin/nc
/bin/netcat
/usr/bin/wget
/usr/bin/curl


[00;31m[-] Can we read/write sensitive files:[00m
-rw-r--r-- 1 root root 1617 May 22  2018 /etc/passwd
-rw-r--r-- 1 root root 777 Sep 25 21:40 /etc/group
-rw-r--r-- 1 root root 581 Apr  9  2018 /etc/profile
-rw-r----- 1 root shadow 1061 May 22  2018 /etc/shadow


[00;31m[-] SUID files:[00m
-rwsr-xr-x 1 root root 40152 Nov 30  2017 /snap/core/4486/bin/mount
-rwsr-xr-x 1 root root 44168 May  7  2014 /snap/core/4486/bin/ping
-rwsr-xr-x 1 root root 44680 May  7  2014 /snap/core/4486/bin/ping6
-rwsr-xr-x 1 root root 40128 May 17  2017 /snap/core/4486/bin/su
-rwsr-xr-x 1 root root 27608 Nov 30  2017 /snap/core/4486/bin/umount
-rwsr-xr-x 1 root root 71824 May 17  2017 /snap/core/4486/usr/bin/chfn
-rwsr-xr-x 1 root root 40432 May 17  2017 /snap/core/4486/usr/bin/chsh
-rwsr-xr-x 1 root root 75304 May 17  2017 /snap/core/4486/usr/bin/gpasswd
-rwsr-xr-x 1 root root 39904 May 17  2017 /snap/core/4486/usr/bin/newgrp
-rwsr-xr-x 1 root root 54256 May 17  2017 /snap/core/4486/usr/bin/passwd
-rwsr-xr-x 1 root root 136808 Jul  4  2017 /snap/core/4486/usr/bin/sudo
-rwsr-xr-- 1 root systemd-resolve 42992 Jan 12  2017 /snap/core/4486/usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-x 1 root root 428240 Jan 18  2018 /snap/core/4486/usr/lib/openssh/ssh-keysign
-rwsr-sr-x 1 root root 94344 Apr 16  2018 /snap/core/4486/usr/lib/snapd/snap-confine
-rwsr-xr-- 1 root dip 390888 Jan 29  2016 /snap/core/4486/usr/sbin/pppd
-rwsr-xr-x 1 root root 436552 Feb 10  2018 /usr/lib/openssh/ssh-keysign
-rwsr-xr-x 1 root root 80056 Apr  2  2018 /usr/lib/x86_64-linux-gnu/lxc/lxc-user-nic
-rwsr-sr-x 1 root root 101208 May 11  2018 /usr/lib/snapd/snap-confine
-rwsr-xr-x 1 root root 10232 Mar 28  2017 /usr/lib/eject/dmcrypt-get-device
-rwsr-xr-- 1 root messagebus 42992 Nov 15  2017 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-x 1 root root 14328 Mar 27  2018 /usr/lib/policykit-1/polkit-agent-helper-1
-rwsr-xr-x 1 root root 37136 Jan 25  2018 /usr/bin/newgidmap
-rwsr-xr-x 1 root root 44528 Jan 25  2018 /usr/bin/chsh
-rwsr-xr-x 1 root root 22520 Mar 27  2018 /usr/bin/pkexec
-rwsr-xr-x 1 root root 76496 Jan 25  2018 /usr/bin/chfn
-rwsr-xr-x 1 root root 37136 Jan 25  2018 /usr/bin/newuidmap
-rwsr-xr-x 1 root root 40344 Jan 25  2018 /usr/bin/newgrp
-rwsr-xr-x 1 root root 75824 Jan 25  2018 /usr/bin/gpasswd
-rwsr-sr-x 1 daemon daemon 51464 Feb 20  2018 /usr/bin/at
-rwsr-xr-x 1 root root 59640 Jan 25  2018 /usr/bin/passwd
-rwsr-xr-x 1 root root 149080 Jan 18  2018 /usr/bin/sudo
-rwsr-xr-x 1 root root 18448 Mar  9  2017 /usr/bin/traceroute6.iputils
-rwsr-xr-x 1 root root 26696 Mar 15  2018 /bin/umount
-rwsr-xr-x 1 root root 30800 Aug 11  2016 /bin/fusermount
-rwsr-xr-x 1 root root 44664 Jan 25  2018 /bin/su
-rwsr-xr-x 1 root root 146128 Nov 30  2017 /bin/ntfs-3g
-rwsr-xr-x 1 root root 64424 Mar  9  2017 /bin/ping
-rwsr-xr-x 1 root root 43088 Mar 15  2018 /bin/mount


[00;31m[-] SGID files:[00m
-rwxr-sr-x 1 root shadow 35632 Apr  9  2018 /snap/core/4486/sbin/pam_extrausers_chkpwd
-rwxr-sr-x 1 root shadow 35600 Apr  9  2018 /snap/core/4486/sbin/unix_chkpwd
-rwxr-sr-x 1 root shadow 62336 May 17  2017 /snap/core/4486/usr/bin/chage
-rwxr-sr-x 1 root systemd-network 36080 Apr  5  2016 /snap/core/4486/usr/bin/crontab
-rwxr-sr-x 1 root mail 14856 Dec  7  2013 /snap/core/4486/usr/bin/dotlockfile
-rwxr-sr-x 1 root shadow 22768 May 17  2017 /snap/core/4486/usr/bin/expiry
-rwxr-sr-x 3 root mail 14592 Dec  3  2012 /snap/core/4486/usr/bin/mail-lock
-rwxr-sr-x 3 root mail 14592 Dec  3  2012 /snap/core/4486/usr/bin/mail-touchlock
-rwxr-sr-x 3 root mail 14592 Dec  3  2012 /snap/core/4486/usr/bin/mail-unlock
-rwxr-sr-x 1 root crontab 358624 Jan 18  2018 /snap/core/4486/usr/bin/ssh-agent
-rwxr-sr-x 1 root tty 27368 Nov 30  2017 /snap/core/4486/usr/bin/wall
-rwsr-sr-x 1 root root 94344 Apr 16  2018 /snap/core/4486/usr/lib/snapd/snap-confine
-rwxr-sr-x 1 root utmp 10232 Mar 11  2016 /usr/lib/x86_64-linux-gnu/utempter/utempter
-rwsr-sr-x 1 root root 101208 May 11  2018 /usr/lib/snapd/snap-confine
-rwxr-sr-x 1 root tty 30800 Mar 15  2018 /usr/bin/wall
-rwxr-sr-x 1 root crontab 39352 Nov 16  2017 /usr/bin/crontab
-rwxr-sr-x 1 root tty 14328 Jan 17  2018 /usr/bin/bsd-write
-rwxr-sr-x 1 root ssh 362640 Feb 10  2018 /usr/bin/ssh-agent
-rwsr-sr-x 1 daemon daemon 51464 Feb 20  2018 /usr/bin/at
-rwxr-sr-x 1 root shadow 22808 Jan 25  2018 /usr/bin/expiry
-rwxr-sr-x 1 root shadow 71816 Jan 25  2018 /usr/bin/chage
-rwxr-sr-x 1 root mlocate 43088 Mar  1  2018 /usr/bin/mlocate
-rwxr-sr-x 1 root shadow 34816 Apr  5  2018 /sbin/unix_chkpwd
-rwxr-sr-x 1 root shadow 34816 Apr  5  2018 /sbin/pam_extrausers_chkpwd


[00;31m[+] Files with POSIX capabilities set:[00m
/usr/bin/mtr-packet = cap_net_raw+ep


[-] Can't search *.conf files as no keyword was entered

[-] Can't search *.php files as no keyword was entered

[-] Can't search *.log files as no keyword was entered

[-] Can't search *.ini files as no keyword was entered

[00;31m[-] All *.conf files in /etc (recursive 1 level):[00m
-rw-r--r-- 1 root root 1358 Jan 30  2018 /etc/rsyslog.conf
-rw-r--r-- 1 root root 2969 Feb 28  2018 /etc/debconf.conf
-rw-r--r-- 1 root root 513 Apr 26  2018 /etc/nsswitch.conf
-rw-r--r-- 1 root root 92 Apr  9  2018 /etc/host.conf
-rw-r--r-- 1 root root 34 Jan 27  2016 /etc/ld.so.conf
-rw-r--r-- 1 root root 812 Mar 24  2018 /etc/mke2fs.conf
-rw-r--r-- 1 root root 604 Aug 13  2017 /etc/deluser.conf
-rw-r--r-- 1 root root 110 Apr 26  2018 /etc/kernel-img.conf
-rw-r--r-- 1 root root 2683 Jan 17  2018 /etc/sysctl.conf
-rw-r--r-- 1 root root 5898 Apr 26  2018 /etc/ca-certificates.conf
-rw-r--r-- 1 root root 100 Apr 11  2017 /etc/sos.conf
-rw-r--r-- 1 root root 191 Feb  7  2018 /etc/libaudit.conf
-rw-r--r-- 1 root root 403 Mar  1  2018 /etc/updatedb.conf
-rw-r--r-- 1 root root 14867 Oct 13  2016 /etc/ltrace.conf
-rw-r--r-- 1 root root 6920 Oct 27  2017 /etc/overlayroot.conf
-rw-r--r-- 1 root root 552 Apr  4  2018 /etc/pam.conf
-rw-r--r-- 1 root root 1260 Feb 26  2018 /etc/ucf.conf
-rw-r--r-- 1 root root 703 Aug 21  2017 /etc/logrotate.conf
-rw-r--r-- 1 root root 2584 Feb  1  2018 /etc/gai.conf
-rw-r--r-- 1 root root 280 Jun 20  2014 /etc/fuse.conf
-rw-r--r-- 1 root root 3028 Apr 26  2018 /etc/adduser.conf
-rw-r--r-- 1 root root 350 Apr 26  2018 /etc/popularity-contest.conf
-rw-r--r-- 1 root root 4861 Feb 22  2018 /etc/hdparm.conf


[00;31m[-] Current user's history files:[00m
lrwxrwxrwx 1 root root 9 May 22  2018 /home/floris/.bash_history -> /dev/null


[00;31m[-] Location and contents (if accessible) of .bash_history file(s):[00m
/home/floris/.bash_history


[00;31m[-] Any interesting mail in /var/mail:[00m
total 8
drwxrwsr-x  2 root mail 4096 Apr 26  2018 .
drwxr-xr-x 14 root root 4096 May 22  2018 ..


[00;33m### SCAN COMPLETE ####################################[00m
