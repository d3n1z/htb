[*] Checking for active internet connection [OK]
                ____               
    _________  /  _/___  ___  _____
   / ___/ __ \ / // __ \/ _ \/ ___/
  (__  ) / / // // /_/ /  __/ /    
 /____/_/ /_/___/ .___/\___/_/     
               /_/                 

 + -- --=[https://xerosecurity.com
 + -- --=[Sn1per v6.1 by @xer0dayz

====================================================================================
 GATHERING DNS INFO 
====================================================================================
dnsenum VERSION:1.2.4

-----   10.10.10.123   -----


Host's addresses:
__________________



Name Servers:
______________

====================================================================================
 CHECKING FOR SUBDOMAIN HIJACKING 
====================================================================================

====================================================================================
 PINGING HOST 
====================================================================================
PING 10.10.10.123 (10.10.10.123) 56(84) bytes of data.
64 bytes from 10.10.10.123: icmp_seq=1 ttl=63 time=64.1 ms

--- 10.10.10.123 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 64.122/64.122/64.122/0.000 ms

====================================================================================
 RUNNING TCP PORT SCAN 
====================================================================================
Starting Nmap 7.70 ( https://nmap.org ) at 2019-02-13 23:08 +03

root@n00b:/media/sf_VM_SHARED/htb/10.10.10.123# clear

root@n00b:/media/sf_VM_SHARED/htb/10.10.10.123# sniper -t 10.10.10.123 
[*] Loaded configuration file from ~/.sniper.conf [OK]
[*] Saving loot to /usr/share/sniper/loot/ [OK]
[*] Checking for active internet connection [OK]
[*] Loaded configuration file from ~/.sniper.conf [OK]
[*] Saving loot to /usr/share/sniper/loot/ [OK]
[*] Checking for active internet connection [OK]
                ____               
    _________  /  _/___  ___  _____
   / ___/ __ \ / // __ \/ _ \/ ___/
  (__  ) / / // // /_/ /  __/ /    
 /____/_/ /_/___/ .___/\___/_/     
               /_/                 

 + -- --=[https://xerosecurity.com
 + -- --=[Sn1per v6.1 by @xer0dayz

====================================================================================
 GATHERING DNS INFO 
====================================================================================
dnsenum VERSION:1.2.4

-----   10.10.10.123   -----


Host's addresses:
__________________



Name Servers:
______________

====================================================================================
 CHECKING FOR SUBDOMAIN HIJACKING 
====================================================================================

====================================================================================
 PINGING HOST 
====================================================================================
PING 10.10.10.123 (10.10.10.123) 56(84) bytes of data.
64 bytes from 10.10.10.123: icmp_seq=1 ttl=63 time=51.1 ms

--- 10.10.10.123 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 51.128/51.128/51.128/0.000 ms

====================================================================================
 RUNNING TCP PORT SCAN 
====================================================================================
Starting Nmap 7.70 ( https://nmap.org ) at 2019-02-13 23:08 +03
Nmap scan report for friendzoneportal.red (10.10.10.123)
Host is up (0.058s latency).
Not shown: 321 closed ports, 148 filtered ports
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT    STATE SERVICE
21/tcp  open  ftp
22/tcp  open  ssh
53/tcp  open  domain
80/tcp  open  http
139/tcp open  netbios-ssn
443/tcp open  https
445/tcp open  microsoft-ds

Nmap done: 1 IP address (1 host up) scanned in 1.83 seconds
====================================================================================
 RUNNING UDP PORT SCAN 
====================================================================================
Starting Nmap 7.70 ( https://nmap.org ) at 2019-02-13 23:08 +03
Nmap scan report for friendzoneportal.red (10.10.10.123)
Host is up (0.050s latency).
Not shown: 11 closed ports
PORT    STATE         SERVICE
53/udp  open          domain
137/udp open          netbios-ns
138/udp open|filtered netbios-dgm

Nmap done: 1 IP address (1 host up) scanned in 5.81 seconds

====================================================================================
 RUNNING INTRUSIVE SCANS 
====================================================================================
 + -- --=[Port 21 opened... running tests...
====================================================================================
 RUNNING NMAP SCRIPTS 
====================================================================================
Starting Nmap 7.70 ( https://nmap.org ) at 2019-02-13 23:08 +03
Nmap scan report for friendzoneportal.red (10.10.10.123)
Host is up (0.071s latency).

PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-brute: 
|   Accounts: No valid accounts found
|_  Statistics: Performed 1040 guesses in 183 seconds, average tps: 5.4
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: Linux 3.2 - 4.9 (95%), Linux 3.16 (95%), Linux 3.18 (95%), ASUS RT-N56U WAP (Linux 3.4) (95%), Linux 3.1 (93%), Linux 3.2 (93%), Linux 3.10 - 4.11 (93%), Oracle VM Server 3.4.2 (Linux 4.1) (93%), Linux 3.12 (93%), Linux 3.13 (93%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
Service Info: OS: Unix

TRACEROUTE (using port 21/tcp)
HOP RTT      ADDRESS
1   57.30 ms 10.10.12.1
2   57.54 ms friendzoneportal.red (10.10.10.123)

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 190.40 seconds
====================================================================================
 RUNNING METASPLOIT FTP VERSION SCANNER 
====================================================================================
[*] Starting persistent handler(s)...
RHOST => 10.10.10.123
RHOSTS => 10.10.10.123
[+] 10.10.10.123:21       - FTP Banner: '220 (vsFTPd 3.0.3)\x0d\x0a'
[*] 10.10.10.123:21       - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
====================================================================================
 RUNNING METASPLOIT ANONYMOUS FTP SCANNER 
====================================================================================
[*] Starting persistent handler(s)...
RHOST => 10.10.10.123
RHOSTS => 10.10.10.123
[*] 10.10.10.123:21       - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
====================================================================================
 RUNNING VSFTPD 2.3.4 BACKDOOR EXPLOIT 
====================================================================================
[*] Starting persistent handler(s)...
RHOST => 10.10.10.123
RHOSTS => 10.10.10.123
[*] 10.10.10.123:21 - Banner: 220 (vsFTPd 3.0.3)
[*] 10.10.10.123:21 - USER: 331 Please specify the password.
[*] Exploit completed, but no session was created.
====================================================================================
 RUNNING PROFTPD 1.3.3C BACKDOOR EXPLOIT 
====================================================================================
[*] Starting persistent handler(s)...
RHOST => 10.10.10.123
RHOSTS => 10.10.10.123
[*] Started reverse TCP double handler on 10.10.14.32:4444 
[*] 10.10.10.123:21 - Sending Backdoor Command
[*] Exploit completed, but no session was created.
 + -- --=[Port 22 opened... running tests...
====================================================================================
 RUNNING SSH AUDIT 
====================================================================================
# general
(gen) banner: SSH-2.0-OpenSSH_7.6p1 Ubuntu-4
(gen) software: OpenSSH 7.6p1
(gen) compatibility: OpenSSH 7.3+, Dropbear SSH 2016.73+
(gen) compression: enabled (zlib@openssh.com)

# key exchange algorithms
(kex) curve25519-sha256                     -- [warn] unknown algorithm
(kex) curve25519-sha256@libssh.org          -- [info] available since OpenSSH 6.5, Dropbear SSH 2013.62
(kex) ecdh-sha2-nistp256                    -- [fail] using weak elliptic curves
                                            `- [info] available since OpenSSH 5.7, Dropbear SSH 2013.62
(kex) ecdh-sha2-nistp384                    -- [fail] using weak elliptic curves
                                            `- [info] available since OpenSSH 5.7, Dropbear SSH 2013.62
(kex) ecdh-sha2-nistp521                    -- [fail] using weak elliptic curves
                                            `- [info] available since OpenSSH 5.7, Dropbear SSH 2013.62
(kex) diffie-hellman-group-exchange-sha256  -- [warn] using custom size modulus (possibly weak)
                                            `- [info] available since OpenSSH 4.4
(kex) diffie-hellman-group16-sha512         -- [info] available since OpenSSH 7.3, Dropbear SSH 2016.73
(kex) diffie-hellman-group18-sha512         -- [info] available since OpenSSH 7.3
(kex) diffie-hellman-group14-sha256         -- [info] available since OpenSSH 7.3, Dropbear SSH 2016.73
(kex) diffie-hellman-group14-sha1           -- [warn] using weak hashing algorithm
                                            `- [info] available since OpenSSH 3.9, Dropbear SSH 0.53

# host-key algorithms
(key) ssh-rsa                               -- [info] available since OpenSSH 2.5.0, Dropbear SSH 0.28
(key) rsa-sha2-512                          -- [info] available since OpenSSH 7.2
(key) rsa-sha2-256                          -- [info] available since OpenSSH 7.2
(key) ecdsa-sha2-nistp256                   -- [fail] using weak elliptic curves
                                            `- [warn] using weak random number generator could reveal the key
                                            `- [info] available since OpenSSH 5.7, Dropbear SSH 2013.62
(key) ssh-ed25519                           -- [info] available since OpenSSH 6.5

# encryption algorithms (ciphers)
(enc) chacha20-poly1305@openssh.com         -- [info] available since OpenSSH 6.5
                                            `- [info] default cipher since OpenSSH 6.9.
(enc) aes128-ctr                            -- [info] available since OpenSSH 3.7, Dropbear SSH 0.52
(enc) aes192-ctr                            -- [info] available since OpenSSH 3.7
(enc) aes256-ctr                            -- [info] available since OpenSSH 3.7, Dropbear SSH 0.52
(enc) aes128-gcm@openssh.com                -- [info] available since OpenSSH 6.2
(enc) aes256-gcm@openssh.com                -- [info] available since OpenSSH 6.2

# message authentication code algorithms
(mac) umac-64-etm@openssh.com               -- [warn] using small 64-bit tag size
                                            `- [info] available since OpenSSH 6.2
(mac) umac-128-etm@openssh.com              -- [info] available since OpenSSH 6.2
(mac) hmac-sha2-256-etm@openssh.com         -- [info] available since OpenSSH 6.2
(mac) hmac-sha2-512-etm@openssh.com         -- [info] available since OpenSSH 6.2
(mac) hmac-sha1-etm@openssh.com             -- [warn] using weak hashing algorithm
                                            `- [info] available since OpenSSH 6.2
(mac) umac-64@openssh.com                   -- [warn] using encrypt-and-MAC mode
                                            `- [warn] using small 64-bit tag size
                                            `- [info] available since OpenSSH 4.7
(mac) umac-128@openssh.com                  -- [warn] using encrypt-and-MAC mode
                                            `- [info] available since OpenSSH 6.2
(mac) hmac-sha2-256                         -- [warn] using encrypt-and-MAC mode
                                            `- [info] available since OpenSSH 5.9, Dropbear SSH 2013.56
(mac) hmac-sha2-512                         -- [warn] using encrypt-and-MAC mode
                                            `- [info] available since OpenSSH 5.9, Dropbear SSH 2013.56
(mac) hmac-sha1                             -- [warn] using encrypt-and-MAC mode
                                            `- [warn] using weak hashing algorithm
                                            `- [info] available since OpenSSH 2.1.0, Dropbear SSH 0.28

# algorithm recommendations (for OpenSSH 7.6)
(rec) -ecdh-sha2-nistp521                   -- kex algorithm to remove 
(rec) -ecdh-sha2-nistp384                   -- kex algorithm to remove 
(rec) -diffie-hellman-group14-sha1          -- kex algorithm to remove 
(rec) -ecdh-sha2-nistp256                   -- kex algorithm to remove 
(rec) -diffie-hellman-group-exchange-sha256 -- kex algorithm to remove 
(rec) -ecdsa-sha2-nistp256                  -- key algorithm to remove 
(rec) -hmac-sha2-512                        -- mac algorithm to remove 
(rec) -umac-128@openssh.com                 -- mac algorithm to remove 
(rec) -hmac-sha2-256                        -- mac algorithm to remove 
(rec) -umac-64@openssh.com                  -- mac algorithm to remove 
(rec) -hmac-sha1                            -- mac algorithm to remove 
(rec) -hmac-sha1-etm@openssh.com            -- mac algorithm to remove 
(rec) -umac-64-etm@openssh.com              -- mac algorithm to remove 

====================================================================================
 RUNNING NMAP SCRIPTS 
====================================================================================
Starting Nmap 7.70 ( https://nmap.org ) at 2019-02-13 23:12 +03
NSE: [ssh-run] Failed to specify credentials and command to run.
NSE: [ssh-brute] Trying username/password pair: root:root
NSE: [ssh-brute] Trying username/password pair: admin:admin
NSE: [ssh-brute] Trying username/password pair: administrator:administrator
NSE: [ssh-brute] Trying username/password pair: webadmin:webadmin
NSE: [ssh-brute] Trying username/password pair: sysadmin:sysadmin
NSE: [ssh-brute] Trying username/password pair: netadmin:netadmin
NSE: [ssh-brute] Trying username/password pair: guest:guest
NSE: [ssh-brute] Trying username/password pair: user:user
NSE: [ssh-brute] Trying username/password pair: web:web
NSE: [ssh-brute] Trying username/password pair: test:test
NSE: [ssh-brute] Trying username/password pair: root:
NSE: [ssh-brute] Trying username/password pair: admin:
NSE: [ssh-brute] Trying username/password pair: administrator:
NSE: [ssh-brute] Trying username/password pair: webadmin:
NSE: [ssh-brute] Trying username/password pair: sysadmin:
NSE: [ssh-brute] Trying username/password pair: netadmin:
NSE: [ssh-brute] Trying username/password pair: guest:
NSE: [ssh-brute] Trying username/password pair: user:
NSE: [ssh-brute] Trying username/password pair: web:
NSE: [ssh-brute] Trying username/password pair: test:
NSE: [ssh-brute] Trying username/password pair: root:123456
NSE: [ssh-brute] Trying username/password pair: admin:123456
NSE: [ssh-brute] Trying username/password pair: administrator:123456
NSE: [ssh-brute] Trying username/password pair: webadmin:123456
NSE: [ssh-brute] Trying username/password pair: sysadmin:123456
NSE: [ssh-brute] Trying username/password pair: netadmin:123456
NSE: [ssh-brute] Trying username/password pair: guest:123456
NSE: [ssh-brute] Trying username/password pair: user:123456
NSE: [ssh-brute] Trying username/password pair: web:123456
NSE: [ssh-brute] Trying username/password pair: test:123456
NSE: [ssh-brute] Trying username/password pair: root:12345
NSE: [ssh-brute] Trying username/password pair: admin:12345
NSE: [ssh-brute] Trying username/password pair: administrator:12345
NSE: [ssh-brute] Trying username/password pair: webadmin:12345
NSE: [ssh-brute] Trying username/password pair: sysadmin:12345
NSE: [ssh-brute] Trying username/password pair: netadmin:12345
NSE: [ssh-brute] Trying username/password pair: guest:12345
NSE: [ssh-brute] Trying username/password pair: user:12345
NSE: [ssh-brute] Trying username/password pair: web:12345
NSE: [ssh-brute] Trying username/password pair: test:12345
NSE: [ssh-brute] Trying username/password pair: root:123456789
NSE: [ssh-brute] Trying username/password pair: admin:123456789
NSE: [ssh-brute] Trying username/password pair: administrator:123456789
NSE: [ssh-brute] Trying username/password pair: webadmin:123456789
NSE: [ssh-brute] Trying username/password pair: sysadmin:123456789
NSE: [ssh-brute] Trying username/password pair: netadmin:123456789
NSE: [ssh-brute] Trying username/password pair: guest:123456789
NSE: [ssh-brute] Trying username/password pair: user:123456789
NSE: [ssh-brute] Trying username/password pair: web:123456789
NSE: [ssh-brute] Trying username/password pair: test:123456789
NSE: [ssh-brute] Trying username/password pair: root:password
NSE: [ssh-brute] Trying username/password pair: admin:password
NSE: [ssh-brute] Trying username/password pair: administrator:password
NSE: [ssh-brute] Trying username/password pair: webadmin:password
NSE: [ssh-brute] Trying username/password pair: sysadmin:password
NSE: [ssh-brute] Trying username/password pair: netadmin:password
NSE: [ssh-brute] Trying username/password pair: guest:password
NSE: [ssh-brute] Trying username/password pair: user:password
NSE: [ssh-brute] Trying username/password pair: web:password
NSE: [ssh-brute] Trying username/password pair: test:password
NSE: [ssh-brute] Trying username/password pair: root:iloveyou
NSE: [ssh-brute] Trying username/password pair: admin:iloveyou
NSE: [ssh-brute] Trying username/password pair: administrator:iloveyou
NSE: [ssh-brute] Trying username/password pair: webadmin:iloveyou
NSE: [ssh-brute] Trying username/password pair: sysadmin:iloveyou
NSE: [ssh-brute] Trying username/password pair: netadmin:iloveyou
NSE: [ssh-brute] Trying username/password pair: guest:iloveyou
NSE: [ssh-brute] Trying username/password pair: user:iloveyou
NSE: [ssh-brute] Trying username/password pair: web:iloveyou
NSE: [ssh-brute] Trying username/password pair: test:iloveyou
NSE: [ssh-brute] Trying username/password pair: root:princess
NSE: [ssh-brute] Trying username/password pair: admin:princess
NSE: [ssh-brute] Trying username/password pair: administrator:princess
NSE: [ssh-brute] Trying username/password pair: webadmin:princess
NSE: [ssh-brute] Trying username/password pair: sysadmin:princess
NSE: [ssh-brute] Trying username/password pair: netadmin:princess
NSE: [ssh-brute] Trying username/password pair: guest:princess
NSE: [ssh-brute] Trying username/password pair: user:princess
NSE: [ssh-brute] Trying username/password pair: web:princess
NSE: [ssh-brute] Trying username/password pair: test:princess
NSE: [ssh-brute] Trying username/password pair: root:12345678
NSE: [ssh-brute] Trying username/password pair: admin:12345678
NSE: [ssh-brute] Trying username/password pair: administrator:12345678
NSE: [ssh-brute] Trying username/password pair: webadmin:12345678
NSE: [ssh-brute] Trying username/password pair: sysadmin:12345678
NSE: [ssh-brute] Trying username/password pair: netadmin:12345678
NSE: [ssh-brute] Trying username/password pair: guest:12345678
NSE: [ssh-brute] Trying username/password pair: user:12345678
NSE: [ssh-brute] Trying username/password pair: web:12345678
NSE: [ssh-brute] Trying username/password pair: test:12345678
NSE: [ssh-brute] Trying username/password pair: root:1234567
NSE: [ssh-brute] Trying username/password pair: admin:1234567
NSE: [ssh-brute] Trying username/password pair: administrator:1234567
NSE: [ssh-brute] Trying username/password pair: webadmin:1234567
NSE: [ssh-brute] Trying username/password pair: sysadmin:1234567
NSE: [ssh-brute] Trying username/password pair: netadmin:1234567
NSE: [ssh-brute] Trying username/password pair: guest:1234567
NSE: [ssh-brute] Trying username/password pair: user:1234567
NSE: [ssh-brute] Trying username/password pair: web:1234567
NSE: [ssh-brute] Trying username/password pair: test:1234567
NSE: [ssh-brute] Trying username/password pair: root:abc123
NSE: [ssh-brute] Trying username/password pair: admin:abc123
NSE: [ssh-brute] Trying username/password pair: administrator:abc123
NSE: [ssh-brute] Trying username/password pair: webadmin:abc123
NSE: [ssh-brute] Trying username/password pair: sysadmin:abc123
NSE: [ssh-brute] Trying username/password pair: netadmin:abc123
NSE: [ssh-brute] Trying username/password pair: guest:abc123
NSE: [ssh-brute] Trying username/password pair: user:abc123
NSE: [ssh-brute] Trying username/password pair: web:abc123
NSE: [ssh-brute] Trying username/password pair: test:abc123
NSE: [ssh-brute] Trying username/password pair: root:nicole
NSE: [ssh-brute] Trying username/password pair: admin:nicole
NSE: [ssh-brute] Trying username/password pair: administrator:nicole
NSE: [ssh-brute] Trying username/password pair: webadmin:nicole
NSE: [ssh-brute] Trying username/password pair: sysadmin:nicole
NSE: [ssh-brute] Trying username/password pair: netadmin:nicole
NSE: [ssh-brute] Trying username/password pair: guest:nicole
NSE: [ssh-brute] Trying username/password pair: user:nicole
NSE: [ssh-brute] Trying username/password pair: web:nicole
NSE: [ssh-brute] Trying username/password pair: test:nicole
NSE: [ssh-brute] Trying username/password pair: root:daniel
NSE: [ssh-brute] Trying username/password pair: admin:daniel
NSE: [ssh-brute] Trying username/password pair: administrator:daniel
NSE: [ssh-brute] Trying username/password pair: webadmin:daniel
NSE: [ssh-brute] Trying username/password pair: sysadmin:daniel
NSE: [ssh-brute] Trying username/password pair: netadmin:daniel
NSE: [ssh-brute] Trying username/password pair: guest:daniel
NSE: [ssh-brute] Trying username/password pair: user:daniel
NSE: [ssh-brute] Trying username/password pair: web:daniel
NSE: [ssh-brute] Trying username/password pair: test:daniel
NSE: [ssh-brute] Trying username/password pair: root:monkey
NSE: [ssh-brute] Trying username/password pair: admin:monkey
NSE: [ssh-brute] Trying username/password pair: administrator:monkey
NSE: [ssh-brute] Trying username/password pair: webadmin:monkey
NSE: [ssh-brute] Trying username/password pair: sysadmin:monkey
NSE: [ssh-brute] Trying username/password pair: netadmin:monkey
NSE: [ssh-brute] Trying username/password pair: guest:monkey
NSE: [ssh-brute] Trying username/password pair: user:monkey
NSE: [ssh-brute] Trying username/password pair: web:monkey
NSE: [ssh-brute] Trying username/password pair: test:monkey
NSE: [ssh-brute] Trying username/password pair: root:babygirl
NSE: [ssh-brute] Trying username/password pair: admin:babygirl
NSE: [ssh-brute] Trying username/password pair: administrator:babygirl
NSE: [ssh-brute] Trying username/password pair: webadmin:babygirl
NSE: [ssh-brute] Trying username/password pair: sysadmin:babygirl
NSE: [ssh-brute] Trying username/password pair: netadmin:babygirl
NSE: [ssh-brute] Trying username/password pair: guest:babygirl
NSE: [ssh-brute] Trying username/password pair: user:babygirl
NSE: [ssh-brute] Trying username/password pair: web:babygirl
NSE: [ssh-brute] Trying username/password pair: test:babygirl
NSE: [ssh-brute] Trying username/password pair: root:qwerty
NSE: [ssh-brute] Trying username/password pair: admin:qwerty
NSE: [ssh-brute] Trying username/password pair: administrator:qwerty
NSE: [ssh-brute] Trying username/password pair: webadmin:qwerty
NSE: [ssh-brute] Trying username/password pair: sysadmin:qwerty
NSE: [ssh-brute] Trying username/password pair: netadmin:qwerty
NSE: [ssh-brute] Trying username/password pair: guest:qwerty
NSE: [ssh-brute] Trying username/password pair: user:qwerty
NSE: [ssh-brute] Trying username/password pair: web:qwerty
NSE: [ssh-brute] Trying username/password pair: test:qwerty
NSE: [ssh-brute] Trying username/password pair: root:lovely
NSE: [ssh-brute] Trying username/password pair: admin:lovely
NSE: [ssh-brute] Trying username/password pair: administrator:lovely
NSE: [ssh-brute] Trying username/password pair: webadmin:lovely
NSE: [ssh-brute] Trying username/password pair: sysadmin:lovely
NSE: [ssh-brute] Trying username/password pair: netadmin:lovely
NSE: [ssh-brute] Trying username/password pair: guest:lovely
NSE: [ssh-brute] Trying username/password pair: user:lovely
NSE: [ssh-brute] Trying username/password pair: web:lovely
NSE: [ssh-brute] Trying username/password pair: test:lovely
NSE: [ssh-brute] Trying username/password pair: root:654321
NSE: [ssh-brute] Trying username/password pair: admin:654321
NSE: [ssh-brute] Trying username/password pair: administrator:654321
NSE: [ssh-brute] Trying username/password pair: webadmin:654321
NSE: [ssh-brute] Trying username/password pair: sysadmin:654321
NSE: [ssh-brute] Trying username/password pair: netadmin:654321
NSE: [ssh-brute] Trying username/password pair: guest:654321
NSE: [ssh-brute] Trying username/password pair: user:654321
NSE: [ssh-brute] Trying username/password pair: web:654321
NSE: [ssh-brute] Trying username/password pair: test:654321
NSE: [ssh-brute] Trying username/password pair: root:michael
NSE: [ssh-brute] Trying username/password pair: admin:michael
NSE: [ssh-brute] Trying username/password pair: administrator:michael
NSE: [ssh-brute] Trying username/password pair: webadmin:michael
NSE: [ssh-brute] Trying username/password pair: sysadmin:michael
NSE: [ssh-brute] Trying username/password pair: netadmin:michael
NSE: [ssh-brute] Trying username/password pair: guest:michael
NSE: [ssh-brute] Trying username/password pair: user:michael
NSE: [ssh-brute] Trying username/password pair: web:michael
NSE: [ssh-brute] Trying username/password pair: test:michael
NSE: [ssh-brute] Trying username/password pair: root:jessica
NSE: [ssh-brute] Trying username/password pair: admin:jessica
NSE: [ssh-brute] Trying username/password pair: administrator:jessica
NSE: [ssh-brute] Trying username/password pair: webadmin:jessica
NSE: [ssh-brute] Trying username/password pair: sysadmin:jessica
NSE: [ssh-brute] Trying username/password pair: netadmin:jessica
NSE: [ssh-brute] Trying username/password pair: guest:jessica
NSE: [ssh-brute] Trying username/password pair: user:jessica
NSE: [ssh-brute] Trying username/password pair: web:jessica
NSE: [ssh-brute] Trying username/password pair: test:jessica
NSE: [ssh-brute] Trying username/password pair: root:111111
NSE: [ssh-brute] Trying username/password pair: admin:111111
NSE: [ssh-brute] Trying username/password pair: administrator:111111
NSE: [ssh-brute] Trying username/password pair: webadmin:111111
NSE: [ssh-brute] Trying username/password pair: sysadmin:111111
NSE: [ssh-brute] Trying username/password pair: netadmin:111111
NSE: [ssh-brute] Trying username/password pair: guest:111111
NSE: [ssh-brute] Trying username/password pair: user:111111
NSE: [ssh-brute] Trying username/password pair: web:111111
NSE: [ssh-brute] Trying username/password pair: test:111111
NSE: [ssh-brute] Trying username/password pair: root:ashley
NSE: [ssh-brute] Trying username/password pair: admin:ashley
NSE: [ssh-brute] Trying username/password pair: administrator:ashley
NSE: [ssh-brute] Trying username/password pair: webadmin:ashley
NSE: [ssh-brute] Trying username/password pair: sysadmin:ashley
NSE: [ssh-brute] Trying username/password pair: netadmin:ashley
NSE: [ssh-brute] Trying username/password pair: guest:ashley
NSE: [ssh-brute] Trying username/password pair: user:ashley
NSE: [ssh-brute] Trying username/password pair: web:ashley
NSE: [ssh-brute] Trying username/password pair: test:ashley
NSE: [ssh-brute] Trying username/password pair: root:000000
NSE: [ssh-brute] Trying username/password pair: admin:000000
NSE: [ssh-brute] Trying username/password pair: administrator:000000
NSE: [ssh-brute] Trying username/password pair: webadmin:000000
NSE: [ssh-brute] Trying username/password pair: sysadmin:000000
NSE: [ssh-brute] Trying username/password pair: netadmin:000000
NSE: [ssh-brute] Trying username/password pair: guest:000000
NSE: [ssh-brute] Trying username/password pair: user:000000
NSE: [ssh-brute] Trying username/password pair: web:000000
NSE: [ssh-brute] Trying username/password pair: test:000000
NSE: [ssh-brute] Trying username/password pair: root:iloveu
NSE: [ssh-brute] Trying username/password pair: admin:iloveu
NSE: [ssh-brute] Trying username/password pair: administrator:iloveu
NSE: [ssh-brute] Trying username/password pair: webadmin:iloveu
NSE: [ssh-brute] Trying username/password pair: sysadmin:iloveu
NSE: [ssh-brute] Trying username/password pair: netadmin:iloveu
NSE: [ssh-brute] Trying username/password pair: guest:iloveu
NSE: [ssh-brute] Trying username/password pair: user:iloveu
NSE: [ssh-brute] Trying username/password pair: web:iloveu
NSE: [ssh-brute] Trying username/password pair: test:iloveu
NSE: [ssh-brute] Trying username/password pair: root:michelle
NSE: [ssh-brute] Trying username/password pair: admin:michelle
NSE: [ssh-brute] Trying username/password pair: administrator:michelle
NSE: [ssh-brute] Trying username/password pair: webadmin:michelle
NSE: [ssh-brute] Trying username/password pair: sysadmin:michelle
NSE: [ssh-brute] Trying username/password pair: netadmin:michelle
NSE: [ssh-brute] Trying username/password pair: guest:michelle
NSE: [ssh-brute] Trying username/password pair: user:michelle
NSE: [ssh-brute] Trying username/password pair: web:michelle
NSE: [ssh-brute] Trying username/password pair: test:michelle
NSE: [ssh-brute] Trying username/password pair: root:tigger
NSE: [ssh-brute] Trying username/password pair: admin:tigger
NSE: [ssh-brute] Trying username/password pair: administrator:tigger
NSE: [ssh-brute] Trying username/password pair: webadmin:tigger
Nmap scan report for friendzoneportal.red (10.10.10.123)
Host is up (0.066s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
| ssh-auth-methods: 
|   Supported authentication methods: 
|     publickey
|_    password
| ssh-brute: 
|   Accounts: No valid accounts found
|_  Statistics: Performed 254 guesses in 180 seconds, average tps: 1.5
| ssh-hostkey: 
|   2048 a9:68:24:bc:97:1f:1e:54:a5:80:45:e7:4c:d9:aa:a0 (RSA)
|   256 e5:44:01:46:ee:7a:bb:7c:e9:1a:cb:14:99:9e:2b:8e (ECDSA)
|_  256 00:4e:1a:4f:33:e8:a0:de:86:a6:e4:2a:5f:84:61:2b (ED25519)
| ssh-publickey-acceptance: 
|_  Accepted Public Keys: No public keys accepted
|_ssh-run: Failed to specify credentials and command to run.
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: Linux 3.18 (95%), Linux 3.2 - 4.9 (95%), Linux 3.16 (95%), ASUS RT-N56U WAP (Linux 3.4) (94%), Linux 3.1 (94%), Linux 3.2 (94%), AXIS 210A or 211 Network Camera (Linux 2.6.17) (94%), Oracle VM Server 3.4.2 (Linux 4.1) (93%), Linux 3.10 - 4.11 (92%), Android 4.1.1 (92%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 22/tcp)
HOP RTT       ADDRESS
1   50.87 ms  10.10.12.1
2   171.21 ms friendzoneportal.red (10.10.10.123)

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 188.84 seconds
====================================================================================
 RUNNING SSH VERSION SCANNER 
====================================================================================
[*] Starting persistent handler(s)...
USER_FILE => /usr/share/brutex/wordlists/simple-users.txt
RHOSTS => 10.10.10.123
RHOST => 10.10.10.123
[+] 10.10.10.123:22       - SSH server version: SSH-2.0-OpenSSH_7.6p1 Ubuntu-4 ( service.version=7.6p1 openssh.comment=Ubuntu-4 service.vendor=OpenBSD service.family=OpenSSH service.product=OpenSSH service.cpe23=cpe:/a:openbsd:openssh:7.6p1 os.vendor=Ubuntu os.family=Linux os.product=Linux os.cpe23=cpe:/o:canonical:ubuntu_linux:- service.protocol=ssh fingerprint_db=ssh.banner )
[*] 10.10.10.123:22       - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
====================================================================================
 RUNNING OPENSSH USER ENUM SCANNER 
====================================================================================
[*] Starting persistent handler(s)...
USER_FILE => /usr/share/brutex/wordlists/simple-users.txt
RHOSTS => 10.10.10.123
RHOST => 10.10.10.123
[*] 10.10.10.123:22 - SSH - Using malformed packet technique
[*] 10.10.10.123:22 - SSH - Starting scan
[-] 10.10.10.123:22 - SSH - User 'admin' not found
[-] 10.10.10.123:22 - SSH - User 'administrator' not found
[-] 10.10.10.123:22 - SSH - User 'anonymous' not found
[+] 10.10.10.123:22 - SSH - User 'backup' found
[-] 10.10.10.123:22 - SSH - User 'bee' not found
[+] 10.10.10.123:22 - SSH - User 'ftp' found
[-] 10.10.10.123:22 - SSH - User 'guest' not found
[-] 10.10.10.123:22 - SSH - User 'GUEST' not found
[-] 10.10.10.123:22 - SSH - User 'info' not found
[+] 10.10.10.123:22 - SSH - User 'mail' found
[-] 10.10.10.123:22 - SSH - User 'mailadmin' not found
[-] 10.10.10.123:22 - SSH - User 'msfadmin' not found
[-] 10.10.10.123:22 - SSH - User 'mysql' not found
[+] 10.10.10.123:22 - SSH - User 'nobody' found
[-] 10.10.10.123:22 - SSH - User 'oracle' not found
[-] 10.10.10.123:22 - SSH - User 'owaspbwa' not found
[-] 10.10.10.123:22 - SSH - User 'postfix' not found
[-] 10.10.10.123:22 - SSH - User 'postgres' not found
[-] 10.10.10.123:22 - SSH - User 'private' not found
[-] 10.10.10.123:22 - SSH - User 'proftpd' not found
[-] 10.10.10.123:22 - SSH - User 'public' not found
[+] 10.10.10.123:22 - SSH - User 'root' found
[-] 10.10.10.123:22 - SSH - User 'superadmin' not found
[-] 10.10.10.123:22 - SSH - User 'support' not found
[+] 10.10.10.123:22 - SSH - User 'sys' found
[-] 10.10.10.123:22 - SSH - User 'system' not found
[-] 10.10.10.123:22 - SSH - User 'systemadmin' not found
[-] 10.10.10.123:22 - SSH - User 'systemadministrator' not found
[-] 10.10.10.123:22 - SSH - User 'test' not found
[-] 10.10.10.123:22 - SSH - User 'tomcat' not found
[-] 10.10.10.123:22 - SSH - User 'user' not found
[-] 10.10.10.123:22 - SSH - User 'webmaster' not found
[+] 10.10.10.123:22 - SSH - User 'www-data' found
[-] 10.10.10.123:22 - SSH - User 'Fortimanager_Access' not found
[*] Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
====================================================================================
 RUNNING LIBSSH AUTH BYPASS EXPLOIT CVE-2018-10933 
====================================================================================
[*] Starting persistent handler(s)...
RHOSTS => 10.10.10.123
RHOST => 10.10.10.123
[*] 10.10.10.123:22 - Attempting authentication bypass
[*] Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
 + -- --=[Port 23 closed... skipping.
 + -- --=[Port 25 closed... skipping.
 + -- --=[Port 53 opened... running tests...
====================================================================================
 RUNNING NMAP SCRIPTS 
====================================================================================
Starting Nmap 7.70 ( https://nmap.org ) at 2019-02-13 23:16 +03
Nmap scan report for friendzoneportal.red (10.10.10.123)
Host is up (0.053s latency).

PORT   STATE SERVICE VERSION
53/tcp open  domain  ISC BIND 9.11.3-1ubuntu1.2 (Ubuntu Linux)
|_dns-fuzz: Server didn't response to our probe, can't fuzz
| dns-nsec-enum: 
|_  No NSEC records found
| dns-nsec3-enum: 
|_  DNSSEC NSEC3 not supported
| dns-nsid: 
|_  bind.version: 9.11.3-1ubuntu1.2-Ubuntu
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: Linux 3.18 (95%), Linux 3.2 - 4.9 (95%), Linux 3.16 (95%), ASUS RT-N56U WAP (Linux 3.4) (95%), Linux 3.1 (93%), Linux 3.2 (93%), Linux 3.10 - 4.11 (93%), Linux 3.13 (93%), DD-WRT (Linux 3.18) (93%), DD-WRT v3.0 (Linux 4.4.2) (93%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
| dns-blacklist: 
|   SPAM
|_    l2.apews.org - FAIL
| dns-brute: 
|_  DNS Brute-force hostnames: No results.

TRACEROUTE (using port 53/tcp)
HOP RTT      ADDRESS
1   56.63 ms 10.10.12.1
2   56.69 ms friendzoneportal.red (10.10.10.123)

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 21.72 seconds
 + -- --=[Port 67 closed... skipping.
 + -- --=[Port 68 closed... skipping.
 + -- --=[Port 69 closed... skipping.
 + -- --=[Port 79 closed... skipping.
 + -- --=[Port 80 opened... running tests...
====================================================================================
 CHECKING FOR WAF 
====================================================================================

                                 ^     ^
        _   __  _   ____ _   __  _    _   ____
       ///7/ /.' \ / __////7/ /,' \ ,' \ / __/
      | V V // o // _/ | V V // 0 // 0 // _/
      |_n_,'/_n_//_/   |_n_,' \_,' \_,'/_/
                                <
                                ...'

    WAFW00F - Web Application Firewall Detection Tool

    By Sandro Gauci && Wendel G. Henrique

Checking http://10.10.10.123
Generic Detection results:
No WAF detected by the generic detection
Number of requests: 14

====================================================================================
 GATHERING HTTP INFO 
====================================================================================
http://10.10.10.123 [200 OK] Apache[2.4.29], Country[RESERVED][ZZ], Email[info@friendzoneportal.red], HTTPServer[Ubuntu Linux][Apache/2.4.29 (Ubuntu)], IP[10.10.10.123], Title[Friend Zone Escape software]
====================================================================================
 GATHERING SERVER INFO 
====================================================================================

wig - WebApp Information Gatherer


Scanning http://10.10.10.123...
_________________ SITE INFO __________________
IP              Title                         
10.10.10.123    Friend Zone Escape software 
                                              
__________________ VERSION ___________________
Name            Versions   Type               
Apache          2.4.29     Platform           
                                              
______________________________________________
Time: 67.1 sec  Urls: 810  Fingerprints: 40401
====================================================================================
 CHECKING HTTP HEADERS 
====================================================================================
HTTP/1.1 200 OK
Date: Wed, 13 Feb 2019 20:18:27 GMT
Server: Apache/2.4.29 (Ubuntu)
Last-Modified: Fri, 05 Oct 2018 22:52:00 GMT
ETag: "144-577831e9005e6"
Accept-Ranges: bytes
Content-Length: 324
Vary: Accept-Encoding
Content-Type: text/html

HTTP/1.1 200 OK
Date: Wed, 13 Feb 2019 20:18:27 GMT
Server: Apache/2.4.29 (Ubuntu)
Last-Modified: Fri, 05 Oct 2018 22:52:00 GMT
ETag: "144-577831e9005e6"
Accept-Ranges: bytes
Content-Length: 324
Vary: Accept-Encoding
Content-Type: text/html

====================================================================================
 DISPLAYING META GENERATOR TAGS 
====================================================================================
====================================================================================
 DISPLAYING COMMENTS 
====================================================================================
====================================================================================
 DISPLAYING SITE LINKS 
====================================================================================
====================================================================================
 SAVING SCREENSHOTS 
====================================================================================
[+] Screenshot saved to /usr/share/sniper/loot//screenshots/10.10.10.123-port80.jpg
 + -- --=[Port 110 closed... skipping.
 + -- --=[Port 111 closed... skipping.
 + -- --=[Port 123 closed... skipping.
 + -- --=[Port 135 closed... skipping.
 + -- --=[Port 137 closed... skipping.
 + -- --=[Port 139 opened... running tests...
====================================================================================
 RUNNING SMB ENUMERATION 
====================================================================================
Starting enum4linux v0.8.9 ( http://labs.portcullis.co.uk/application/enum4linux/ ) on Wed Feb 13 23:18:30 2019

 ========================== 
|    Target Information    |
 ========================== 
Target ........... 10.10.10.123
RID Range ........ 500-550,1000-1050
Username ......... ''
Password ......... ''
Known Usernames .. administrator, guest, krbtgt, domain admins, root, bin, none


 ==================================================== 
|    Enumerating Workgroup/Domain on 10.10.10.123    |
 ==================================================== 
[+] Got domain/workgroup name: WORKGROUP

 ============================================ 
|    Nbtstat Information for 10.10.10.123    |
 ============================================ 
Looking up status of 10.10.10.123
	FRIENDZONE      <00> -         B <ACTIVE>  Workstation Service
	FRIENDZONE      <03> -         B <ACTIVE>  Messenger Service
	FRIENDZONE      <20> -         B <ACTIVE>  File Server Service
	..__MSBROWSE__. <01> - <GROUP> B <ACTIVE>  Master Browser
	WORKGROUP       <00> - <GROUP> B <ACTIVE>  Domain/Workgroup Name
	WORKGROUP       <1d> -         B <ACTIVE>  Master Browser
	WORKGROUP       <1e> - <GROUP> B <ACTIVE>  Browser Service Elections

	MAC Address = 00-00-00-00-00-00

 ===================================== 
|    Session Check on 10.10.10.123    |
 ===================================== 
[+] Server 10.10.10.123 allows sessions using username '', password ''

 =========================================== 
|    Getting domain SID for 10.10.10.123    |
 =========================================== 
Domain Name: WORKGROUP
Domain Sid: (NULL SID)
[+] Can't determine if host is part of domain or part of a workgroup

 ====================================== 
|    OS information on 10.10.10.123    |
 ====================================== 
Use of uninitialized value $os_info in concatenation (.) or string at ./enum4linux.pl line 464.
[+] Got OS info for 10.10.10.123 from smbclient: 
[+] Got OS info for 10.10.10.123 from srvinfo:
	FRIENDZONE     Wk Sv PrQ Unx NT SNT FriendZone server (Samba, Ubuntu)
	platform_id     :	500
	os version      :	6.1
	server type     :	0x809a03

 ============================= 
|    Users on 10.10.10.123    |
 ============================= 
Use of uninitialized value $users in print at ./enum4linux.pl line 874.
Use of uninitialized value $users in pattern match (m//) at ./enum4linux.pl line 877.

Use of uninitialized value $users in print at ./enum4linux.pl line 888.
Use of uninitialized value $users in pattern match (m//) at ./enum4linux.pl line 890.

 ========================================= 
|    Share Enumeration on 10.10.10.123    |
 ========================================= 

	Sharename       Type      Comment
	---------       ----      -------
	print$          Disk      Printer Drivers
	Files           Disk      FriendZone Samba Server Files /etc/Files
	general         Disk      FriendZone Samba Server Files
	Development     Disk      FriendZone Samba Server Files
	IPC$            IPC       IPC Service (FriendZone server (Samba, Ubuntu))
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            FRIENDZONE
	YPUFFY               YPUFFY

[+] Attempting to map shares on 10.10.10.123
//10.10.10.123/print$	Mapping: DENIED, Listing: N/A
//10.10.10.123/Files	Mapping: DENIED, Listing: N/A
//10.10.10.123/general	Mapping: OK, Listing: OK
//10.10.10.123/Development	Mapping: OK, Listing: OK
//10.10.10.123/IPC$	[E] Can't understand response:
NT_STATUS_OBJECT_NAME_NOT_FOUND listing \*

 ==================================================== 
|    Password Policy Information for 10.10.10.123    |
 ==================================================== 


[+] Attaching to 10.10.10.123 using a NULL share

[+] Trying protocol 445/SMB...

[+] Found domain(s):

	[+] FRIENDZONE
	[+] Builtin

[+] Password Info for Domain: FRIENDZONE

	[+] Minimum password length: 5
	[+] Password history length: None
	[+] Maximum password age: 37 days 6 hours 21 minutes 
	[+] Password Complexity Flags: 000000

		[+] Domain Refuse Password Change: 0
		[+] Domain Password Store Cleartext: 0
		[+] Domain Password Lockout Admins: 0
		[+] Domain Password No Clear Change: 0
		[+] Domain Password No Anon Change: 0
		[+] Domain Password Complex: 0

	[+] Minimum password age: None
	[+] Reset Account Lockout Counter: 30 minutes 
	[+] Locked Account Duration: 30 minutes 
	[+] Account Lockout Threshold: None
	[+] Forced Log off Time: 37 days 6 hours 21 minutes 


[+] Retieved partial password policy with rpcclient:

Password Complexity: Disabled
Minimum Password Length: 5


 ============================== 
|    Groups on 10.10.10.123    |
 ============================== 

[+] Getting builtin groups:

[+] Getting builtin group memberships:

[+] Getting local groups:

[+] Getting local group memberships:

[+] Getting domain groups:

[+] Getting domain group memberships:

 ======================================================================= 
|    Users on 10.10.10.123 via RID cycling (RIDS: 500-550,1000-1050)    |
 ======================================================================= 
[I] Found new SID: S-1-22-1
[I] Found new SID: S-1-5-21-3651157261-4258463691-276428382
[I] Found new SID: S-1-5-32
[+] Enumerating users using SID S-1-5-32 and logon username '', password ''
S-1-5-32-500 *unknown*\*unknown* (8)
S-1-5-32-501 *unknown*\*unknown* (8)
S-1-5-32-502 *unknown*\*unknown* (8)
S-1-5-32-503 *unknown*\*unknown* (8)
S-1-5-32-504 *unknown*\*unknown* (8)
S-1-5-32-505 *unknown*\*unknown* (8)
S-1-5-32-506 *unknown*\*unknown* (8)
S-1-5-32-507 *unknown*\*unknown* (8)
S-1-5-32-508 *unknown*\*unknown* (8)
S-1-5-32-509 *unknown*\*unknown* (8)
S-1-5-32-510 *unknown*\*unknown* (8)
S-1-5-32-511 *unknown*\*unknown* (8)
S-1-5-32-512 *unknown*\*unknown* (8)
S-1-5-32-513 *unknown*\*unknown* (8)
S-1-5-32-514 *unknown*\*unknown* (8)
S-1-5-32-515 *unknown*\*unknown* (8)
S-1-5-32-516 *unknown*\*unknown* (8)
S-1-5-32-517 *unknown*\*unknown* (8)
S-1-5-32-518 *unknown*\*unknown* (8)
S-1-5-32-519 *unknown*\*unknown* (8)
S-1-5-32-520 *unknown*\*unknown* (8)
S-1-5-32-521 *unknown*\*unknown* (8)
S-1-5-32-522 *unknown*\*unknown* (8)
S-1-5-32-523 *unknown*\*unknown* (8)
S-1-5-32-524 *unknown*\*unknown* (8)
S-1-5-32-525 *unknown*\*unknown* (8)
S-1-5-32-526 *unknown*\*unknown* (8)
S-1-5-32-527 *unknown*\*unknown* (8)
S-1-5-32-528 *unknown*\*unknown* (8)
S-1-5-32-529 *unknown*\*unknown* (8)
S-1-5-32-530 *unknown*\*unknown* (8)
S-1-5-32-531 *unknown*\*unknown* (8)
S-1-5-32-532 *unknown*\*unknown* (8)
S-1-5-32-533 *unknown*\*unknown* (8)
S-1-5-32-534 *unknown*\*unknown* (8)
S-1-5-32-535 *unknown*\*unknown* (8)
S-1-5-32-536 *unknown*\*unknown* (8)
S-1-5-32-537 *unknown*\*unknown* (8)
S-1-5-32-538 *unknown*\*unknown* (8)
S-1-5-32-539 *unknown*\*unknown* (8)
S-1-5-32-540 *unknown*\*unknown* (8)
S-1-5-32-541 *unknown*\*unknown* (8)
S-1-5-32-542 *unknown*\*unknown* (8)
S-1-5-32-543 *unknown*\*unknown* (8)
S-1-5-32-544 BUILTIN\Administrators (Local Group)
S-1-5-32-545 BUILTIN\Users (Local Group)
S-1-5-32-546 BUILTIN\Guests (Local Group)
S-1-5-32-547 BUILTIN\Power Users (Local Group)
S-1-5-32-548 BUILTIN\Account Operators (Local Group)
S-1-5-32-549 BUILTIN\Server Operators (Local Group)
S-1-5-32-550 BUILTIN\Print Operators (Local Group)
S-1-5-32-1000 *unknown*\*unknown* (8)
S-1-5-32-1001 *unknown*\*unknown* (8)
S-1-5-32-1002 *unknown*\*unknown* (8)
S-1-5-32-1003 *unknown*\*unknown* (8)
S-1-5-32-1004 *unknown*\*unknown* (8)
S-1-5-32-1005 *unknown*\*unknown* (8)
S-1-5-32-1006 *unknown*\*unknown* (8)
S-1-5-32-1007 *unknown*\*unknown* (8)
S-1-5-32-1008 *unknown*\*unknown* (8)
S-1-5-32-1009 *unknown*\*unknown* (8)
S-1-5-32-1010 *unknown*\*unknown* (8)
S-1-5-32-1011 *unknown*\*unknown* (8)
S-1-5-32-1012 *unknown*\*unknown* (8)
S-1-5-32-1013 *unknown*\*unknown* (8)
S-1-5-32-1014 *unknown*\*unknown* (8)
S-1-5-32-1015 *unknown*\*unknown* (8)
S-1-5-32-1016 *unknown*\*unknown* (8)
S-1-5-32-1017 *unknown*\*unknown* (8)
S-1-5-32-1018 *unknown*\*unknown* (8)
S-1-5-32-1019 *unknown*\*unknown* (8)
S-1-5-32-1020 *unknown*\*unknown* (8)
S-1-5-32-1021 *unknown*\*unknown* (8)
S-1-5-32-1022 *unknown*\*unknown* (8)
S-1-5-32-1023 *unknown*\*unknown* (8)
S-1-5-32-1024 *unknown*\*unknown* (8)
S-1-5-32-1025 *unknown*\*unknown* (8)
S-1-5-32-1026 *unknown*\*unknown* (8)
S-1-5-32-1027 *unknown*\*unknown* (8)
S-1-5-32-1028 *unknown*\*unknown* (8)
S-1-5-32-1029 *unknown*\*unknown* (8)
S-1-5-32-1030 *unknown*\*unknown* (8)
S-1-5-32-1031 *unknown*\*unknown* (8)
S-1-5-32-1032 *unknown*\*unknown* (8)
S-1-5-32-1033 *unknown*\*unknown* (8)
S-1-5-32-1034 *unknown*\*unknown* (8)
S-1-5-32-1035 *unknown*\*unknown* (8)
S-1-5-32-1036 *unknown*\*unknown* (8)
S-1-5-32-1037 *unknown*\*unknown* (8)
S-1-5-32-1038 *unknown*\*unknown* (8)
S-1-5-32-1039 *unknown*\*unknown* (8)
S-1-5-32-1040 *unknown*\*unknown* (8)
S-1-5-32-1041 *unknown*\*unknown* (8)
S-1-5-32-1042 *unknown*\*unknown* (8)
S-1-5-32-1043 *unknown*\*unknown* (8)
S-1-5-32-1044 *unknown*\*unknown* (8)
S-1-5-32-1045 *unknown*\*unknown* (8)
S-1-5-32-1046 *unknown*\*unknown* (8)
S-1-5-32-1047 *unknown*\*unknown* (8)
S-1-5-32-1048 *unknown*\*unknown* (8)
S-1-5-32-1049 *unknown*\*unknown* (8)
S-1-5-32-1050 *unknown*\*unknown* (8)
[+] Enumerating users using SID S-1-5-21-3651157261-4258463691-276428382 and logon username '', password ''
S-1-5-21-3651157261-4258463691-276428382-500 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-501 FRIENDZONE\nobody (Local User)
S-1-5-21-3651157261-4258463691-276428382-502 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-503 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-504 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-505 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-506 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-507 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-508 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-509 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-510 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-511 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-512 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-513 FRIENDZONE\None (Domain Group)
S-1-5-21-3651157261-4258463691-276428382-514 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-515 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-516 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-517 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-518 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-519 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-520 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-521 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-522 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-523 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-524 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-525 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-526 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-527 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-528 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-529 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-530 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-531 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-532 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-533 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-534 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-535 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-536 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-537 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-538 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-539 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-540 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-541 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-542 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-543 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-544 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-545 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-546 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-547 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-548 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-549 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-550 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1000 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1001 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1002 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1003 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1004 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1005 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1006 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1007 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1008 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1009 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1010 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1011 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1012 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1013 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1014 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1015 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1016 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1017 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1018 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1019 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1020 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1021 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1022 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1023 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1024 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1025 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1026 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1027 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1028 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1029 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1030 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1031 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1032 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1033 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1034 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1035 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1036 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1037 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1038 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1039 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1040 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1041 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1042 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1043 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1044 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1045 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1046 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1047 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1048 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1049 *unknown*\*unknown* (8)
S-1-5-21-3651157261-4258463691-276428382-1050 *unknown*\*unknown* (8)
[+] Enumerating users using SID S-1-22-1 and logon username '', password ''
S-1-22-1-1000 Unix User\friend (Local User)

 ============================================= 
|    Getting printer info for 10.10.10.123    |
 ============================================= 
No printers returned.


enum4linux complete on Wed Feb 13 23:28:33 2019

Impacket v0.9.15 - Copyright 2002-2016 Core Security Technologies

[*] Retrieving endpoint list from 10.10.10.123
[*] Trying protocol 445/SMB...
Found domain(s):
 . FRIENDZONE
 . Builtin
[*] Looking up users in domain FRIENDZONE
[*] No entries received.
Doing NBT name scan for addresses from 10.10.10.123

IP address       NetBIOS Name     Server    User             MAC address      
------------------------------------------------------------------------------
10.10.10.123     FRIENDZONE       <server>  FRIENDZONE       00:00:00:00:00:00
====================================================================================
 RUNNING NMAP SCRIPTS 
====================================================================================
Starting Nmap 7.70 ( https://nmap.org ) at 2019-02-13 23:28 +03
Nmap scan report for friendzoneportal.red (10.10.10.123)
Host is up (0.13s latency).

PORT    STATE SERVICE     VERSION
139/tcp open  netbios-ssn Samba smbd 4.7.6-Ubuntu (workgroup: WORKGROUP)
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: Linux 3.2 - 4.9 (95%), Linux 3.16 (95%), ASUS RT-N56U WAP (Linux 3.4) (95%), Linux 3.18 (94%), Linux 3.1 (93%), Linux 3.2 (93%), Oracle VM Server 3.4.2 (Linux 4.1) (93%), Linux 4.10 (93%), AXIS 210A or 211 Network Camera (Linux 2.6.17) (92%), Linux 3.10 - 3.13 (92%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
Service Info: Host: FRIENDZONE

Host script results:
| smb-brute: 
|_  No accounts found
| smb-enum-domains: 
|   FRIENDZONE
|     Groups: n/a
|     Users: n/a
|     Creation time: unknown
|     Passwords: min length: 5; min age: n/a days; max age: n/a days; history: n/a passwords
|     Account lockout disabled
|   Builtin
|     Groups: n/a
|     Users: n/a
|     Creation time: unknown
|     Passwords: min length: 5; min age: n/a days; max age: n/a days; history: n/a passwords
|_    Account lockout disabled
| smb-enum-sessions: 
|_  <nobody>
| smb-enum-shares: 
|   account_used: guest
|   \\10.10.10.123\Development: 
|     Type: STYPE_DISKTREE
|     Comment: FriendZone Samba Server Files
|     Users: 19
|     Max Users: <unlimited>
|     Path: C:\etc\Development
|     Anonymous access: READ/WRITE
|     Current user access: READ/WRITE
|   \\10.10.10.123\Files: 
|     Type: STYPE_DISKTREE
|     Comment: FriendZone Samba Server Files /etc/Files
|     Users: 0
|     Max Users: <unlimited>
|     Path: C:\etc\hole
|     Anonymous access: <none>
|     Current user access: <none>
|   \\10.10.10.123\IPC$: 
|     Type: STYPE_IPC_HIDDEN
|     Comment: IPC Service (FriendZone server (Samba, Ubuntu))
|     Users: 7
|     Max Users: <unlimited>
|     Path: C:\tmp
|     Anonymous access: READ/WRITE
|     Current user access: READ/WRITE
|   \\10.10.10.123\general: 
|     Type: STYPE_DISKTREE
|     Comment: FriendZone Samba Server Files
|     Users: 3
|     Max Users: <unlimited>
|     Path: C:\etc\general
|     Anonymous access: READ/WRITE
|     Current user access: READ/WRITE
|   \\10.10.10.123\print$: 
|     Type: STYPE_DISKTREE
|     Comment: Printer Drivers
|     Users: 0
|     Max Users: <unlimited>
|     Path: C:\var\lib\samba\printers
|     Anonymous access: <none>
|_    Current user access: <none>
| smb-ls: Volume \\10.10.10.123\Development
|   maxfiles limit reached (10)
| SIZE   TIME                 FILENAME
| <DIR>  2019-02-13 23:31:48  .
| <DIR>  2019-01-24 00:51:02  ..
| 5494   2019-02-13 23:14:24  shell123.php
| 78     2019-02-13 22:31:25  d.php
| 320    2019-02-13 23:31:04  walt.php
| 55     2019-02-13 22:56:29  backdoor.php
| 27     2019-02-13 22:52:52  who.php
| 5494   2019-02-13 22:15:35  rev.php
| 5494   2019-02-13 22:55:37  1234.php
| 5493   2019-02-13 23:05:14  be.php
| 
| 
| Volume \\10.10.10.123\general
| SIZE   TIME                 FILENAME
| <DIR>  2019-01-16 23:10:51  .
| <DIR>  2019-01-24 00:51:02  ..
| 57     2018-10-10 02:52:42  creds.txt
|_
| smb-mbenum: 
|   DFS Root
|     FRIENDZONE  0.0  FriendZone server (Samba, Ubuntu)
|     FROLIC      0.0  frolic server (Samba, Ubuntu)
|   Master Browser
|     FRIENDZONE  0.0  FriendZone server (Samba, Ubuntu)
|   Potential Browser
|     FROLIC  0.0  frolic server (Samba, Ubuntu)
|   Print server
|     FRIENDZONE  0.0  FriendZone server (Samba, Ubuntu)
|     FROLIC      0.0  frolic server (Samba, Ubuntu)
|   Server
|     FRIENDZONE  0.0  FriendZone server (Samba, Ubuntu)
|     FROLIC      0.0  frolic server (Samba, Ubuntu)
|   Server service
|     CONCEAL     0.0  
|     FRIENDZONE  0.0  FriendZone server (Samba, Ubuntu)
|     FROLIC      0.0  frolic server (Samba, Ubuntu)
|   Unix server
|     FRIENDZONE  0.0  FriendZone server (Samba, Ubuntu)
|     FROLIC      0.0  frolic server (Samba, Ubuntu)
|   Windows NT/2000/XP/2003 server
|     CONCEAL     0.0  
|     FRIENDZONE  0.0  FriendZone server (Samba, Ubuntu)
|     FROLIC      0.0  frolic server (Samba, Ubuntu)
|   Workstation
|     CONCEAL     0.0  
|     FRIENDZONE  0.0  FriendZone server (Samba, Ubuntu)
|_    FROLIC      0.0  frolic server (Samba, Ubuntu)
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.7.6-Ubuntu)
|   Computer name: friendzone
|   NetBIOS computer name: FRIENDZONE\x00
|   Domain name: \x00
|   FQDN: friendzone
|_  System time: 2019-02-13T22:30:17+02:00
|_smb-print-text: false
| smb-protocols: 
|   dialects: 
|     NT LM 0.12 (SMBv1) [dangerous, but default]
|     2.02
|     2.10
|     3.00
|     3.02
|_    3.11
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb-system-info: ERROR: Script execution failed (use -d to debug)
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: false

TRACEROUTE (using port 139/tcp)
HOP RTT       ADDRESS
1   53.45 ms  10.10.12.1
2   216.94 ms friendzoneportal.red (10.10.10.123)

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 200.42 seconds
====================================================================================
 RUNNING METASPLOIT MODULES 
====================================================================================
[*] Starting persistent handler(s)...
RHOSTS => 10.10.10.123
RHOST => 10.10.10.123
[+] 10.10.10.123:139      - Pipes: \lsarpc, \samr, \eventlog, \InitShutdown, \ntsvcs, \srvsvc, \wkssvc
[*] 10.10.10.123:         - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[*] 10.10.10.123:         - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[-] 10.10.10.123:445      - Error runing query against HKU. Rex::Proto::DCERPC::Exceptions::BindError. Failed to bind. Could not bind to 367abb81-9844-35f1-ad32-98f038001003:2.0@ncacn_np:10.10.10.123[\svcctl]
[*] 10.10.10.123:445      - Executing cleanup
[-] 10.10.10.123:445      - Unable to processes cleanup commands: Failed to bind. Could not bind to 367abb81-9844-35f1-ad32-98f038001003:2.0@ncacn_np:10.10.10.123[\svcctl]
[!] 10.10.10.123:445      - Maybe %SYSTEMDRIVE%\WINDOWS\Temp\prDInKxfLWBUCjas.txt must be deleted manually
[!] 10.10.10.123:445      - Maybe %SYSTEMDRIVE%\WINDOWS\Temp\yGijNmMFlKJsXcMC.bat must be deleted manually
[*] 10.10.10.123:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[+] 10.10.10.123:445      - 10.10.10.123 supports SMB 2 [dialect 255.2] and has been online for 3665156 hours
[*] 10.10.10.123:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[*] 10.10.10.123:445      - Connecting to the server...
[*] 10.10.10.123:445      - Mounting the remote share \\10.10.10.123\SYSVOL'...
[-] 10.10.10.123:445      - 10.10.10.123: Rex::Proto::SMB::Exceptions::ErrorCode The server responded with error: STATUS_BAD_NETWORK_NAME (Command=117 WordCount=0)
[*] 10.10.10.123:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[+] 10.10.10.123:139      - print$ - (DS) Printer Drivers
[+] 10.10.10.123:139      - Files - (DS) FriendZone Samba Server Files /etc/Files
[+] 10.10.10.123:139      - general - (DS) FriendZone Samba Server Files
[+] 10.10.10.123:139      - Development - (DS) FriendZone Samba Server Files
[+] 10.10.10.123:139      - IPC$ - (I) IPC Service (FriendZone server (Samba
[+] 10.10.10.123:139      - Ubuntu))
[*] 10.10.10.123:         - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[+] 10.10.10.123:445      - FRIENDZONE [  ] ( LockoutTries=0 PasswordMin=5 )
[*] 10.10.10.123:         - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[-] 10.10.10.123:139      - UUID 6bffd098-a112-3610-9833-46c3f87e345a 1.0 ERROR Failed to bind. Could not bind to 6bffd098-a112-3610-9833-46c3f87e345a:1.0@ncacn_np:10.10.10.123[\wkssvc]
[*] 10.10.10.123:         - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[*] 10.10.10.123:445      - 10.10.10.123:445 - Starting SMB login bruteforce
[*] 10.10.10.123:445      - Error: 10.10.10.123: Metasploit::Framework::LoginScanner::Invalid Cred details can't be blank, Cred details can't be blank (Metasploit::Framework::LoginScanner::SMB)
[*] 10.10.10.123:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[*] 10.10.10.123:139      - PIPE(LSARPC) LOCAL(FRIENDZONE - 5-21-3651157261-4258463691-276428382) DOMAIN(WORKGROUP - )
[*] 10.10.10.123:139      - USER=nobody RID=501
[*] 10.10.10.123:139      - GROUP=None RID=513
Error: Rex::Proto::DCERPC::Exceptions::NoResponse no response from dcerpc service
[*] 10.10.10.123:445      - PIPE(LSARPC) LOCAL(FRIENDZONE - 5-21-3651157261-4258463691-276428382) DOMAIN(WORKGROUP - )
[*] 10.10.10.123:445      - USER=nobody RID=501
[*] 10.10.10.123:445      - GROUP=None RID=513
Error: Rex::Proto::DCERPC::Exceptions::NoResponse no response from dcerpc service
[*] 10.10.10.123:         - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[*] 10.10.10.123:139      - The target appears to be running Samba.
[*] 10.10.10.123:         - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[*] 10.10.10.123:445      - Host could not be identified: Windows 6.1 (Samba 4.7.6-Ubuntu)
[*] 10.10.10.123:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
[*] Started reverse TCP handler on 10.10.14.32:4444 
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] 10.10.10.123:139 - Trying return address 0x081ed5f2...
[-] 10.10.10.123:139 - The SMB server did not reply to our request
[*] Exploit completed, but no session was created.
[*] Started reverse TCP handler on 10.10.14.32:4444 
[*] 10.10.10.123:445 - Automatically detecting the target...
[*] 10.10.10.123:445 - Fingerprint: Unknown -  - lang:Unknown
[-] 10.10.10.123:445 - Exploit aborted due to failure: no-target: No matching target
[*] Exploit completed, but no session was created.
[-] 10.10.10.123:445      - Host does NOT appear vulnerable.
[*] 10.10.10.123:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
 + -- --=[Port 161 closed... skipping.
 + -- --=[Port 162 closed... skipping.
 + -- --=[Port 389 closed... skipping.
 + -- --=[Port 443 opened... running tests...
====================================================================================
 CHECKING FOR WAF 
====================================================================================
ERROR:root:Site https://10.10.10.123 appears to be down

                                 ^     ^
        _   __  _   ____ _   __  _    _   ____
       ///7/ /.' \ / __////7/ /,' \ ,' \ / __/
      | V V // o // _/ | V V // 0 // 0 // _/
      |_n_,'/_n_//_/   |_n_,' \_,' \_,'/_/
                                <
                                ...'

    WAFW00F - Web Application Firewall Detection Tool

    By Sandro Gauci && Wendel G. Henrique

Checking https://10.10.10.123

====================================================================================
 GATHERING HTTP INFO 
====================================================================================
https://10.10.10.123 ERROR: Timed out execution expired
https://10.10.10.123 [ Unassigned] 
====================================================================================
 GATHERING SERVER INFO 
====================================================================================

wig - WebApp Information Gatherer


====================================================================================
 CHECKING HTTP HEADERS 
====================================================================================
====================================================================================
 DISPLAYING META GENERATOR TAGS 
====================================================================================
====================================================================================
 DISPLAYING COMMENTS 
====================================================================================
====================================================================================
 DISPLAYING SITE LINKS 
====================================================================================
====================================================================================
 GATHERING SSL/TLS INFO 
====================================================================================
ERROR: Could not open a connection to host 10.10.10.123 (10.10.10.123) on port 443.
ERROR: Could not connect.
Version: 1.11.12-static
OpenSSL 1.0.2-chacha (1.0.2g-dev)

Connected to 10.10.10.123

Testing SSL server 10.10.10.123 on port 443 using SNI name 10.10.10.123

  TLS Fallback SCSV:
Server does not support TLS Fallback SCSV

  TLS renegotiation:
Session renegotiation not supported

  TLS Compression:
Compression disabled

  Heartbleed:
TLS 1.2 dying
====================================================================================
 SAVING SCREENSHOTS 
====================================================================================
[+] Screenshot saved to /usr/share/sniper/loot//screenshots/10.10.10.123-port443.jpg
 + -- --=[Port 445 scanned... skipping.
 + -- --=[Port 512 closed... skipping.
 + -- --=[Port 513 closed... skipping.
 + -- --=[Port 514 closed... skipping.
 + -- --=[Port 623 closed... skipping.
 + -- --=[Port 624 closed... skipping.
 + -- --=[Port 1099 closed... skipping.
 + -- --=[Port 1433 closed... skipping.
 + -- --=[Port 2049 closed... skipping.
 + -- --=[Port 3306 closed... skipping.
 + -- --=[Port 3310 closed... skipping.
 + -- --=[Port 3128 closed... skipping.
 + -- --=[Port 3389 closed... skipping.
 + -- --=[Port 3632 closed... skipping.
 + -- --=[Port 5432 closed... skipping.
 + -- --=[Port 5555 closed... skipping.
 + -- --=[Port 5800 closed... skipping.
 + -- --=[Port 5900 closed... skipping.
 + -- --=[Port 5984 closed... skipping.
 + -- --=[Port 6000 closed... skipping.
 + -- --=[Port 6667 closed... skipping.
 + -- --=[Port 7001 closed... skipping.
 + -- --=[Port 10000 closed... skipping.
 + -- --=[Port 16992 closed... skipping.
 + -- --=[Port 27017 closed... skipping.
 + -- --=[Port 27018 closed... skipping.
 + -- --=[Port 27019 closed... skipping.
 + -- --=[Port 28017 closed... skipping.
====================================================================================
 SCANNING FOR COMMON VULNERABILITIES 
====================================================================================
#########################################################################################
  oooooo   oooo       .o.        .oooooo..o ooooo     ooo   .oooooo.
   `888.   .8'       .888.      d8P'    `Y8 `888'     `8'  d8P'  `Y8b
    `888. .8'       .88888.     Y88bo.       888       8  888      888
     `888.8'       .8' `888.     `ZY8888o.   888       8  888      888
      `888'       .88ooo8888.        `0Y88b  888       8  888      888
       888       .8'     `888.  oo     .d8P  `88.    .8'  `88b    d88'
      o888o     o88o     o8888o 88888888P'     `YbodP'     `Y8bood8P'
Welcome to Yasuo v2.3
Author: Saurabh Harit (@0xsauby) | Contribution & Coolness: Stephen Hall (@logicalsec)
#########################################################################################

I, [2019-02-13T23:38:39.438462 #8068]  INFO -- : Initiating port scan
Traceback (most recent call last):
	3: from yasuo.rb:700:in `<main>'
	2: from yasuo.rb:132:in `run'
	1: from yasuo.rb:232:in `process_nmap_scan'
yasuo.rb:232:in `each_slice': invalid slice size (ArgumentError)
I, [2019-02-13T23:38:42.819817 #8068]  INFO -- : Using nmap scan output file logs/nmap_output_2019-02-13_23-38-39.xml
====================================================================================
 SKIPPING FULL NMAP PORT SCAN 
====================================================================================
====================================================================================
 SKIPPING BRUTE FORCE 
====================================================================================
====================================================================================
 SCAN COMPLETE! 
====================================================================================
                ____               
    _________  /  _/___  ___  _____
   / ___/ __ \ / // __ \/ _ \/ ___/
  (__  ) / / // // /_/ /  __/ /    
 /____/_/ /_/___/ .___/\___/_/     
               /_/                 

[*] Opening loot directory /usr/share/sniper/loot/ [OK]
 + -- --=[Generating reports...
[||||||]
 + -- --=[Sorting all domains...
 + -- --=[Removing blank screenshots...
 + -- --=[Sn1per Professional is not installed. To download Sn1per Professional, go to https://xerosecurity.com. 
 + -- --=[Done!
root@n00b:/media/sf_VM_SHARED/htb/10.10.10.123# 
