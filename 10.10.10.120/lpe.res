=================================================================================================
LINUX PRIVILEGE ESCALATION CHECKER
=================================================================================================

[*] GETTING BASIC SYSTEM INFO...

[+] Kernel
    Linux version 4.18.0-12-generic (buildd@lgw01-amd64-033) (gcc version 8.2.0 (Ubuntu 8.2.0-7ubuntu1)) #13-Ubuntu SMP Wed Nov 14 15:17:05 UTC 2018

[+] Hostname
    chaos

[+] Operating System
    Ubuntu 18.10 \n \l

[*] GETTING NETWORKING INFO...

[+] Interfaces
    ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
    inet 10.10.10.120  netmask 255.255.255.0  broadcast 10.10.10.255
    inet6 dead:beef::250:56ff:feb9:cdb0  prefixlen 64  scopeid 0x0<global>
    inet6 fe80::250:56ff:feb9:cdb0  prefixlen 64  scopeid 0x20<link>
    ether 00:50:56:b9:cd:b0  txqueuelen 1000  (Ethernet)
    RX packets 2529037  bytes 477380477 (477.3 MB)
    RX errors 0  dropped 20  overruns 0  frame 0
    TX packets 2433365  bytes 924875484 (924.8 MB)
    TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
    lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
    inet 127.0.0.1  netmask 255.0.0.0
    inet6 ::1  prefixlen 128  scopeid 0x10<host>
    loop  txqueuelen 1000  (Local Loopback)
    RX packets 78900  bytes 5272134 (5.2 MB)
    RX errors 0  dropped 0  overruns 0  frame 0
    TX packets 78900  bytes 5272134 (5.2 MB)
    TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

[+] Netstat
    Active Internet connections (servers and established)
    Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
    tcp        0      0 0.0.0.0:995             0.0.0.0:*               LISTEN      -
    tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -
    tcp        0      0 0.0.0.0:110             0.0.0.0:*               LISTEN      -
    tcp        0      0 0.0.0.0:143             0.0.0.0:*               LISTEN      -
    tcp        0      0 0.0.0.0:10000           0.0.0.0:*               LISTEN      -
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -
    tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      -
    tcp        0      0 0.0.0.0:993             0.0.0.0:*               LISTEN      -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44170       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44130       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44184       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44122       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44188       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44198       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44146       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44194       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44138       ESTABLISHED -
    tcp        0      0 10.10.10.120:43766      10.10.15.2:4446         ESTABLISHED 22236/nc
    tcp        0      0 10.10.10.120:995        10.10.13.64:44168       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44176       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44190       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44126       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44182       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44158       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44164       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44128       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44140       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44118       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44192       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44178       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44134       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44156       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44152       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44212       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44200       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44180       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44144       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44196       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44210       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44160       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44166       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44186       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44116       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44124       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44172       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44162       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44132       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44148       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44154       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44206       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44136       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44208       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44150       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44204       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44142       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44120       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44114       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44174       ESTABLISHED -
    tcp        0      0 10.10.10.120:995        10.10.13.64:44202       ESTABLISHED -
    tcp6       0      0 :::995                  :::*                    LISTEN      -
    tcp6       0      0 :::110                  :::*                    LISTEN      -
    tcp6       0      0 :::143                  :::*                    LISTEN      -
    tcp6       0      0 :::80                   :::*                    LISTEN      -
    tcp6       0      0 ::1:25                  :::*                    LISTEN      -
    tcp6       0      0 :::993                  :::*                    LISTEN      -
    tcp6       0    421 10.10.10.120:80         10.10.15.2:49188        FIN_WAIT1   -
    tcp6       0    421 10.10.10.120:80         10.10.15.2:49180        FIN_WAIT1   -
    tcp6       0    421 10.10.10.120:80         10.10.15.2:49186        FIN_WAIT1   -
    tcp6       0      0 10.10.10.120:80         10.10.15.2:56656        ESTABLISHED -
    tcp6       0    421 10.10.10.120:80         10.10.15.2:49182        FIN_WAIT1   -
    udp        0      0 0.0.0.0:10000           0.0.0.0:*                           -
    udp        0      0 127.0.0.53:53           0.0.0.0:*                           -

[+] Route
    Kernel IP routing table
    Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
    default         _gateway        0.0.0.0         UG    0      0        0 ens33
    10.10.10.0      0.0.0.0         255.255.255.0   U     0      0        0 ens33

[*] GETTING FILESYSTEM INFO...

[+] Mount results
    /dev/sda2 on / type ext4 (rw,relatime)
    udev on /dev type devtmpfs (rw,nosuid,relatime,size=473192k,nr_inodes=118298,mode=755)
    devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000)
    tmpfs on /dev/shm type tmpfs (rw,nosuid,nodev)
    mqueue on /dev/mqueue type mqueue (rw,relatime)
    hugetlbfs on /dev/hugepages type hugetlbfs (rw,relatime,pagesize=2M)
    tmpfs on /run type tmpfs (rw,nosuid,noexec,relatime,size=100948k,mode=755)
    tmpfs on /run/lock type tmpfs (rw,nosuid,nodev,noexec,relatime,size=5120k)
    sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime)
    securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
    tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,mode=755)
    cgroup2 on /sys/fs/cgroup/unified type cgroup2 (rw,nosuid,nodev,noexec,relatime,nsdelegate)
    cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,xattr,name=systemd)
    cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,hugetlb)
    cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,devices)
    cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,cpu,cpuacct)
    cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,net_cls,net_prio)
    cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,perf_event)
    cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,freezer)
    cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,cpuset)
    cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,pids)
    cgroup on /sys/fs/cgroup/rdma type cgroup (rw,nosuid,nodev,noexec,relatime,rdma)
    cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,blkio)
    cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,memory)
    pstore on /sys/fs/pstore type pstore (rw,nosuid,nodev,noexec,relatime)
    bpf on /sys/fs/bpf type bpf (rw,nosuid,nodev,noexec,relatime,mode=700)
    debugfs on /sys/kernel/debug type debugfs (rw,relatime)
    fusectl on /sys/fs/fuse/connections type fusectl (rw,relatime)
    configfs on /sys/kernel/config type configfs (rw,relatime)
    proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
    systemd-1 on /proc/sys/fs/binfmt_misc type autofs (rw,relatime,fd=45,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=14718)
    /dev/sda2 on /tmp type ext4 (rw,relatime)
    /dev/sda2 on /var/tmp type ext4 (rw,relatime)
    binfmt_misc on /proc/sys/fs/binfmt_misc type binfmt_misc (rw,relatime)

[+] fstab entries
    UUID=38f4e3ae-d4c1-4297-a5e8-a0ff98dde5fa / ext4 defaults 0 0
    /swap.img	none	swap	sw	0	0

[+] Scheduled cron jobs
    -rw-r--r-- 1 root root  722 Nov 16  2017 /etc/crontab
    /etc/cron.d:
    total 24
    drwxr-xr-x   2 root root 4096 Oct 28 10:32 .
    drwxr-xr-x 105 root root 4096 Dec  9 17:22 ..
    -rw-r--r--   1 root root  102 Nov 16  2017 .placeholder
    -rw-r--r--   1 root root  589 Jul 23  2018 mdadm
    -rw-r--r--   1 root root  712 Jul  9  2018 php
    -rw-r--r--   1 root root  190 Oct 17 23:03 popularity-contest
    /etc/cron.daily:
    total 72
    drwxr-xr-x   2 root root 4096 Oct 28 12:45 .
    drwxr-xr-x 105 root root 4096 Dec  9 17:22 ..
    -rw-r--r--   1 root root  102 Nov 16  2017 .placeholder
    -rwxr-xr-x   1 root root  539 Aug  3 20:09 apache2
    -rwxr-xr-x   1 root root  376 Aug 10 22:39 apport
    -rwxr-xr-x   1 root root 1478 Oct  7 05:56 apt-compat
    -rwxr-xr-x   1 root root   77 Sep  5  2008 apt-show-versions
    -rwxr-xr-x   1 root root  355 Dec 29  2017 bsdmainutils
    -rwxr-xr-x   1 root root 1176 Sep 13 11:46 dpkg
    -rwxr-xr-x   1 root root  377 Sep 18 18:51 logrotate
    -rwxr-xr-x   1 root root 1065 Jul 27  2018 man-db
    -rwxr-xr-x   1 root root  539 Jul 23  2018 mdadm
    -rwxr-xr-x   1 root root  538 Mar  1  2018 mlocate
    -rwxr-xr-x   1 root root  249 Jan 25  2018 passwd
    -rwxr-xr-x   1 root root 4574 Aug 13 20:50 popularity-contest
    -rwxr-xr-x   1 root root  246 Mar 21  2018 ubuntu-advantage-tools
    -rwxr-xr-x   1 root root  214 Jun 26  2018 update-notifier-common
    /etc/cron.hourly:
    total 12
    drwxr-xr-x   2 root root 4096 Oct 17 23:01 .
    drwxr-xr-x 105 root root 4096 Dec  9 17:22 ..
    -rw-r--r--   1 root root  102 Nov 16  2017 .placeholder
    /etc/cron.monthly:
    total 12
    drwxr-xr-x   2 root root 4096 Oct 17 23:01 .
    drwxr-xr-x 105 root root 4096 Dec  9 17:22 ..
    -rw-r--r--   1 root root  102 Nov 16  2017 .placeholder
    /etc/cron.weekly:
    total 20
    drwxr-xr-x   2 root root 4096 Oct 17 23:03 .
    drwxr-xr-x 105 root root 4096 Dec  9 17:22 ..
    -rw-r--r--   1 root root  102 Nov 16  2017 .placeholder
    -rwxr-xr-x   1 root root  723 Jul 27  2018 man-db
    -rwxr-xr-x   1 root root  211 Jun 26  2018 update-notifier-common

[+] Writable cron dirs


[*] ENUMERATING USER AND ENVIRONMENTAL INFO...

[+] Logged in User Activity
    00:12:13 up  3:46,  0 users,  load average: 2.20, 1.95, 1.96
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT

[+] Super Users Found:
    root

[+] Environment
    SHLVL=1
    OLDPWD=/etc/webmin
    APACHE_RUN_DIR=/var/run/apache2
    APACHE_PID_FILE=/var/run/apache2/apache2.pid
    SELFAUTOLOC=/usr/bin
    JOURNAL_STREAM=9:19918
    _=/usr/bin/python
    SELFAUTODIR=/usr
    SELFAUTOPARENT=/
    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    INVOCATION_ID=289445dba8304e40b5109bd879ef5ed4
    APACHE_LOCK_DIR=/var/lock/apache2
    engine=pdftex
    LANG=C
    APACHE_RUN_GROUP=www-data
    APACHE_RUN_USER=www-data
    APACHE_LOG_DIR=/var/log/apache2
    SELFAUTOGRANDPARENT=/
    PWD=/tmp
    progname=pdflatex

[+] Root and current user history (depends on privs)

[+] Sudoers (privileged)

[+] All users
    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
    bin:x:2:2:bin:/bin:/usr/sbin/nologin
    sys:x:3:3:sys:/dev:/usr/sbin/nologin
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/usr/sbin/nologin
    man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
    lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
    mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
    news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
    uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
    proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
    www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
    backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
    list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
    irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
    gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
    nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
    systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
    systemd-network:x:101:103:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
    systemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
    syslog:x:103:108::/home/syslog:/usr/sbin/nologin
    _apt:x:104:65534::/nonexistent:/usr/sbin/nologin
    messagebus:x:105:109::/nonexistent:/usr/sbin/nologin
    uuidd:x:106:111::/run/uuidd:/usr/sbin/nologin
    landscape:x:107:113::/var/lib/landscape:/usr/sbin/nologin
    pollinate:x:108:1::/var/cache/pollinate:/bin/false
    systemd-coredump:x:998:998:systemd Core Dumper:/:/sbin/nologin
    sahay:x:1000:1000:choas:/home/sahay:/bin/bash
    lxd:x:999:100::/var/snap/lxd/common/lxd:/bin/false
    mysql:x:110:115:MySQL Server,,,:/nonexistent:/bin/false
    ayush:x:1001:1001:,,,:/home/ayush:/opt/rbash
    postfix:x:111:116::/var/spool/postfix:/usr/sbin/nologin
    dovecot:x:112:118:Dovecot mail server,,,:/usr/lib/dovecot:/usr/sbin/nologin
    dovenull:x:113:119:Dovecot login user,,,:/nonexistent:/usr/sbin/nologin

[+] Current User
    www-data

[+] Current User ID
    uid=33(www-data) gid=33(www-data) groups=33(www-data)

[*] ENUMERATING FILE AND DIRECTORY PERMISSIONS/CONTENTS...

[+] World Writeable Directories for User/Group 'Root'
    drwxrwxrwt 2 root root 4096 Feb  1 00:12 /tmp
    drwxrwxrwt 2 root root 40 Jan 31 20:26 /dev/mqueue
    drwxrwxrwt 2 root root 40 Jan 31 20:26 /dev/shm
    drwx-wx-wt 2 root root 4096 Oct 28 12:39 /var/lib/php/sessions
    drwxrwxrwt 2 root root 4096 Oct 17 23:03 /var/crash
    drwxrwxrwt 2 root root 4096 Jan 31 20:26 /var/tmp
    drwxrwxrwx 4 root root 4096 Oct 15 05:48 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/assets
    drwxrwxrwx 3 root root 4096 Jan 31 23:47 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/compile
    drwxrwxrwx 2 root root 4096 Jan 31 23:28 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/pdf
    drwxrwxrwx 3 root root 4096 Apr  8  2018 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/tex
    drwxrwxrwx 3 root root 4096 Apr  8  2018 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/source
    drwxrwxrwx 3 root root 4096 Apr  8  2018 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/doc
    drwxrwxrwx 5 root root 4096 Oct 26 04:16 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/templates
    drwxrwxrwt 2 root utmp 40 Jan 31 20:26 /run/screen
    drwxrwxrwt 5 root root 100 Jan 31 20:26 /run/lock

[+] World Writeable Directories for Users other than Root
    drwxrwxrwx 3 www-data www-data 4096 Oct 28 12:58 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/compile/.texlive2018
    drwxrwxrwx 3 www-data www-data 4096 Oct 28 12:58 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/compile/.texlive2018/texmf-var
    drwxrwxrwt 3 www-data www-data 4096 Oct 28 12:58 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/compile/.texlive2018/texmf-var/fonts
    drwxrwxrwt 3 www-data www-data 4096 Oct 28 12:58 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/compile/.texlive2018/texmf-var/fonts/pk
    drwxrwxrwt 3 www-data www-data 4096 Oct 28 12:58 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/compile/.texlive2018/texmf-var/fonts/pk/ljfour
    drwxrwxrwt 3 www-data www-data 4096 Oct 28 12:58 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/compile/.texlive2018/texmf-var/fonts/pk/ljfour/jknappen
    drwxrwxrwt 2 www-data www-data 4096 Oct 28 12:58 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/compile/.texlive2018/texmf-var/fonts/pk/ljfour/jknappen/ec

[+] World Writable Files
    -rw-rw-rw- 1 root root 0 Jan 31 20:26 /sys/kernel/security/apparmor/.remove
    -rw-rw-rw- 1 root root 0 Jan 31 20:26 /sys/kernel/security/apparmor/.replace
    -rw-rw-rw- 1 root root 0 Jan 31 20:26 /sys/kernel/security/apparmor/.load
    -rw-rw-rw- 1 root root 0 Jan 31 20:26 /sys/kernel/security/apparmor/.access
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/user.slice/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/apache2.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/open-vm-tools.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/systemd-networkd.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/systemd-udevd.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/cron.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/sys-fs-fuse-connections.mount/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/sys-kernel-config.mount/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/polkit.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/networkd-dispatcher.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/sys-kernel-debug.mount/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/accounts-daemon.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/webmin.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/systemd-journald.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/atd.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/dev-mqueue.mount/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/swap.img.swap/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/vgauth.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/mysql.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/rsyslog.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/proc-sys-fs-binfmt_misc.mount/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/lvm2-lvmetad.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/system-postfix.slice/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/system-postfix.slice/postfix@-.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/dovecot.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/systemd-resolved.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/dev-hugepages.mount/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/dbus.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/systemd-timesyncd.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/system-getty.slice/getty@tty1.service/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/system-getty.slice/cgroup.event_control
    --w--w--w- 1 root root 0 Jan 31 23:57 /sys/fs/cgroup/memory/system.slice/systemd-logind.service/cgroup.event_control
    -rwxrwxrwx 1 root root 279 Oct 15 05:44 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/config.php.sample
    -rwxrwxrwx 1 root root 1090 Oct 20 08:59 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/ajax.php
    -rwxrwxrwx 1 root root 254 Oct 26 04:08 /var/www/main/J00_w1ll_f1Nd_n07H1n9_H3r3/config.php

[+] Checking if root's home folder is accessible

[+] SUID/SGID Files and Directories
    -rwxr-sr-x 1 root utmp 10232 Mar 11  2016 /usr/lib/x86_64-linux-gnu/utempter/utempter
    -rwsr-xr-x 1 root root 14328 Jul 11  2018 /usr/lib/policykit-1/polkit-agent-helper-1
    -rwsr-xr-x 1 root root 10232 Mar 28  2017 /usr/lib/eject/dmcrypt-get-device
    -rwsr-xr-- 1 root messagebus 51184 Sep  6 03:56 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    drwxrwsr-x 3 root staff 4096 Oct 17 23:00 /usr/local/lib/python3.6
    drwxrwsr-x 2 root staff 4096 Oct 17 23:00 /usr/local/lib/python3.6/dist-packages
    drwxrwsr-x 4 root staff 4096 Oct 28 10:22 /usr/local/lib/python2.7
    drwxrwsr-x 2 root staff 4096 Oct 28 10:22 /usr/local/lib/python2.7/site-packages
    drwxrwsr-x 2 root staff 4096 Oct 28 10:15 /usr/local/lib/python2.7/dist-packages
    drwxrwsr-x 2 root staff 4096 Oct 28 10:09 /usr/local/share/texmf
    drwxrwsr-x 2 root staff 4096 Oct 28 10:10 /usr/local/share/fonts
    -rwxr-sr-x 1 root mlocate 43088 Mar  1  2018 /usr/bin/mlocate
    -rwsr-xr-x 1 root root 76496 Jan 25  2018 /usr/bin/chfn
    -rwsr-xr-x 1 root root 22544 Aug 29 08:25 /usr/bin/traceroute6.iputils
    -rwsr-xr-x 1 root root 44528 Jan 25  2018 /usr/bin/chsh
    -rwsr-xr-x 1 root root 59640 Jan 25  2018 /usr/bin/passwd
    -rwxr-sr-x 1 root shadow 22808 Jan 25  2018 /usr/bin/expiry
    -rwsr-sr-x 1 daemon daemon 51464 Feb 20  2018 /usr/bin/at
    -rwxr-sr-x 1 root crontab 39352 Nov 16  2017 /usr/bin/crontab
    -rwsr-xr-x 1 root root 40344 Jan 25  2018 /usr/bin/newgrp
    -rwxr-sr-x 1 root tty 34896 Oct 15 20:25 /usr/bin/wall
    -rwsr-xr-x 1 root root 22520 Jul 11  2018 /usr/bin/pkexec
    -rwxr-sr-x 1 root shadow 71816 Jan 25  2018 /usr/bin/chage
    -rwsr-xr-x 1 root root 157192 Aug 23 17:36 /usr/bin/sudo
    -rwxr-sr-x 1 root tty 14328 May  3  2018 /usr/bin/bsd-write
    -rwsr-xr-x 1 root root 75824 Jan 25  2018 /usr/bin/gpasswd
    -r-xr-sr-x 1 root postdrop 18552 Sep 12 06:06 /usr/sbin/postdrop
    -r-xr-sr-x 1 root postdrop 22600 Sep 12 06:06 /usr/sbin/postqueue
    drwxrwsr-x 2 root staff 4096 Oct 15 17:10 /var/local
    drwxrwsr-x 2 root mail 4096 Jan 31 23:28 /var/mail
    drwxr-sr-x+ 3 root systemd-journal 4096 Oct 28 09:23 /var/log/journal
    drwxr-sr-x+ 2 root systemd-journal 4096 Jan 31 20:26 /var/log/journal/d308d645eb304138a09b9be5a718027a
    drwx--s--- 2 postfix postdrop 4096 Jan 31 20:26 /var/spool/postfix/public
    -rwsr-xr-x 1 root root 146128 Nov 30  2017 /bin/ntfs-3g
    -rwsr-xr-x 1 root root 44664 Jan 25  2018 /bin/su
    -rwsr-xr-x 1 root root 34888 Oct 15 20:25 /bin/umount
    -rwsr-xr-x 1 root root 34896 Jul 30  2018 /bin/fusermount
    -rwsr-xr-x 1 root root 68520 Aug 29 08:25 /bin/ping
    -rwsr-xr-x 1 root root 47184 Oct 15 20:25 /bin/mount
    -rwxr-sr-x 1 root shadow 34816 Apr  5  2018 /sbin/pam_extrausers_chkpwd
    -rwxr-sr-x 1 root shadow 34816 Apr  5  2018 /sbin/unix_chkpwd

[+] Logs containing keyword 'password'
    /var/log/bootstrap.log:Shadow passwords are now on.
    /var/log/installer/subiquity-debug.log:2018-10-28 09:02:16,125 subiquity.views.identity:375 User input: {'hostname': 'chaos', 'realname': 'choas', 'username': 'sahay', 'password': '$6$6oS2Tckp2aVQfusC$r7.IrwH0fMbglM/U30vmEeKlKRAKjxqUTZWiCp8etjIzPRpvCgOtwR/mtgW/vISyVRY8lD0lKwrmPU5WVZNvf.'}
    /var/log/installer/subiquity-debug.log:2018-10-28 09:02:16,125 subiquity.controllers.identity:121 User input: {'hostname': 'chaos', 'realname': 'choas', 'username': 'sahay', 'password': '$6$6oS2Tckp2aVQfusC$r7.IrwH0fMbglM/U30vmEeKlKRAKjxqUTZWiCp8etjIzPRpvCgOtwR/mtgW/vISyVRY8lD0lKwrmPU5WVZNvf.'}
    /var/log/cloud-init.log:2018-10-28 09:25:30,219 - util.py[DEBUG]: Running hidden command to protect sensitive input/output logstring: ['useradd', 'sahay', '--comment', 'choas', '--groups', 'adm,cdrom,dip,plugdev,sudo', '--password', 'REDACTED', '--shell', '/bin/bash', '-m']
    /var/log/cloud-init.log:2018-10-28 09:26:59,183 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-10-28 09:26:59,185 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-10-28 09:26:59,186 - util.py[DEBUG]: Writing to /var/lib/cloud/instances/9e241a2a-5787-4236-a8ed-1af5df198351/sem/config_set_passwords - wb: [644] 25 bytes
    /var/log/cloud-init.log:2018-10-28 09:26:59,188 - helpers.py[DEBUG]: Running config-set-passwords using lock (<FileLock using file '/var/lib/cloud/instances/9e241a2a-5787-4236-a8ed-1af5df198351/sem/config_set_passwords'>)
    /var/log/cloud-init.log:2018-10-28 09:26:59,188 - cc_set_passwords.py[DEBUG]: Leaving ssh config 'PasswordAuthentication' unchanged. ssh_pwauth=None
    /var/log/cloud-init.log:2018-10-28 09:26:59,189 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords ran successfully
    /var/log/cloud-init.log:2018-10-28 09:53:55,884 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-10-28 09:53:55,886 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-10-28 09:53:55,887 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-10-28 09:53:55,887 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-10-28 12:01:52,934 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-10-28 12:01:52,935 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-10-28 12:01:52,936 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-10-28 12:01:52,936 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-10-28 13:04:00,858 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-10-28 13:04:00,865 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-10-28 13:04:00,865 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-10-28 13:04:00,865 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-11-22 21:54:24,474 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-22 21:54:24,475 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-22 21:54:24,475 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-11-22 21:54:24,475 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-11-24 23:18:11,577 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-24 23:18:11,577 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-24 23:18:11,577 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-11-24 23:18:11,577 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-11-24 23:43:08,782 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-24 23:43:08,782 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-24 23:43:08,782 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-11-24 23:43:08,782 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-11-25 00:01:17,590 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-25 00:01:17,590 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-25 00:01:17,590 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-11-25 00:01:17,590 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-11-25 00:21:47,983 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-25 00:21:47,984 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-25 00:21:47,984 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-11-25 00:21:47,984 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-11-25 00:59:52,872 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-25 00:59:52,873 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-11-25 00:59:52,873 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-11-25 00:59:52,873 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-12-09 17:17:53,452 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-12-09 17:17:53,453 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-12-09 17:17:53,453 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-12-09 17:17:53,453 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2018-12-09 17:24:28,418 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2018-12-09 17:24:28,418 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2018-12-09 17:24:28,418 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2018-12-09 17:24:28,418 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran
    /var/log/cloud-init.log:2019-01-31 20:26:34,561 - stages.py[DEBUG]: Running module set-passwords (<module 'cloudinit.config.cc_set_passwords' from '/usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py'>) with frequency once-per-instance
    /var/log/cloud-init.log:2019-01-31 20:26:34,567 - handlers.py[DEBUG]: start: modules-config/config-set-passwords: running config-set-passwords with frequency once-per-instance
    /var/log/cloud-init.log:2019-01-31 20:26:34,567 - helpers.py[DEBUG]: config-set-passwords already ran (freq=once-per-instance)
    /var/log/cloud-init.log:2019-01-31 20:26:34,567 - handlers.py[DEBUG]: finish: modules-config/config-set-passwords: SUCCESS: config-set-passwords previously ran

[+] Config files containing keyword 'password'
    /etc/dovecot/conf.d/10-auth.conf:# We also try to handle password changes automatically: If user's previous
    /etc/dovecot/conf.d/10-auth.conf:# TTL for negative hits (user not found, password mismatch).
    /etc/dovecot/conf.d/10-auth.conf:# Password database is used to verify user's password (and nothing more).
    /etc/dovecot/conf.d/10-auth.conf:#!include auth-checkpassword.conf.ext
    /etc/dovecot/conf.d/auth-static.conf.ext:# username or the password, or if there is a single password for all users:
    /etc/dovecot/conf.d/auth-static.conf.ext:#  - proxy frontend, where the backend verifies the password
    /etc/dovecot/conf.d/auth-static.conf.ext:#  - proxy backend, where the frontend already verified the password
    /etc/dovecot/conf.d/auth-static.conf.ext:#  args = proxy=y host=%1Mu.example.com nopassword=y
    /etc/dovecot/conf.d/auth-static.conf.ext:#  args = password=test
    /etc/dovecot/conf.d/10-logging.conf:# In case of password mismatches, log the attempted password. Valid values are
    /etc/dovecot/conf.d/10-logging.conf:# no, plain and sha1. sha1 can be useful for detecting brute force password
    /etc/dovecot/conf.d/10-logging.conf:# attempts vs. user simply trying the same password over and over again.
    /etc/dovecot/conf.d/10-logging.conf:#auth_verbose_passwords = no
    /etc/dovecot/conf.d/10-logging.conf:# In case of password mismatches, log the passwords and used scheme so the
    /etc/dovecot/conf.d/10-logging.conf:#auth_debug_passwords = no
    /etc/dovecot/conf.d/auth-system.conf.ext:# Shadow passwords for system users (NSS, /etc/shadow or similar).
    /etc/dovecot/conf.d/auth-checkpassword.conf.ext:# Authentication for checkpassword users. Included from 10-auth.conf.
    /etc/dovecot/conf.d/auth-checkpassword.conf.ext:  driver = checkpassword
    /etc/dovecot/conf.d/auth-checkpassword.conf.ext:  args = /usr/bin/checkpassword
    /etc/dovecot/conf.d/auth-checkpassword.conf.ext:# Standard checkpassword doesn't support direct userdb lookups.
    /etc/dovecot/conf.d/auth-checkpassword.conf.ext:# If you need checkpassword userdb, the checkpassword must support
    /etc/dovecot/conf.d/auth-checkpassword.conf.ext:#  driver = checkpassword
    /etc/dovecot/conf.d/auth-checkpassword.conf.ext:#  args = /usr/bin/checkpassword
    /etc/dovecot/conf.d/10-ssl.conf:# If key file is password protected, give the password here. Alternatively
    /etc/dovecot/conf.d/10-ssl.conf:# root owned 0600 file by using ssl_key_password = <path.
    /etc/dovecot/conf.d/10-ssl.conf:#ssl_key_password =
    /etc/debconf.conf:# World-readable, and accepts everything but passwords.
    /etc/debconf.conf:Reject-Type: password
    /etc/debconf.conf:# Not world readable (the default), and accepts only passwords.
    /etc/debconf.conf:Name: passwords
    /etc/debconf.conf:Accept-Type: password
    /etc/debconf.conf:Filename: /var/cache/debconf/passwords.dat
    /etc/debconf.conf:# databases, one to hold passwords and one for everything else.
    /etc/debconf.conf:Stack: config, passwords
    /etc/debconf.conf:# A remote LDAP database. It is also read-only. The password is really
    /etc/apache2/sites-available/default-ssl.conf:		#	 Note that no password is obtained from the user. Every entry in the user
    /etc/apache2/sites-available/default-ssl.conf:		#	 file needs this password: `xxj31ZMTZzkVA'.
    /etc/ssl/openssl.cnf:# input_password = secret
    /etc/ssl/openssl.cnf:# output_password = secret
    /etc/ssl/openssl.cnf:challengePassword		= A challenge password
    /etc/hdparm.conf:# --security-set-pass Set security password
    /etc/hdparm.conf:# security_pass = password
    /etc/hdparm.conf:# --user-master Select password to use
    /etc/postfix/main.cf.proto:# NOTE: if you use this feature for accounts not in the UNIX password
    /etc/postfix/main.cf.proto:# NOTE: if you use this feature for accounts not in the UNIX password
    /etc/postfix/main.cf.proto:# NOTE: if you use this feature for accounts not in the UNIX password
    /etc/ltrace.conf:; pwd.h
    /etc/mysql/mysql.conf.d/mysqld.cnf:# It has been reported that passwords should be enclosed with ticks/quotes
    /etc/mysql/my.cnf.fallback:# It has been reported that passwords should be enclosed with ticks/quotes
    /etc/overlayroot.conf:#       if not provided or empty, password is randomly generated
    /etc/overlayroot.conf:#       the generated password will be stored for recovery in
    /etc/overlayroot.conf:#     crypt:dev=/dev/vdb,pass=somepassword,mkfs=0
    /etc/overlayroot.conf:#    Randomly generated passwords are more secure, but you won't be able to
    /etc/overlayroot.conf:#    Randomly generated passwords are generated by calculating the sha512sum
    /etc/cloud/cloud.cfg: - set-passwords

[+] Shadow File (Privileged)

[*] ENUMERATING PROCESSES AND APPLICATIONS...

[+] Installed Packages
    Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
    Err?=(none)/Reinst-required (Status,Err:
    Name Version  Description
    accountsservice 0.6.45-1ubuntu1  query and manipulate user account information
    acpid 1:2.0.28-1ubuntu1  Advanced Configuration and Power Interface event daemon
    adduser 3.117ubuntu1  add and remove users and groups
    amd64-microcode 3.20180524.1ubuntu1  Processor microcode firmware for AMD CPUs
    apache2 2.4.34-1ubuntu2  Apache HTTP Server
    apache2-bin 2.4.34-1ubuntu2  Apache HTTP Server (modules and other binary files)
    apache2-data 2.4.34-1ubuntu2  Apache HTTP Server (common files)
    apache2-utils 2.4.34-1ubuntu2  Apache HTTP Server (utility programs for web servers)
    apparmor 2.12-4ubuntu8  user-space parser utility for AppArmor
    apport 2.20.10-0ubuntu13  automatically generate crash reports for debugging
    apport-symptoms 0.20  symptom scripts for apport
    apt 1.7.0  commandline package manager
    apt-show-versions 0.22.7ubuntu1  lists available package versions with distribution
    apt-utils 1.7.0  package management related utility programs
    at 3.1.20-3.1ubuntu2  Delayed job execution and batch processing
    base-files 10.1ubuntu7  Debian base system miscellaneous files
    base-passwd 3.5.45  Debian base system master password and group files
    bash 4.4.18-2ubuntu3  GNU Bourne Again SHell
    bash-completion 1:2.8-1ubuntu1  programmable completion for the bash shell
    bc 1.07.1-2  GNU bc arbitrary precision calculator language
    bcache-tools 1.0.8-3  bcache userspace tools
    bind9-host 1:9.11.4+dfsg-3ubuntu5  DNS lookup utility (deprecated)
    bsdmainutils 11.1.2ubuntu2  collection of more utilities from FreeBSD
    bsdutils 1:2.32-0.1ubuntu2  basic utilities from 4.4BSD-Lite
    btrfs-progs 4.16.1-2ubuntu1  Checksumming Copy on Write Filesystem utilities
    busybox-initramfs 1:1.27.2-2ubuntu4  Standalone shell setup for initramfs
    busybox-static 1:1.27.2-2ubuntu4  Standalone rescue shell with tons of builtin utilities
    byobu 5.127-0ubuntu1  text window manager, shell multiplexer, integrated DevOps environment
    bzip2 1.0.6-9  high-quality block-sorting file compressor - utilities
    ca-certificates 20180409  Common CA certificates
    cloud-guest-utils 0.30-0ubuntu9  cloud guest utilities
    cloud-init 18.4-7-g4652b196-0ubuntu1  Init scripts for cloud instances
    cloud-initramfs-copymods 0.43ubuntu1  copy initramfs modules into root filesystem for later use
    cloud-initramfs-dyn-netconf 0.43ubuntu1  write a network interface file in /run for BOOTIF
    command-not-found 18.10.0~pre1  Suggest installation of packages in interactive bash sessions
    command-not-found-data 18.10.0~pre1  Set of data files for command-not-found.
    composer 1.7.2-1  dependency manager for PHP
    console-setup 1.178ubuntu9  console font and keymap setup program
    console-setup-linux 1.178ubuntu9  Linux specific part of console-setup
    coreutils 8.28-1ubuntu2  GNU core utilities
    cpio 2.12+dfsg-6  GNU cpio -- a program to manage archives of files
    cpp 4:8.2.0-1ubuntu1  GNU C preprocessor (cpp)
    cpp-8 8.2.0-7ubuntu1  GNU C preprocessor
    crda 3.18-1build1  wireless Central Regulatory Domain Agent
    cron 3.0pl1-128.1ubuntu1  process scheduling daemon
    cryptsetup 2:2.0.4-2ubuntu2  transitional dummy package for cryptsetup-{run,initramfs}
    cryptsetup-bin 2:2.0.4-2ubuntu2  disk encryption support - command line tools
    cryptsetup-initramfs 2:2.0.4-2ubuntu2  disk encryption support - initramfs integration
    cryptsetup-run 2:2.0.4-2ubuntu2  disk encryption support - startup scripts
    curl 7.61.0-1ubuntu2.2  command line tool for transferring data with URL syntax
    dash 0.5.8-2.10  POSIX-compliant shell
    dbus 1.12.10-1ubuntu2  simple interprocess messaging system (daemon and utilities)
    debconf 1.5.69  Debian configuration management system
    debconf-i18n 1.5.69  full internationalization support for debconf
    debianutils 4.8.6  Miscellaneous utilities specific to Debian
    diffutils 1:3.6-1  File comparison utilities
    dirmngr 2.2.8-3ubuntu1  GNU privacy guard - network certificate management service
    distro-info-data 0.38ubuntu0.1  information about the distributions' releases (data files)
    dmeventd 2:1.02.145-4.1ubuntu3  Linux Kernel Device Mapper event daemon
    dmidecode 3.1-2  SMBIOS/DMI table decoder
    dmsetup 2:1.02.145-4.1ubuntu3  Linux Kernel Device Mapper userspace library
    dnsutils 1:9.11.4+dfsg-3ubuntu5  Clients provided with BIND
    dosfstools 4.1-2  utilities for making and checking MS-DOS FAT filesystems
    dovecot-core 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - core files
    dovecot-imapd 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - IMAP daemon
    dovecot-lmtpd 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - LMTP server
    dovecot-mysql 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - MySQL support
    dovecot-pop3d 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - POP3 daemon
    dpkg 1.19.0.5ubuntu5  Debian package management system
    e2fsprogs 1.44.4-2  ext2/ext3/ext4 file system utilities
    eatmydata 105-6  Library and utilities designed to disable fsync and friends
    ed 1.14.2-2  classic UNIX line editor
    eject 2.1.5+deb1+cvs20081104-13.2  ejects CDs and operates CD-Changers under Linux
    ethtool 1:4.16-1  display or change Ethernet device settings
    fdisk 2.32-0.1ubuntu2  collection of partitioning utilities
    file 1:5.34-2  Recognize the type of data in a file using "magic" numbers
    finalrd 3  final runtime directory for shutdown
    findutils 4.6.0+git+20180808-2  utilities for finding files--find, xargs
    fontconfig-config 2.13.0-5ubuntu3  generic font configuration library - configuration
    fonts-dejavu-core 2.37-1  Vera font family derivate with additional characters
    fonts-droid-fallback 1:6.0.1r16-1.1  handheld device font with extensive style and language support (fallback)
    fonts-lato 2.0-2  sans-serif typeface family font
    fonts-lmodern 2.004.5-5  OpenType fonts based on Computer Modern
    fonts-noto-mono 20171026-2  "No Tofu" monospaced font family with large Unicode coverage
    fonts-texgyre 20180621-2  OpenType fonts based on URW Fonts
    fonts-ubuntu-console 0.83-4  console version of the Ubuntu Mono font
    friendly-recovery 0.2.39  Make recovery boot mode more user-friendly
    ftp 0.17-34  classical file transfer client
    fuse 2.9.8-1ubuntu1  Filesystem in Userspace
    gawk 1:4.1.4+dfsg-1build1  GNU awk, a pattern scanning and processing language
    gcc-8-base:amd64 8.2.0-7ubuntu1  GCC, the GNU Compiler Collection (base package)
    gdisk 1.0.3-1build1  GPT fdisk text-mode partitioning tool
    geoip-database 20180315-1  IP lookup command line tools that use the GeoIP library (country database)
    gettext-base 0.19.8.1-8ubuntu0.1  GNU Internationalization utilities for the base system
    gir1.2-glib-2.0:amd64 1.58.0-1  Introspection data for GLib, GObject, Gio and GModule
    git 1:2.19.1-1ubuntu1.1  fast, scalable, distributed revision control system
    git-man 1:2.19.1-1ubuntu1.1  fast, scalable, distributed revision control system (manual pages)
    gnupg 2.2.8-3ubuntu1  GNU privacy guard - a free PGP replacement
    gnupg-l10n 2.2.8-3ubuntu1  GNU privacy guard - localization files
    gnupg-utils 2.2.8-3ubuntu1  GNU privacy guard - utility programs
    gpg 2.2.8-3ubuntu1  GNU Privacy Guard -- minimalist public key operations
    gpg-agent 2.2.8-3ubuntu1  GNU privacy guard - cryptographic agent
    gpg-wks-client 2.2.8-3ubuntu1  GNU privacy guard - Web Key Service client
    gpg-wks-server 2.2.8-3ubuntu1  GNU privacy guard - Web Key Service server
    gpgconf 2.2.8-3ubuntu1  GNU privacy guard - core configuration utilities
    gpgsm 2.2.8-3ubuntu1  GNU privacy guard - S/MIME version
    gpgv 2.2.8-3ubuntu1  GNU privacy guard - signature verification tool
    grep 3.1-2ubuntu1  GNU grep, egrep and fgrep
    groff-base 1.22.3-10  GNU troff text-formatting system (base system components)
    grub-common 2.02+dfsg1-5ubuntu8  GRand Unified Bootloader (common files)
    grub-gfxpayload-lists 0.7  GRUB gfxpayload blacklist
    grub-pc 2.02+dfsg1-5ubuntu8  GRand Unified Bootloader, version 2 (PC/BIOS version)
    grub-pc-bin 2.02+dfsg1-5ubuntu8  GRand Unified Bootloader, version 2 (PC/BIOS binaries)
    grub2-common 2.02+dfsg1-5ubuntu8  GRand Unified Bootloader (common files for version 2)
    gzip 1.6-5ubuntu2  GNU compression utilities
    hdparm 9.56+ds-2  tune hard disk parameters for high performance
    hostname 3.20  utility to set/show the host name or domain name
    htop 2.2.0-1  interactive processes viewer
    ifupdown 0.8.34ubuntu2  high level tools to configure network interfaces
    info 6.5.0.dfsg.1-4  Standalone GNU Info documentation browser
    init 1.54  metapackage ensuring an init system is installed
    init-system-helpers 1.54  helper tools for all init systems
    initramfs-tools 0.131ubuntu15  generic modular initramfs generator (automation)
    initramfs-tools-bin 0.131ubuntu15  binaries used by initramfs-tools
    initramfs-tools-core 0.131ubuntu15  generic modular initramfs generator (core tools)
    install-info 6.5.0.dfsg.1-4  Manage installed documentation in info format
    intel-microcode 3.20180807a.1  Processor microcode firmware for Intel CPUs
    iproute2 4.18.0-1ubuntu2  networking and traffic control tools
    iptables 1.6.1-2ubuntu2  administration tools for packet filtering and NAT
    iputils-ping 3:20180629-2ubuntu1  Tools to test the reachability of network hosts
    iputils-tracepath 3:20180629-2ubuntu1  Tools to trace the network path to a remote host
    irqbalance 1.3.0-0.1build1  Daemon to balance interrupts for SMP systems
    isc-dhcp-client 4.3.5-3ubuntu9  DHCP client for automatically obtaining an IP address
    isc-dhcp-common 4.3.5-3ubuntu9  common manpages relevant to all of the isc-dhcp packages
    iso-codes 3.79-1  ISO language, territory, currency, script codes and their translations
    iucode-tool 2.3.1-1  Intel processor microcode tool
    iw 4.14-0.1  tool for configuring Linux wireless devices
    javascript-common 11  Base support for JavaScript library packages
    jsonlint 1.7.1-1  validating parser of JSON data structures
    kbd 2.0.4-2ubuntu1  Linux console font and keytable utilities
    keyboard-configuration 1.178ubuntu9  system-wide keyboard preferences
    klibc-utils 2.0.4-9ubuntu2  small utilities built with klibc for early boot
    kmod 25-1ubuntu1  tools for managing Linux kernel modules
    krb5-locales 1.16-2ubuntu1  internationalization support for MIT Kerberos
    landscape-common 18.01-0ubuntu4  Landscape administration system client - Common files
    language-selector-common 0.190  Language selector for Ubuntu
    less 487-0.1build1  pager program similar to more
    libaccountsservice0:amd64 0.6.45-1ubuntu1  query and manipulate user account information - shared libraries
    libacl1:amd64 2.2.52-3build1  Access control list shared library
    libaio1:amd64 0.3.111-1  Linux kernel AIO access library - shared library
    libapache2-mod-php 1:7.2+62  server-side, HTML-embedded scripting language (Apache 2 module) (default)
    libapache2-mod-php7.2 7.2.10-0ubuntu1  server-side, HTML-embedded scripting language (Apache 2 module)
    libapparmor1:amd64 2.12-4ubuntu8  changehat AppArmor library
    libapr1:amd64 1.6.3-3  Apache Portable Runtime Library
    libaprutil1:amd64 1.6.1-3  Apache Portable Runtime Utility Library
    libaprutil1-dbd-sqlite3:amd64 1.6.1-3  Apache Portable Runtime Utility Library - SQLite3 Driver
    libaprutil1-ldap:amd64 1.6.1-3  Apache Portable Runtime Utility Library - LDAP Driver
    libapt-inst2.0:amd64 1.7.0  deb package format runtime library
    libapt-pkg-perl 0.1.34  Perl interface to libapt-pkg
    libapt-pkg5.0:amd64 1.7.0  package management runtime library
    libargon2-1:amd64 0~20171227-0.1  memory-hard hashing function - runtime library
    libasn1-8-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - ASN.1 library
    libassuan0:amd64 2.5.1-2  IPC library for the GnuPG components
    libatm1:amd64 1:2.5.1-2build1  shared library for ATM (Asynchronous Transfer Mode)
    libattr1:amd64 1:2.4.47-2build1  Extended attribute shared library
    libaudit-common 1:2.8.3-1ubuntu2  Dynamic library for security auditing - common files
    libaudit1:amd64 1:2.8.3-1ubuntu2  Dynamic library for security auditing
    libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
    libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
    libavahi-client3:amd64 0.7-4ubuntu2  Avahi client library
    libavahi-common-data:amd64 0.7-4ubuntu2  Avahi common data files
    libavahi-common3:amd64 0.7-4ubuntu2  Avahi common library
    libbind9-160:amd64 1:9.11.4+dfsg-3ubuntu5  BIND9 Shared Library used by BIND
    libblas3:amd64 3.8.0-1build1  Basic Linear Algebra Reference implementations, shared library
    libblkid1:amd64 2.32-0.1ubuntu2  block device ID library
    libbrotli1:amd64 1.0.6-1  library implementing brotli encoder and decoder (shared libraries)
    libbsd0:amd64 0.9.1-1  utility functions from BSD systems - shared library
    libbz2-1.0:amd64 1.0.6-9  high-quality block-sorting file compressor library - runtime
    libc-bin 2.28-0ubuntu1  GNU C Library: Binaries
    libc6:amd64 2.28-0ubuntu1  GNU C Library: Shared libraries
    libcairo2:amd64 1.15.12-1  Cairo 2D vector graphics library
    libcap-ng0:amd64 0.7.9-1  An alternate POSIX capabilities library
    libcap2:amd64 1:2.25-1.2  POSIX 1003.1e capabilities (library)
    libcap2-bin 1:2.25-1.2  POSIX 1003.1e capabilities (utilities)
    libcgi-fast-perl 1:2.13-1  CGI subclass for work with FCGI
    libcgi-pm-perl 4.40-1  module for Common Gateway Interface applications
    libcom-err2:amd64 1.44.4-2  common error description library
    libcryptsetup12:amd64 2:2.0.4-2ubuntu2  disk encryption support - shared library
    libcups2:amd64 2.2.8-5ubuntu1  Common UNIX Printing System(tm) - Core library
    libcupsfilters1:amd64 1.21.3-1  OpenPrinting CUPS Filters - Shared library
    libcupsimage2:amd64 2.2.8-5ubuntu1  Common UNIX Printing System(tm) - Raster image library
    libcurl3-gnutls:amd64 7.61.0-1ubuntu2.2  easy-to-use client-side URL transfer library (GnuTLS flavour)
    libcurl4:amd64 7.61.0-1ubuntu2.2  easy-to-use client-side URL transfer library (OpenSSL flavour)
    libdata-dump-perl 1.23-1  Perl module to help dump data structures
    libdb5.3:amd64 5.3.28+dfsg1-0.1  Berkeley v5.3 Database Libraries [runtime]
    libdbus-1-3:amd64 1.12.10-1ubuntu2  simple interprocess messaging system (library)
    libdbus-glib-1-2:amd64 0.110-3  deprecated library for D-Bus IPC
    libdebconfclient0:amd64 0.244ubuntu1  Debian Configuration Management System (C-implementation library)
    libdevmapper-event1.02.1:amd64 2:1.02.145-4.1ubuntu3  Linux Kernel Device Mapper event support library
    libdevmapper1.02.1:amd64 2:1.02.145-4.1ubuntu3  Linux Kernel Device Mapper userspace library
    libdns-export1102 1:9.11.4+dfsg-3ubuntu5  Exported DNS Shared Library
    libdns1102:amd64 1:9.11.4+dfsg-3ubuntu5  DNS Shared Library used by BIND
    libdrm-amdgpu1:amd64 2.4.95-1  Userspace interface to amdgpu-specific kernel DRM services -- runtime
    libdrm-common 2.4.95-1  Userspace interface to kernel DRM services -- common files
    libdrm-intel1:amd64 2.4.95-1  Userspace interface to intel-specific kernel DRM services -- runtime
    libdrm-nouveau2:amd64 2.4.95-1  Userspace interface to nouveau-specific kernel DRM services -- runtime
    libdrm-radeon1:amd64 2.4.95-1  Userspace interface to radeon-specific kernel DRM services -- runtime
    libdrm2:amd64 2.4.95-1  Userspace interface to kernel DRM services -- runtime
    libdumbnet1:amd64 1.12-7build1  dumb, portable networking library -- shared library
    libeatmydata1:amd64 105-6  Library and utilities to disable fsync and friends - shared library
    libedit2:amd64 3.1-20180525-1  BSD editline and history libraries
    libelf1:amd64 0.170-0.5  library to read and write ELF files
    libencode-locale-perl 1.05-1  utility to determine the locale encoding
    liberror-perl 0.17026-1  Perl module for error/exception handling in an OO-ish way
    libestr0:amd64 0.1.10-2.1  Helper functions for handling strings (lib)
    libevent-2.1-6:amd64 2.1.8-stable-4build1  Asynchronous event notification library
    libevent-core-2.1-6:amd64 2.1.8-stable-4build1  Asynchronous event notification library (core)
    libexpat1:amd64 2.2.6-1  XML parsing C library - runtime library
    libext2fs2:amd64 1.44.4-2  ext2/ext3/ext4 file system libraries
    libexttextcat-2.0-0:amd64 3.4.5-1  Language detection library
    libexttextcat-data 3.4.5-1  Language detection library - data files
    libfastjson4:amd64 0.99.8-2  fast json library for C
    libfcgi-perl 0.78-2build1  helper module for FastCGI
    libfdisk1:amd64 2.32-0.1ubuntu2  fdisk partitioning library
    libffi6:amd64 3.2.1-8  Foreign Function Interface library runtime
    libfile-basedir-perl 0.08-1  Perl module to use the freedesktop basedir specification
    libfile-desktopentry-perl 0.22-1  Perl module to handle freedesktop .desktop files
    libfile-listing-perl 6.04-1  module to parse directory listings
    libfile-mimeinfo-perl 0.29-1  Perl module to determine file types
    libfont-afm-perl 1.20-2  Font::AFM - Interface to Adobe Font Metrics files
    libfontconfig1:amd64 2.13.0-5ubuntu3  generic font configuration library - runtime
    libfontenc1:amd64 1:1.1.3-1  X11 font encoding library
    libfreetype6:amd64 2.8.1-2ubuntu2  FreeType 2 font engine, shared library files
    libfribidi0:amd64 1.0.5-3  Free Implementation of the Unicode BiDi algorithm
    libfuse2:amd64 2.9.8-1ubuntu1  Filesystem in Userspace (library)
    libgcc1:amd64 1:8.2.0-7ubuntu1  GCC support library
    libgcrypt20:amd64 1.8.3-1ubuntu1  LGPL Crypto library - runtime library
    libgdbm-compat4:amd64 1.14.1-6  GNU dbm database routines (legacy support runtime version)
    libgdbm5:amd64 1.14.1-6  GNU dbm database routines (runtime version)
    libgeoip1:amd64 1.6.12-1  non-DNS IP-to-country resolver library
    libgirepository-1.0-1:amd64 1.58.0-1  Library for handling GObject introspection data (runtime library)
    libgl1:amd64 1.1.0-1  Vendor neutral GL dispatch library -- legacy GL support
    libgl1-mesa-dri:amd64 18.2.2-0ubuntu1  free implementation of the OpenGL API -- DRI modules
    libglapi-mesa:amd64 18.2.2-0ubuntu1  free implementation of the GL API -- shared library
    libglib2.0-0:amd64 2.58.1-2  GLib library of C routines
    libglib2.0-data 2.58.1-2  Common files for GLib library
    libglvnd0:amd64 1.1.0-1  Vendor neutral GL dispatch library
    libglx-mesa0:amd64 18.2.2-0ubuntu1  free implementation of the OpenGL API -- GLX vendor library
    libglx0:amd64 1.1.0-1  Vendor neutral GL dispatch library -- GLX support
    libgmp10:amd64 2:6.1.2+dfsg-3  Multiprecision arithmetic library
    libgnutls30:amd64 3.6.4-2ubuntu1  GNU TLS library - main runtime library
    libgpg-error0:amd64 1.32-1  GnuPG development runtime library
    libgpm2:amd64 1.20.7-5  General Purpose Mouse - shared library
    libgraphite2-3:amd64 1.3.12-1  Font rendering engine for Complex Scripts -- library
    libgs9:amd64 9.26~dfsg+0-0ubuntu0.18.10.3  interpreter for the PostScript language and for PDF - Library
    libgs9-common 9.26~dfsg+0-0ubuntu0.18.10.3  interpreter for the PostScript language and for PDF - common files
    libgssapi-krb5-2:amd64 1.16-2ubuntu1  MIT Kerberos runtime libraries - krb5 GSS-API Mechanism
    libgssapi3-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - GSSAPI support library
    libharfbuzz-icu0:amd64 1.8.8-2  OpenType text shaping engine ICU backend
    libharfbuzz0b:amd64 1.8.8-2  OpenType text shaping engine (shared library)
    libhcrypto4-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - crypto library
    libheimbase1-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - Base library
    libheimntlm0-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - NTLM support library
    libhogweed4:amd64 3.4-1  low level cryptographic library (public-key cryptos)
    libhtml-form-perl 6.03-1  module that represents an HTML form element
    libhtml-format-perl 2.12-1  module for transforming HTML into various formats
    libhtml-parser-perl 3.72-3build1  collection of modules that parse HTML text documents
    libhtml-tagset-perl 3.20-3  Data tables pertaining to HTML
    libhtml-template-perl 2.97-1  module for using HTML templates with Perl
    libhtml-tree-perl 5.07-1  Perl module to represent and create HTML syntax trees
    libhttp-cookies-perl 6.04-1  HTTP cookie jars
    libhttp-daemon-perl 6.01-1  simple http server class
    libhttp-date-perl 6.02-1  module of date conversion routines
    libhttp-message-perl 6.18-1  perl interface to HTTP style messages
    libhttp-negotiate-perl 6.01-1  implementation of content negotiation
    libhx509-5-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - X509 support library
    libice6:amd64 2:1.0.9-2  X11 Inter-Client Exchange library
    libicu60:amd64 60.2-6ubuntu1  International Components for Unicode
    libidn11:amd64 1.33-2.2ubuntu1  GNU Libidn library, implementation of IETF IDN specifications
    libidn2-0:amd64 2.0.5-1  Internationalized domain names (IDNA2008/TR46) library
    libijs-0.35:amd64 0.35-13  IJS raster image transport protocol: shared library
    libio-html-perl 1.001-1  open an HTML file with automatic charset detection
    libio-pty-perl 1:1.08-1.1build4  Perl module for pseudo tty IO
    libio-socket-ssl-perl 2.060-3  Perl module implementing object oriented interface to SSL sockets
    libio-stringy-perl 2.111-2  Perl modules for IO from scalars and arrays
    libip4tc0:amd64 1.6.1-2ubuntu2  netfilter libip4tc library
    libip6tc0:amd64 1.6.1-2ubuntu2  netfilter libip6tc library
    libipc-system-simple-perl 1.25-4  Perl module to run commands simply, with detailed diagnostics
    libiptc0:amd64 1.6.1-2ubuntu2  netfilter libiptc library
    libirs160:amd64 1:9.11.4+dfsg-3ubuntu5  DNS Shared Library used by BIND
    libisc-export169:amd64 1:9.11.4+dfsg-3ubuntu5  Exported ISC Shared Library
    libisc169:amd64 1:9.11.4+dfsg-3ubuntu5  ISC Shared Library used by BIND
    libisccc160:amd64 1:9.11.4+dfsg-3ubuntu5  Command Channel Library used by BIND
    libisccfg160:amd64 1:9.11.4+dfsg-3ubuntu5  Config File Handling Library used by BIND
    libisl19:amd64 0.20-2  manipulating sets and relations of integer points bounded by linear constraints
    libisns0:amd64 0.97-2build1  Internet Storage Name Service - shared libraries
    libjbig0:amd64 2.1-3.1build1  JBIGkit libraries
    libjbig2dec0:amd64 0.15-1  JBIG2 decoder library - shared libraries
    libjpeg-turbo8:amd64 2.0.0-0ubuntu2  IJG JPEG compliant runtime library.
    libjpeg8:amd64 8c-2ubuntu8  Independent JPEG Group's JPEG runtime library (dependency package)
    libjs-jquery 3.2.1-1  JavaScript library for dynamic web applications
    libjson-c3:amd64 0.12.1-1.3  JSON manipulation library - shared library
    libk5crypto3:amd64 1.16-2ubuntu1  MIT Kerberos runtime libraries - Crypto Library
    libkeyutils1:amd64 1.5.9-9.3  Linux Key Management Utilities (library)
    libklibc 2.0.4-9ubuntu2  minimal libc subset for use with initramfs
    libkmod2:amd64 25-1ubuntu1  libkmod shared library
    libkpathsea6:amd64 2018.20180824.48463-1ubuntu0.1  TeX Live: path search library for TeX (runtime part)
    libkrb5-26-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - libraries
    libkrb5-3:amd64 1.16-2ubuntu1  MIT Kerberos runtime libraries
    libkrb5support0:amd64 1.16-2ubuntu1  MIT Kerberos runtime libraries - Support library
    libksba8:amd64 1.3.5-2  X.509 and CMS support library
    libldap-2.4-2:amd64 2.4.46+dfsg-5ubuntu1  OpenLDAP libraries
    libldap-common 2.4.46+dfsg-5ubuntu1  OpenLDAP common files for libraries
    liblinear3:amd64 2.1.0+dfsg-2  Library for Large Linear Classification
    libllvm7:amd64 1:7-3  Modular compiler and toolchain technologies, runtime library
    liblocale-gettext-perl 1.07-3build2  module using libc functions for internationalization in Perl
    liblua5.2-0:amd64 5.2.4-1.1build1  Shared library for the Lua interpreter version 5.2
    liblua5.3-0:amd64 5.3.3-1  Shared library for the Lua interpreter version 5.3
    liblvm2app2.2:amd64 2.02.176-4.1ubuntu3  LVM2 application library
    liblvm2cmd2.02:amd64 2.02.176-4.1ubuntu3  LVM2 command library
    liblwp-mediatypes-perl 6.02-1  module to guess media type for a file or a URL
    liblwp-protocol-https-perl 6.07-2  HTTPS driver for LWP::UserAgent
    liblwres160:amd64 1:9.11.4+dfsg-3ubuntu5  Lightweight Resolver Library used by BIND
    liblz4-1:amd64 1.8.2-1ubuntu1  Fast LZ compression algorithm library - runtime
    liblzma5:amd64 5.2.2-1.3  XZ-format compression library
    liblzo2-2:amd64 2.10-0.1  data compression library
    libmagic-mgc 1:5.34-2  File type determination library using "magic" numbers (compiled magic file)
    libmagic1:amd64 1:5.34-2  Recognize the type of data in a file using "magic" numbers - library
    libmailtools-perl 2.18-1  Manipulate email in perl programs
    libmnl0:amd64 1.0.4-2  minimalistic Netlink communication library
    libmount1:amd64 2.32-0.1ubuntu2  device mounting library
    libmpc3:amd64 1.1.0-1  multiple precision complex floating-point library
    libmpdec2:amd64 2.4.2-2  library for decimal floating point arithmetic (runtime library)
    libmpfr6:amd64 4.0.1-1  multiple precision floating-point computation
    libmspack0:amd64 0.7-1ubuntu0.1  library for Microsoft compression formats (shared library)
    libmysqlclient20:amd64 5.7.24-0ubuntu0.18.10.1  MySQL database client library
    libncurses6:amd64 6.1+20180210-4ubuntu1  shared libraries for terminal handling
    libncursesw6:amd64 6.1+20180210-4ubuntu1  shared libraries for terminal handling (wide character support)
    libnet-dbus-perl 1.1.0-4build2  Perl extension for the DBus bindings
    libnet-http-perl 6.18-1  module providing low-level HTTP connection client
    libnet-smtp-ssl-perl 1.04-1  Perl module providing SSL support to Net::SMTP
    libnet-ssleay-perl 1.85-2ubuntu2  Perl module for Secure Sockets Layer (SSL)
    libnetfilter-conntrack3:amd64 1.0.7-1  Netfilter netlink-conntrack library
    libnettle6:amd64 3.4-1  low level cryptographic library (symmetric and one-way cryptos)
    libnewt0.52:amd64 0.52.20-6ubuntu1  Not Erik's Windowing Toolkit - text mode windowing with slang
    libnfnetlink0:amd64 1.0.1-3  Netfilter netlink library
    libnghttp2-14:amd64 1.32.1-1build1  library implementing HTTP/2 protocol (shared library)
    libnl-3-200:amd64 3.4.0-1  library for dealing with netlink sockets
    libnl-genl-3-200:amd64 3.4.0-1  library for dealing with netlink sockets - generic netlink
    libnpth0:amd64 1.6-1  replacement for GNU Pth using system threads
    libnspr4:amd64 2:4.18-1ubuntu1  NetScape Portable Runtime Library
    libnss-systemd:amd64 239-7ubuntu10.4  nss module providing dynamic user and group name resolution
    libnss3:amd64 2:3.36.1-1ubuntu1  Network Security Service libraries
    libntfs-3g88 1:2017.3.23-2  read/write NTFS driver for FUSE (runtime library)
    libnuma1:amd64 2.0.11-2.2  Libraries for controlling NUMA policy
    libp11-kit0:amd64 0.23.14-2  library for loading and coordinating access to PKCS#11 modules - runtime
    libpam-cap:amd64 1:2.25-1.2  POSIX 1003.1e capabilities (PAM module)
    libpam-modules:amd64 1.1.8-3.6ubuntu2  Pluggable Authentication Modules for PAM
    libpam-modules-bin 1.1.8-3.6ubuntu2  Pluggable Authentication Modules for PAM - helper binaries
    libpam-runtime 1.1.8-3.6ubuntu2  Runtime support for the PAM library
    libpam-systemd:amd64 239-7ubuntu10.4  system and service manager - PAM module
    libpam0g:amd64 1.1.8-3.6ubuntu2  Pluggable Authentication Modules library
    libpaper-utils 1.1.24+nmu5ubuntu1  library for handling paper characteristics (utilities)
    libpaper1:amd64 1.1.24+nmu5ubuntu1  library for handling paper characteristics
    libparted2:amd64 3.2-21ubuntu1  disk partition manipulator - shared library
    libpcap0.8:amd64 1.8.1-6ubuntu1  system interface for user-level packet capture
    libpci3:amd64 1:3.5.2-1ubuntu2  Linux PCI Utilities (shared library)
    libpciaccess0:amd64 0.14-1  Generic PCI access library for X
    libpcre3:amd64 2:8.39-11  Old Perl 5 Compatible Regular Expression Library - runtime files
    libperl5.26:amd64 5.26.2-7ubuntu0.1  shared Perl library
    libpipeline1:amd64 1.5.0-1  pipeline manipulation library
    libpixman-1-0:amd64 0.34.0-2  pixel-manipulation library for X and cairo
    libplymouth4:amd64 0.9.3-1ubuntu10  graphical boot animation and logger - shared libraries
    libpng16-16:amd64 1.6.34-2  PNG library - runtime (version 1.6)
    libpolkit-agent-1-0:amd64 0.105-21  PolicyKit Authentication Agent API
    libpolkit-backend-1-0:amd64 0.105-21  PolicyKit backend API
    libpolkit-gobject-1-0:amd64 0.105-21  PolicyKit Authorization API
    libpopt0:amd64 1.16-11  lib for parsing cmdline parameters
    libpotrace0:amd64 1.15-1  library for tracing bitmaps
    libprocps7:amd64 2:3.3.15-2ubuntu1  library for accessing process information from /proc
    libpsl5:amd64 0.20.2-1  Library for Public Suffix List (shared libraries)
    libptexenc1:amd64 2018.20180824.48463-1ubuntu0.1  TeX Live: pTeX encoding library
    libpython-stdlib:amd64 2.7.15-3  interactive high-level object-oriented language (Python2)
    libpython2-stdlib:amd64 2.7.15-3  interactive high-level object-oriented language (Python2)
    libpython2.7-minimal:amd64 2.7.15-4ubuntu4  Minimal subset of the Python language (version 2.7)
    libpython2.7-stdlib:amd64 2.7.15-4ubuntu4  Interactive high-level object-oriented language (standard library, version 2.7)
    libpython3-stdlib:amd64 3.6.6-1  interactive high-level object-oriented language (default python3 version)
    libpython3.6:amd64 3.6.7~rc1-1  Shared Python runtime library (version 3.6)
    libpython3.6-minimal:amd64 3.6.7~rc1-1  Minimal subset of the Python language (version 3.6)
    libpython3.6-stdlib:amd64 3.6.7~rc1-1  Interactive high-level object-oriented language (standard library, version 3.6)
    libreadline5:amd64 5.2+dfsg-3build2  GNU readline and history libraries, run-time libraries
    libreadline7:amd64 7.0-5  GNU readline and history libraries, run-time libraries
    libroken18-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - roken support library
    librtmp1:amd64 2.4+20151223.gitfa8646d.1-2  toolkit for RTMP streams (shared library)
    libruby2.5:amd64 2.5.1-5ubuntu4.1  Libraries necessary to run Ruby 2.5
    libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
    libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
    libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
    libseccomp2:amd64 2.3.3-3ubuntu1  high level interface to Linux seccomp filter
    libselinux1:amd64 2.8-1build1  SELinux runtime shared libraries
    libsemanage-common 2.8-1build1  Common files for SELinux policy management libraries
    libsemanage1:amd64 2.8-1build1  SELinux policy management library
    libsensors4:amd64 1:3.4.0-4  library to read temperature/voltage/fan sensors
    libsepol1:amd64 2.8-1  SELinux library for manipulating binary security policies
    libsigsegv2:amd64 2.12-2  Library for handling page faults in a portable way
    libslang2:amd64 2.3.2-1ubuntu1  S-Lang programming library - runtime version
    libsm6:amd64 2:1.2.2-1  X11 Session Management library
    libsmartcols1:amd64 2.32-0.1ubuntu2  smart column output alignment library
    libsodium23:amd64 1.0.16-2build1  Network communication, cryptography and signaturing library
    libsqlite3-0:amd64 3.24.0-1  SQLite 3 shared library
    libss2:amd64 1.44.4-2  command-line interface parsing library
    libssl1.0.0:amd64 1.0.2n-1ubuntu6  Secure Sockets Layer toolkit - shared libraries
    libssl1.1:amd64 1.1.1-1ubuntu2.1  Secure Sockets Layer toolkit - shared libraries
    libstdc++6:amd64 8.2.0-7ubuntu1  GNU Standard C++ Library v3
    libstemmer0d:amd64 0+svn585-1build1  Snowball stemming algorithms for use in Information Retrieval
    libsynctex2:amd64 2018.20180824.48463-1ubuntu0.1  TeX Live: SyncTeX parser library
    libsystemd0:amd64 239-7ubuntu10.4  systemd utility library
    libtasn1-6:amd64 4.13-3  Manage ASN.1 structures (runtime)
    libtcl8.6:amd64 8.6.8+dfsg-4  Tcl (the Tool Command Language) v8.6 - run-time library files
    libtexlua52:amd64 2018.20180824.48463-1ubuntu0.1  TeX Live: Lua 5.2, modified for use with LuaTeX
    libtexlua53:amd64 2018.20180824.48463-1ubuntu0.1  TeX Live: Lua 5.3, modified for use with LuaTeX
    libtexluajit2:amd64 2018.20180824.48463-1ubuntu0.1  TeX Live: LuaJIT, modified for use with LuaJITTeX
    libtext-charwidth-perl 0.04-7.1  get display widths of characters on the terminal
    libtext-iconv-perl 1.7-5build6  converts between character sets in Perl
    libtext-wrapi18n-perl 0.06-7.1  internationalized substitute of Text::Wrap
    libtie-ixhash-perl 1.23-2  Perl module to order associative arrays
    libtiff5:amd64 4.0.9-6  Tag Image File Format (TIFF) library
    libtimedate-perl 2.3000-2  collection of modules to manipulate date/time information
    libtinfo6:amd64 6.1+20180210-4ubuntu1  shared low-level terminfo library for terminal handling
    libtk8.6:amd64 8.6.8-4  Tk toolkit for Tcl and X11 v8.6 - run-time files
    libtry-tiny-perl 0.30-1  module providing minimalistic try/catch
    libudev1:amd64 239-7ubuntu10.4  libudev shared library
    libunistring2:amd64 0.9.10-1ubuntu1  Unicode string library for C
    libunwind8:amd64 1.2.1-8  library to determine the call-chain of a program - runtime
    liburi-perl 1.74-1  module to manipulate and access URI strings
    libusb-1.0-0:amd64 2:1.0.22-2  userspace USB programming library
    libutempter0:amd64 1.1.6-3  privileged helper for utmp/wtmp updates (runtime)
    libuuid1:amd64 2.32-0.1ubuntu2  Universally Unique ID library
    libwind0-heimdal:amd64 7.5.0+dfsg-2  Heimdal Kerberos - stringprep implementation
    libwrap0:amd64 7.6.q-27  Wietse Venema's TCP wrappers library
    libwww-perl 6.35-2  simple and consistent interface to the world-wide web
    libwww-robotrules-perl 6.02-1  database of robots.txt-derived permissions
    libx11-6:amd64 2:1.6.7-1  X11 client-side library
    libx11-data 2:1.6.7-1  X11 client-side library
    libx11-protocol-perl 0.56-7  Perl module for the X Window System Protocol, version 11
    libx11-xcb1:amd64 2:1.6.7-1  Xlib/XCB interface library
    libxau6:amd64 1:1.0.8-1  X11 authorisation library
    libxaw7:amd64 2:1.0.13-1  X11 Athena Widget library
    libxcb-dri2-0:amd64 1.13.1-1  X C Binding, dri2 extension
    libxcb-dri3-0:amd64 1.13.1-1  X C Binding, dri3 extension
    libxcb-glx0:amd64 1.13.1-1  X C Binding, glx extension
    libxcb-present0:amd64 1.13.1-1  X C Binding, present extension
    libxcb-render0:amd64 1.13.1-1  X C Binding, render extension
    libxcb-shape0:amd64 1.13.1-1  X C Binding, shape extension
    libxcb-shm0:amd64 1.13.1-1  X C Binding, shm extension
    libxcb-sync1:amd64 1.13.1-1  X C Binding, sync extension
    libxcb1:amd64 1.13.1-1  X C Binding
    libxcomposite1:amd64 1:0.4.4-2  X11 Composite extension library
    libxcursor1:amd64 1:1.1.15-1  X cursor management library
    libxdamage1:amd64 1:1.1.4-3  X11 damaged region extension library
    libxdmcp6:amd64 1:1.1.2-3  X11 Display Manager Control Protocol library
    libxext6:amd64 2:1.3.3-1  X11 miscellaneous extension library
    libxfixes3:amd64 1:5.0.3-1  X11 miscellaneous 'fixes' extension library
    libxft2:amd64 2.3.2-2  FreeType-based font drawing library for X
    libxi6:amd64 2:1.7.9-1  X11 Input extension library
    libxinerama1:amd64 2:1.1.4-1  X11 Xinerama extension library
    libxml-parser-perl 2.44-2build3  Perl module for parsing XML files
    libxml-twig-perl 1:3.50-1  Perl module for processing huge XML documents in tree mode
    libxml-xpathengine-perl 0.14-1  re-usable XPath engine for DOM-like trees
    libxml2:amd64 2.9.4+dfsg1-7ubuntu1  GNOME XML library
    libxmlsec1:amd64 1.2.26-3  XML security library
    libxmlsec1-openssl:amd64 1.2.26-3  Openssl engine for the XML security library
    libxmu6:amd64 2:1.1.2-2  X11 miscellaneous utility library
    libxmuu1:amd64 2:1.1.2-2  X11 miscellaneous micro-utility library
    libxpm4:amd64 1:3.5.12-1  X11 pixmap library
    libxrandr2:amd64 2:1.5.1-1  X11 RandR extension library
    libxrender1:amd64 1:0.9.10-1  X Rendering Extension client library
    libxshmfence1:amd64 1.3-1  X shared memory fences - shared library
    libxslt1.1:amd64 1.1.32-2  XSLT 1.0 processing library - runtime library
    libxss1:amd64 1:1.2.3-1  X11 Screen Saver extension library
    libxt6:amd64 1:1.1.5-1  X11 toolkit intrinsics library
    libxtables12:amd64 1.6.1-2ubuntu2  netfilter xtables library
    libxtst6:amd64 2:1.2.3-1  X11 Testing -- Record extension library
    libxv1:amd64 2:1.0.11-1  X11 Video extension library
    libxxf86dga1:amd64 2:1.1.4-1  X11 Direct Graphics Access extension library
    libxxf86vm1:amd64 1:1.1.4-1  X11 XFree86 video mode extension library
    libyaml-0-2:amd64 0.2.1-1  Fast YAML 1.1 parser and emitter library
    libzip4:amd64 1.1.2-1.1  library for reading, creating, and modifying zip archives (runtime)
    libzstd1:amd64 1.3.5+dfsg-1ubuntu1  fast lossless compression algorithm
    libzzip-0-13:amd64 0.13.62-3.1ubuntu1  library providing read access on ZIP-archives - library
    linux-base 4.5ubuntu1  Linux image base package
    linux-firmware 1.175  Firmware for Linux kernel drivers
    linux-generic 4.18.0.12.13  Complete Generic Linux kernel and headers
    linux-headers-4.18.0-10 4.18.0-10.11  Header files related to Linux kernel version 4.18.0
    linux-headers-4.18.0-10-generic 4.18.0-10.11  Linux kernel headers for version 4.18.0 on 64 bit x86 SMP
    linux-headers-4.18.0-11 4.18.0-11.12  Header files related to Linux kernel version 4.18.0
    linux-headers-4.18.0-11-generic 4.18.0-11.12  Linux kernel headers for version 4.18.0 on 64 bit x86 SMP
    linux-headers-4.18.0-12 4.18.0-12.13  Header files related to Linux kernel version 4.18.0
    linux-headers-4.18.0-12-generic 4.18.0-12.13  Linux kernel headers for version 4.18.0 on 64 bit x86 SMP
    linux-headers-generic 4.18.0.12.13  Generic Linux kernel headers
    linux-image-4.18.0-10-generic 4.18.0-10.11  Signed kernel image generic
    linux-image-4.18.0-11-generic 4.18.0-11.12  Signed kernel image generic
    linux-image-4.18.0-12-generic 4.18.0-12.13  Signed kernel image generic
    linux-image-generic 4.18.0.12.13  Generic Linux kernel image
    linux-modules-4.18.0-10-generic 4.18.0-10.11  Linux kernel extra modules for version 4.18.0 on 64 bit x86 SMP
    linux-modules-4.18.0-11-generic 4.18.0-11.12  Linux kernel extra modules for version 4.18.0 on 64 bit x86 SMP
    linux-modules-4.18.0-12-generic 4.18.0-12.13  Linux kernel extra modules for version 4.18.0 on 64 bit x86 SMP
    linux-modules-extra-4.18.0-10-generic 4.18.0-10.11  Linux kernel extra modules for version 4.18.0 on 64 bit x86 SMP
    linux-modules-extra-4.18.0-11-generic 4.18.0-11.12  Linux kernel extra modules for version 4.18.0 on 64 bit x86 SMP
    linux-modules-extra-4.18.0-12-generic 4.18.0-12.13  Linux kernel extra modules for version 4.18.0 on 64 bit x86 SMP
    linux-signed-generic 4.18.0.12.13  Complete Signed Generic Linux kernel and headers (dummy transitional package)
    lmodern 2.004.5-5  scalable PostScript and OpenType fonts based on Computer Modern
    locales 2.28-0ubuntu1  GNU C Library: National Language (locale) data [support]
    login 1:4.5-1ubuntu1  system login tools
    logrotate 3.14.0-4ubuntu1  Log rotation utility
    lsb-base 9.20170808ubuntu1  Linux Standard Base init script functionality
    lsb-release 9.20170808ubuntu1  Linux Standard Base version reporting utility
    lshw 02.18-0.1ubuntu7  information about hardware configuration
    lsof 4.89+dfsg-0.1ubuntu1  Utility to list open files
    ltrace 0.7.3-6ubuntu1  Tracks runtime library calls in dynamically linked programs
    lvm2 2.02.176-4.1ubuntu3  Linux Logical Volume Manager
    man-db 2.8.4-2  on-line manual pager
    manpages 4.16-1  Manual pages about using a GNU/Linux system
    mawk 1.3.3-17ubuntu3  a pattern scanning and text processing language
    mdadm 4.1~rc1-4ubuntu1  tool to administer Linux MD arrays (software RAID)
    mime-support 3.60ubuntu1  MIME files 'mime.types' & 'mailcap', and support programs
    mlocate 0.26-2ubuntu3.1  quickly find files on the filesystem based on their name
    mount 2.32-0.1ubuntu2  tools for mounting and manipulating filesystems
    mtr-tiny 0.92-2  Full screen ncurses traceroute tool
    multiarch-support 2.28-0ubuntu1  Transitional package to ensure multiarch compatibility
    mysql-client-5.7 5.7.24-0ubuntu0.18.10.1  MySQL database client binaries
    mysql-client-core-5.7 5.7.24-0ubuntu0.18.10.1  MySQL database core client binaries
    mysql-common 5.8+1.0.4  MySQL database common files, e.g. /etc/mysql/my.cnf
    mysql-server 5.7.24-0ubuntu0.18.10.1  MySQL database server (metapackage depending on the latest version)
    mysql-server-5.7 5.7.24-0ubuntu0.18.10.1  MySQL database server binaries and system database setup
    mysql-server-core-5.7 5.7.24-0ubuntu0.18.10.1  MySQL database server binaries
    nano 2.9.8-1  small, friendly text editor inspired by Pico
    ncurses-base 6.1+20180210-4ubuntu1  basic terminal type definitions
    ncurses-bin 6.1+20180210-4ubuntu1  terminal-related programs and man pages
    ncurses-term 6.1+20180210-4ubuntu1  additional terminal type definitions
    net-tools 1.60+git20161116.90da8a0-2ubuntu1  NET-3 networking toolkit
    netbase 5.4  Basic TCP/IP networking system
    netcat-openbsd 1.190-2  TCP/IP swiss army knife
    netplan.io 0.40.2  YAML network configuration abstraction for various backends
    networkd-dispatcher 1.7-0ubuntu8  Dispatcher service for systemd-networkd connection status changes
    nmap 7.60-1ubuntu5  The Network Mapper
    ntfs-3g 1:2017.3.23-2  read/write NTFS driver for FUSE
    open-iscsi 2.0.874-5ubuntu9  iSCSI initiator tools
    open-vm-tools 2:10.3.0-0ubuntu3  Open VMware Tools for virtual machines hosted on VMware (CLI)
    openssl 1.1.1-1ubuntu2.1  Secure Sockets Layer toolkit - cryptographic utility
    os-prober 1.74ubuntu1  utility to detect other OSes on a set of drives
    overlayroot 0.43ubuntu1  use an overlayfs on top of a read-only root filesystem
    parted 3.2-21ubuntu1  disk partition manipulator
    passwd 1:4.5-1ubuntu1  change and administer password and group data
    pastebinit 1.5-2  command-line pastebin client
    patch 2.7.6-3  Apply a diff file to an original
    pciutils 1:3.5.2-1ubuntu2  Linux PCI Utilities
    perl 5.26.2-7ubuntu0.1  Larry Wall's Practical Extraction and Report Language
    perl-base 5.26.2-7ubuntu0.1  minimal Perl system
    perl-modules-5.26 5.26.2-7ubuntu0.1  Core Perl modules
    perl-openssl-defaults:amd64 3build1  version compatibility baseline for Perl OpenSSL packages
    php 1:7.2+62  server-side, HTML-embedded scripting language (default)
    php-common 1:62  Common files for PHP packages
    php-composer-ca-bundle 1.1.2-1  utility library to find a path to the system CA bundle
    php-composer-semver 1.4.2-1  utilities, version constraint parsing and validation
    php-composer-spdx-licenses 1.4.0-1  SPDX licenses list and validation library
    php-composer-xdebug-handler 1.2.0-1  Restarts a process without xdebug
    php-intl 1:7.2+62  Internationalisation module for PHP [default]
    php-json-schema 5.2.7-1  implementation of JSON schema
    php-mbstring 1:7.2+62  MBSTRING module for PHP [default]
    php-mysql 1:7.2+62  MySQL module for PHP [default]
    php-pear 1:1.10.5+submodules+notgz-1ubuntu1  PEAR Base System
    php-psr-log 1.0.2-1  common interface for logging libraries
    php-symfony-console 3.4.15+dfsg-2ubuntu4  run tasks from the command line
    php-symfony-debug 3.4.15+dfsg-2ubuntu4  tools to make debugging of PHP code easier
    php-symfony-filesystem 3.4.15+dfsg-2ubuntu4  basic filesystem utilities
    php-symfony-finder 3.4.15+dfsg-2ubuntu4  find files and directories
    php-symfony-polyfill-ctype 1.9.0-1  Symfony polyfill for ctype functions
    php-symfony-polyfill-mbstring 1.9.0-1  Symfony polyfill for the Mbstring extension
    php-symfony-process 3.4.15+dfsg-2ubuntu4  execute commands in sub-processes
    php-xml 1:7.2+62  DOM, SimpleXML, WDDX, XML, and XSL module for PHP [default]
    php-zip 1:7.2+62  Zip module for PHP [default]
    php7.2 7.2.10-0ubuntu1  server-side, HTML-embedded scripting language (metapackage)
    php7.2-cli 7.2.10-0ubuntu1  command-line interpreter for the PHP scripting language
    php7.2-common 7.2.10-0ubuntu1  documentation, examples and common module for PHP
    php7.2-intl 7.2.10-0ubuntu1  Internationalisation module for PHP
    php7.2-json 7.2.10-0ubuntu1  JSON module for PHP
    php7.2-mbstring 7.2.10-0ubuntu1  MBSTRING module for PHP
    php7.2-mysql 7.2.10-0ubuntu1  MySQL module for PHP
    php7.2-opcache 7.2.10-0ubuntu1  Zend OpCache module for PHP
    php7.2-readline 7.2.10-0ubuntu1  readline module for PHP
    php7.2-xml 7.2.10-0ubuntu1  DOM, SimpleXML, WDDX, XML, and XSL module for PHP
    php7.2-zip 7.2.10-0ubuntu1  Zip module for PHP
    pinentry-curses 1.1.0-1build2  curses-based PIN or pass-phrase entry dialog for GnuPG
    plymouth 0.9.3-1ubuntu10  boot animation, logger and I/O multiplexer
    plymouth-theme-ubuntu-text 0.9.3-1ubuntu10  boot animation, logger and I/O multiplexer - ubuntu text theme
    policykit-1 0.105-21  framework for managing administrative policies and privileges
    pollinate 4.33-0ubuntu1  seed the pseudo random number generator
    poppler-data 0.4.9-2  encoding data for the poppler PDF rendering library
    popularity-contest 1.67ubuntu2  Vote for your favourite packages automatically
    postfix 3.3.0-1ubuntu2  High-performance mail transport agent
    postfix-mysql 3.3.0-1ubuntu2  MySQL map support for Postfix
    powermgmt-base 1.33  common utils for power management
    preview-latex-style 11.91-1ubuntu1  extraction of elements from LaTeX documents as graphics
    procps 2:3.3.15-2ubuntu1  /proc file system utilities
    psmisc 23.1-1build1  utilities that use the proc file system
    publicsuffix 20180523.2326-2  accurate, machine-readable list of domain name suffixes
    python 2.7.15-3  interactive high-level object-oriented language (Python2 version)
    python-apt-common 1.7.0  Python interface to libapt-pkg (locales)
    python-minimal 2.7.15-3  minimal subset of the Python2 language
    python2 2.7.15-3  interactive high-level object-oriented language (Python2 version)
    python2-minimal 2.7.15-3  minimal subset of the Python2 language
    python2.7 2.7.15-4ubuntu4  Interactive high-level object-oriented language (version 2.7)
    python2.7-minimal 2.7.15-4ubuntu4  Minimal subset of the Python language (version 2.7)
    python3 3.6.6-1  interactive high-level object-oriented language (default python3 version)
    python3-apport 2.20.10-0ubuntu13  Python 3 library for Apport crash report handling
    python3-apt 1.7.0  Python 3 interface to libapt-pkg
    python3-asn1crypto 0.24.0-1  Fast ASN.1 parser and serializer (Python 3)
    python3-attr 17.4.0-2  Attributes without boilerplate (Python 3)
    python3-automat 0.6.0-1  Self-service finite-state machines for the programmer on the go
    python3-blinker 1.4+dfsg1-0.2  fast, simple object-to-object and broadcast signaling library
    python3-certifi 2018.8.24-1  root certificates for validating SSL certs and verifying TLS hosts (python3)
    python3-cffi-backend 1.11.5-3ubuntu1  Foreign Function Interface for Python 3 calling C code - runtime
    python3-chardet 3.0.4-1  universal character encoding detector for Python3
    python3-click 6.7+git20180829-1  Wrapper around optparse for command line utilities - Python 3.x
    python3-colorama 0.3.7-1  Cross-platform colored terminal text in Python - Python 3.x
    python3-commandnotfound 18.10.0~pre1  Python 3 bindings for command-not-found.
    python3-configobj 5.0.6-3  simple but powerful config file reader and writer for Python 3
    python3-constantly 15.1.0-1  Symbolic constants in Python
    python3-cryptography 2.3-1ubuntu1  Python library exposing cryptographic recipes and primitives (Python 3)
    python3-dbus 1.2.8-2build1  simple interprocess messaging system (Python 3 interface)
    python3-debconf 1.5.69  interact with debconf from Python 3
    python3-debian 0.1.33  Python 3 modules to work with Debian-related data formats
    python3-distro-info 0.18  information about distributions' releases (Python 3 module)
    python3-distupgrade 1:18.10.11  manage release upgrades
    python3-gdbm:amd64 3.6.6-1  GNU dbm database support for Python 3.x
    python3-gi 3.30.1-1ubuntu1  Python 3 bindings for gobject-introspection libraries
    python3-httplib2 0.11.3-1  comprehensive HTTP client library written for Python3
    python3-hyperlink 17.3.1-2  Immutable, Pythonic, correct URLs.
    python3-idna 2.6-1  Python IDNA2008 (RFC 5891) handling (Python 3)
    python3-incremental 16.10.1-3  Library for versioning Python projects.
    python3-jinja2 2.10-1  small but fast and easy to use stand-alone template engine
    python3-json-pointer 1.10-1  resolve JSON pointers - Python 3.x
    python3-jsonpatch 1.21-1  library to apply JSON patches - Python 3.x
    python3-jsonschema 2.6.0-2  An(other) implementation of JSON Schema (Draft 3 and 4) - Python 3.x
    python3-jwt 1.6.4-1  Python 3 implementation of JSON Web Token
    python3-markupsafe 1.0-1build2  HTML/XHTML/XML string library for Python 3
    python3-minimal 3.6.6-1  minimal subset of the Python language (default python3 version)
    python3-netifaces 0.10.4-1build1  portable network interface information - Python 3.x
    python3-newt:amd64 0.52.20-6ubuntu1  NEWT module for Python3
    python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    python3-openssl 18.0.0-1  Python 3 wrapper around the OpenSSL library
    python3-pam 0.4.2-13.2ubuntu5  Python interface to the PAM library
    python3-pkg-resources 40.2.0-1  Package Discovery and Resource Access using pkg_resources
    python3-problem-report 2.20.10-0ubuntu13  Python 3 library to handle problem reports
    python3-pyasn1 0.4.2-3  ASN.1 library for Python (Python 3 module)
    python3-pyasn1-modules 0.2.1-0.2  Collection of protocols modules written in ASN.1 language (Python 3)
    python3-requests 2.18.4-2ubuntu0.18.10.1  elegant and simple HTTP library for Python3, built for human beings
    python3-requests-unixsocket 0.1.5-3  Use requests to talk HTTP via a UNIX domain socket - Python 3.x
    python3-serial 3.4-4  pyserial - module encapsulating access for the serial port
    python3-service-identity 16.0.0-2  Service identity verification for pyOpenSSL (Python 3 module)
    python3-six 1.11.0-2  Python 2 and 3 compatibility library (Python 3 interface)
    python3-software-properties 0.96.27  manage the repositories that you install software from
    python3-systemd 234-2build1  Python 3 bindings for systemd
    python3-twisted 18.7.0-2  Event-based framework for internet applications
    python3-twisted-bin:amd64 18.7.0-2  Event-based framework for internet applications
    python3-update-manager 1:18.10.10  python 3.x module for update-manager
    python3-urllib3 1.22-1  HTTP library with thread-safe connection pooling for Python3
    python3-yaml 3.12-1build3  YAML parser and emitter for Python3
    python3-zope.interface 4.3.2-1build3  Interfaces for Python3
    python3.6 3.6.7~rc1-1  Interactive high-level object-oriented language (version 3.6)
    python3.6-minimal 3.6.7~rc1-1  Minimal subset of the Python language (version 3.6)
    rake 12.3.1-3  ruby make-like utility
    readline-common 7.0-5  GNU readline and history libraries, common files
    rsync 3.1.2-2.2  fast, versatile, remote (and local) file-copying tool
    rsyslog 8.32.0-1ubuntu5  reliable system and kernel logging daemon
    ruby 1:2.5.1  Interpreter of object-oriented scripting language Ruby (default version)
    ruby-did-you-mean 1.2.1-1  smart error messages for Ruby > 2.3
    ruby-minitest 5.11.3-1  Ruby test tools supporting TDD, BDD, mocking, and benchmarking
    ruby-net-telnet 0.1.1-2  telnet client library
    ruby-power-assert 1.1.1-1  library showing values of variables and method calls in an expression
    ruby-test-unit 3.2.8-1  unit testing framework for Ruby
    ruby-xmlrpc 0.3.0-2  XMLRPC library for Ruby
    ruby2.5 2.5.1-5ubuntu4.1  Interpreter of object-oriented scripting language Ruby
    rubygems-integration 1.11  integration of Debian Ruby packages with Rubygems
    run-one 1.17-0ubuntu1  run just one instance of a command and its args at a time
    screen 4.6.2-3  terminal multiplexer with VT100/ANSI terminal emulation
    sed 4.5-1  GNU stream editor for filtering/transforming text
    sensible-utils 0.0.12  Utilities for sensible alternative selection
    shared-mime-info 1.10-1  FreeDesktop.org shared MIME database and spec
    software-properties-common 0.96.27  manage the repositories that you install software from (common)
    sosreport 3.5-1ubuntu4  Set of tools to gather troubleshooting data from a system
    ssl-cert 1.0.39  simple debconf wrapper for OpenSSL
    strace 4.21-1ubuntu1  System call tracer
    sudo 1.8.23-2ubuntu1  Provide limited super user privileges to specific users
    systemd 239-7ubuntu10.4  system and service manager
    systemd-sysv 239-7ubuntu10.4  system and service manager - SysV links
    sysvinit-utils 2.88dsf-59.10ubuntu2  System-V-like utilities
    t1utils 1.41-2  Collection of simple Type 1 font manipulation programs
    tar 1.30+dfsg-2  GNU version of the tar archiving utility
    tcl 8.6.0+9  Tool Command Language (default version) - shell
    tcl8.6 8.6.8+dfsg-4  Tcl (the Tool Command Language) v8.6 - shell
    tcpdump 4.9.2-3  command-line network traffic analyzer
    telnet 0.17-41  basic telnet client
    tex-common 6.10  common infrastructure for building and installing TeX
    tex-gyre 20180621-2  scalable PostScript and OpenType fonts based on URW Fonts
    texlive 2018.20180824-1  TeX Live: A decent selection of the TeX Live packages
    texlive-base 2018.20180824-1  TeX Live: Essential programs and files
    texlive-binaries 2018.20180824.48463-1ubuntu0.1  Binaries for TeX Live
    texlive-fonts-recommended 2018.20180824-1  TeX Live: Recommended fonts
    texlive-latex-base 2018.20180824-1  TeX Live: LaTeX fundamental packages
    texlive-latex-base-doc 2018.20180824-1  TeX Live: Documentation files for texlive-latex-base
    texlive-latex-extra 2018.20180824-1  TeX Live: LaTeX additional packages
    texlive-latex-extra-doc 2018.20180824-1  TeX Live: Documentation files for texlive-latex-extra
    texlive-latex-recommended 2018.20180824-1  TeX Live: LaTeX recommended packages
    texlive-latex-recommended-doc 2018.20180824-1  TeX Live: Documentation files for texlive-latex-recommended
    texlive-pictures 2018.20180824-1  TeX Live: Graphics, pictures, diagrams
    texlive-plain-generic 2018.20180824-1  TeX Live: Plain (La)TeX packages
    thermald 1.7.0-8  Thermal monitoring and controlling daemon
    time 1.7-25.1build1  GNU time program for measuring CPU resource usage
    tipa 2:1.3-20  system for processing phonetic symbols in LaTeX
    tk 8.6.0+9  Toolkit for Tcl and X11 (default version) - windowing shell
    tk8.6 8.6.8-4  Tk toolkit for Tcl and X11 v8.6 - windowing shell
    tmux 2.7-1  terminal multiplexer
    tzdata 2018g-0ubuntu0.18.10  time zone and daylight-saving time data
    ubuntu-advantage-tools 17  management tools for Ubuntu Advantage
    ubuntu-keyring 2018.09.18.1  GnuPG keys of the Ubuntu archive
    ubuntu-minimal 1.425  Minimal core of Ubuntu
    ubuntu-release-upgrader-core 1:18.10.11  manage release upgrades
    ubuntu-server 1.425  The Ubuntu Server system
    ubuntu-standard 1.425  The Ubuntu standard system
    ucf 3.0038  Update Configuration File(s): preserve user changes to config files
    udev 239-7ubuntu10.4  /dev/ and hotplug management daemon
    ufw 0.35-6  program for managing a Netfilter firewall
    unattended-upgrades 1.5ubuntu3  automatic installation of security upgrades
    unzip 6.0-21ubuntu1  De-archiver for .zip files
    update-manager-core 1:18.10.10  manage release upgrades
    update-notifier-common 3.192.7  Files shared between update-notifier and other packages
    usbutils 1:007-4build1  Linux USB utilities
    util-linux 2.32-0.1ubuntu2  miscellaneous system utilities
    uuid-runtime 2.32-0.1ubuntu2  runtime components for the Universally Unique ID library
    vim 2:8.0.1766-1ubuntu1  Vi IMproved - enhanced vi editor
    vim-common 2:8.0.1766-1ubuntu1  Vi IMproved - Common files
    vim-runtime 2:8.0.1766-1ubuntu1  Vi IMproved - Runtime files
    vim-tiny 2:8.0.1766-1ubuntu1  Vi IMproved - enhanced vi editor - compact version
    webmin 1.890  web-based administration interface for Unix systems
    wget 1.19.5-1ubuntu1  retrieves files from the web
    whiptail 0.52.20-6ubuntu1  Displays user-friendly dialog boxes from shell scripts
    wireless-regdb 2018.05.09-0ubuntu1  wireless regulatory database
    x11-common 1:7.7+19ubuntu8  X Window System (X.Org) infrastructure
    x11-utils 7.7+4  X11 utilities
    x11-xserver-utils 7.7+8  X server utilities
    xbitmaps 1.1.1-2  Base X bitmaps
    xdg-user-dirs 0.17-1ubuntu1  tool to manage well known user directories
    xdg-utils 1.1.3-1ubuntu2  desktop integration utilities from freedesktop.org
    xfonts-encodings 1:1.0.4-2  Encodings for X.Org fonts
    xfonts-utils 1:7.7+6  X Window System font utility programs
    xfsprogs 4.15.1-1ubuntu1  Utilities for managing the XFS filesystem
    xkb-data 2.23.1-1ubuntu1  X Keyboard Extension (XKB) configuration data
    xterm 330-1ubuntu3  X terminal emulator
    xxd 2:8.0.1766-1ubuntu1  tool to make (or reverse) a hex dump
    xz-utils 5.2.2-1.3  XZ-format compression utilities
    zerofree 1.1.1-1  zero free blocks from ext2, ext3 and ext4 file-systems
    zip 3.0-11build1  Archiver for .zip files
    zlib1g:amd64 1:1.2.11.dfsg-0ubuntu2  compression library - runtime

[+] Current processes
    USER PID START TIME COMMAND
    root 1 Jan31 0:06 /sbin/init
    root 2 Jan31 0:00 [kthreadd]
    root 3 Jan31 0:00 [rcu_gp]
    root 4 Jan31 0:00 [rcu_par_gp]
    root 6 Jan31 0:00 [kworker/0:0H]
    root 8 Jan31 0:00 [mm_percpu_wq]
    root 9 Jan31 0:15 [ksoftirqd/0]
    root 10 Jan31 0:26 [rcu_sched]
    root 11 Jan31 0:00 [rcu_bh]
    root 12 Jan31 0:00 [migration/0]
    root 13 Jan31 0:00 [watchdog/0]
    root 14 Jan31 0:00 [cpuhp/0]
    root 15 Jan31 0:00 [kdevtmpfs]
    root 16 Jan31 0:00 [netns]
    root 17 Jan31 0:00 [rcu_tasks_kthre]
    root 18 Jan31 0:00 [kauditd]
    root 19 Jan31 0:00 [khungtaskd]
    root 20 Jan31 0:00 [oom_reaper]
    root 21 Jan31 0:00 [writeback]
    root 22 Jan31 0:00 [kcompactd0]
    root 23 Jan31 0:00 [ksmd]
    root 24 Jan31 0:00 [khugepaged]
    root 25 Jan31 0:00 [crypto]
    root 26 Jan31 0:00 [kintegrityd]
    root 27 Jan31 0:00 [kblockd]
    root 28 Jan31 0:00 [ata_sff]
    root 29 Jan31 0:00 [md]
    root 30 Jan31 0:00 [edac-poller]
    root 31 Jan31 0:00 [devfreq_wq]
    root 33 Jan31 0:00 [watchdogd]
    root 36 Jan31 0:05 [kswapd0]
    root 37 Jan31 0:00 [kworker/u3:0]
    root 38 Jan31 0:00 [ecryptfs-kthrea]
    root 83 Jan31 0:00 [kthrotld]
    root 84 Jan31 0:00 [acpi_thermal_pm]
    root 85 Jan31 0:00 [scsi_eh_0]
    root 86 Jan31 0:00 [scsi_tmf_0]
    root 87 Jan31 0:00 [scsi_eh_1]
    root 88 Jan31 0:00 [scsi_tmf_1]
    root 94 Jan31 0:00 [ipv6_addrconf]
    root 103 Jan31 0:00 [kstrp]
    root 120 Jan31 0:00 [charger_manager]
    root 181 Jan31 0:00 [scsi_eh_2]
    root 182 Jan31 0:00 [scsi_tmf_2]
    root 183 Jan31 0:00 [scsi_eh_3]
    root 184 Jan31 0:00 [scsi_tmf_3]
    root 185 Jan31 0:00 [scsi_eh_4]
    root 186 Jan31 0:00 [ttm_swap]
    root 187 Jan31 0:00 [irq/16-vmwgfx]
    root 188 Jan31 0:00 [scsi_tmf_4]
    root 189 Jan31 0:00 [scsi_eh_5]
    root 190 Jan31 0:00 [scsi_tmf_5]
    root 191 Jan31 0:00 [scsi_eh_6]
    root 192 Jan31 0:00 [scsi_tmf_6]
    root 193 Jan31 0:00 [scsi_eh_7]
    root 194 Jan31 0:00 [scsi_tmf_7]
    root 195 Jan31 0:00 [scsi_eh_8]
    root 196 Jan31 0:00 [scsi_tmf_8]
    root 197 Jan31 0:00 [scsi_eh_9]
    root 198 Jan31 0:00 [scsi_tmf_9]
    root 199 Jan31 0:00 [scsi_eh_10]
    root 200 Jan31 0:00 [scsi_tmf_10]
    root 201 Jan31 0:00 [scsi_eh_11]
    root 202 Jan31 0:00 [scsi_tmf_11]
    root 203 Jan31 0:00 [scsi_eh_12]
    root 204 Jan31 0:00 [scsi_tmf_12]
    root 205 Jan31 0:00 [scsi_eh_13]
    root 206 Jan31 0:00 [scsi_tmf_13]
    root 207 Jan31 0:00 [scsi_eh_14]
    root 208 Jan31 0:00 [scsi_tmf_14]
    root 209 Jan31 0:00 [scsi_eh_15]
    root 214 Jan31 0:00 [scsi_tmf_15]
    root 216 Jan31 0:00 [scsi_eh_16]
    root 217 Jan31 0:00 [scsi_tmf_16]
    root 218 Jan31 0:00 [scsi_eh_17]
    root 219 Jan31 0:00 [scsi_tmf_17]
    root 221 Jan31 0:00 [scsi_eh_18]
    root 225 Jan31 0:00 [scsi_tmf_18]
    root 226 Jan31 0:00 [scsi_eh_19]
    root 227 Jan31 0:00 [scsi_tmf_19]
    root 228 Jan31 0:00 [scsi_eh_20]
    root 229 Jan31 0:00 [scsi_tmf_20]
    root 230 Jan31 0:00 [scsi_eh_21]
    root 231 Jan31 0:00 [scsi_tmf_21]
    root 233 Jan31 0:00 [scsi_eh_22]
    root 234 Jan31 0:00 [scsi_tmf_22]
    root 235 Jan31 0:00 [scsi_eh_23]
    root 236 Jan31 0:00 [scsi_tmf_23]
    root 237 Jan31 0:00 [scsi_eh_24]
    root 239 Jan31 0:00 [scsi_tmf_24]
    root 241 Jan31 0:00 [scsi_eh_25]
    root 243 Jan31 0:00 [scsi_tmf_25]
    root 246 Jan31 0:00 [scsi_eh_26]
    root 248 Jan31 0:00 [scsi_tmf_26]
    root 249 Jan31 0:00 [scsi_eh_27]
    root 252 Jan31 0:00 [scsi_tmf_27]
    root 254 Jan31 0:00 [scsi_eh_28]
    root 256 Jan31 0:00 [scsi_tmf_28]
    root 258 Jan31 0:00 [scsi_eh_29]
    root 260 Jan31 0:00 [scsi_tmf_29]
    root 261 Jan31 0:00 [scsi_eh_30]
    root 264 Jan31 0:00 [scsi_tmf_30]
    root 266 Jan31 0:00 [scsi_eh_31]
    root 268 Jan31 0:00 [scsi_tmf_31]
    root 303 Jan31 0:02 [kworker/0:1H-kblockd]
    root 371 Jan31 0:00 [raid5wq]
    root 421 Jan31 0:01 [jbd2/sda2-8]
    root 422 Jan31 0:00 [ext4-rsv-conver]
    root 488 Jan31 0:00 [iscsi_eh]
    root 490 Jan31 0:00 [ib-comp-wq]
    root 491 Jan31 0:00 [ib_mcast]
    root 493 Jan31 0:00 [ib_nl_sa_wq]
    root 494 Jan31 0:15 /usr/bin/vmtoolsd
    root 498 Jan31 0:12 /lib/systemd/systemd-journald
    root 499 Jan31 0:00 [rdma_cm]
    root 508 Jan31 0:00 /sbin/lvmetad
    root 541 Jan31 0:00 /lib/systemd/systemd-udevd
    systemd+ 643 Jan31 0:01 /lib/systemd/systemd-timesyncd
    systemd+ 795 Jan31 0:00 /lib/systemd/systemd-networkd
    systemd+ 842 Jan31 0:06 /lib/systemd/systemd-resolved
    daemon 986 Jan31 0:00 /usr/sbin/atd
    syslog 996 Jan31 0:03 /usr/sbin/rsyslogd
    root 1001 Jan31 0:00 /usr/sbin/cron
    root 1003 Jan31 0:01 /usr/lib/accountsservice/accounts-daemon
    root 1004 Jan31 0:00 /lib/systemd/systemd-logind
    root 1007 Jan31 0:00 /usr/bin/python3
    root 1009 Jan31 0:00 /usr/bin/VGAuthService
    message+ 1010 Jan31 0:13 /usr/bin/dbus-daemon
    root 1011 Jan31 0:01 /usr/sbin/dovecot
    dovecot 1039 Jan31 0:02 dovecot/anvil
    root 1040 Jan31 0:00 dovecot/log
    root 1048 Jan31 0:01 dovecot/config
    root 1057 Jan31 0:06 /usr/lib/policykit-1/polkitd
    root 1098 Jan31 0:00 /sbin/agetty
    root 1115 Jan31 0:01 /usr/sbin/apache2
    root 1347 Jan31 0:00 /usr/lib/postfix/sbin/master
    postfix 1350 Jan31 0:00 qmgr
    mysql 1404 Jan31 6:01 /usr/sbin/mysqld
    root 1494 Jan31 0:21 /usr/bin/perl
    dovecot 1641 Jan31 0:00 dovecot/stats
    root 6655 Jan31 0:13 [kworker/0:0-events]
    root 19499 Jan31 0:00 [kworker/0:1-cgroup_destroy]
    dovecot 21415 Jan31 0:03 dovecot/auth
    www-data 22078 Jan31 0:11 /usr/sbin/apache2
    postfix 22229 Jan31 0:00 pickup
    www-data 22231 Jan31 0:00 sh
    www-data 22232 Jan31 0:00 /usr/bin/pdflatex
    www-data 22233 Jan31 0:00 sh
    www-data 22234 Jan31 0:00 cat
    www-data 22235 Jan31 0:00 /bin/sh
    www-data 22236 Jan31 0:00 nc
    www-data 22291 Jan31 0:00 python
    www-data 22292 Jan31 0:00 /bin/bash
    root 22390 Jan31 0:00 dovecot/auth
    root 22442 Jan31 0:00 dovecot/auth
    root 22443 Jan31 0:00 dovecot/auth
    root 22470 Jan31 0:00 dovecot/auth
    root 22471 Jan31 0:00 dovecot/auth
    root 22472 Jan31 0:00 dovecot/auth
    root 22585 Jan31 0:00 [kworker/u2:0-events_power_efficient]
    root 22586 Jan31 0:00 dovecot/auth
    root 22790 Jan31 0:00 dovecot/auth
    root 22791 Jan31 0:00 dovecot/auth
    root 22792 Jan31 0:00 dovecot/auth
    root 22793 Jan31 0:00 dovecot/auth
    root 22794 Jan31 0:00 dovecot/auth
    www-data 22905 00:00 0:21 /usr/sbin/apache2
    www-data 22906 00:00 0:21 /usr/sbin/apache2
    www-data 22908 00:00 0:21 /usr/sbin/apache2
    www-data 22909 00:00 0:21 /usr/sbin/apache2
    www-data 22911 00:00 0:20 /usr/sbin/apache2
    www-data 22930 00:00 0:20 /usr/sbin/apache2
    www-data 22935 00:00 0:20 /usr/sbin/apache2
    root 22936 00:00 0:00 dovecot/auth
    www-data 22937 00:00 0:20 /usr/sbin/apache2
    www-data 22938 00:00 0:20 /usr/sbin/apache2
    root 22939 00:00 0:00 dovecot/auth
    www-data 22940 00:00 0:19 /usr/sbin/apache2
    root 22941 00:00 0:00 dovecot/auth
    root 22993 00:03 0:00 dovecot/auth
    root 22994 00:03 0:00 dovecot/auth
    root 22995 00:04 0:00 dovecot/auth
    root 22996 00:04 0:00 dovecot/auth
    root 22997 00:04 0:00 [kworker/u2:2-events_power_efficient]
    root 22998 00:05 0:00 dovecot/auth
    root 23022 00:05 0:00 dovecot/auth
    root 23073 00:06 0:00 dovecot/auth
    root 23074 00:06 0:00 dovecot/auth
    root 23075 00:06 0:00 dovecot/auth
    root 23076 00:07 0:00 dovecot/auth
    root 23212 00:10 0:00 dovecot/auth
    root 23236 00:10 0:00 [kworker/u2:1-events_power_efficient]
    root 23237 00:11 0:00 dovecot/auth
    root 23238 00:11 0:00 dovecot/auth
    dovenull 23239 00:12 0:00 dovecot/pop3-login
    dovenull 23240 00:12 0:00 dovecot/pop3-login
    dovenull 23241 00:12 0:00 dovecot/pop3-login
    dovenull 23242 00:12 0:00 dovecot/pop3-login
    dovenull 23243 00:12 0:00 dovecot/pop3-login
    dovenull 23244 00:12 0:00 dovecot/pop3-login
    dovenull 23245 00:12 0:00 dovecot/pop3-login
    dovenull 23246 00:12 0:00 dovecot/pop3-login
    dovenull 23247 00:12 0:00 dovecot/pop3-login
    dovenull 23248 00:12 0:00 dovecot/pop3-login
    dovenull 23249 00:12 0:00 dovecot/pop3-login
    dovenull 23250 00:12 0:00 dovecot/pop3-login
    dovenull 23251 00:12 0:00 dovecot/pop3-login
    dovenull 23252 00:12 0:00 dovecot/pop3-login
    dovenull 23253 00:12 0:00 dovecot/pop3-login
    dovenull 23254 00:12 0:00 dovecot/pop3-login
    dovenull 23255 00:12 0:00 dovecot/pop3-login
    dovenull 23256 00:12 0:00 dovecot/pop3-login
    dovenull 23257 00:12 0:00 dovecot/pop3-login
    dovenull 23258 00:12 0:00 dovecot/pop3-login
    dovenull 23259 00:12 0:00 dovecot/pop3-login
    dovenull 23260 00:12 0:00 dovecot/pop3-login
    dovenull 23261 00:12 0:00 dovecot/pop3-login
    dovenull 23262 00:12 0:00 dovecot/pop3-login
    dovenull 23263 00:12 0:00 dovecot/pop3-login
    dovenull 23264 00:12 0:00 dovecot/pop3-login
    dovenull 23265 00:12 0:00 dovecot/pop3-login
    dovenull 23266 00:12 0:00 dovecot/pop3-login
    dovenull 23267 00:12 0:00 dovecot/pop3-login
    dovenull 23268 00:12 0:00 dovecot/pop3-login
    dovenull 23269 00:12 0:00 dovecot/pop3-login
    dovenull 23270 00:12 0:00 dovecot/pop3-login
    dovenull 23271 00:12 0:00 dovecot/pop3-login
    dovenull 23272 00:12 0:00 dovecot/pop3-login
    dovenull 23273 00:12 0:00 dovecot/pop3-login
    dovenull 23274 00:12 0:00 dovecot/pop3-login
    dovenull 23275 00:12 0:00 dovecot/pop3-login
    dovenull 23276 00:12 0:00 dovecot/pop3-login
    dovenull 23277 00:12 0:00 dovecot/pop3-login
    dovenull 23278 00:12 0:00 dovecot/pop3-login
    dovenull 23279 00:12 0:00 dovecot/pop3-login
    dovenull 23280 00:12 0:00 dovecot/pop3-login
    dovenull 23281 00:12 0:00 dovecot/pop3-login
    dovenull 23282 00:12 0:00 dovecot/pop3-login
    dovenull 23283 00:12 0:00 dovecot/pop3-login
    dovenull 23284 00:12 0:00 dovecot/pop3-login
    dovenull 23285 00:12 0:00 dovecot/pop3-login
    dovenull 23286 00:12 0:00 dovecot/pop3-login
    dovenull 23287 00:12 0:00 dovecot/pop3-login
    dovenull 23288 00:12 0:00 dovecot/pop3-login
    www-data 23289 00:12 0:00 python
    root 23336 00:12 0:00 dovecot/auth
    root 23385 00:12 0:00 [kworker/0:2]
    www-data 23527 00:12 0:00 /bin/sh
    www-data 23528 00:12 0:00 ps
    www-data 23529 00:12 0:00 awk

[+] Apache Version and Modules
    Server version: Apache/2.4.34 (Ubuntu)
    Server built:   2018-10-03T13:57:22
    Loaded Modules:
    core_module (static)
    so_module (static)
    watchdog_module (static)
    http_module (static)
    log_config_module (static)
    logio_module (static)
    version_module (static)
    unixd_module (static)
    access_compat_module (shared)
    alias_module (shared)
    auth_basic_module (shared)
    authn_core_module (shared)
    authn_file_module (shared)
    authz_core_module (shared)
    authz_host_module (shared)
    authz_user_module (shared)
    autoindex_module (shared)
    deflate_module (shared)
    dir_module (shared)
    env_module (shared)
    filter_module (shared)
    mime_module (shared)
    mpm_prefork_module (shared)
    negotiation_module (shared)
    php7_module (shared)
    reqtimeout_module (shared)
    rewrite_module (shared)
    setenvif_module (shared)
    status_module (shared)
    Compiled in modules:
    core.c
    mod_so.c
    mod_watchdog.c
    http_core.c
    mod_log_config.c
    mod_logio.c
    mod_version.c
    mod_unixd.c

[+] Apache Config File
    # This is the main Apache server configuration file.  It contains the
    # configuration directives that give the server its instructions.
    # See http://httpd.apache.org/docs/2.4/ for detailed information about
    # the directives and /usr/share/doc/apache2/README.Debian about Debian specific
    # hints.
    #
    #
    # Summary of how the Apache 2 configuration works in Debian:
    # The Apache 2 web server configuration in Debian is quite different to
    # upstream's suggested way to configure the web server. This is because Debian's
    # default Apache2 installation attempts to make adding and removing modules,
    # virtual hosts, and extra configuration directives as flexible as possible, in
    # order to make automating the changes and administering the server as easy as
    # possible.
    # It is split into several files forming the configuration hierarchy outlined
    # below, all located in the /etc/apache2/ directory:
    #
    #	/etc/apache2/
    #	|-- apache2.conf
    #	|	`--  ports.conf
    #	|-- mods-enabled
    #	|	|-- *.load
    #	|	`-- *.conf
    #	|-- conf-enabled
    #	|	`-- *.conf
    # 	`-- sites-enabled
    #	 	`-- *.conf
    #
    #
    # * apache2.conf is the main configuration file (this file). It puts the pieces
    #   together by including all remaining configuration files when starting up the
    #   web server.
    #
    # * ports.conf is always included from the main configuration file. It is
    #   supposed to determine listening ports for incoming connections which can be
    #   customized anytime.
    #
    # * Configuration files in the mods-enabled/, conf-enabled/ and sites-enabled/
    #   directories contain particular configuration snippets which manage modules,
    #   global configuration fragments, or virtual host configurations,
    #   respectively.
    #
    #   They are activated by symlinking available configuration files from their
    #   respective *-available/ counterparts. These should be managed by using our
    #   helpers a2enmod/a2dismod, a2ensite/a2dissite and a2enconf/a2disconf. See
    #   their respective man pages for detailed information.
    #
    # * The binary is called apache2. Due to the use of environment variables, in
    #   the default configuration, apache2 needs to be started/stopped with
    #   /etc/init.d/apache2 or apache2ctl. Calling /usr/bin/apache2 directly will not
    #   work with the default configuration.
    # Global configuration
    #
    #
    # ServerRoot: The top of the directory tree under which the server's
    # configuration, error, and log files are kept.
    #
    # NOTE!  If you intend to place this on an NFS (or otherwise network)
    # mounted filesystem then please read the Mutex documentation (available
    # at <URL:http://httpd.apache.org/docs/2.4/mod/core.html#mutex>);
    # you will save yourself a lot of trouble.
    #
    # Do NOT add a slash at the end of the directory path.
    #
    #ServerRoot "/etc/apache2"
    #
    # The accept serialization lock file MUST BE STORED ON A LOCAL DISK.
    #
    #Mutex file:${APACHE_LOCK_DIR} default
    #
    # The directory where shm and other runtime files will be stored.
    #
    DefaultRuntimeDir ${APACHE_RUN_DIR}
    #
    # PidFile: The file in which the server should record its process
    # identification number when it starts.
    # This needs to be set in /etc/apache2/envvars
    #
    PidFile ${APACHE_PID_FILE}
    #
    # Timeout: The number of seconds before receives and sends time out.
    #
    Timeout 300
    #
    # KeepAlive: Whether or not to allow persistent connections (more than
    # one request per connection). Set to "Off" to deactivate.
    #
    KeepAlive On
    #
    # MaxKeepAliveRequests: The maximum number of requests to allow
    # during a persistent connection. Set to 0 to allow an unlimited amount.
    # We recommend you leave this number high, for maximum performance.
    #
    MaxKeepAliveRequests 100
    #
    # KeepAliveTimeout: Number of seconds to wait for the next request from the
    # same client on the same connection.
    #
    KeepAliveTimeout 5
    # These need to be set in /etc/apache2/envvars
    User ${APACHE_RUN_USER}
    Group ${APACHE_RUN_GROUP}
    #
    # HostnameLookups: Log the names of clients or just their IP addresses
    # e.g., www.apache.org (on) or 204.62.129.132 (off).
    # The default is off because it'd be overall better for the net if people
    # had to knowingly turn this feature on, since enabling it means that
    # each client request will result in AT LEAST one lookup request to the
    # nameserver.
    #
    HostnameLookups Off
    # ErrorLog: The location of the error log file.
    # If you do not specify an ErrorLog directive within a <VirtualHost>
    # container, error messages relating to that virtual host will be
    # logged here.  If you *do* define an error logfile for a <VirtualHost>
    # container, that host's errors will be logged there and not here.
    #
    ErrorLog ${APACHE_LOG_DIR}/error.log
    #
    # LogLevel: Control the severity of messages logged to the error_log.
    # Available values: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.
    # It is also possible to configure the log level for particular modules, e.g.
    # "LogLevel info ssl:warn"
    #
    LogLevel warn
    # Include module configuration:
    IncludeOptional mods-enabled/*.load
    IncludeOptional mods-enabled/*.conf
    # Include list of ports to listen on
    Include ports.conf
    # Sets the default security model of the Apache2 HTTPD server. It does
    # not allow access to the root filesystem outside of /usr/share and /var/www.
    # The former is used by web applications packaged in Debian,
    # the latter may be used for local directories served by the web server. If
    # your system is serving content from a sub-directory in /srv you must allow
    # access here, or in any related virtual host.
    <Directory />
    Options FollowSymLinks
    AllowOverride None
    Require all denied
    </Directory>
    <Directory /usr/share>
    AllowOverride None
    Require all granted
    </Directory>
    <Directory /var/www/>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
    </Directory>
    #<Directory /srv/>
    #	Options Indexes FollowSymLinks
    #	AllowOverride None
    #	Require all granted
    #</Directory>
    # AccessFileName: The name of the file to look for in each directory
    # for additional configuration directives.  See also the AllowOverride
    # directive.
    #
    AccessFileName .htaccess
    #
    # The following lines prevent .htaccess and .htpasswd files from being
    # viewed by Web clients.
    #
    <FilesMatch "^\.ht">
    Require all denied
    </FilesMatch>
    #
    # The following directives define some format nicknames for use with
    # a CustomLog directive.
    #
    # These deviate from the Common Log Format definitions in that they use %O
    # (the actual bytes sent including headers) instead of %b (the size of the
    # requested file), because the latter makes it impossible to detect partial
    # requests.
    #
    # Note that the use of %{X-Forwarded-For}i instead of %h is not recommended.
    # Use mod_remoteip instead.
    #
    LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
    LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %O" common
    LogFormat "%{Referer}i -> %U" referer
    LogFormat "%{User-agent}i" agent
    # Include of directories ignores editors' and dpkg's backup files,
    # see README.Debian for details.
    # Include generic snippets of statements
    IncludeOptional conf-enabled/*.conf
    # Include the virtual host configurations:
    IncludeOptional sites-enabled/*.conf
    # vim: syntax=apache ts=4 sw=4 sts=4 sr noet

[+] Sudo Version (Check out http://www.exploit-db.com/search/?action=search&filter_page=1&filter_description=sudo)
    Sudo version 1.8.23
    Sudoers policy plugin version 1.8.23
    Sudoers file grammar version 46
    Sudoers I/O plugin version 1.8.23

[*] IDENTIFYING PROCESSES AND PACKAGES RUNNING AS ROOT OR OTHER SUPERUSER...

    root 8 Jan31 0:00 [mm_percpu_wq]
    root 6 Jan31 0:00 [kworker/0:0H]
    root 254 Jan31 0:00 [scsi_eh_28]
    root 268 Jan31 0:00 [scsi_tmf_31]
    root 22790 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 22794 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 230 Jan31 0:00 [scsi_eh_21]
    root 214 Jan31 0:00 [scsi_tmf_15]
    root 1347 Jan31 0:00 /usr/lib/postfix/sbin/master
        Possible Related Packages: 
             base-passwd 3.5.45  Debian base system master password and group files
    root 248 Jan31 0:00 [scsi_tmf_26]
    root 2 Jan31 0:00 [kthreadd]
    root 11 Jan31 0:00 [rcu_bh]
    root 1009 Jan31 0:00 /usr/bin/VGAuthService
    root 23236 00:10 0:00 [kworker/u2:1-events_power_efficient]
    root 189 Jan31 0:00 [scsi_eh_5]
    root 186 Jan31 0:00 [ttm_swap]
    root 33 Jan31 0:00 [watchdogd]
    root 488 Jan31 0:00 [iscsi_eh]
    root 12 Jan31 0:00 [migration/0]
    root 22 Jan31 0:00 [kcompactd0]
    root 19499 Jan31 0:00 [kworker/0:1-cgroup_destroy]
    root 1040 Jan31 0:00 dovecot/log
        Possible Related Packages: 
             libllvm7:amd64 1:7-3  Modular compiler and toolchain technologies, runtime library
             libplymouth4:amd64 0.9.3-1ubuntu10  graphical boot animation and logger - shared libraries
             login 1:4.5-1ubuntu1  system login tools
             logrotate 3.14.0-4ubuntu1  Log rotation utility
             php-psr-log 1.0.2-1  common interface for logging libraries
             pinentry-curses 1.1.0-1build2  curses-based PIN or pass-phrase entry dialog for GnuPG
             plymouth 0.9.3-1ubuntu10  boot animation, logger and I/O multiplexer
             plymouth-theme-ubuntu-text 0.9.3-1ubuntu10  boot animation, logger and I/O multiplexer - ubuntu text theme
             rsyslog 8.32.0-1ubuntu5  reliable system and kernel logging daemon
             whiptail 0.52.20-6ubuntu1  Displays user-friendly dialog boxes from shell scripts
    root 1003 Jan31 0:01 /usr/lib/accountsservice/accounts-daemon
    root 6655 Jan31 0:13 [kworker/0:0-events]
    root 28 Jan31 0:00 [ata_sff]
    root 231 Jan31 0:00 [scsi_tmf_21]
    root 13 Jan31 0:00 [watchdog/0]
    root 216 Jan31 0:00 [scsi_eh_16]
    root 23073 00:06 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 1057 Jan31 0:06 /usr/lib/policykit-1/polkitd
    root 493 Jan31 0:00 [ib_nl_sa_wq]
    root 190 Jan31 0:00 [scsi_tmf_5]
    root 237 Jan31 0:00 [scsi_eh_24]
    root 22995 00:04 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 1001 Jan31 0:00 /usr/sbin/cron
        Possible Related Packages: 
             cron 3.0pl1-128.1ubuntu1  process scheduling daemon
    root 22939 00:00 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 235 Jan31 0:00 [scsi_eh_23]
    root 19 Jan31 0:00 [khungtaskd]
    root 22791 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 204 Jan31 0:00 [scsi_tmf_12]
    root 371 Jan31 0:00 [raid5wq]
    root 30 Jan31 0:00 [edac-poller]
    root 218 Jan31 0:00 [scsi_eh_17]
    root 195 Jan31 0:00 [scsi_eh_8]
    root 1 Jan31 0:06 /sbin/init
        Possible Related Packages: 
             busybox-initramfs 1:1.27.2-2ubuntu4  Standalone shell setup for initramfs
             cloud-init 18.4-7-g4652b196-0ubuntu1  Init scripts for cloud instances
             cloud-initramfs-copymods 0.43ubuntu1  copy initramfs modules into root filesystem for later use
             cloud-initramfs-dyn-netconf 0.43ubuntu1  write a network interface file in /run for BOOTIF
             cryptsetup 2:2.0.4-2ubuntu2  transitional dummy package for cryptsetup-{run,initramfs}
             cryptsetup-initramfs 2:2.0.4-2ubuntu2  disk encryption support - initramfs integration
             init 1.54  metapackage ensuring an init system is installed
             init-system-helpers 1.54  helper tools for all init systems
             initramfs-tools 0.131ubuntu15  generic modular initramfs generator (automation)
             initramfs-tools-bin 0.131ubuntu15  binaries used by initramfs-tools
             initramfs-tools-core 0.131ubuntu15  generic modular initramfs generator (core tools)
             libklibc 2.0.4-9ubuntu2  minimal libc subset for use with initramfs
             lsb-base 9.20170808ubuntu1  Linux Standard Base init script functionality
             ncurses-base 6.1+20180210-4ubuntu1  basic terminal type definitions
             ncurses-term 6.1+20180210-4ubuntu1  additional terminal type definitions
             open-iscsi 2.0.874-5ubuntu9  iSCSI initiator tools
             pastebinit 1.5-2  command-line pastebin client
             python3-automat 0.6.0-1  Self-service finite-state machines for the programmer on the go
             ruby-minitest 5.11.3-1  Ruby test tools supporting TDD, BDD, mocking, and benchmarking
             sysvinit-utils 2.88dsf-59.10ubuntu2  System-V-like utilities
    root 303 Jan31 0:02 [kworker/0:1H-kblockd]
    root 22442 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 256 Jan31 0:00 [scsi_tmf_28]
    root 21 Jan31 0:00 [writeback]
    root 22472 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 29 Jan31 0:00 [md]
    root 202 Jan31 0:00 [scsi_tmf_11]
    root 83 Jan31 0:00 [kthrotld]
    root 23336 00:12 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 260 Jan31 0:00 [scsi_tmf_29]
    root 1011 Jan31 0:01 /usr/sbin/dovecot
        Possible Related Packages: 
             dovecot-core 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - core files
             dovecot-imapd 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - IMAP daemon
             dovecot-lmtpd 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - LMTP server
             dovecot-mysql 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - MySQL support
             dovecot-pop3d 1:2.3.2.1-1ubuntu3  secure POP3/IMAP server - POP3 daemon
    root 14 Jan31 0:00 [cpuhp/0]
    root 36 Jan31 0:05 [kswapd0]
    root 264 Jan31 0:00 [scsi_tmf_30]
    root 188 Jan31 0:00 [scsi_tmf_4]
    root 4 Jan31 0:00 [rcu_par_gp]
    root 182 Jan31 0:00 [scsi_tmf_2]
    root 86 Jan31 0:00 [scsi_tmf_0]
    root 22936 00:00 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 87 Jan31 0:00 [scsi_eh_1]
    root 209 Jan31 0:00 [scsi_eh_15]
    root 22471 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 84 Jan31 0:00 [acpi_thermal_pm]
    root 243 Jan31 0:00 [scsi_tmf_25]
    root 88 Jan31 0:00 [scsi_tmf_1]
    root 181 Jan31 0:00 [scsi_eh_2]
    root 203 Jan31 0:00 [scsi_eh_12]
    root 1098 Jan31 0:00 /sbin/agetty
    root 226 Jan31 0:00 [scsi_eh_19]
    root 200 Jan31 0:00 [scsi_tmf_10]
    root 266 Jan31 0:00 [scsi_eh_31]
    root 22586 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 37 Jan31 0:00 [kworker/u3:0]
    root 25 Jan31 0:00 [crypto]
    root 191 Jan31 0:00 [scsi_eh_6]
    root 85 Jan31 0:00 [scsi_eh_0]
    root 192 Jan31 0:00 [scsi_tmf_6]
    root 1048 Jan31 0:01 dovecot/config
        Possible Related Packages: 
             debconf 1.5.69  Debian configuration management system
             fontconfig-config 2.13.0-5ubuntu3  generic font configuration library - configuration
             gpgconf 2.2.8-3ubuntu1  GNU privacy guard - core configuration utilities
             ifupdown 0.8.34ubuntu2  high level tools to configure network interfaces
             iw 4.14-0.1  tool for configuring Linux wireless devices
             keyboard-configuration 1.178ubuntu9  system-wide keyboard preferences
             libfontconfig1:amd64 2.13.0-5ubuntu3  generic font configuration library - runtime
             lshw 02.18-0.1ubuntu7  information about hardware configuration
             netplan.io 0.40.2  YAML network configuration abstraction for various backends
             python3-configobj 5.0.6-3  simple but powerful config file reader and writer for Python 3
             ucf 3.0038  Update Configuration File(s): preserve user changes to config files
             xkb-data 2.23.1-1ubuntu1  X Keyboard Extension (XKB) configuration data
    root 508 Jan31 0:00 /sbin/lvmetad
    root 10 Jan31 0:26 [rcu_sched]
    root 490 Jan31 0:00 [ib-comp-wq]
    root 22994 00:03 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 23212 00:10 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 187 Jan31 0:00 [irq/16-vmwgfx]
    root 23022 00:05 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 217 Jan31 0:00 [scsi_tmf_16]
    root 22997 00:04 0:00 [kworker/u2:2-events_power_efficient]
    root 94 Jan31 0:00 [ipv6_addrconf]
    root 103 Jan31 0:00 [kstrp]
    root 120 Jan31 0:00 [charger_manager]
    root 3 Jan31 0:00 [rcu_gp]
    root 22470 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 184 Jan31 0:00 [scsi_tmf_3]
    root 22793 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 252 Jan31 0:00 [scsi_tmf_27]
    root 258 Jan31 0:00 [scsi_eh_29]
    root 1115 Jan31 0:01 /usr/sbin/apache2
        Possible Related Packages: 
             apache2 2.4.34-1ubuntu2  Apache HTTP Server
             apache2-bin 2.4.34-1ubuntu2  Apache HTTP Server (modules and other binary files)
             apache2-data 2.4.34-1ubuntu2  Apache HTTP Server (common files)
             apache2-utils 2.4.34-1ubuntu2  Apache HTTP Server (utility programs for web servers)
             libapache2-mod-php 1:7.2+62  server-side, HTML-embedded scripting language (Apache 2 module) (default)
             libapache2-mod-php7.2 7.2.10-0ubuntu1  server-side, HTML-embedded scripting language (Apache 2 module)
    root 22443 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 498 Jan31 0:12 /lib/systemd/systemd-journald
    root 22998 00:05 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 26 Jan31 0:00 [kintegrityd]
    root 225 Jan31 0:00 [scsi_tmf_18]
    root 22996 00:04 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 219 Jan31 0:00 [scsi_tmf_17]
    root 1007 Jan31 0:00 /usr/bin/python3
        Possible Related Packages: 
             libpython3-stdlib:amd64 3.6.6-1  interactive high-level object-oriented language (default python3 version)
             libpython3.6:amd64 3.6.7~rc1-1  Shared Python runtime library (version 3.6)
             libpython3.6-minimal:amd64 3.6.7~rc1-1  Minimal subset of the Python language (version 3.6)
             libpython3.6-stdlib:amd64 3.6.7~rc1-1  Interactive high-level object-oriented language (standard library, version 3.6)
             python3 3.6.6-1  interactive high-level object-oriented language (default python3 version)
             python3-apport 2.20.10-0ubuntu13  Python 3 library for Apport crash report handling
             python3-apt 1.7.0  Python 3 interface to libapt-pkg
             python3-asn1crypto 0.24.0-1  Fast ASN.1 parser and serializer (Python 3)
             python3-attr 17.4.0-2  Attributes without boilerplate (Python 3)
             python3-automat 0.6.0-1  Self-service finite-state machines for the programmer on the go
             python3-blinker 1.4+dfsg1-0.2  fast, simple object-to-object and broadcast signaling library
             python3-certifi 2018.8.24-1  root certificates for validating SSL certs and verifying TLS hosts (python3)
             python3-cffi-backend 1.11.5-3ubuntu1  Foreign Function Interface for Python 3 calling C code - runtime
             python3-chardet 3.0.4-1  universal character encoding detector for Python3
             python3-click 6.7+git20180829-1  Wrapper around optparse for command line utilities - Python 3.x
             python3-colorama 0.3.7-1  Cross-platform colored terminal text in Python - Python 3.x
             python3-commandnotfound 18.10.0~pre1  Python 3 bindings for command-not-found.
             python3-configobj 5.0.6-3  simple but powerful config file reader and writer for Python 3
             python3-constantly 15.1.0-1  Symbolic constants in Python
             python3-cryptography 2.3-1ubuntu1  Python library exposing cryptographic recipes and primitives (Python 3)
             python3-dbus 1.2.8-2build1  simple interprocess messaging system (Python 3 interface)
             python3-debconf 1.5.69  interact with debconf from Python 3
             python3-debian 0.1.33  Python 3 modules to work with Debian-related data formats
             python3-distro-info 0.18  information about distributions' releases (Python 3 module)
             python3-distupgrade 1:18.10.11  manage release upgrades
             python3-gdbm:amd64 3.6.6-1  GNU dbm database support for Python 3.x
             python3-gi 3.30.1-1ubuntu1  Python 3 bindings for gobject-introspection libraries
             python3-httplib2 0.11.3-1  comprehensive HTTP client library written for Python3
             python3-hyperlink 17.3.1-2  Immutable, Pythonic, correct URLs.
             python3-idna 2.6-1  Python IDNA2008 (RFC 5891) handling (Python 3)
             python3-incremental 16.10.1-3  Library for versioning Python projects.
             python3-jinja2 2.10-1  small but fast and easy to use stand-alone template engine
             python3-json-pointer 1.10-1  resolve JSON pointers - Python 3.x
             python3-jsonpatch 1.21-1  library to apply JSON patches - Python 3.x
             python3-jsonschema 2.6.0-2  An(other) implementation of JSON Schema (Draft 3 and 4) - Python 3.x
             python3-jwt 1.6.4-1  Python 3 implementation of JSON Web Token
             python3-markupsafe 1.0-1build2  HTML/XHTML/XML string library for Python 3
             python3-minimal 3.6.6-1  minimal subset of the Python language (default python3 version)
             python3-netifaces 0.10.4-1build1  portable network interface information - Python 3.x
             python3-newt:amd64 0.52.20-6ubuntu1  NEWT module for Python3
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
             python3-openssl 18.0.0-1  Python 3 wrapper around the OpenSSL library
             python3-pam 0.4.2-13.2ubuntu5  Python interface to the PAM library
             python3-pkg-resources 40.2.0-1  Package Discovery and Resource Access using pkg_resources
             python3-problem-report 2.20.10-0ubuntu13  Python 3 library to handle problem reports
             python3-pyasn1 0.4.2-3  ASN.1 library for Python (Python 3 module)
             python3-pyasn1-modules 0.2.1-0.2  Collection of protocols modules written in ASN.1 language (Python 3)
             python3-requests 2.18.4-2ubuntu0.18.10.1  elegant and simple HTTP library for Python3, built for human beings
             python3-requests-unixsocket 0.1.5-3  Use requests to talk HTTP via a UNIX domain socket - Python 3.x
             python3-serial 3.4-4  pyserial - module encapsulating access for the serial port
             python3-service-identity 16.0.0-2  Service identity verification for pyOpenSSL (Python 3 module)
             python3-six 1.11.0-2  Python 2 and 3 compatibility library (Python 3 interface)
             python3-software-properties 0.96.27  manage the repositories that you install software from
             python3-systemd 234-2build1  Python 3 bindings for systemd
             python3-twisted 18.7.0-2  Event-based framework for internet applications
             python3-twisted-bin:amd64 18.7.0-2  Event-based framework for internet applications
             python3-update-manager 1:18.10.10  python 3.x module for update-manager
             python3-urllib3 1.22-1  HTTP library with thread-safe connection pooling for Python3
             python3-yaml 3.12-1build3  YAML parser and emitter for Python3
             python3-zope.interface 4.3.2-1build3  Interfaces for Python3
             python3.6 3.6.7~rc1-1  Interactive high-level object-oriented language (version 3.6)
             python3.6-minimal 3.6.7~rc1-1  Minimal subset of the Python language (version 3.6)
    root 541 Jan31 0:00 /lib/systemd/systemd-udevd
    root 221 Jan31 0:00 [scsi_eh_18]
    root 494 Jan31 0:15 /usr/bin/vmtoolsd
    root 23 Jan31 0:00 [ksmd]
    root 194 Jan31 0:00 [scsi_tmf_7]
    root 16 Jan31 0:00 [netns]
    root 499 Jan31 0:00 [rdma_cm]
    root 23385 00:12 0:00 [kworker/0:2]
    root 229 Jan31 0:00 [scsi_tmf_20]
    root 22941 00:00 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 421 Jan31 0:01 [jbd2/sda2-8]
    root 18 Jan31 0:00 [kauditd]
    root 31 Jan31 0:00 [devfreq_wq]
    root 22792 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 15 Jan31 0:00 [kdevtmpfs]
    root 206 Jan31 0:00 [scsi_tmf_13]
    root 23237 00:11 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 38 Jan31 0:00 [ecryptfs-kthrea]
    root 22390 Jan31 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 1004 Jan31 0:00 /lib/systemd/systemd-logind
    root 239 Jan31 0:00 [scsi_tmf_24]
    root 227 Jan31 0:00 [scsi_tmf_19]
    root 185 Jan31 0:00 [scsi_eh_4]
    root 233 Jan31 0:00 [scsi_eh_22]
    root 422 Jan31 0:00 [ext4-rsv-conver]
    root 20 Jan31 0:00 [oom_reaper]
    root 27 Jan31 0:00 [kblockd]
    root 183 Jan31 0:00 [scsi_eh_3]
    root 197 Jan31 0:00 [scsi_eh_9]
    root 1494 Jan31 0:21 /usr/bin/perl
        Possible Related Packages: 
             libapt-pkg-perl 0.1.34  Perl interface to libapt-pkg
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libcgi-fast-perl 1:2.13-1  CGI subclass for work with FCGI
             libcgi-pm-perl 4.40-1  module for Common Gateway Interface applications
             libdata-dump-perl 1.23-1  Perl module to help dump data structures
             libencode-locale-perl 1.05-1  utility to determine the locale encoding
             liberror-perl 0.17026-1  Perl module for error/exception handling in an OO-ish way
             libfcgi-perl 0.78-2build1  helper module for FastCGI
             libfile-basedir-perl 0.08-1  Perl module to use the freedesktop basedir specification
             libfile-desktopentry-perl 0.22-1  Perl module to handle freedesktop .desktop files
             libfile-listing-perl 6.04-1  module to parse directory listings
             libfile-mimeinfo-perl 0.29-1  Perl module to determine file types
             libfont-afm-perl 1.20-2  Font::AFM - Interface to Adobe Font Metrics files
             libhtml-form-perl 6.03-1  module that represents an HTML form element
             libhtml-format-perl 2.12-1  module for transforming HTML into various formats
             libhtml-parser-perl 3.72-3build1  collection of modules that parse HTML text documents
             libhtml-tagset-perl 3.20-3  Data tables pertaining to HTML
             libhtml-template-perl 2.97-1  module for using HTML templates with Perl
             libhtml-tree-perl 5.07-1  Perl module to represent and create HTML syntax trees
             libhttp-cookies-perl 6.04-1  HTTP cookie jars
             libhttp-daemon-perl 6.01-1  simple http server class
             libhttp-date-perl 6.02-1  module of date conversion routines
             libhttp-message-perl 6.18-1  perl interface to HTTP style messages
             libhttp-negotiate-perl 6.01-1  implementation of content negotiation
             libio-html-perl 1.001-1  open an HTML file with automatic charset detection
             libio-pty-perl 1:1.08-1.1build4  Perl module for pseudo tty IO
             libio-socket-ssl-perl 2.060-3  Perl module implementing object oriented interface to SSL sockets
             libio-stringy-perl 2.111-2  Perl modules for IO from scalars and arrays
             libipc-system-simple-perl 1.25-4  Perl module to run commands simply, with detailed diagnostics
             liblocale-gettext-perl 1.07-3build2  module using libc functions for internationalization in Perl
             liblwp-mediatypes-perl 6.02-1  module to guess media type for a file or a URL
             liblwp-protocol-https-perl 6.07-2  HTTPS driver for LWP::UserAgent
             libmailtools-perl 2.18-1  Manipulate email in perl programs
             libnet-dbus-perl 1.1.0-4build2  Perl extension for the DBus bindings
             libnet-http-perl 6.18-1  module providing low-level HTTP connection client
             libnet-smtp-ssl-perl 1.04-1  Perl module providing SSL support to Net::SMTP
             libnet-ssleay-perl 1.85-2ubuntu2  Perl module for Secure Sockets Layer (SSL)
             libperl5.26:amd64 5.26.2-7ubuntu0.1  shared Perl library
             libtext-charwidth-perl 0.04-7.1  get display widths of characters on the terminal
             libtext-iconv-perl 1.7-5build6  converts between character sets in Perl
             libtext-wrapi18n-perl 0.06-7.1  internationalized substitute of Text::Wrap
             libtie-ixhash-perl 1.23-2  Perl module to order associative arrays
             libtimedate-perl 2.3000-2  collection of modules to manipulate date/time information
             libtry-tiny-perl 0.30-1  module providing minimalistic try/catch
             liburi-perl 1.74-1  module to manipulate and access URI strings
             libwww-perl 6.35-2  simple and consistent interface to the world-wide web
             libwww-robotrules-perl 6.02-1  database of robots.txt-derived permissions
             libx11-protocol-perl 0.56-7  Perl module for the X Window System Protocol, version 11
             libxml-parser-perl 2.44-2build3  Perl module for parsing XML files
             libxml-twig-perl 1:3.50-1  Perl module for processing huge XML documents in tree mode
             libxml-xpathengine-perl 0.14-1  re-usable XPath engine for DOM-like trees
             perl 5.26.2-7ubuntu0.1  Larry Wall's Practical Extraction and Report Language
             perl-base 5.26.2-7ubuntu0.1  minimal Perl system
             perl-modules-5.26 5.26.2-7ubuntu0.1  Core Perl modules
             perl-openssl-defaults:amd64 3build1  version compatibility baseline for Perl OpenSSL packages
             python3-hyperlink 17.3.1-2  Immutable, Pythonic, correct URLs.
    root 207 Jan31 0:00 [scsi_eh_14]
    root 9 Jan31 0:15 [ksoftirqd/0]
    root 22585 Jan31 0:00 [kworker/u2:0-events_power_efficient]
    root 23238 00:11 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 193 Jan31 0:00 [scsi_eh_7]
    root 17 Jan31 0:00 [rcu_tasks_kthre]
    root 23076 00:07 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 228 Jan31 0:00 [scsi_eh_20]
    root 236 Jan31 0:00 [scsi_tmf_23]
    root 22993 00:03 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 196 Jan31 0:00 [scsi_tmf_8]
    root 23074 00:06 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 205 Jan31 0:00 [scsi_eh_13]
    root 234 Jan31 0:00 [scsi_tmf_22]
    root 208 Jan31 0:00 [scsi_tmf_14]
    root 246 Jan31 0:00 [scsi_eh_26]
    root 201 Jan31 0:00 [scsi_eh_11]
    root 241 Jan31 0:00 [scsi_eh_25]
    root 261 Jan31 0:00 [scsi_eh_30]
    root 198 Jan31 0:00 [scsi_tmf_9]
    root 23075 00:06 0:00 dovecot/auth
        Possible Related Packages: 
             libauthen-pam-perl 0.16-3build5  Perl interface to PAM library
             libauthen-sasl-perl 2.1600-1  Authen::SASL - SASL Authentication framework
             libsasl2-2:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - authentication abstraction library
             libsasl2-modules:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules
             libsasl2-modules-db:amd64 2.1.27~101-g0780600+dfsg-3ubuntu2  Cyrus SASL - pluggable authentication modules (DB)
             libxau6:amd64 1:1.0.8-1  X11 authorisation library
             python3-oauthlib 2.0.6-1  generic, spec-compliant implementation of OAuth for Python3
    root 491 Jan31 0:00 [ib_mcast]
    root 24 Jan31 0:00 [khugepaged]
    root 199 Jan31 0:00 [scsi_eh_10]
    root 249 Jan31 0:00 [scsi_eh_27]

[*] ENUMERATING INSTALLED LANGUAGES/TOOLS FOR SPLOIT BUILDING...

[+] Installed Tools
    /usr/bin/awk
    /usr/bin/perl
    /usr/bin/python
    /usr/bin/ruby
    /usr/bin/vi
    /usr/bin/vim
    /usr/bin/nmap
    /usr/bin/find
    /bin/netcat
    /bin/nc
    /usr/bin/wget
    /usr/bin/ftp

[+] Related Shell Escape Sequences...

    nmap-->	--interactive
    vi-->	:!bash
    vi-->	:set shell=/bin/bash:shell
    vi-->	:!bash
    vi-->	:set shell=/bin/bash:shell
    awk-->	awk 'BEGIN {system("/bin/bash")}'
    find-->	find / -exec /usr/bin/awk 'BEGIN {system("/bin/bash")}' \;
    perl-->	perl -e 'exec "/bin/bash";'

[*] FINDING RELEVENT PRIVILEGE ESCALATION EXPLOITS...

    Note: Exploits relying on a compile/scripting language not detected on this system are marked with a '**' but should still be tested!

    The following exploits are ranked higher in probability of success because this script detected a related running process, OS, or mounted file system
    - MySQL 4.x/5.0 User-Defined Function Local Privilege Escalation Exploit || http://www.exploit-db.com/exploits/1518 || Language=c

    The following exploits are applicable to this kernel version and should be investigated as well
    - Kernel ia32syscall Emulation Privilege Escalation || http://www.exploit-db.com/exploits/15023 || Language=c
    - Sendpage Local Privilege Escalation || http://www.exploit-db.com/exploits/19933 || Language=ruby
    - CAP_SYS_ADMIN to Root Exploit 2 (32 and 64-bit) || http://www.exploit-db.com/exploits/15944 || Language=c
    - CAP_SYS_ADMIN to root Exploit || http://www.exploit-db.com/exploits/15916 || Language=c
    - MySQL 4.x/5.0 User-Defined Function Local Privilege Escalation Exploit || http://www.exploit-db.com/exploits/1518 || Language=c
    - open-time Capability file_ns_capable() Privilege Escalation || http://www.exploit-db.com/exploits/25450 || Language=c
    - open-time Capability file_ns_capable() - Privilege Escalation Vulnerability || http://www.exploit-db.com/exploits/25307 || Language=c

Finished
=================================================================================================
