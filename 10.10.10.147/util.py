
from pwn import *

def write_to_file(filename, payload):
    info ("writing to file %s" %filename)
    with open(filename,'w') as f:
        f.write(payload)
        
def append_to_file(filename, payload):
    info ("writing to file %s" %filename)
    with open(filename,'w+') as f:
        f.write(payload)