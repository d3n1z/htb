- Nikto v2.1.6
---------------------------------------------------------------------------
+ Target IP:          10.10.10.153
+ Target Hostname:    10.10.10.153
+ Target Port:        80
+ Start Time:         2019-01-29 07:08:03 (GMT7)
---------------------------------------------------------------------------
+ Server: Apache/2.4.25 (Debian)
+ Server leaks inodes via ETags, header found with file /, fields: 0x1f5c 0x56f96b7bed26f 
+ The anti-clickjacking X-Frame-Options header is not present.
+ The X-XSS-Protection header is not defined. This header can hint to the user agent to protect against some forms of XSS
+ The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ OSVDB-630: IIS may reveal its internal or real IP in the Location header via a request to the /images directory. The value is "http://127.0.0.1/images/".
+ Allowed HTTP Methods: OPTIONS, HEAD, GET, POST 
+ OSVDB-3092: /manual/: Web server manual found.
+ OSVDB-3268: /manual/images/: Directory indexing found.
+ OSVDB-3268: /images/: Directory indexing found.
+ OSVDB-3268: /images/?pattern=/etc/*&sort=name: Directory indexing found.
+ OSVDB-3233: /icons/README: Apache default file found.
+ 7499 requests: 0 error(s) and 11 item(s) reported on remote host
+ End Time:           2019-01-29 07:19:19 (GMT7) (676 seconds)
---------------------------------------------------------------------------
